﻿package
{
   import flash.display.Sprite;
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.text.TextField;
   import flash.display.Loader;
   import flash.net.URLRequest;

	[SWF(width="460", height="460", backgroundColor="#2A3776", frameRate="30")]
	
   public class frameTest_bckwdFromFast extends Sprite
   {
	
	
	private var counter:int = 0;
	private var tf:TextField;
	private var pres_rotation:Number = 0;
	private var rotationCounter:int = 0;
	private var rotationSpan:Number = 10;
	private var lastRotationSpan:Number = 10;
	private var rotationMax:Number = 1.67;
	private var maxFrames:int = 40;
	private var spn:MovieClip;
	private var myLoader:Loader;
	
	/*
	private var counter:int = 0;
	private var tf:TextField;
	private var pres_rotation:Number = 0;
	private var rotationCounter:int = 0;
	private var rotationSpan:Number = 10;
	private var lastRotationSpan:Number = 10;
	private var rotationMax:Number = 32;
	private var maxFrames:int = 150;
	private var spn:MovieClip;
	private var myLoader:Loader;
	*/

	public function frameTest_bckwdFromFast()
	{
		tf = new TextField();
		this.stage.addChild(tf);
		tf.width = 400;
		tf.height = 400;
		//tf.opaqueBackground  = 0xCCCCCC;
		tf.multiline = true;
		tf.wordWrap = true;
		
		
		spn = new MovieClip();
		this.addChild(spn);

		myLoader = new Loader();
		//myLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgressStatus);
		myLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderReady);
		//var d:String = "zurich-gol/images/wheel.png";
		var d:String = "Game_LiFE_Wheel.png";

		var fileRequest:URLRequest = new URLRequest(d);
		myLoader.load(fileRequest);
		
	}

	private function onLoaderReady(evt:Event):void
	{
		spn.addChild(myLoader);
		myLoader.x = -230;
		myLoader.y = -230;
		spn.x= 230;
		spn.y = 230;
		this.addEventListener("enterFrame",ef);
	}

	private function ef(evt:Event):void
	{
		if(counter>0&&rotationSpan>0)
		{
		
		//var perc:Number = Math.ceil((rotationCounter/maxFrames)*100);
		var perc:Number = (rotationCounter/maxFrames)*100;
		//tf.text = perc.toString();
		rotationSpan = ((100-perc)/100)*rotationMax;

		//if(rotationSpan<1||rotationSpan==1){rotationSpan = this.lastRotationSpan;}

		pres_rotation-=rotationSpan;
		this.lastRotationSpan = rotationSpan;
		//tf.appendText(rotationSpan+"\n");
		
		if(pres_rotation>360||pres_rotation==360)
        {pres_rotation = pres_rotation - 360;}

		tf.appendText(pres_rotation+", ");

		spn.rotationZ = pres_rotation;

		counter = 0;
		rotationCounter++;
		}
		counter++;
	}

   }
}