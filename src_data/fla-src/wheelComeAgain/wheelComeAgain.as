﻿package
{
   import flash.display.Sprite;
   import flash.display.MovieClip;
   import flash.events.Event;
   import flash.text.TextField;
   
   [SWF(width="403", height="434", backgroundColor="#2A3776", frameRate="30")]
	
   public class wheelComeAgain extends Sprite
   {
   
		private var wheel:MovieClip;
		private var frameCount:int = 0;
		private var maxFrames:int = 150;
		private var rotationSpan:Number = 20;
		private var regularRotation:Number = 1;
		private var tf:TextField;
   
		public function wheelComeAgain()
		{
			//this.wheel
			this.addEventListener("addedToStage", this.comeAlive);
		}
   
		private function comeAlive(evt:Event):void
		{
		
		this.tf = new TextField();
		this.addChild(this.tf);
		this.tf.text = "text here";
		
			this.wheel = new wheel_360_mc() as MovieClip;
			this.addChild(this.wheel);
			this.wheel.x = 201.5;
			this.wheel.y = 217;
			this.addEventListener("enterFrame",ef);
		}
		
		private var myRotation:int = 1;

		private function ef(evt:Event):void
		{
		
			if(this.frameCount!=this.maxFrames&&rotationSpan>0)
			{
				var perc:Number = 
				(100 - Math.ceil((this.frameCount/maxFrames)*100));
				
				this.wheel._360MC.gotoAndStop(this.myRotation);
				
				this.tf.text = perc+" -- "+this.frameCount;
				
				var adjustedRotation:int = 
				Math.floor((perc/100)*this.rotationSpan);
				
				this.myRotation += (this.regularRotation+adjustedRotation);
				
				if(this.myRotation>360){this.myRotation=this.myRotation-360;}
				
				this.frameCount++;
			}
		
		}
		
		
   }
}