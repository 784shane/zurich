package
{
	
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.text.TextField;
	
	import Config;
	import GlobalFunctions;
	import src.app_classes.appLayers;
	
	
	public class GeneralAnimations
	{
		
		private static var tabStartPosition:Number=0;
		private static var tabEndPosition:Number=0;
		private static var tabPositionMovement:Number=0;
	
		public function GeneralAnimations()
		{
		
		}
		
		public static function tab_transition(transitionComplete:Function, item:Sprite):void
		{
			
			var leftPos:int = 0;
			
			//set the start position as we want to know
			//where this tab is beginning from so we could
			//correctly calculate the percentage if movement.
			GeneralAnimations.tabStartPosition = item.x;
			//Now set the movement to position so we will know
			//the tab's final destination
			if(item.name=="tab1"){GeneralAnimations.tabEndPosition = 0;}
			if(item.name=="tab2"){GeneralAnimations.tabEndPosition = 234;}
			if(item.name=="tab3"){GeneralAnimations.tabEndPosition = 474;}
			
			//tf.text = item.name;
			//Now get the difference between moves so we can
			//know by how much pixels will our tab be moving.
			GeneralAnimations.tabPositionMovement = 
			GeneralAnimations.tabEndPosition>GeneralAnimations.tabStartPosition?
			GeneralAnimations.tabEndPosition-GeneralAnimations.tabStartPosition:
			GeneralAnimations.tabStartPosition-GeneralAnimations.tabEndPosition;
			
			TweenLite.to(item, 0.2, 
			{
				delay:0, 
				//scaleX:0.9, 
				//scaleY:0.9,
				y:0,
				x:GeneralAnimations.tabEndPosition, 
				//ease:Back.easeOut,
				onUpdate:GeneralAnimations.ff,
				onUpdateParams:[item],
				onComplete:transitionComplete,
				onCompleteParams:[item.name]
			});
 
 
		}
		
		public static function ff(item:Sprite):void
		{
			
			var d:String;
			
			var currentMove:Number = item.x>GeneralAnimations.tabEndPosition?
			item.x-GeneralAnimations.tabEndPosition:GeneralAnimations.tabEndPosition-item.x;
			
			var perc:Number = 
			((GeneralAnimations.tabPositionMovement - currentMove)
			/GeneralAnimations.tabPositionMovement)*100;
			
			//var perc:Number = 
			//(currentMove/GeneralAnimations.tabPositionMovement)*100;
			
			//var perc:Number = /*parseInt(*/(item.x/474)*100;//);
			var yPos:Number = (perc/100)*12;
			
			
			var t1:Sprite;
			var t2:Sprite;
			var t3:Sprite;
			
			switch(item.name)
			{
				case "tab1":
				{
					t2 = item.parent.getChildByName("tab2") as Sprite;
					t3 = item.parent.getChildByName("tab3") as Sprite;
					t2.x = item.x+item.width+3;
					t3.x = t2.x+t2.width+3;
					
					if(t3.y != 12){t3.y = yPos;}
					if(t2.y != 12){t2.y = yPos;}
					//item.parent.getChildByName("tab3").x = item.x;
					break;
				}
				
				case "tab2":
				{
					t1 = item.parent.getChildByName("tab1") as Sprite;
					t3 = item.parent.getChildByName("tab3") as Sprite;
					t1.x = item.x-(t1.width+3);
					t3.x = item.x + item.width+3;
					
					if(t1.y != 12){t1.y = yPos;}
					if(t3.y != 12){t3.y = yPos;}
					//item.parent.getChildByName("tab3").x = item.x;
					break;
				}
				
				case "tab3":
				{
					t1 = item.parent.getChildByName("tab1") as Sprite;
					t2 = item.parent.getChildByName("tab2") as Sprite;
					t2.x = item.x-(t2.width+3);
					t1.x = t2.x-(t1.width+3);
					
					if(t1.y != 12){t1.y = yPos;}
					if(t2.y != 12){t2.y = yPos;}
					//item.parent.getChildByName("tab3").x = item.x;
					break;
				}
			}
			
		}
		
		public static function dropContainerRows(itemsArray:Array):void
		{
			//dropContainerRowsx("down",itemsArray);
		}
		
		public static function dropContainerRowsx
		(dir:String, itemsArray:Array, 
		tab:String, funcAfterTween:Function):void
		{
			
			//(itemsArray[4].item.getChildByName("leaderboardCompanyNameTxt") as TextField).text = 
			//"b4 animatiion - "+itemsArray[4].item.y;
			
			var totalNumberOfItems:int = itemsArray.length;
			var actorItem:Sprite = 
			(itemsArray[totalNumberOfItems-1].item as Sprite);
			
			var finalPos:Number = 0;
			var easing:Function;
			var dur:Number = 1;
			switch(dir)
			{
				case "up":{ 
					finalPos = 0; 
					easing = Linear.easeNone;
					dur = 0.5;
					break;
					}
				case "down":{ finalPos = itemsArray[totalNumberOfItems-1].Ypos; 
					easing = Elastic.easeOut;
					dur = 1;
					break;}
				default:{ finalPos = 0; }
			}
			
			
			
			
			
			TweenLite.to(itemsArray[totalNumberOfItems-1].item, dur, 
			{
				delay:0, 
				alpha:1,
				y:finalPos, 
				ease:easing,
				onUpdate:GeneralAnimations.xxx,
				onUpdateParams:[itemsArray,(totalNumberOfItems-1)],
				onComplete:afterLeaderBoardAction,
				onCompleteParams:[funcAfterTween, tab]
			});
		}
		
		private static function afterLeaderBoardAction
		(funcAfterTween:Function, tab:String):void
		{
			
			funcAfterTween(tab);
			//(s.getChildByName("leaderboardCompanyNameTxt") as TextField).text = 
			//""+itemsArray.length;
		}
		
		public static function xxx(itemsArray:Array, num:int):void
		{
			
			var perc:Number = (itemsArray[num].item.y/itemsArray[num].Ypos)*100;
			for(var i:int = 0; i<itemsArray.length; i++)
			{
				if(i!=num)
				{
					var theYPos:Number = (perc/100)*itemsArray[i].Ypos;
					itemsArray[i].item.y = theYPos;
				}
			}
			
		}
		
		//////////////////////////////BUTTON EVENTS/////////////////////////////////
		public static function setButtonEvents(button:Sprite):void
		{
			button.addEventListener(MouseEvent.MOUSE_OVER, buttonMousedOver);
			button.addEventListener(MouseEvent.MOUSE_OUT, buttonMousedOut);
		}
		
		private static function buttonMousedOver(evt:MouseEvent):void
		{
			var buttonSprite:Sprite = (evt.target as Sprite);
			var buttonOver:Sprite = buttonSprite.getChildByName("buttonOver") as Sprite;
			var buttonOn:Sprite = buttonSprite.getChildByName("buttonOn") as Sprite;
			var buttonLabel:TextField = buttonSprite.getChildByName("buttonLabel") as TextField;
			//buttonOver.alpha = 1;
			buttonLabel.textColor = 0xFFFFFF;
			
			
			TweenLite.to(buttonOver, 0.7, 
			{
				delay:0, 
				alpha:1/*,
				
				onUpdate:function():void
				{
					var perc:Number = (buttonOver.alpha/1)*100;
					var rgb1:int = (int)(perc/100)*255;
					var rgb2:int = (int)(perc/100)*255;
					var rgb3:int = (int)(perc/100)*255;
					
					buttonLabel.textColor = GlobalFunctions.RGBtoHEX(rgb1, rgb2, rgb3);
					//buttonLabel.textColor = 0xFFFFFF;
				},
				
				onComplete:function():void
				{
					buttonLabel.textColor = 0xFFFFFF;
				}
				*/
			});
			
			
			//evt.target.parent.removeChild(evt.target);
		}
		
		private static function buttonMousedOut(evt:MouseEvent):void
		{
			var buttonSprite:Sprite = (evt.target as Sprite);
			var buttonOver:Sprite = buttonSprite.getChildByName("buttonOver") as Sprite;
			var buttonOn:Sprite = buttonSprite.getChildByName("buttonOn") as Sprite;
			var buttonLabel:TextField = buttonSprite.getChildByName("buttonLabel") as TextField;
			//buttonOver.alpha = 0;
			buttonLabel.textColor = Config.APP_COLORS.DARK_BLUE;
			
			TweenLite.to(buttonOver, 0.7, 
			{
				delay:0, 
				alpha:0//, 
				//ease:Elastic.easeOut,
				//onUpdate:GeneralAnimations.xxx,
				//onUpdateParams:[itemsArray,4]
			});
			//evt.target.parent.removeChild(evt.target);
		}
		//////////////////////////////BUTTON EVENTS END//////////////////////////////
		
		/*
		/////////////////////////WEALTH CARD EVENTS/////////////////////////////////
		public static function setWealthCardEvents(card:Sprite):void
		{
			card.buttonMode = true;
			card.addEventListener(MouseEvent.MOUSE_OVER, wealthCardMousedOver);
			card.addEventListener(MouseEvent.MOUSE_OUT, wealthCardMousedOut);
		}
		
		public static function wealthCardMousedOver(evt:MouseEvent):void
		{
			var cardRing:Sprite = (evt.target.getChildByName("wealthCardExternal") as Sprite);
			//cardRing.alpha = 1;
			
			//evt.target.scaleX = 1.001;
			//evt.target.scaleY = 1.001;
			
			TweenLite.to(cardRing, 0.6, 
			{
				//delay:0, 
				alpha:1  
				//ease:Elastic.easeOut,
				//onUpdate:GeneralAnimations.xxx,
				//onUpdateParams:[itemsArray,4]
			});
			
		}
		*/
		
		public static function wealthCardMousedOut(evt:MouseEvent):void
		{
			var cardRing:Sprite = (evt.target.getChildByName("wealthCardExternal") as Sprite);
			//cardRing.alpha = 0;
			
			
			
			//evt.target.scaleX = 1;
			//evt.target.scaleY = 1;
			
			
			TweenLite.to(cardRing, 0.6, 
			{
				//delay:0, 
				alpha:0
				//ease:Elastic.easeOut,
				//onUpdate:GeneralAnimations.xxx,
				//onUpdateParams:[itemsArray,4]
			});
			
		}
		
		public static function applyBasicMove(spr:*, newPos:Number):void
		{
			TweenLite.to(spr, 0.2, {delay:0, y:newPos});
		}
		/////////////////////////WEALTH CARD EVENTS/////////////////////////////////
		
		public static function blurBackground():void
		{
		
		}
		
		public static function moveCarousel(theCarouselSlider:Sprite, pos:int):void
		{
			TweenLite.to(theCarouselSlider, 1, {delay:0, ease:Back.easeOut, x:pos});
		}
		
		public static function animatePanelsIn(lyrs:appLayers, onCompleteAnimation:Function):void
		{
			
			var uiHidePos:Number = parseFloat(Config.PANELS.uiPanel.positions.hideY);
			var uiShowPos:Number = parseFloat(Config.PANELS.uiPanel.positions.showY);
			var feedShowPos:Number = parseFloat(Config.PANELS.feedPanel.positions.showY);
			
			var uiLayer:Sprite = lyrs.getLayer("uiLayer");
			var feedLayer:Sprite = lyrs.getLayer("feedLayer");
			var feedLayerStartPos:Number = feedLayer.y;
			var feedMovement:Number = feedShowPos - feedLayerStartPos;
			
			TweenLite.to(uiLayer, 1, 
			{delay:0, /*ease:Back.easeIn,*/ y:uiShowPos,
				
				onUpdate:function():void
				{
					var perc:Number = ((uiHidePos-uiLayer.y)/(uiHidePos-uiShowPos))*100;
					var feedAmtThisTurn:Number = (perc/100)*feedMovement;
					feedLayer.y = feedLayerStartPos+feedAmtThisTurn;
				},
				
				onComplete:function():void
				{
					onCompleteAnimation();
					//feedLayer.y = feedLayerStartPos;
					//161
					//feedPanel
				}
				
			});
		}
		
		public static function animatePanelsOut(lyrs:appLayers, onCompleteAnimation:Function):void
		{
			
			var uiHidePos:Number = parseFloat(Config.PANELS.uiPanel.positions.hideY);
			var uiShowPos:Number = parseFloat(Config.PANELS.uiPanel.positions.showY);
			
			var feedShowPos:Number = parseFloat(Config.PANELS.feedPanel.positions.showY);
			var feedHidePos:Number = lyrs.getLayer("feedLayer").height*-1;
			//parseFloat(Config.PANELS.feedPanel.positions.hideY);
			
			var uiLayer:Sprite = lyrs.getLayer("uiLayer");
			var feedLayer:Sprite = lyrs.getLayer("feedLayer");
			
			var feedLayerStartPos:Number = feedLayer.y;
			var feedMovement:Number = feedLayerStartPos - feedHidePos;
			
			
			TweenLite.to(uiLayer, 1, 
			{delay:0, y:uiHidePos,
				
				onUpdate:function():void
				{
					var perc:Number = ((uiHidePos-uiLayer.y)/(uiHidePos-uiShowPos))*100;
					var feedAmtThisTurn:Number = ((100-perc)/100)*feedMovement;
					feedLayer.y = feedLayerStartPos-feedAmtThisTurn;
				},
				
				onComplete:function():void
				{
					onCompleteAnimation();
				}
				
			});
		}
		
		public static function animatePanelsIn_ForSpinToWin(lyrs:appLayers, onCompleteAnimation:Function):void
		{
			
			var uiHidePos:Number = parseFloat(Config.PANELS.uiPanel.positions.hideY);
			var uiShowPos:Number = parseFloat(Config.PANELS.uiPanel.positions.showY);
			var feedShowPos:Number = parseFloat(Config.PANELS.feedPanel.positions.showY);
			
			var uiLayer:Sprite = lyrs.getLayer("uiLayer");
			var feedLayer:Sprite = lyrs.getLayer("feedLayer");
			var feedLayerStartPos:Number = feedLayer.y;
			var feedMovement:Number = feedShowPos - feedLayerStartPos;
			
			TweenLite.to(uiLayer, 1, 
			{delay:0, /*ease:Back.easeIn,*/ y:uiShowPos,
				
				onUpdate:function():void
				{
					var perc:Number = ((uiHidePos-uiLayer.y)/(uiHidePos-uiShowPos))*100;
					var feedAmtThisTurn:Number = (perc/100)*feedMovement;
					feedLayer.y = feedLayerStartPos+feedAmtThisTurn;
				},
				
				onComplete:function():void
				{
					onCompleteAnimation();
					//feedLayer.y = feedLayerStartPos;
					//161
					//feedPanel
				}
				
			});
		}
		
		public static function animatePanelsOut_ForSpinToWin(lyrs:appLayers, onCompleteAnimation:Function):void
		{
			
			var uiHidePos:Number = parseFloat(Config.PANELS.uiPanel.positions.hideY);
			var uiShowPos:Number = parseFloat(Config.PANELS.uiPanel.positions.showY);
			
			var feedShowPos:Number = parseFloat(Config.PANELS.feedPanel.positions.showY);
			var feedHidePos:Number = lyrs.getLayer("feedLayer").height*-1;
			//parseFloat(Config.PANELS.feedPanel.positions.hideY);
			
			var uiLayer:Sprite = lyrs.getLayer("uiLayer");
			var feedLayer:Sprite = lyrs.getLayer("feedLayer");
			
			var feedLayerStartPos:Number = feedLayer.y;
			var feedMovement:Number = feedLayerStartPos - feedHidePos;
			
			
			TweenLite.to(uiLayer, 1, 
			{delay:0, y:uiHidePos,
				
				onUpdate:function():void
				{
					var perc:Number = ((uiHidePos-uiLayer.y)/(uiHidePos-uiShowPos))*100;
					var feedAmtThisTurn:Number = ((100-perc)/100)*feedMovement;
					feedLayer.y = feedLayerStartPos-feedAmtThisTurn;
				},
				
				onComplete:function():void
				{
					onCompleteAnimation();
				}
				
			});
		}
		
		public static function toggleFeedExpansion
		(
		a:Sprite, num1:Number,
		b:Sprite,
		c:Sprite,
		arrow:Sprite,
		functionOnComplete:Function
		):void
		{
			
			var startPoint:Number = a.height;
			var startPoint1:Number = b.height;
			var diffMove:Number = 
			startPoint>num1?startPoint-num1:
			num1-startPoint;
			
			
			TweenLite.to(a, 1.5, 
			{delay:0, ease:Back.easeIn, height:num1,
				
				onUpdate:function():void
				{
					var currMoveDiff:Number = 
					a.height>startPoint?a.height-startPoint:
					startPoint-a.height;
					
					var perc:Number = 
					(currMoveDiff/diffMove)*100;
					
					b.height = a.height;
					
					c.y = (b.y+b.height)-14;
					
					//arrow.rotationZ = (perc/100)*180
					
					//var height(perc/100)*diffMove1
					//a.height/
				},
				
				onComplete:function():void
				{
					
					b.height = a.height;
					
					c.y = (b.y+b.height)-13;
					
					arrow.rotationZ = -1*(arrow.rotationZ+180);
					
					functionOnComplete();
				}
				
			});
		}
		
		
		public static function dropFeedSingleFeed(feedSingle:Sprite, pos:Number,
		onDroppedComplete:Function):void
		{
			var feed1:Sprite = 
			feedSingle.parent.getChildByName("feed_single_0") as Sprite;
			
			var feed2:Sprite = 
			feedSingle.parent.getChildByName("feed_single_1") as Sprite;
			
			var feed3:Sprite = 
			feedSingle.parent.getChildByName("feed_single_2") as Sprite;
			
			
			TweenLite.to(feedSingle, 0.4, 
			{
				ease:Elastic.easeOut, y:pos,
				
				onUpdate:function():void
				{
					feed1.y = feedSingle.y+feedSingle.height+20;
					feed2.y = feed1.y+feed1.height+20;
					feed3.y = feed2.y+feed2.height+20;
				},
				
				onComplete:function():void
				{
					onDroppedComplete(feedSingle);
				}
				
			});
		}
		
		public static function rollInText(textContainer:Sprite, onComplete:Function):void
		{
			
			//Split string by the hyphen
			var tempArray:Array=textContainer.name.split("-");
			var rollNumber:int = parseInt(tempArray[0]);
			var rollNumberTotal:int = parseInt(tempArray[1]);
			
			var perc:Number = ((rollNumberTotal-rollNumber)/rollNumberTotal)*100;
			var dur:Number = (perc/100)*2.5;
			
			var hh:Number = textContainer.getChildAt(0).height;
			
			TweenLite.to(textContainer, dur,{y:(-1*textContainer.height)+hh,
				
				onComplete:function():void
				{
					onComplete();
				}
			});
		}
		
		public static function rotateAround(textContainer:Sprite):void
		{
			TweenLite.to(textContainer, 5,{rotationZ:0});
		}
		
		public static function moveRibbonHorizontally
		(actor:Sprite, pos:Number, ribbonOpened:Function):void
		{
			
			TweenLite.to(actor, 0.5, 
			{
				x:pos,
				
				onComplete:function():void
				{
					ribbonOpened();
				}
				
			});
			
		}
		
		public static function basic_displayItemToggle
		( actor:* , 
		onComplete:Function, 
		desiredAlpha:Number=1, 
		theTime:Number = 0.5,
		theDelay:Number = 0 ):void
		{
			
			TweenLite.to(actor, theTime, 
			{
				delay:theDelay,
				
				alpha:desiredAlpha,
				
				onComplete:function():void
				{
					onComplete();
				}
				
			});
			
		}
		
		public static function positionTo
		( actor:* , 
		onComplete:Function, 
		xPos:Number=0, 
		yPos:Number=0, 
		theTime:Number = 0.5,
		theDelay:Number = 0 ):void
		{
			
			TweenLite.to(actor, theTime, 
			{
				delay:theDelay,
				
				x:xPos,
				
				y:yPos,
				
				onComplete:function():void
				{
					onComplete();
				}
				
			});
			
		}
		
		public static function movePageInPosition
		(pages:Sprite, makeInactive:Sprite, onComplete:Function = null):void
		{
			
			TweenLite.to(pages, 0.5, 
			{
				x:0,
				ease:Back.easeIn,
				onComplete:function():void
				{
					makeInactive.removeChildAt(0);
					onComplete();
				}
				
			});
			
		}
		
		public static function popLoadingCircle
		(circ:Sprite, onComplete:Function = null):void
		{
			
			TweenLite.to(circ, 0.8, 
			{
				scaleX:1,
				scaleY:1,
				x:232,
				y:119,
				ease:/*Bounce.easeOut,//*/Back.easeOut,
				onComplete:function():void
				{
					onComplete();
				}
				
			});
			
		}
		
	
	}
}