package
{
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.StyleSheet;
	import flash.text.TextFieldAutoSize;
	
	public class back extends Sprite
	{
		
		public function back()
		{
			
			this.graphics.beginFill(0xBBCC00);
			this.graphics.drawRect(0,0,980,750);
			this.graphics.endFill();
			
			/*
			var heading:Object = new Object();
            heading.fontWeight = "bold";
            heading.color = "#FF0000";
            heading.backgroundColor = "#bb00bb";
            heading.width = "200px";
            heading.height = "200px";
			
			var myStyleSheet:StyleSheet = new StyleSheet();
			myStyleSheet.setStyle("div", heading);
			
			
			var tf:TextField = new TextField();
			this.addChild(tf);
			tf.width = 500;
			tf.height = 500;
			//tf.styleSheet = myStyleSheet;
			tf.htmlText = "<div backgroundColor:color='#0000FF'>"+"December 22 - January 19<br>Capricorn"+"</div>"+"<br>"+"<font color='#006600'>"+"You are hardworking and neat."+"</font>";//'<div>yep</div>';
			
			*/
			
			var myHTMLText:String = "<body><span class='defaultStyle'><h1><b>HTML</b> Text <i>(sample <u>header</u>)</i></h1>Here is some <em>sample</em> <strong>html text</strong> "+"filling a text box <a href='http://www.adrianparr.com'>this link to adrianparr.com</a> and example headers"+"<br><br><br><h1>Header h1</h1><h2>Header h2</h2><br><br><br>Hello world<br><br><br><redText>This text <i>will be red</i></redText><br><br><h1>Boo</h1></span></body>";
 
var defaultStyleObj:Object = new Object();
defaultStyleObj.fontFamily = "Verdana";
defaultStyleObj.color = '#bb00bb';
 
var myStyleSheet:StyleSheet = new StyleSheet();
myStyleSheet.setStyle("body", {fontSize:'15',color:'#000066', backgroundColor:'#bb00bb'});
myStyleSheet.setStyle("h1", {fontSize:'32',color:'#000000'});
myStyleSheet.setStyle("h2", {fontSize:'19',color:'#000000'});
myStyleSheet.setStyle("a:link", {color:'#0000CC',textDecoration:'none'});
myStyleSheet.setStyle("a:hover", {color:'#0000FF',textDecoration:'underline'});
myStyleSheet.setStyle("b", {fontWeight:'bold'});
myStyleSheet.setStyle("em", {fontWeight:'bold'});
myStyleSheet.setStyle(".defaultStyle", defaultStyleObj);
myStyleSheet.setStyle("redText", {color:'#FF0000'});
 
var myTextField:TextField = new TextField();
myTextField.width = 500;
myTextField.multiline = true;
myTextField.styleSheet = myStyleSheet;
myTextField.htmlText = myHTMLText;
myTextField.autoSize = TextFieldAutoSize.LEFT;
myTextField.wordWrap = true;
myTextField.border = true;
addChild(myTextField);
			
		}
	
	}
}