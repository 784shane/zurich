package
{
	//flash classes
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.DisplayObject;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import mx.collections.ArrayList;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	//local classes
	import src.app_classes.GameFlow;
	import src.app_classes.actions.FeedActions;
	import src.app_classes.tools.Resources;
	import src.app_classes.tools.PictureLoader;
	import src.app_classes.appLayers;
	import src.app_classes.GamePlay;
	
	//temp
	//import src.app_classes.tools.gameStyles;
	
	
	
	import PlayerResources;
	import src.app_classes.PlayerUi;
	import src.app_classes.TitleBar;
	//import src.app_classes.FeedBar;
	import src.app_classes.tools.KeyboardControlTool;
	
	[SWF(width="980", height="750", backgroundColor="#999999", frameRate="30")]
	
	public class main_app extends Sprite
	{
		
		//[Embed (source="res/trash/16.Zurich_GOL_T_AND_C.jpg" )]
		//[Embed (source="res/trash/01.Zurich_GOL_LOGOUT.jpg" )]
		[Embed (source="res/trash/15.Zurich_GOL_PRIZES.jpg" )]
		protected var testScreen:Class;
		
		
			
		private var layers:appLayers;
		private var tf:TextField;
		private var carTrack:MovieClip;
		private var spinner:MovieClip;
		
		private var gp:GamePlay;
		private var gf:GameFlow;
		private var feedActs:FeedActions;
		private var Res:Resources;
		
		private var theTitle:TitleBar;
		private var playerUiClass:PlayerUi;
		
		
		
		public function main_app()
		{
			//as soon as this Sprite is added to the stage
			//then we will know that the stage is present and ready
			//to use if we have to use it
			this.addEventListener("addedToStage",stageReady);
		}
		
		private function stageReady(evt:Event):void
		{
			
			PlayerResources.mainStage = this.stage;
			this.stage.stageFocusRect = false;
			this.stageReady_step1();
			
			
			this.tf = new TextField();
			this.stage.addChild(tf);
			//tf.background = true;
			tf.width = 200;
			tf.height = 200;
			//tf.y = 300;
			tf.wordWrap = true;
			tf.textColor = 0xFFFFFF;
			
			
			if (this.stage.loaderInfo.parameters["app_location"] !== undefined)
				{
					if(this.stage.loaderInfo.parameters["app_location"] != "local")
					{
						tf.y = 1000;
					}
				}
			
			
			
			
			
			
			
			
			
			
			
		}
		
		private function stageReady_step1():void
		{
			//Step 1 - Create layers
			var myLoader:URLLoader = new URLLoader();
			
			myLoader.addEventListener(Event.COMPLETE, xmlLoaded);
			myLoader.load(new URLRequest("/board_spacesFinal.xml"));
		}
		
		private function xmlLoaded(evt:Event):void
		{
			
			this.Res = new Resources();
			
			//Step 1 - Create layers
			this.layers = this.getLayers(this.layersCreated);
			this.feedActs = new FeedActions();
			this.gp = new GamePlay(this.layers, this.Res, 
			this.feedActs, new XML(evt.target.data), this.tf);
			 
			
			this.layers.createLayers();
			
		}
		
		private function getLayers(callBackFunc:Function):appLayers
		{
			return new appLayers(this, callBackFunc);
		}
		
		public function layersCreated():void
		{
			this.stageReady_step2();
		}
		
		private function stageReady_step2():void
		{
			var loadingResources:ArrayList = Resources.main_loading_resources();
			//tf.text = loadingResources.getItemAt(0).res;
			
			//Layers are now in place as created in step one
			//We can now add items to our layers
			//We are going to add the mainControl car track.
			this.carTrack = new mainTrack() as MovieClip;
			//Add this car track to the mainTraclk layer.
			
			this.carTrack.addEventListener("enterFrame",carTrackIsReady);
			
			
			
			
			
			
			
			
			this.layers.getLayer("mainTrack").addChild(this.carTrack);
			//this.layers.getLayer("mainTrack").addChild(new testScreen());
			this.carTrack.x = 490;
			this.carTrack.y = 373;
			
		}
		
		public function carTrackIsReady(evt:Event):void
		{
			this.carTrack.stop();
			//Once car track swc is ready and we have entered the frame
			//we take note of this. At this time, we will remove this listener
			//since we will only need to use it once to signal to our swc
			//that we want to start processing code on the swc.
			this.carTrack.removeEventListener("enterFrame",carTrackIsReady);
			
			//We can do actions within game play
			//First of all lets make sure that the money grow and money lose
			//animations are not seen
			(this.carTrack.moneyGrow as MovieClip).alpha = 0;
			(this.carTrack._MoneyLoseContainer as MovieClip).alpha = 0;
			gp.setActors(this.carTrack,"carTrack");
			
			//seeing that carTrack is ready, we initiate it here
			//this.carTrack.init(gp.onCarMoveCompleted());
			
			//this.carTrack.addEventListener(MouseEvent.CLICK,gp.carTrackclicked);
			
			//At this point we will start the processes in the swc
			//... placesXML will load here for example.
			
			//No need to call startProcesses again since we will be loading the
			//xml on the main swf now
			//this.carTrack.startProcesses(this.carTrackLoadComplete);
			
			
			
			this.carTrackLoadComplete();
		}
		
		public function carTrackLoadComplete():void
		{	
			this.stageReady_step3();
		}
		
		private function stageReady_step3():void
		{
			
			this.theTitle = new TitleBar(this.Res);
			this.theTitle.addEventListener("addedToStage",this.whenTitleAdded);
			this.layers.getLayer("titleLayer").addChild(this.theTitle);
			//Set the title bar just above the top of the edge of the game.
			this.theTitle.theBar.y = this.theTitle.theBar.height*-1;
		}
		
		public function whenTitleAdded(evt:Event):void
		{
			this.playerUiClass = new PlayerUi(this.Res, this.layers, this.gp);
			//this.addChild(plui);
			this.playerUiClass.addEventListener("addedToStage",this.whenUiAdded);
			this.layers.getLayer("uiLayer").addChild(this.playerUiClass);
			this.playerUiClass.x = 154;
			this.layers.getLayer("uiLayer").y = 745;
			//this.layers.getLayer("uiLayer").y = 622;
		}
		
		public function whenUiAdded(evt:Event):void
		{
			
			
			this.gf = new GameFlow(this.layers,this.tf, 
			this.playerUiClass, this.gp, this.Res, 
			this.feedActs);
			
			//Lets now make sure that the same instance of GameFlow is
			//passed to our dependencies
			//We pass it now to the TitleClass
			this.theTitle.setGameFlowInstance(this.gf);
			
			
			this.gf.loadFirstScreen();
			
			
			
				
				
			this.spinner = new wheel_360_mc();
			this.spinner.addEventListener("addedToStage",this.spinner_added);
			this.carTrack._board._spinnerPlace.addChild(this.spinner);
			this.spinner.x = 197.9;
			this.spinner.y = 213.9;
			//this.spinner.y = 195;
			
			
			/*
			var spinnerContainer:Sprite = new Sprite();
			var aNewSpinner:MovieClip = 
			new wheel_360_mc() as MovieClip;
			this.carTrack._board._spinnerPlace.addChild(spinnerContainer);
			spinnerContainer.addChild(aNewSpinner);
			spinnerContainer.x = 197.9;
			spinnerContainer.y = 213.9;
			
			var ttt:TextField = new TextField();
			spinnerContainer.addChild(ttt);
			ttt.text = "This is a test";
			
			spinnerContainer.alpha = 0;
			*/
			
			//new KeyboardControlTool(spinnerContainer);
					
			
			if (this.stage.loaderInfo.parameters["app_location"] !== undefined)
			{
				if(this.stage.loaderInfo.parameters["app_location"] == "local")
				{
					this.Res.positionLocal = true;
				}
			}
			
			
		}
		
		private function theCallBack():void
		{
			
		}
		
		private function spinner_added(evt:Event):void
		{
			//We can do actions within game play
			gp.setActors(this.spinner,"spinner");
		}
		
		private function loadSceneImages():void
		{
			var pl:PictureLoader = new PictureLoader(this.loadComplete);
			var img:String = "/resources/images/Zurich_GOL_001.jpg";
			pl.load_image("background",img,0);
			
			var pl2:PictureLoader = new PictureLoader(this.loadComplete);
			var img2:String = "/resources/images/wheel.png";
			pl2.load_image("wheel",img2,0);
		}
		
		public function loadComplete(loadedItem:PictureLoader, loadedItemData:Object):void
		{
			switch(loadedItemData.id)
			{
				case "wheel":
				{
					this.layers.getLayer("wheel").addChild(loadedItem);
					break;
				}
				
				case "background":
				{
					this.layers.getLayer("background").addChild(loadedItem);
					break;
				}
			}
			
		}
	}
}