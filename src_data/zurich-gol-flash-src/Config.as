package
{
	
	import mx.collections.ArrayList;
	
	public class Config
	{
		public static var APP_COLORS:Object = 
		{
			DARK_BLUE:"0x2A3776",
			BUTTON_OVER:"0x2A3776",
			BUTTON_BORDER:"0x1e286f",
			BUTTON_ON:"0x3a457f",
			BORDER_GREY:"0xd6d6d6",
			FONT_GREY:"0x6f6f6f",
			TABLE_ROW1:"0xFFFFFF",
			TABLE_ROW2:"0xE8E8E8",
			REGULAR_GREEN:"0x4FB901",
			CARD_BLUE:"0x1E286F",
			APP_ORANGE:"0xEF6B00",
			APP_HIGH_GREEN:"0x4FB901",
			APP_GREY:"0x959596"
			//5b5b5b
			//2a3677
		};
		
		
		public static var PANELS:Object = 
		{
			uiPanel:{positions:{hideY:745,showY:622}},
			feedPanel:{positions:{hideY:745,showY:90}}
		};
		
		public static var PLAYER_START_MONEY:Number = 10000;
		
		public static var PLAYER_START_LIFECARDS:Number = 0;
		
    	public static var MULTIPLIER:int = 3;
    	
    	public static var SCREEN_CONFIG:Object = {width:980, height:750};
    	
		public static var CARD_MOVE_TO_Y_VALUE:int = -20;
		public static var NUM_WEALTH_CARDS_TO_SELECT:int = 3;
		
		
    	public static var PlayerUiResources:Object = new Object();
    	PlayerUiResources.assets = 
    	[{
    		name:"UI_Background",
    		src:"/resources/images/ui/ui_backGround.png",
    		positions:{x:0, y:0}
    	},/*
    	{
    		name:"COMPASS_ICON_BG_OFF",
    		src:"/resources/images/ui/compass/bg-off.png",
    		positions:{x:569, y:44}
    	},
    	{
    		name:"COMPASS_ICON_BG_ON",
    		src:"/resources/images/ui/compass/bg-on.png",
    		positions:{x:569, y:44}
    	},
    	{
    		name:"COMPASS_ICON_NEEDLE",
    		src:"/resources/images/ui/compass/needle.png",
    		positions:{x:569, y:44}
    	},*/
    	{
    		name:"UI_LIFE",
    		src:"/resources/images/ui/ui_life.png",
    		positions:{x:107, y:86}
    	},
    	{
    		name:"med_Avatar-select.1",
    		src:"/resources/images/avatar/player_ui/Avatar-GUI.1.png",
    		positions:{x:32, y:37}
    	},
    	{
    		name:"med_Avatar-select.2",
    		src:"/resources/images/avatar/player_ui/Avatar-GUI.2.png",
    		positions:{x:32, y:37}
    	},
    	{
    		name:"med_Avatar-select.3",
    		src:"/resources/images/avatar/player_ui/Avatar-GUI.3.png",
    		positions:{x:32, y:37}
    	},
    	{
    		name:"med_Avatar-select.4",
    		src:"/resources/images/avatar/player_ui/Avatar-GUI.4.png",
    		positions:{x:32, y:37}
    	},
    	{
    		name:"med_Avatar-select.5",
    		src:"/resources/images/avatar/player_ui/Avatar-GUI.5.png",
    		positions:{x:32, y:37}
    	},
    	{
    		name:"med_Avatar-select.6",
    		src:"/resources/images/avatar/player_ui/Avatar-GUI.6.png",
    		positions:{x:32, y:37}
    	}];
    	
    	public static var possible_cards_toLoad:ArrayList = new ArrayList();
    	possible_cards_toLoad.addItem({"cardName":"insurance_card", "src":"/resources/images/wealth_cards/possible_cards/shareWealth1.png"});
    	possible_cards_toLoad.addItem({"cardName":"exemption_card", "src":"/resources/images/wealth_cards/possible_cards/shareWealth2.png"});
    	possible_cards_toLoad.addItem({"cardName":"pay_card", "src":"/resources/images/wealth_cards/possible_cards/shareWealth3.png"});
    	
    	
    	public static var carSelectImagesToLoad:ArrayList = new ArrayList();
    	carSelectImagesToLoad.addItem({"car":"ORANGE", "src":"/resources/images/car_select/cars/orange.png", pos:{x:-2, y:0}});
    	carSelectImagesToLoad.addItem({"car":"GREEN", "src":"/resources/images/car_select/cars/green.png", pos:{x:169, y:0}});
    	carSelectImagesToLoad.addItem({"car":"PURPLE", "src":"/resources/images/car_select/cars/purple.png", pos:{x:340, y:0}});
    	carSelectImagesToLoad.addItem({"car":"YELLOW", "src":"/resources/images/car_select/cars/yellow.png", pos:{x:85, y:127}});
    	carSelectImagesToLoad.addItem({"car":"RED", "src":"/resources/images/car_select/cars/red.png", pos:{x:256, y:127}});
    	carSelectImagesToLoad.addItem({"car":"BLUE", "src":"/resources/images/car_select/cars/blue.png", pos:{x:171, y:254}});
    	carSelectImagesToLoad.addItem({"car":"car_ring", "src":"/resources/images/car_select/car_ring.png"});
		
		
		
		public static var cardTemplateResource:ArrayList = new ArrayList();
    	cardTemplateResource.addItem({"name":"carTemplate", "src":"/resources/images/cards/carTemplate.png"});
		
    	
    	public static var twoScreenResources:Object = new Object();
    	twoScreenResources.collegeOrCareer = 
    	[{
    		name:"COLLEGE",
    		src:"/resources/images/cards/images/college.png",
    		positions:{x:273, y:0}
    	},
    	{
    		name:"CAREER",
    		src:"/resources/images/cards/images/career.png",
    		positions:{x:0, y:0}
    	}];
		
    	
    	public static var threeScreenResources:Object = new Object();
    	threeScreenResources.collegeOrCareer = 
    	[{
    		name:"carousel1",
    		src:"/resources/images/cards/three-screen-carousel/carousel1.png",
    		
    		positions:{x:0, y:0}
    	},
    	{
    		name:"carousel2",
    		src:"/resources/images/cards/three-screen-carousel/carousel2.png",
    		positions:{x:197, y:0}
    	},
    	{
    		name:"carousel3",
    		src:"/resources/images/cards/three-screen-carousel/carousel3.png",
    		positions:{x:394, y:0}
    	},
    	{
    		name:"carousel1",
    		src:"/resources/images/cards/three-screen-carousel/carousel1.png",
    		
    		positions:{x:0, y:0}
    	},
    	{
    		name:"carousel2",
    		src:"/resources/images/cards/three-screen-carousel/carousel2.png",
    		positions:{x:197, y:0}
    	}];
    	
    	
    	public static var avatarImages:Object = new Object();
    	avatarImages.selectPlayerImages = 
    	[{
    		name:"Avatar-select.1",
    		src:"/resources/images/avatar/player_select/Avitar-select.1.png",
    		positions:{x:267, y:237}
    	},
    	{
    		name:"Avatar-select.2",
    		src:"/resources/images/avatar/player_select/Avitar-select.2.png",
    		positions:{x:426, y:237}
    	},
    	{
    		name:"Avatar-select.3",
    		src:"/resources/images/avatar/player_select/Avitar-select.3.png",
    		positions:{x:585, y:237}
    	},
    	{
    		name:"Avatar-select.4",
    		src:"/resources/images/avatar/player_select/Avitar-select.4.png",
    		positions:{x:348, y:370}
    	},
    	{
    		name:"Avatar-select.5",
    		src:"/resources/images/avatar/player_select/Avitar-select.5.png",
    		positions:{x:506, y:370}
    	},
    	{
    		name:"Avatar-select.6",
    		src:"/resources/images/avatar/player_select/Avitar-select.6.png",
    		positions:{x:426, y:500}
    	}];
    	
    	
    	public static var loadingScreenImages:Object = new Object();
    	loadingScreenImages.images = 
    	[{
    		name:"life-graphic",
    		src:"/resources/images/loadingScreenImages/life-graphic.png",
    		positions:{x:175, y:240}//positions:{x:407, y:359}
    	},
    	{
    		name:"The-game-of",
    		src:"/resources/images/loadingScreenImages/The-game-of.png",
    		positions:{x:51, y:240}//positions:{x:283, y:358}
    	},
    	{
    		name:"Zurich-logo",
    		src:"/resources/images/loadingScreenImages/Zurich-logo.png",
    		positions:{x:163, y:45}//positions:{x:388, y:162}
    	}];
    	
    	
    	public static var howToPlayImages:Object = new Object();
    	howToPlayImages.images = 
    	[{
    		name:"lifeTiles",
    		src:"/resources/images/howToPlay/lifeTiles.png",
    		positions:{x:357, y:261}
    	},
    	{
    		name:"retirement",
    		src:"/resources/images/howToPlay/retirement.png",
    		positions:{x:668, y:215}
    	},
    	{
    		name:"shareWealth",
    		src:"/resources/images/howToPlay/shareWealth.png",
    		positions:{x:506, y:206}
    	},
    	{
    		name:"spinningAndMoving",
    		src:"/resources/images/howToPlay/spinningAndMoving.png",
    		positions:{x:192, y:220}
    	}];
		
		
    	public static var alternative_routes:Object = new Object();
    	alternative_routes.route1 = 
    	{
    		start:{frame:3360, board_num:129, entry_board_num:128},
    		end:{frame:3465, board_num:132, exitFrame:3465},
			regular:
			{
				start:{board_num:1},
				end:{board_num:12}
			}
    	};
		
    	alternative_routes.route2 = 
    	{
    		start:{frame:2735, board_num:133, entry_board_num:132},
    		end:{frame:2895, board_num:139, exitFrame:2895},
			regular:
			{
				start:{board_num:50},
				end:{board_num:58}
			}
    	};
		
    	alternative_routes.route3 = 
    	{
    		start:{frame:2925, board_num:140, entry_board_num:139},
    		end:{frame:3110, board_num:146, exitFrame:3110},
			regular:
			{
				start:{board_num:81},
				end:{board_num:87}
			}
    	};
		
    	alternative_routes.route4 = 
    	{
    		start:{frame:3145, board_num:147, entry_board_num:146},
    		end:{frame:3335, board_num:154, exitFrame:3335},
			regular:
			{
				start:{board_num:104},
				end:{board_num:109}
			}
    	};
    	
    	
	}
}