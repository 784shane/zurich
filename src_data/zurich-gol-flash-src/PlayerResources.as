package
{
	
	import mx.collections.ArrayList;
	import flash.display.Stage;
	
	public class PlayerResources
	{
		
		public static var player_name:String = "1";
		
		public static var player_last_name:String = "1";
		
		public static var player_company:String = "1";
		
		public static var player_uid:String = "1";
		
		public static var player_email:String = "1";
		
		public static var sex:String = "m";
		
		public static var mainStage:Stage;
		
		public static var player_car:String = "yellow";
		
		public static var player_avatar:String = "Avitar-select.2";
		
		public static var player_wealth_cards:ArrayList;
		
		public static var player_wealthCards:Array;//1,0,0);
		
		public static var salary:Number = 0;
		
		public static var maximum_salary:Number = 10000;
		
		public static var salary_taxes:Number = 10000;
		
		public static var job:String = "";
		
		public static var money:Number = 10000;
		
		public static var house_buying_price:Number = 0;
		
		public static var house_selling_price:Number = 0;
		
		public static var house_insurance:Number = 0;
		
		public static var lifeCards:int = 0;
		
		public static var lifePath:String = "GRADUATE";
		
		public static var currentSquare:int = 1;
		
		public static var playerRoutes:Object = 
		{
			careerOrCollege : "CAREER"
		};
		
		public static var playerPegs:String = "";
		
		public static var numOfChildren:int = 0;
		
		public static var gamesavedAtEnd:Boolean = false;
		
		public function PlayerResources()
		{
			
		}
		
	}
	
}