package
{
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.*;

	import src.app_classes.screens.BlockScreen;
	import Config;
	
	
	public class GlobalFunctions
	{
		
		
		public static function formatMoneyToString(amt:Number):String
		{
			
			var money:String = amt.toString();
			if(money.length>3){money = money.substr(0,-3)+","+money.substr(-3);}
			if(money.length>7){money = money.substr(0,-7)+","+money.substr(-7);}
			if(money.length>11){money = money.substr(0,-11)+","+money.substr(-11);}
			if(money.length>15){money = money.substr(0,-15)+","+money.substr(-15);}
			if(money.length>19){money = money.substr(0,-19)+","+money.substr(-19);}
			if(money.length>23){money = money.substr(0,-23)+","+money.substr(-23);}
			if(money.length>27){money = money.substr(0,-27)+","+money.substr(-27);}
			
			return "&#163;"+money;
		}
		
		public static function formatNumberToMoneyString(amt:Number):String
		{
			
			var money:String = amt.toString();
			if(money.length>3){money = money.substr(0,-3)+","+money.substr(-3);}
			if(money.length>7){money = money.substr(0,-7)+","+money.substr(-7);}
			if(money.length>11){money = money.substr(0,-11)+","+money.substr(-11);}
			if(money.length>15){money = money.substr(0,-15)+","+money.substr(-15);}
			if(money.length>19){money = money.substr(0,-19)+","+money.substr(-19);}
			if(money.length>23){money = money.substr(0,-23)+","+money.substr(-23);}
			if(money.length>27){money = money.substr(0,-27)+","+money.substr(-27);}
			
			return ""+money;
		}
		
		public static function emptyContainer(layername:*):void
		{
			while (layername.numChildren > 0) 
			{
				layername.removeChildAt(0);
			}
		}
		
		public static function formatLifeCardString(amt:int):String
		{
			return "x "+amt;
		}
		
		public static function getBackDrop():Sprite
		{
			
			var backDrop:Sprite = new Sprite();
			backDrop.addChild(new BlockScreen());
			return backDrop;
			
		}
		
		public static function addSprite_appTransparentBackground
		(
		container:Sprite,
		w:int,
		h:int,
		roundedCorners:Boolean = false
		):void
		{
			
			var rpBack:Sprite = new Sprite();
			rpBack.graphics.beginFill(0xFFFFFF,0.9);//0.9);
			//rpBack.graphics.beginFill(0xFFFFFF,0);
			rpBack.graphics.drawRect(0,0,10,10);
			rpBack.graphics.endFill();
            
            var matrix:Matrix = new Matrix();
            
			//var r:Bitmap = new tile() as Bitmap;
			var myBitmap:BitmapData = new BitmapData(10, 10, true, 0xBB0000);
            myBitmap.draw(rpBack, matrix);
            
            
            //matrix.rotate(Math.PI/4);
			
		    //var backGroundSprite:Sprite = new Sprite();
		    //Background Class is actually the linkage to background.jpg in de Library
		    container.graphics.beginBitmapFill(myBitmap, matrix, true, true);//new BitmapData(200, 200));
			if(roundedCorners)
			{
				//container.graphics.drawRect(0,0,w,h);
				container.graphics.drawRoundRect(0, 0, w, h, 6, 6);
			}
			else
			{
    			container.graphics.drawRect(0,0,w,h);
			}
			container.graphics.endFill();
		    //Add and display on stage
		    //addChild(backGroundSprite);
		    
		    //this.rf.text = (container.width+36).toString();
		    
		}
		
		public static function RGBtoHEX(r:int, g:int, b:int):uint
		{
			return r << 16 | g << 8 | b;
		}
		
		public static function getRandomInt(start:int = 0, end:int = 3):uint
		{
			return Math.random()*end;
		}
		
		public static function changeAvatarNameToInt(avatarName:String):int
		{
			//Avitar-select.2
			return parseInt(avatarName.substr(14));
		}
		
		public static function getWealthCardNameFromNumber(cardNo:int):String
		{
			switch(cardNo)
			{
				case 0:
				{
					return "pay";
					break;	
				}
				
				case 1:
				{
					return "exemption";
					break;	
				}
				
				case 2:
				{
					return "car";
					break;	
				}
				
				case 3:
				{
					return "home";
					break;	
				}
				
				case 4:
				{
					return "health";
					break;	
				}
			}
			
			return "exemption";
			
		}
		
		public static function getWhiteCircle():Sprite
		{
			//draw a circle on screen
			var circle:Sprite = new Sprite();
			circle.graphics.lineStyle(3, 0xFFFFFF, 1);
			circle.graphics.beginFill(0xFFFFFF,1);
			//circle.graphics.drawRoundRect(0, 0, 510, 60, 5, 5);
			circle.graphics.drawCircle(0, 0, 255);
			circle.graphics.endFill();
			
			circle.x=256;
			circle.y=256;
			circle.alpha = 0.4;
			
			return circle;
			
		}
		
		public static function getActivePage(pagesLayer:Sprite):Sprite
		{
			if((pagesLayer.getChildAt(0)as Sprite).numChildren>0)
			{return (pagesLayer.getChildAt(0)as Sprite);}
			if((pagesLayer.getChildAt(1)as Sprite).numChildren>0)
			{return (pagesLayer.getChildAt(1)as Sprite);}
			return new Sprite();
		}
		
		public static function getInactivePage(pagesLayer:Sprite):Sprite
		{
			if((pagesLayer.getChildAt(0)as Sprite).numChildren==0)
			{return (pagesLayer.getChildAt(0)as Sprite);}
			if((pagesLayer.getChildAt(1)as Sprite).numChildren==0)
			{return (pagesLayer.getChildAt(1)as Sprite);}
			return new Sprite();
		}
		
		public static function deleteBothPages(pagesLayer:Sprite):void
		{
			if((pagesLayer.getChildAt(0) as Sprite).numChildren>0)
			{
				(pagesLayer.getChildAt(0) as Sprite).removeChildAt(0);
			}
			
			if((pagesLayer.getChildAt(1) as Sprite).numChildren>0)
			{
				(pagesLayer.getChildAt(1) as Sprite).removeChildAt(0);
			}
			
		}
		
		public static function repositionPages
		(activePage:Sprite, inactivePage:Sprite):void
		{
			//First of all move parent back -980
			activePage.parent.x = -980;
			activePage.x = 980;
			inactivePage.x = 0;
		}
		
	}
}