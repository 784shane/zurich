package
{
	//flash classes
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import src.app_classes.forms.LoadingScreen;
	import GeneralAnimations;
	
	
    import flash.utils.Timer;
    import flash.events.TimerEvent;
	
	[SWF(width="980", height="750", backgroundColor="#CCCCCC", frameRate="30")]
	
	
	
	public class loading extends Sprite
	{
		
		[Embed (source="res/trash/loading.jpg" )]
		protected var testScreen:Class;
		
		private var tf:TextField;
		private var loadedSwf:Sprite;
		
		private var ldnScreen:LoadingScreen;
		private var delay:uint = 1500;//000;
        private var repeat:uint = 1;
        private var myTimer:Timer = new Timer(delay, repeat);
		private var swfLoaded:Boolean = false;
		
		
		
		public function loading()
		{
			//as soon as this Sprite is added to the stage
			//then we will know that the stage is present and ready
			//to use if we have to use it
			this.addEventListener("addedToStage",stageReady);
		}
		
		private function stageReady(evt:Event):void
		{
			
			//this.addChild(new testScreen());
			this.loadFirstScreen();
			
			
			
			
			
		}
		
		private function onProgressHandler(mProgress:ProgressEvent):void
		{
			var percent:Number = mProgress.bytesLoaded/mProgress.bytesTotal;
			ldnScreen.loadingTxtIcre.text = Math.round(percent*100)+"%";
		}
		
		private function onCompleteHandler(loadEvent:Event):void
		{
			
			this.loadedSwf = (loadEvent.currentTarget.content as Sprite);
			this.addChild(this.loadedSwf);
			this.loadedSwf.alpha = 0;
			
			this.swfLoaded = true;
			
			if(!this.myTimer.running)
			{
			//Transistion in the new swf.
			GeneralAnimations.basic_displayItemToggle(this.loadedSwf, this.gameLoaded, 1, 1);
			}
		}
		
		private function gameLoaded():void
		{
			//remove loading screen now
			//We can discard this
			this.removeChildAt(0);
		}
		
		private function afterLoadingScreenResourcesLoadeddd():void
		{
			var loadingScreenContents:Sprite = this.ldnScreen.getLoadingScreen();
			loadingScreenContents.addEventListener("addedToStage", this.loadingScreenContentsLoaded);
			this.addChild(loadingScreenContents);
			
			//UNCOMMENT OUT THIS
			
			
		}
		
		private function loadSwift(evt:Event):void
		{
			this.myTimer.addEventListener(TimerEvent.TIMER, xxtimerHandler);
		    this.myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, xxcompleteHandler);
			this.myTimer.start();
			
			var mLoader:Loader = new Loader();
			var mRequest:URLRequest = new URLRequest("/resources/swfs/main_game.swf");
			mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleteHandler);
			mLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgressHandler);
			mLoader.load(mRequest);
			
		}
		
		private function loadingScreenContentsLoaded(evt:Event):void
		{
			this.ldnScreen.startAnimation(this.loadSwf);
			
		}
		
		public function loadSwf():void
		{
			this.myTimer.addEventListener(TimerEvent.TIMER, xxtimerHandler);
		    this.myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, xxcompleteHandler);
			this.myTimer.start();
			
			var mLoader:Loader = new Loader();
			var mRequest:URLRequest = new URLRequest("/resources/swfs/main_game.swf");
			mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleteHandler);
			mLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgressHandler);
			mLoader.load(mRequest);
			
		}
		
		public function loadFirstScreen():void
		{
			
			this.ldnScreen = new LoadingScreen(this.onLoadingScreenComplete);
			this.ldnScreen.loadScreenImages(this.afterLoadingScreenResourcesLoadeddd);
		}
		
		public function onLoadingScreenComplete():void
		{
			
		}
		
		private function xxtimerHandler(evt:TimerEvent):void
		{
		
		}
		
		private function xxcompleteHandler(evt:TimerEvent):void
		{
			//this.onLoadingScreenComplete();
			if(this.swfLoaded)
			{
				GeneralAnimations
				.basic_displayItemToggle(this.loadedSwf, this.gameLoaded, 1, 1);
			}
		}
		
		
		
		
	}
}