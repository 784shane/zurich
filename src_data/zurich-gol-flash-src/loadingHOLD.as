package
{
	//flash classes
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import src.app_classes.forms.LoadingScreen;
	import GeneralAnimations;
	
	[SWF(width="980", height="750", backgroundColor="#CCCCCC", frameRate="30")]
	
	
	
	public class loading extends Sprite
	{
		
		[Embed (source="res/trash/loading.jpg" )]
		protected var testScreen:Class;
		
		private var tf:TextField;
		
		private var ldnScreen:LoadingScreen;
		
		
		
		public function loading()
		{
			//as soon as this Sprite is added to the stage
			//then we will know that the stage is present and ready
			//to use if we have to use it
			this.addEventListener("addedToStage",stageReady);
		}
		
		private function stageReady(evt:Event):void
		{
			
			//this.addChild(new testScreen());
			this.loadFirstScreen();
			
			
			
			
			
		}
		
		private function onProgressHandler(mProgress:ProgressEvent):void
		{
			var percent:Number = mProgress.bytesLoaded/mProgress.bytesTotal;
			ldnScreen.loadingTxtIcre.text = Math.round(percent*100)+"%";
		}
		
		private function onCompleteHandler(loadEvent:Event):void
		{
			
			var ls:Sprite = (loadEvent.currentTarget.content as Sprite);
			this.addChild(ls);
			ls.alpha = 0;
			
			//Transistion in the new swf.
			GeneralAnimations.basic_displayItemToggle(ls, this.gameLoaded, 1, 1);
			
		}
		
		private function gameLoaded():void
		{
			//remove loading screen now
			//We can discard this
			this.removeChildAt(0);
		}
		
		private function afterLoadingScreenResourcesLoadeddd():void
		{
			
			this.addChild(this.ldnScreen.getLoadingScreen());
			
			
			var mLoader:Loader = new Loader();
			var mRequest:URLRequest = new URLRequest("/resources/swfs/main_game.swf");
			mLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleteHandler);
			mLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, onProgressHandler);
			mLoader.load(mRequest);
			
		}
		
		public function loadFirstScreen():void
		{
			
			this.ldnScreen = new LoadingScreen(this.onLoadingScreenComplete);
			this.ldnScreen.loadScreenImages(this.afterLoadingScreenResourcesLoadeddd);
		}
		
		public function onLoadingScreenComplete():void
		{
			//this.getScreen("POST_LOADING_SCREEN");
		}
		
		
		
		
	}
}