		private function getFeedSingle(thumbnail:Bitmap,
										FeedMsg:String,
										FeedName:String,
										FeedCompany:String):Sprite
		{
				var single:Sprite = new Sprite();
				var header:Sprite = new Sprite();
				var leftHeader:Sprite = new Sprite();
				header.addChild(leftHeader);
				leftHeader.x = 20;
	
				leftHeader.addChild(thumbnail);
				
				var rightHeader:Sprite = new Sprite();
				
				
				////////////////Header main TextField///////////////////////////
				var feedHeaderMainTxt:TextField = this.aF.getFeedNameText(true);
				rightHeader.addChild(feedHeaderMainTxt);
				feedHeaderMainTxt.width=120;
				//feedHeaderMainTxt.bold=true;
				feedHeaderMainTxt.htmlText = FeedName;//"Brent Bailey";
				////////////////////////////////////////////////////////////////
				
				
				//var feedHeaderMainTxt:TextField  = new TextField();
				
				
				////////////////Header sub TextField////////////////////////////
				var feedHeaderSubTxt:TextField = this.aF.getFeedText();
				rightHeader.addChild(feedHeaderSubTxt);
				feedHeaderSubTxt.width=120;
				//feedHeaderSubTxt.background = true;
				//feedHeaderSubTxt.backgroundColor = 0xBBCC00;
				feedHeaderSubTxt.htmlText = FeedCompany;//"Brooker Insurance";
				
				//Set the y position to the height of the feedHeaderMainTxt
				//so that it remains to the bottom of the mainText ALWAYS
				//When text is assigned, we will need to measure this then
				//resize and reposition these properties
				feedHeaderSubTxt.y = feedHeaderMainTxt.height;
				////////////////////////////////////////////////////////////////
				
				
				header.addChild(rightHeader);
				rightHeader.x = 67;
				
				
				
				single.addChild(header);
				
				var body:Sprite = new Sprite();
				
				var feedBodyTextSpr:Sprite = new Sprite();
				var feedBodyText:TextField = this.aF.getFeedText();
				feedBodyTextSpr.addChild(feedBodyText);
				body.addChild(feedBodyTextSpr);
				feedBodyText.width=170;
				//feedBodyText.background = true;
				//feedBodyText.backgroundColor = 0xBBCC00;
				
				//feedBodyText.htmlText = "John has just sold his house for <b>$900000</b>. Happy for you!";
				feedBodyText.htmlText = FeedMsg;
				
				
				single.addChild(body);
				//body.alpha = 0.4;
				
				body.x = 20;
				body.y = header.height+14;
				
				
				var feedSeparatorSpr:Sprite = new Sprite();
				feedSeparatorSpr.addChild(new feedSeparator());
				feedSeparatorSpr.name = "feedSeparatorSpr";
				single.addChild(feedSeparatorSpr);
				feedSeparatorSpr.x=21;
				//Now that we know the height of the single' sprite,
				//we need to place the feed separator just a few pixels 
				//below this point
				feedSeparatorSpr.y= (single.height) +10;
				
				
				
				return single;
		}
		
		private function addSingle(evt:MouseEvent):void
		{
			this.dropSingleFeed();
		}
		
		public function dropSingleFeed(
		thumbnailId:int = 2, 
		msg:String = "", 
		name:String = "", 
		company:String = ""):void
		{
			var thumbnail:Bitmap = this.getFeedThumbnail(thumbnailId);
			var single:Sprite = this.getFeedSingle(thumbnail, msg, name, company);
			this.feedSingleContainer.addChild(single);
			single.name="feed_single_"+(single.parent.numChildren-1);
			//Setting the single container above and out of site
			single.y = single.height*-1;
			
			GeneralAnimations.dropFeedSingleFeed(single, 0, this.feedSingleDropped);
		}
		
		private function feedSingleDropped(feedSingle:Sprite):void
		{
			
			var parentSpr:Sprite = feedSingle.parent as Sprite;
			
			//Remove the child that should be removed before looping through the parent.
			//We are using the contains method to see if there is a child within
			//the parent.
			if(parentSpr.contains(parentSpr.getChildByName("feed_single_2")))
			{
				parentSpr.removeChild(parentSpr.getChildByName("feed_single_2"));
			}
			
			for(var i:int = 0; i<parentSpr.numChildren; i++)
			{
				var childSpr:Sprite = parentSpr.getChildAt(i) as Sprite;
				var sprName:String = childSpr.name;
				var indx:int = parseInt(sprName.substr(12));
				//we are renaming the feed single children here. We want one
				//always to be named feed_single_1, feed_single_2 and the 
				//new one to be named feed_single_0
				if(indx<2){childSpr.name = "feed_single_"+(indx+1);}
				if(indx>2){childSpr.name = "feed_single_0";}
			}
			
			
		}