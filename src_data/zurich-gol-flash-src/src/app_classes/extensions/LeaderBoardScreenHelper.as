		private function defineSpecificTextBoxes(tf:TextField):void
		{
			
			switch(tf.name)
			{
				case "numText":
				{
					tf.x=7;
					tf.y=12;
					tf.width = 53;
					tf.height = 30;
					break;
				}
				
				case "leaderboardNameTxt":
				{
					tf.x=105;
					tf.y=16;
					tf.width = 187;
					tf.height = 30;
					break;
				}
				
				case "leaderboardCompanyNameTxt":
				{
					if(this.leaderboardType == "default")
					{
						tf.x=294;
					}
					else
					{
						tf.x=129;
					}
					
						tf.y=16;
						tf.width = 187;
						tf.height = 30;
					break;
				}
				
				case "leaderboardTotalMoneyTxt":
				{
					if(this.leaderboardType == "default")
					{
						tf.x=524;
					}
					else
					{
						tf.x=381;
					}

					tf.y=16;
					tf.width = 116;
					tf.height = 30;
					break;
				}
			}
		}
		
		private function defineSpecificTextBoxStyle(tf:TextField):void
		{
			var tformat:TextFormat = tf.getTextFormat();
			switch(tf.name)
			{
				case "numText":
				{
					tformat.size = 26;
					tformat.align = "center";
					break;
				}
				
				case "leaderboardNameTxt":
				{
					tformat.size = 20;
					break;
				}
				
				case "leaderboardCompanyNameTxt":
				{
					tformat.size = 20;
					tformat.color = Config.APP_COLORS.FONT_GREY;
					break;
				}
				
				case "leaderboardTotalMoneyTxt":
				{
					
					tformat.size = 20;
					//tformat.align = "right";
					break;
				}
			}
			
			tf.defaultTextFormat = tformat;
		
		}
		
		private function getAppropriateIcon(iconNumber:int):Bitmap
		{
			switch(iconNumber)
			{
				case 1:
				{
					return new feedImg1() as Bitmap;
					break;
				}
				
				case 2:
				{
					return new feedImg2() as Bitmap;
					break;
				}
				
				case 3:
				{
					return new feedImg3() as Bitmap;
					break;
				}
				
				case 4:
				{
					return new feedImg4() as Bitmap;
					break;
				}
				
				case 5:
				{
					return new feedImg5() as Bitmap;
					break;
				}
				
				case 6:
				{
					return new feedImg6() as Bitmap;
					break;
				}
				
				default:
				{
					return new feedImg1() as Bitmap;
				}
			}
		}
		
		private function setButtonListeners(button:Sprite):void
		{
			
			button.mouseChildren = false;
			button.buttonMode = true;
			button.addEventListener(MouseEvent.CLICK, this.leaderboardTabClicked);
			
		}
		
		private function exitClicked(evt:MouseEvent):void
		{
			this.gf.setAppropriateTab("", "off");
			this.gf.clearLeaderboard();
		}
		
		private function leaderboardTabClicked(evt:MouseEvent):void
		{
			GeneralAnimations.tab_transition(onlbTab_Inplace, (evt.target as Sprite));
		}
		
		private function onlbTab_Inplace(tab:String):void
		{
			this.performLeaderBoardAnimation(tab);
		}
		
		private function cBack(tab:String):void
		{
			GlobalFunctions.emptyContainer(this.lb_rows);
			
			this.performLeaderBoardAnimation(tab);
		}
		
		private function performLeaderBoardAnimation(tab:String):void
		{
			//this.gf.tf.text = "yep - "+this.lb_rows.parent.numChildren+"";
			if(this.lb_rows.numChildren>0)
			{
				this.removeLeaderBoardRows(tab);
			}
			else
			{
				
				
			switch(tab)
			{
				case "tab1":
				{
					this.leaderboardType = "default";
					this.buildDefaultRow("MeAndOthers");
					break;
				}
				
				case "tab2":
				{
					this.leaderboardType = "default";
					this.buildDefaultRow("topFive");
					break;
				}
				
				case "tab3":
				{
					this.leaderboardType = "company";
					this.buildCompanyRow();
					break;
				}
				
				default:
				{
					this.leaderboardType = "default";
					this.buildDefaultRow();
				}
			}
			
			GeneralAnimations.dropContainerRowsx
			("down", this.animatedParts, tab, null);
			
			}
		}
		
		public function removeLeaderBoardRows(tab:String):void
		{
			GeneralAnimations.dropContainerRowsx
			("up", this.animatedParts, 
			tab, this.cBack);
		}
		
		public function revealLeaderboardContents():void
		{
			//this.animatedParts.push
			
			GeneralAnimations.dropContainerRowsx
			("down", this.animatedParts, "tab3", null);
			/*
			for(var i:int = 0; i<this.animatedParts.length; i++)
			{
				this.animatedParts[i].item.alpha = 1;
			}
			*/
		}
		