
	
	public function getPopupCarousel(sprites:Array, carouselType:String):Sprite
	{
			var mainCarouselContainer:Sprite = new Sprite();
			var scrollerItemsContainer:Sprite = new Sprite();
			this.carouselSlider = new Sprite();
			this.carouselSlider.name = "carouselSlider";
			
			
			//search our xml in our assigned class to get the number
			//of items we need
			//temp remove this
			var numberOfCarouselItems:int = sprites.length;
			var startingPoint:Number = 10;
			//viewPort Width = 595;
			//CardWidth = 185
			//margin = 20
			switch(numberOfCarouselItems)
			{
				case 1:{startingPoint = 10+((618/2)-(191/2)); break;}
				case 2:{startingPoint = 10+((618-((191*2)+20))/2); break;}
			}
			
			
			
			//ONLY SHOW CAROUSEL BUTTONS IF NECESSARY
			if(numberOfCarouselItems>3)
			{
			var carouselButtons:Sprite = this.getCarouselButtons();
			mainCarouselContainer.addChild(carouselButtons);
			carouselButtons.y = 129;
			}	
			
			
			for(var carouIndex:int = 0; carouIndex<numberOfCarouselItems; carouIndex++)
			{
					
				
				var singleContainer:Sprite = new Sprite();

				//place items into our single box
				var cardHolder1:Sprite = sprites[carouIndex];
				var outsideGlow:Sprite = getSelectBoxRing(191, 296);
				outsideGlow.name = "outsideGlow";
				
				
				/*cardHolder1.addChild(cardHolder1);
				cardHolder1.x = 13;
				cardHolder1.y = 13;*/
				
				cardHolder1.addChild(outsideGlow);
				outsideGlow.x=-3;
				outsideGlow.y=-3;
				outsideGlow.alpha = 0;
				
				
				
				this.carouselSlider.addChild(cardHolder1);
				
				//Sometimes we may not want to apply button move here
				//For example, when we are choosing a wealth card to spend,
				//the button mode will be applied before on individual cards
				//and not necessarily on the global scale.
				if(carouselType == "default")
				{ 
					cardHolder1.buttonMode = true; 
					cardHolder1.name = "card_"+carouIndex;
				}	
				
				//Mouse children makes sure that the items inside the
				//sprite isnt clickable if false and clickable
				//if set to true.
				cardHolder1.mouseChildren = false;
				
				var singleBoxRightMargin:Number = 
						(carouIndex==numberOfCarouselItems)?0:20-6;
				
				cardHolder1.x = 
				(startingPoint)+(((/*185*/191)*carouIndex)
				+(singleBoxRightMargin*carouIndex));
				cardHolder1.y = 13;
				
				cardHolder1.addEventListener
				(MouseEvent.MOUSE_OVER, this.carouselItemMoused);
				cardHolder1.addEventListener
				(MouseEvent.MOUSE_OUT, this.carouselItemMoused);
				
			}
				
			
			//create a mask here for the viewport of the carousel
			var mainCarouselViewport:Sprite = new Sprite();
			mainCarouselViewport.graphics.beginFill(0xFFFFFF);
			mainCarouselViewport.graphics.drawRect(0, 0, /*621*/595+23, this.carouselSlider.height+20);
			mainCarouselViewport.graphics.endFill();
			mainCarouselContainer.addChild(mainCarouselViewport);
			mainCarouselViewport.alpha = 0.7;
			
			
			scrollerItemsContainer.addChild(this.carouselSlider);
			mainCarouselContainer.addChild(scrollerItemsContainer);
			this.carouselSlider.mask = mainCarouselViewport;
			
			
			
			 
			
			
			//this.addChild(mainCarouselContainer);
			//mainCarouselViewport.y = mainCarouselViewport.y-140;
			mainCarouselViewport.x = 40;//26;
			scrollerItemsContainer.x = 40//26;
			//mainCarouselContainer.x = 167;
			//mainCarouselContainer.x = 153;
			//mainCarouselContainer.y = 243;
			
			return mainCarouselContainer;
			
	}
	
	private function carouselItemMoused(evt:MouseEvent):void
	{
		switch(evt.type)
		{
			case "mouseOver":
			{
				((evt.currentTarget as Sprite)
				.getChildByName("outsideGlow") as Sprite)
				.alpha = 1;
				break;
			}
			
			case "mouseOut":
			{
				((evt.currentTarget as Sprite)
				.getChildByName("outsideGlow") as Sprite)
				.alpha = 0;
				break;
			}
		}
	}
	
	
		private function getCarouselButtons():Sprite
		{
			var carouselButtons:Sprite = new Sprite();
			this.carouselLeftButton = new Sprite();//aCarouselButton();
			this.carouselLeftButton.name = "carouselLeftButton";
			carouselButtons.addChild(this.carouselLeftButton);
			this.carouselLeftButton.addChild(new buttonLeftOff());
			this.carouselRightButton = new Sprite();
			this.carouselRightButton.name = "carouselRightButton";
			this.carouselLeftButton.buttonMode = true;
			this.carouselRightButton.buttonMode = true;
			
			
			this.carouselRightButton.x = 661;
			this.carouselLeftButton.alpha = 0;
			
			carouselButtons.addChild(this.carouselRightButton);
			this.carouselRightButton.addChild(new buttonRightOff());
			
			
			this.carouselLeftButton.addEventListener(MouseEvent.CLICK ,this.carouselButtonClicked);
			this.carouselRightButton.addEventListener(MouseEvent.CLICK ,this.carouselButtonClicked);
			
			return carouselButtons;
		}
		
		private function carouselButtonClicked(evt:MouseEvent):void
		{
			var dir:String = "right";
			switch(evt.target.name)
			{
				case "carouselLeftButton":{ dir="right"; break; }
				case "carouselRightButton":{ dir="left"; break; }
				default:{}
			}
			
			this.slideCarousel(dir);
		}
		
		private function slideCarousel(dir:String):void
		{
			var pos:int = 0;
			switch(dir)
			{
				case "left":
				{
					pos=((this.carouselSlider.width*-1)+618)-16;
					this.carouselLeftButton.alpha = 1;
					this.carouselRightButton.alpha = 0;
					break;
				}
				case "right":
				{
					pos=0;
					this.carouselLeftButton.alpha = 0;
					this.carouselRightButton.alpha = 1;
					break;
				}
			}	
			
			GeneralAnimations.moveCarousel(this.carouselSlider,pos);
			//CardAnimation.moveCarousel(this.carouselSlider,pos);
			//this.carouselSlider.x = pos;
		}