package src.app_classes.forms
{
	
	import flash.display.Sprite;
	import flash.display.Graphics;
	import src.app_classes.forms.AppForms;
	import flash.text.TextField;
	
	public class RegisterForm extends AppForms
	{
		
		private var tfieldMarginBottom:int = 5;
		
		public var firstName_tf:TextField;
		public var lastName_tf:TextField;
		public var companyName_tf:TextField;
		public var emailAddress_tf:TextField;
		public var postCode_tf:TextField;
		public var password1_tf:TextField;
		public var password2_tf:TextField;
		public var reg_button:Sprite;
		
		public function RegisterForm()
		{
			
		}
		
		public function getRegisterForm():Sprite
		{
			var form:Sprite = new Sprite();
			
			var d:Sprite = new Sprite();
			d.graphics.beginFill(0xFF0000);
			d.graphics.drawRect(0, 0, 300,400);
			d.graphics.endFill();
			form.addChild(d);
			d.alpha = 0.4;
			
			
			this.firstName_tf = this.getTextField();
			form.addChild(this.firstName_tf);
			this.firstName_tf.y=this.setYPositions(true, 0);
			
			this.lastName_tf = this.getTextField();
			form.addChild(this.lastName_tf);
			this.lastName_tf.y=this.setYPositions(true, 1);
			
			this.companyName_tf = this.getTextField();
			form.addChild(this.companyName_tf);
			this.companyName_tf.y=this.setYPositions(true, 2);
			
			this.emailAddress_tf = this.getTextField();
			form.addChild(this.emailAddress_tf);
			this.emailAddress_tf.y=this.setYPositions(true, 3);
			
			this.postCode_tf = this.getTextField();
			form.addChild(this.postCode_tf);
			this.postCode_tf.y=this.setYPositions(true, 4);
			
			this.password1_tf = this.getTextField(true);
			form.addChild(this.password1_tf);
			this.password1_tf.y=this.setYPositions(true, 5);
			
			this.password2_tf = this.getTextField(true);
			form.addChild(this.password2_tf);
			this.password2_tf.y=this.setYPositions(true, 6);
			
			
			this.reg_button = this.getFormButton();
			form.addChild(this.reg_button);
			this.reg_button.y = 325;
			this.reg_button.x = 100;
			
			return form;
		}
		
		private function setYPositions(setMargin:Boolean, numberPosition:int):int
		{
			var marginBottom:int = 0;
			var ff:Boolean = true;
			if(setMargin){marginBottom = this.tfieldMarginBottom;}
			return (this.tf_height*numberPosition)+(marginBottom*(numberPosition));
		}
	}
	
}