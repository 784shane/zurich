package src.app_classes.forms
{
	import src.app_classes.forms.AppForms;
	import flash.text.TextField;
	
	public class PopUpForms extends AppForms
	{
		
		private var tfieldMarginBottom:int = 5;
		
		public function PopUpForms()
		{
			
		}
		
		public function getMajorUiTextField():TextField
		{
			var majorUiTF:TextField = this.getUiTextField();
			//majorUiTF.background = true;
			//majorUiTF.backgroundColor = 0x999999;
			return majorUiTF;
		}
		
		public function getMinorUiTextField():TextField
		{
			var minorUiTF:TextField = this.getUiTextField();
			//minorUiTF.background = true;
			//minorUiTF.backgroundColor = 0x999999;
			return minorUiTF;
		}
	}
	
}