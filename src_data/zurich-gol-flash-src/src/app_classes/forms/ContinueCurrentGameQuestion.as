package src.app_classes.forms
{
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import src.app_classes.forms.TwoButtonQuestion;
	
	
	public class ContinueCurrentGameQuestion extends TwoButtonQuestion
	{
		
		private var logoutDecisionFunction:Function;
		
		public function ContinueCurrentGameQuestion()
		{
			
		}
		
		override public function getLogoutScreen(onDecisionMade:Function, buttonNames:Object, displayBackground:Boolean = false):Sprite
		{
			
			this.logoutDecisionFunction = onDecisionMade;
			
			var r:Sprite = super.getLogoutScreen(onDecisionMade, buttonNames, displayBackground);
			/*
			this.noCancelTxtBox.text = "Yes please";
			this.yesAndSaveTxtBox.text = "Start a new game";
			*/
			return r;
		}
		
		override public function assignClickListener(buttonSprite:Sprite):void
		{
			buttonSprite.addEventListener(MouseEvent.CLICK, buttonClickedx);
		}
		
		private function buttonClickedx(evt:MouseEvent):void
		{
			
			//evt.target.parent.removeChild(evt.target);
			
			
			switch(evt.currentTarget.name)
			{
				case "noCancel":
				{
					this.logoutDecisionFunction("no");
					//evt.target.parent.removeChild(evt.target);
					break;
				}
				
				case "yesAndSave":
				{
					this.logoutDecisionFunction("yes");
					//evt.target.parent.removeChild(evt.target);
					break;
				}
			}
			
		}
		
		
		override public function applyButtonDefaultSettings
		(buttonSprite:Sprite):void
		{
			
			super.applyButtonDefaultSettings(buttonSprite);//, buttonTxtBox);
			/*
			//Always assign default Text Format before changing text
			var tformat:TextFormat = buttonTxtBox.getTextFormat();
			tformat.align = "center";
			buttonTxtBox.defaultTextFormat = tformat;
			
			buttonTxtBox.x = 0;
			buttonTxtBox.width = 178;
			*/
		}
		
	}
	
}