package src.app_classes.forms
{
	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	import src.app_classes.forms.AppForms;
	import GlobalFunctions;
	import src.app_classes.tools.KeyboardControlTool;
	import src.app_classes.GameFlow;
	
	public class PostLoadingScreen extends AppForms
	{
		
		[Embed (source="../../../res/loadingscreen/amazingPrizesImg.png" )]
		protected var amazingPrizesImg:Class;
		
		private var notifyOnClose:Function;
		
		private var gf:GameFlow;
		
		public function PostLoadingScreen(gf:GameFlow, functionWhenClosed:Function)
		{
			this.gf = gf;
			this.notifyOnClose = functionWhenClosed;
		}
		
		public function getPostLoadingScreen():Sprite
		{
			var postLoadingScreen:Sprite = new Sprite();
			
			
			//postLoadingScreen.addChild(GlobalFunctions.getBackDrop());
			
			var postLdnScrnContainer:Sprite = new Sprite();
			
			
			//draw a circle on screen
			var circle:Sprite = new Sprite();
			circle.graphics.lineStyle(3, 0xFFFFFF, 1);
			circle.graphics.beginFill(0xFFFFFF,1);
			//circle.graphics.drawRoundRect(0, 0, 510, 60, 5, 5);
			circle.graphics.drawCircle(0, 0, 255);
			circle.graphics.endFill();
			postLdnScrnContainer.addChild(circle);
			
			circle.x=256;
			circle.y=256;
			circle.alpha = 0.4;
			
			
			var amzPrizesSpr:Sprite = new Sprite();
			var amzPrizesImg:Bitmap = new amazingPrizesImg() as Bitmap;
			amzPrizesSpr.addChild(amzPrizesImg);
			postLdnScrnContainer.addChild(amzPrizesSpr);
			amzPrizesSpr.x = 96;
			amzPrizesSpr.y = 84;
			
			//new KeyboardControlTool(amzPrizesSpr);
			
			
			
			
			var headTxt:TextField = bodyHeaderTextBox();
			var tff:TextFormat = headTxt.getTextFormat();
			tff.size = 18;
			headTxt.defaultTextFormat = tff;
			postLdnScrnContainer.addChild(headTxt);
			headTxt.x = 83;
			headTxt.y = 253;
			headTxt.width = 345;
			//headTxt.background = true;
			headTxt.wordWrap = true;
			headTxt.autoSize = TextFieldAutoSize.CENTER;
			headTxt.htmlText = "There's an iPad, an iPad mini and up to &#163;2,500 worth "+
			"of Red Letter day vouchers up for grabs - so what are you "+
			"waiting for? Be a winner for life.";
			
			var continueButton:Sprite = getFormButton("Cool, let's play");
			postLdnScrnContainer.addChild(continueButton);
			continueButton.x = 169;
			//continueButton.y = 376;
			continueButton.y = headTxt.y+headTxt.textHeight+47;
			
			continueButton.addEventListener(MouseEvent.CLICK, this.continueButtonClicked);
			
			
			
			
			
			var tandCSprite:Sprite = new Sprite();
			tandCSprite.buttonMode = true;
			tandCSprite.mouseChildren = false;
			
			var advertLinkTxt:TextField = bodyHeaderTextBox();
			var advertLinkTxtFormat:TextFormat = advertLinkTxt.getTextFormat();
			advertLinkTxtFormat.size = 15;
			advertLinkTxtFormat.bold = false;
			advertLinkTxt.defaultTextFormat = advertLinkTxtFormat;
			tandCSprite.addChild(advertLinkTxt);
			//advertLinkTxt.y = advertBodyTxt.y+advertBodyTxt.textHeight+12;
			advertLinkTxt.width = 460;
			//headTxt.background = true;
			advertLinkTxt.wordWrap = true;
			advertLinkTxt.autoSize = TextFieldAutoSize.CENTER;
			advertLinkTxt.htmlText = 'Terms and Conditions';
			
			
			postLdnScrnContainer.addChild(tandCSprite);
			tandCSprite.addEventListener(MouseEvent.MOUSE_DOWN, this.openTandCs);
			
			//new KeyboardControlTool(tandCSprite);
			tandCSprite.x= 30;
			tandCSprite.y= 440;
			
			
			
			
			
			
			
			
			
			postLoadingScreen.addChild(postLdnScrnContainer);
			postLdnScrnContainer.x = 232;
			postLdnScrnContainer.y = 119;
			
			
			
			return postLoadingScreen;
		}
		
		private function openTandCs(evt:MouseEvent):void
		{
			this.gf.openTandCs();
		}
		
		private function continueButtonClicked(evt:MouseEvent):void
		{
			this.notifyOnClose();
		}
		
	}
	
}