package src.app_classes.forms
{
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	
	import GlobalFunctions;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	
	import src.app_classes.forms.AppForms;
	
	public class TwoButtonQuestion extends AppForms
	{
		
		
		private var logoutDecisionFunction:Function;
		public var noCancelTxtBox:TextField;
		public var yesAndSaveTxtBox:TextField;
		public var screenTitle:TextField;
		
		//[Embed (source="../../../res/trash/01.Zurich_GOL_LOGOUT.jpg" )]
		//protected var continueScreen:Class;
		
		public function TwoButtonQuestion()
		{
			
		}
		
		public function getLogoutScreen(onDecisionMade:Function, buttonNames:Object, displayBackground:Boolean = false):Sprite
		{
			
			this.logoutDecisionFunction = onDecisionMade;
			
			var r:Sprite = new Sprite();
			
			//r.addChild(new continueScreen());
			
			if(displayBackground)
			{
				r.addChild(GlobalFunctions.getBackDrop());
			}
			
			var titleSpr:Sprite = this.getWealthScreenTitleText();
			r.addChild(titleSpr);
			titleSpr.x = 192;
			titleSpr.y = 319;
		
		
			var yesAndSave:Sprite = this.getButton(buttonNames.button2);
			r.addChild(yesAndSave);
			yesAndSave.name = "yesAndSave";
			yesAndSave.x = 502;//314
			yesAndSave.y = 404;
			
			this.applyButtonDefaultSettings(yesAndSave);
			
			
			var noCancel:Sprite = this.getButton(buttonNames.button1);
			r.addChild(noCancel);
			noCancel.name = "noCancel";
			noCancel.x = 301;
			noCancel.y = 404;
			
			this.applyButtonDefaultSettings(noCancel);
			
			
			r.addChild(noCancel);
			r.addChild(yesAndSave);
			
			
			return r;
			
		}
		
		public function applyButtonDefaultSettings
		(buttonSprite:Sprite):void
		{
			this.assignClickListener(buttonSprite);
		}
		
		public function assignClickListener(buttonSprite:Sprite):void
		{
			buttonSprite.addEventListener(MouseEvent.CLICK, buttonClicked);
		}
		
		private function buttonClicked(evt:MouseEvent):void
		{
			
			switch(evt.currentTarget.name)
			{
				case "noCancel":
				{
					this.logoutDecisionFunction("no");
					//evt.target.parent.removeChild(evt.target);
					break;
				}
				
				case "yesAndSave":
				{
					this.logoutDecisionFunction("yes");
					//evt.target.parent.removeChild(evt.target);
					break;
				}
			}
		}
		
		private function getWealthScreenTitleText():Sprite
		{
			var poppupTitleTextSpr:Sprite = new Sprite();
			this.screenTitle = this.popUpTitleText();
			poppupTitleTextSpr.addChild(this.screenTitle);
			//this.screenTitle.background = true;
			this.screenTitle.text = "Continue with your current game?";
			
			
			return poppupTitleTextSpr
		}
		
		private function getButton(buttonName:String):Sprite
		{
			var OkButton:Sprite = getFormButton(buttonName);
			//OkButton.addEventListener(MouseEvent.CLICK, this.OkButtonClicked);
			return OkButton;
		}
		
	}
	
}