package src.app_classes.forms
{
	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.filters.GlowFilter;
    import flash.utils.Timer;
    import flash.events.TimerEvent;
    import flash.events.Event;
	
	
    import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
    import flash.events.MouseEvent;
	
	
	
	//import src.app_classes.GameFlow;
	import src.app_classes.forms.AppForms;
	import src.app_classes.tools.PictureLoader;
	import Config;
	import src.app_classes.tools.KeyboardControlTool;
	import GlobalFunctions;
	
	import src.app_classes.tools.ClassImageLoader;
	
	public class LoadingScreen extends Sprite
	{
		
		public var loadingTxtIcre:TextField;
		private var loadScreenResources:Array = new Array();
		private var afterResourcesLoaded:Function;
		private var onLoadingScreenComplete:Function;
		
		private var classImgLdr:ClassImageLoader;
		
		private var delay:uint = 1;//000;
        private var repeat:uint = 10;
        private var myTimer:Timer = new Timer(delay, repeat);
		
		
		
        private var debugText:TextField;
		
      	
      	[Embed(source="../../../res/fonts/MouseMemoirs-Regular.ttf",
		fontName = "myFont",
		mimeType = "application/x-font",
		fontWeight="normal",
		fontStyle="normal",
		//unicodeRange="englishRange",
		advancedAntiAliasing="true",
		embedAsCFF="false")]
		private var myEmbeddedFont:Class;
		
		[Embed (source="../../../res/trash/loading.jpg" )]
		protected var loading:Class;
		
		[Embed (source="../../../res/loadingscreen/casing.png" )]
		protected var casing:Class;
		
		[Embed (source="../../../res/loadingscreen/colours-spin.png" )]
		protected var coloursSpin:Class;
		
		private var aF:AppForms;
		
		private var colorsSpr:Sprite;
		
		public function LoadingScreen(loadScreenDone:Function = null)
		{
			this.aF = new AppForms();
			//this.gf = gf;
			//This function will be called when the loading screen
			//has completed its duty. On the end of load, we will call
			//the onLoadingScreenComplete function.
			this.onLoadingScreenComplete = loadScreenDone;
		}
		
		public function loadScreenImages(functionAfterResLoaded:Function):void
		{
			this.classImgLdr = new ClassImageLoader();
			this.afterResourcesLoaded = functionAfterResLoaded;
			this.classImgLdr.saveImages
			(
				this.calledUponImagesSaved,
				Config.loadingScreenImages.images
			);
		}
		
		private function calledUponImagesSaved(imagesSaved:Array):void
		{
			this.loadScreenResources = imagesSaved;
			this.afterResourcesLoaded();
		}
		
		public function getLoadingScreen():Sprite
		{
			
			this.addChild(GlobalFunctions.getBackDrop());
			
			this.addChild(new loading());
			
			//Add strip of blue around the edges of the sprite
			var borderLine:Sprite = new Sprite();
			//we are not adding a fill here as we want a hole in the
			//middle of this sprite
			borderLine.graphics.lineStyle(2, Config.APP_COLORS.DARK_BLUE);
			//borderLine.graphics.lineStyle(4, 0xBB0000);
			borderLine.graphics.drawRect(1, 1, 
										Config.SCREEN_CONFIG.width-2, 
										Config.SCREEN_CONFIG.height-2);
			borderLine.graphics.endFill();
			this.addChild(borderLine);
			
			
			
			var circularContents:Sprite = new Sprite();
			this.addChild(circularContents);
			circularContents.x = 232;
			circularContents.y = 119;
			
			

			
			
			var backgroundCircle:Sprite = GlobalFunctions.getWhiteCircle();
			circularContents.addChild(backgroundCircle);
			//backgroundCircle.x = 487;
			//backgroundCircle.y = 375;
			
			
			
			var buttonHeadSpr:Sprite = new Sprite();
			var headTxt:TextField = this.aF.bodyHeaderTextBox();
			buttonHeadSpr.addChild(headTxt);
			var tff:TextFormat = headTxt.getTextFormat();
			tff.size = 18;
			headTxt.defaultTextFormat = tff;
			headTxt.width = 970;
			headTxt.htmlText = 
			"PROUDLY PRESENTS";
			
			circularContents.addChild(buttonHeadSpr);
			buttonHeadSpr.x = -227;
			buttonHeadSpr.y = 169;
			
			
			this.debugText = new TextField();
			circularContents.addChild(this.debugText);
			this.debugText.text = "text";
			
			
			//circularContents.addEventListener(MouseEvent.CLICK, this.moveMe);
			
			for(var i:int=0; i<this.loadScreenResources.length; i++ )
			{
				
				var s:Sprite = new Sprite();
				var bmp:Bitmap = this.loadScreenResources[i].imgObj.content as Bitmap;
				s.addChild(bmp)
				circularContents.addChild(s);
				
				for(var lsc:int = 0; lsc<Config.loadingScreenImages.images.length; lsc++)
				{
					if(this.loadScreenResources[i].imgName==Config.loadingScreenImages.images[lsc].name)
					{
						s.x = Config.loadingScreenImages.images[lsc].positions.x;
						s.y = Config.loadingScreenImages.images[lsc].positions.y;
						break;
					}
				}
				
				
				
			}
			
				
				
				
				
				
				var colorsCaseSpr:Sprite = new Sprite();
				var colorsCase:Bitmap = new casing() as Bitmap;
				colorsCaseSpr.addChild(colorsCase);
				circularContents.addChild(colorsCaseSpr);
				colorsCaseSpr.y = 385;
				colorsCaseSpr.x = 236;
				
				

				
				
				
				
				this.colorsSpr = new Sprite();
				var colors:Bitmap = new coloursSpin() as Bitmap;
				this.colorsSpr.addChild(colors);
				colors.x = -22.5
				colors.y = -22;
				circularContents.addChild(this.colorsSpr);
				this.colorsSpr.y = 407.5;
				this.colorsSpr.x = 258;
				
				
				
				
				
				
				var loadingTxtSpr:Sprite = new Sprite();
				this.loadingTxtIcre = new TextField();
				loadingTxtSpr.addChild(this.loadingTxtIcre);
				circularContents.addChild(loadingTxtSpr);
				//this.loadingTxtIcre.text = "1%";
				this.loadingTxtIcre.embedFonts= true;
				this.loadingTxtIcre.width = 80;
				this.loadingTxtIcre.height = 30;
				loadingTxtSpr.x = 223;
				loadingTxtSpr.y = 433;
				
				
				
				var format:TextFormat = new TextFormat();
				format.font = "myFont";
				format.color = 0xFFFFFF;
				format.size = 24;
				format.align = "center";
				
				this.loadingTxtIcre.defaultTextFormat = format;
				
				var outline:GlowFilter=new GlowFilter();
				outline.color = Config.APP_COLORS.DARK_BLUE;
				outline.blurX = 4
				outline.blurY = 4;
				outline.strength = 10;
				//outline.knockout = true;
				//var outline:GlowFilter=new GlowFilter(0x000000,3.0,3.0,2.0,10);
				outline.quality=2;//BitmapFilterQuality.MEDIUM;
				this.loadingTxtIcre.filters = [outline];
				
				
				
				
				
				//this.setTimeout();
				this.startAnimation();
			
			return this;
		}
		
		
		private function moveMe(evt:MouseEvent):void
		{
			evt.currentTarget.addEventListener(KeyboardEvent.KEY_DOWN, this.kdown);
		}

		public function kdown(evt:KeyboardEvent):void
		{
			
		    if (evt.keyCode == Keyboard.UP)
		    {
				evt.currentTarget.y+=-1;
				this.debugText.text = "y - "+evt.currentTarget.y;
		    }
		    if (evt.keyCode == Keyboard.DOWN)
		    {
				evt.currentTarget.y+=1;
				this.debugText.text = "y - "+evt.currentTarget.y;
		    }
		    if (evt.keyCode == Keyboard.LEFT)
		    {
				evt.currentTarget.x+=-1;
				this.debugText.text = "x - "+evt.currentTarget.x;
		    }
		    if (evt.keyCode == Keyboard.RIGHT)
		    {
				evt.currentTarget.x+=1;
				this.debugText.text = "x - "+evt.currentTarget.x;
		    }
			
		}
		
		private function setTimeout():void
		{
			//this.myTimer = new Timer(2000, 1);
		    this.myTimer.addEventListener(TimerEvent.TIMER, xxtimerHandler);
		    this.myTimer.addEventListener(TimerEvent.TIMER_COMPLETE, xxcompleteHandler);
			this.myTimer.start();	
		}
		
		private function startAnimation():void
		{
			this.colorsSpr.addEventListener("enterFrame", this.jj);	
		}
		
		private function jj(evt:Event):void
		{
			this.colorsSpr.rotationZ+=1;
			
		}
		
		
		
		private function xxtimerHandler(evt:TimerEvent):void
		{
		
		}
		
		private function xxcompleteHandler(evt:TimerEvent):void
		{
			//this.onLoadingScreenComplete();
		}
		
	}
	
}