package src.app_classes.forms
{
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import src.app_classes.forms.AppForms;
	import src.app_classes.GameFlow;
	import GlobalFunctions;
	import src.app_classes.tools.KeyboardControlTool;
	
	public class LogoutQuestion extends TwoButtonQuestion
	{
		
		private var logoutDecisionFunction:Function;
		
		private var gf:GameFlow;
		
		public function LogoutQuestion(gf:GameFlow)
		{
			this.gf = gf;
		}
		
		override public function getLogoutScreen(onDecisionMade:Function, buttonNames:Object, displayBackground:Boolean = false):Sprite
		{
		
			this.gf.setAppropriateTab("", "off");
			this.gf.setAppropriateTab("login", "on");
			
			this.logoutDecisionFunction = onDecisionMade;
			
			var r:Sprite = super.getLogoutScreen(onDecisionMade, buttonNames, displayBackground);
			this.screenTitle.text = "Sure you want to Log out?";
			return r;
			
		}
		
		/*private function applyButtonDefaultSettings
		(buttonSprite:Sprite, buttonTxtBox:TextField):void
		{
			//Always assign default Text Format before changing text
			var tformat:TextFormat = buttonTxtBox.getTextFormat();
			tformat.align = "left";
			buttonTxtBox.defaultTextFormat = tformat;
			
			buttonTxtBox.x = 52;
			buttonTxtBox.width = 117;
			
			buttonSprite.buttonMode = true;
			
			this.assignClickListener(buttonSprite);
		}
		
		private function assignClickListener(buttonSprite:Sprite):void
		{
			buttonSprite.addEventListener(MouseEvent.CLICK, buttonClicked);
		}
		
		private function buttonClicked(evt:MouseEvent):void
		{
		
			
			
			switch(evt.currentTarget.name)
			{
				case "noCancel":
				{
					this.logoutDecisionFunction("no");
					//evt.target.parent.removeChild(evt.target);
					break;
				}
				
				case "yesAndSave":
				{
					this.logoutDecisionFunction("yes");
					//evt.target.parent.removeChild(evt.target);
					break;
				}
			}
		}*/
		
		
		override public function assignClickListener(buttonSprite:Sprite):void
		{
			buttonSprite.addEventListener(MouseEvent.CLICK, buttonClickedx);
		}
		
		private function buttonClickedx(evt:MouseEvent):void
		{
			
			//evt.target.parent.removeChild(evt.target);
			
			
			switch(evt.currentTarget.name)
			{
				case "noCancel":
				{
					this.logoutDecisionFunction("no");
					//evt.target.parent.removeChild(evt.target);
					break;
				}
				
				case "yesAndSave":
				{
					this.logoutDecisionFunction("yes");
					//evt.target.parent.removeChild(evt.target);
					break;
				}
			}
			
		}
		
		
		override public function applyButtonDefaultSettings
		(buttonSprite:Sprite):void
		{
			
			super.applyButtonDefaultSettings(buttonSprite);//, buttonTxtBox);
			/*
			//Always assign default Text Format before changing text
			var tformat:TextFormat = buttonTxtBox.getTextFormat();
			tformat.align = "center";
			buttonTxtBox.defaultTextFormat = tformat;
			
			buttonTxtBox.x = 0;
			buttonTxtBox.width = 178;
			*/
		}
		
	}
	
}