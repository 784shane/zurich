package src.app_classes.forms
{
	import src.app_classes.forms.AppForms;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	public class UiForm extends AppForms
	{
		
		private var tfieldMarginBottom:int = 5;
		
		public function UiForm()
		{
			
		}
		
		public function getMajorUiTextField():TextField
		{
			
			var majorUiTF:TextField = getUiReportText("major");
			var format2:TextFormat = majorUiTF.getTextFormat();
			format2.color = 0xFFB629;
			format2.size = 28;
			format2.bold = true;
			majorUiTF.defaultTextFormat = format2;
			return majorUiTF;
			
		}
		
		public function getMinorUiTextField():TextField
		{
			/*
			var minorUiTF:TextField = this.getUiTextField();
			
			var format2:TextFormat = this.getBasicTextFormat();
			format2.color = 0x999999;
			format2.size = 20;
			//format2.bold = true;
			format2.align = TextFormatAlign.CENTER;
			minorUiTF.setTextFormat(format2);
			minorUiTF.defaultTextFormat = format2;
			
			return minorUiTF;
			*/
			
			
			var minorUiTF:TextField = getUiReportText("minor");
			var format2:TextFormat = minorUiTF.getTextFormat();
			format2.color = 0x999999;
			format2.size = 20;
			minorUiTF.defaultTextFormat = format2;
			return minorUiTF;
			
			
		}
		
		public function getNameField():TextField
		{
		/*
			var majorUiTF:TextField = this.getUiTextField();
			
			var format2:TextFormat = this.getBasicTextFormat();
			format2.color = 0x999999;
			format2.size = 20;
			format2.bold = true;
			majorUiTF.setTextFormat(format2);
			majorUiTF.defaultTextFormat = format2;
			
			return majorUiTF;
			*/
			
			var minorUiTF:TextField = getUiReportText("minor");
			//minorUiTF.background = true;
			//minorUiTF.backgroundColor = 0xBBCC00;
			var format2:TextFormat = minorUiTF.getTextFormat();
			format2.color = 0x999999;
			format2.size = 18;
			format2.align = "left";
			minorUiTF.defaultTextFormat = format2;
			return minorUiTF;
		}
		
		public function getMoneyReportField():TextField
		{
			
			var minorUiTF:TextField = getUiReportText("major");
			//minorUiTF.background = true;
			//minorUiTF.backgroundColor = 0xBBCC00;
			var format2:TextFormat = minorUiTF.getTextFormat();
			format2.color = 0xFFB629;
			format2.size = 20;
			format2.align = "left";
			format2.bold = true;
			minorUiTF.defaultTextFormat = format2;
			return minorUiTF;
		}
		
		public function getLifeReportField():TextField
		{
			var majorUiTF:TextField = this.getUiTextField("minor");
			
			
			var format2:TextFormat = this.getBasicTextFormat();
			format2.color = 0x999999;
			format2.size = 14;
			format2.bold = true;
			majorUiTF.setTextFormat(format2);
			majorUiTF.defaultTextFormat = format2;
			
			return majorUiTF;
		}
	}
	
}