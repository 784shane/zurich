package src.app_classes.forms
{
	
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.StyleSheet;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	
	import src.app_classes.tools.gameStyles;
	import src.app_classes.config.GlowFilters;
	import GeneralAnimations;
	import Config;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class AppForms
	{
		
		[Embed (source="../../../res/form/validationYes.png" )]
		protected var validationYes:Class;
		
		[Embed (source="../../../res/form/validationNo.png" )]
		protected var validationNo:Class;
		
		[Embed (source="../../../res/cardImages/close.png" )]
		public var cardCloseButton:Class;
		
		[Embed (source="../../../res/cardImages/popupClose.png" )]
		public var popupClose:Class;
		
		[Embed (source="../../../res/mapView/cross.png" )]
		protected var mapViewCross:Class;
		
		[Embed (source="../../../res/global/scroll-bar-knob.png" )]
		protected var scrollKnob:Class;
		
		[Embed (source="../../../res/global/scroll-bar-long.png" )]
		protected var scrollLong:Class;
		
		
		
		////////////////////////////////////////////////////////////////////////
		/////////////////////CAROUSEL DEFINITIONS///////////////////////////////
		////////////////////////////////////////////////////////////////////////
		
		[Embed (source="../../../res/cardImages/carousel/buttonLeftOff.png" )]
		private var buttonLeftOff:Class;
		
		[Embed (source="../../../res/cardImages/carousel/buttonLeftOn.png" )]
		private var buttonLeftOn:Class;
		
		[Embed (source="../../../res/cardImages/carousel/buttonRightOn.png" )]
		private var buttonRightOn:Class;
		
		[Embed (source="../../../res/cardImages/carousel/buttonRightOff.png" )]
		private var buttonRightOff:Class;
		
		private var carouselSlider:Sprite;
		
		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////
		
		include "../extensions/AppFormsExtensions.as";
	
		
		protected var tf_height:int = 44;
		protected var tf_width:int = 298;
		
		private var carouselLeftButton:Sprite = new Sprite();
		private var carouselRightButton:Sprite = new Sprite();
		
		private var selectFilter:Array = [];
		
		public function AppForms()
		{	
			this.selectFilter
			.push(new GlowFilters().getWealthCardGlow());
		}
		
		protected function getTextField(isPassword:Boolean = false):TextField
		{
			
			var tB_holder:Sprite = new Sprite();
			var myTextField:TextField = new TextField();
			
			//set style for this text Box
			gameStyles.form_TextboxStyle(myTextField, new Sprite());
			
			myTextField.width = this.tf_width;
			myTextField.height = this.tf_height;
			myTextField.displayAsPassword = isPassword;
			return myTextField;
		}
		
		protected function getFieldLabel():TextField
		{
			var buttonLabel:TextField = new TextField();
			gameStyles.form_TextLabelStyle(buttonLabel);
			/*
			var bLblTxtFormat:TextFormat = 
			buttonLabel.getTextFormat();
			bLblTxtFormat.size = 21;
			bLblTxtFormat.align = "right";
			buttonLabel.defaultTextFormat = bLblTxtFormat;
			*/
			
			buttonLabel.name = "fieldLabel";
			
			return buttonLabel;
		}
		
		protected function _getTextField(tab_index:int = 1, isPassword:Boolean = false):MovieClip
		{
			
			var tB_holder:MovieClip = new MovieClip();
			var myTextField:TextField = new TextField();
			myTextField.name = "field_txtBox";
			myTextField.tabIndex = tab_index;
			myTextField.embedFonts = true;
			
			//set style for this text Box
			gameStyles.form_TextboxStyle(myTextField, tB_holder);
			myTextField.selectable = true;
			myTextField.displayAsPassword = isPassword;
			tB_holder.addChild(myTextField);
			
			
			return tB_holder;
		}
		
		protected function _getLoginFormRow(
		theLabel:String = "Email Address", 
		tab_index:int = 1, 
		errorMsg:String = "",
		isPassword:Boolean = false
		):Sprite
		{
			var loginRow:Sprite = new Sprite();
			
			var textBoxHolder:MovieClip = this._getTextField(tab_index, isPassword);
			loginRow.addChild(textBoxHolder);
			textBoxHolder.name = "txtBoxContainer";
			
			var emLabel:TextField = this.getFieldLabel();
			loginRow.addChild(emLabel);
			emLabel.text = theLabel;
			emLabel.y = 6;
			emLabel.x = -199;
			
			var valid1:Sprite = new Sprite();
			var validYes:Bitmap = new validationYes() as Bitmap;
			valid1.addChild(validYes);
			loginRow.addChild(valid1);
			valid1.x = 312;
			valid1.y = 7;
			valid1.alpha = 0;
			valid1.name = "validYes";
			
			var valid2:Sprite = new Sprite();
			loginRow.addChild(valid2);
			valid2.x = 312;
			valid2.y = 13;
			valid2.alpha = 0;
			valid2.name = "validNo";
			
			var validNo:Bitmap = new validationNo() as Bitmap;
			valid2.addChild(validNo);
			
			var emLabel1:TextField = this.getFieldLabel();
			var emLabel1TFrm:TextFormat = emLabel1.getTextFormat();
			emLabel1TFrm.align="left";
			emLabel1TFrm.color=0xD22121;
			emLabel1TFrm.size=18;
			emLabel1.defaultTextFormat = emLabel1TFrm;
			valid2.addChild(emLabel1);
			emLabel1.width = 290;
			emLabel1.text = errorMsg;
			emLabel1.y = -4;
			emLabel1.x = 22;
			
			
			return loginRow;
		}
		
		public function getFormButton(ButtonName:String = "Send"):Sprite
		{
			var theButton:Sprite = new Sprite();
			
			
			var buttonOver:Sprite = new Sprite();
			buttonOver.graphics.beginFill(Config.APP_COLORS.BUTTON_OVER,1);
			buttonOver.graphics.drawRoundRect(0, 0, 178, 60, 5, 5);
			buttonOver.graphics.endFill();
			theButton.addChild(buttonOver);
			buttonOver.name = "buttonOver";
			buttonOver.alpha = 0;
			
			
			var buttonOn:Sprite = new Sprite();
			buttonOn.graphics.beginFill(Config.APP_COLORS.BUTTON_ON,1);
			buttonOn.graphics.drawRoundRect(0, 0, 178, 60, 5, 5);
			buttonOn.graphics.endFill();
			theButton.addChild(buttonOn);
			buttonOn.name = "buttonOn";
			buttonOn.alpha = 0;
			
			
			var buttonBack:Sprite = new Sprite();
			buttonBack.graphics.beginFill(0x000000,0);
			buttonBack.graphics.lineStyle(2,Config.APP_COLORS.BUTTON_BORDER);
			buttonBack.graphics.drawRoundRect(0, 0, 178, 60, 5, 5);
			buttonBack.graphics.endFill();
			theButton.addChild(buttonBack);
			buttonBack.name = "buttonBack";
			
			
			var buttonLabel:TextField = new TextField();
			theButton.addChild(buttonLabel);
			gameStyles.form_ButtonTextStyle(buttonLabel);
			//buttonLabel.textColor = 0xFFFFFF;
			buttonLabel.text = ButtonName;
			buttonLabel.name = "buttonLabel";
			
			
			
			
			theButton.buttonMode = true;
			theButton.mouseChildren = false;
			
			GeneralAnimations.setButtonEvents(theButton);
			
			return theButton;
		}
		
		protected function getBasicFormButton():Sprite
		{
			var theButton:Sprite = new Sprite();
			
			
			
			var buttonLabel:TextField = new TextField();
			theButton.addChild(buttonLabel);
			gameStyles.form_ButtonTextStyle(buttonLabel);
			buttonLabel.text = "Send";
			buttonLabel.name = "buttonLabel";
			
			
			
			var buttonBack:Sprite = new Sprite();
			buttonBack.buttonMode = true;
			buttonBack.graphics.beginFill(0x000000,0);
			buttonBack.graphics.lineStyle(2,0x1e286f);
			//buttonBack.graphics.drawRect(0, 0, 180, 60);
			buttonBack.graphics.drawRoundRect(0, 0, 178, 60, 5, 5);
			buttonBack.graphics.endFill();
			
			
			theButton.addChild(buttonBack);
			
			return theButton;
		}
		
		protected function CardSelectOkButton():Sprite
		{
			var theButton:Sprite = new Sprite();
			theButton.buttonMode = true;
			theButton.graphics.beginFill(0xBB00BB);
			theButton.graphics.drawRect(0, 0, 200, 50);
			theButton.graphics.endFill();
			return theButton;
		}
		
		protected function getUiTextField(txtName:String="major"):TextField
		{
			var myTextField:TextField = new TextField();
			myTextField.embedFonts = true;
			myTextField.width = 326;
			myTextField.height = 40;
			//set style for this text Box
			gameStyles.majorUiTfStyle(myTextField, txtName);
			return myTextField;
		}
		
		protected function getBasicTextFormat():TextFormat
		{
			var format2:TextFormat = new TextFormat();
			format2.bold = true;
			format2.align = TextFormatAlign.LEFT;
			
			return format2;
			
		}
		
		public function getTitleText():TextField
		{
			var myTextField:TextField = new TextField();
			//myTextField.embedFonts = true;
			//myTextField.width = 100;
			myTextField.height = 25;
			
			//myTextField.background=true;
			//myTextField.backgroundColor=0x999999;
			
			//set style for this text Box
			gameStyles.titleBarTextStyle(myTextField);
			return myTextField;
			
		}
		
		public function getLeaderBoardText(txtTypeName:String):TextField
		{
			var myTextField:TextField = new TextField();
			myTextField.width = 57;
			myTextField.height = 30;
			
			//myTextField.background=true;
			//myTextField.backgroundColor=0x999999;
			
			//set style for this text Box
			gameStyles.leaderboardTextStyle(myTextField,txtTypeName);
			return myTextField;
			
		}
		
		public function getActivityText():TextField
		{
			var myTextField:TextField = new TextField();
			
			//set style for this text Box
			gameStyles.gt_activityText(myTextField);
			myTextField.wordWrap = true;
			myTextField.width = 258;
			myTextField.height = 110;
			return myTextField;
			
		}
		
		public function getLoginRegisterButtonText():TextField
		{
			var myTextField:TextField = new TextField();
			
			//set style for this text Box
			gameStyles.form_ButtonTextStyle(myTextField);
			myTextField.width = 141;
			//my.height = 30;
			return myTextField;
			
		}
		
		public function getUiReportText(txtName:String = "major"):TextField
		{
			var majorUiTF:TextField = this.getUiTextField(txtName);
			return majorUiTF;
		}
		
		public function getCardText():TextField
		{
			var myTextField:TextField = new TextField();
			myTextField.embedFonts = true
			//myTextField.width = 100;
			myTextField.height = 25;
			//myTextField.alpha = 0;
			
			//myTextField.background=true;
			//myTextField.backgroundColor=0x999999;
			
			//set style for this text Box
			gameStyles.cardTextStyle(myTextField);
			return myTextField;
			
		}
		
		public function getFinalScreenLargeText():TextField
		{
			var myTextField:TextField = new TextField();
			myTextField.embedFonts = true;
			//myTextField.width = 100;
			//myTextField.height = 150;
			//myTextField.alpha = 0;
			
			//myTextField.background=true;
			//myTextField.backgroundColor=0x999999;
			
			//set style for this text Box
			gameStyles.largeNumbersTextStyle(myTextField);
			return myTextField;
			
		}
		
		public function getTwoCardText(getBold:Boolean=true):TextField
		{
			var myTextField:TextField = new TextField();
			myTextField.embedFonts = true;
			myTextField.height = 60;
			myTextField.wordWrap = true;
			gameStyles.cardTextStyle(myTextField, getBold);
			return myTextField;
			
		}
		
		public function popUpTitleText():TextField
		{
			var myTextField:TextField = new TextField();
			myTextField.embedFonts = true;
			myTextField.width = 595;
			myTextField.height = 60;
			myTextField.wordWrap = true;
			
			//set style for this text Box
			gameStyles.popupTitleTextStyle(myTextField);
			return myTextField;
			
		}
		
		public function leaderBoardTab(buttonLabelTxt:String, w:int, h:int, border:int):Sprite
		{
			var leaderBoardTab:Sprite = new Sprite();
			
			var tabBackMask:Sprite = new Sprite();
			tabBackMask.graphics.beginFill(0xBBBB00, 0);
			tabBackMask.graphics.drawRect(0, 0, w, 60);
			tabBackMask.graphics.endFill();
			leaderBoardTab.addChild(tabBackMask);
			
			var tabBack:Sprite = new Sprite();
			tabBack.graphics.lineStyle(border, Config.APP_COLORS.DARK_BLUE, 1, true);
			//tabBack.graphics.beginFill(0xFFFFFF, 0.5);
			tabBack.graphics.drawRoundRect(border, border, w-(border*2), 70, 6, 6);
			tabBack.graphics.endFill();
			
			leaderBoardTab.addChild(tabBack);
			
			
			var tabColorInsert:Sprite = new Sprite();
			tabColorInsert.graphics.beginFill(0xFFFFFF, 0.5);
			tabColorInsert.graphics.drawRoundRect(border, border, w-(border*2), 70, 6, 6);
			tabColorInsert.graphics.endFill();
			tabColorInsert.name = "interiorColor"
			
			leaderBoardTab.addChild(tabColorInsert);
			
			tabBack.mask = tabBackMask;
			
			
			
			var buttonLabel:TextField = new TextField();
			leaderBoardTab.addChild(buttonLabel);
			gameStyles.form_ButtonTextStyle(buttonLabel);
			buttonLabel.width = w;
			buttonLabel.text = buttonLabelTxt;
			buttonLabel.name = "tabLabel";
			//buttonLabel.background = true;
			
			//new KeyboardControlTool(cc);
			
			//leaderBoardTab.scaleX = 2;
			//leaderBoardTab.scaleY = 2;
			
			return leaderBoardTab;
		}
		
		public function getFeedText(boldIt:Boolean = false):TextField
		{
			var bodyHeadTxt:TextField = new TextField();
			bodyHeadTxt.embedFonts = true;
			gameStyles.feedText_TfStyle(bodyHeadTxt,boldIt);
			var txtFormat:TextFormat = bodyHeadTxt.getTextFormat();
			txtFormat.size=12;
			txtFormat.align="left";
			if(boldIt){txtFormat.bold=true;}
			bodyHeadTxt.defaultTextFormat = txtFormat;
			return bodyHeadTxt;
		}
		
		public function getFeedNameText(boldIt:Boolean = false):TextField
		{
			var bodyHeadTxt:TextField = new TextField();
			bodyHeadTxt.embedFonts = true;
			gameStyles.FeedNameText_TfStyle(bodyHeadTxt);
			var txtFormat:TextFormat = bodyHeadTxt.getTextFormat();
			txtFormat.size=12;
			txtFormat.align="left";
			if(boldIt){txtFormat.bold=true;}
			bodyHeadTxt.defaultTextFormat = txtFormat;
			return bodyHeadTxt;
		}
		
		public function bodyHeaderTextBox():TextField
		{
			var bodyHeadTxt:TextField = new TextField();
			bodyHeadTxt.embedFonts = true;
			gameStyles.form_ButtonTextStyle(bodyHeadTxt);
			return bodyHeadTxt;
		}
		
		public function paragraphTextBox():TextField
		{
			var paragraphTxt:TextField = new TextField();
			paragraphTxt.embedFonts = true;
			//gameStyles.feedText_TfStyle(paragraphTxt);
			gameStyles.htpPara_TfStyle(paragraphTxt);
			return paragraphTxt;
		}
		
		public function scoreRibbon():Sprite
		{
			return new Sprite();
		}
		
		public function spinToWinBanner():Sprite
		{
			var banner:Sprite = new Sprite();
			
			
			var backgroundSpr:Sprite = new Sprite();
			backgroundSpr.graphics.lineStyle(2,0x0055a2);
			backgroundSpr.graphics.beginFill(0xFFFFFF,0.3);
			backgroundSpr.graphics.drawRoundRect(0, 0, 526, 68, 10, 10);
			backgroundSpr.graphics.endFill();
			banner.addChild(backgroundSpr);
			
			
			
			/*
			poppupTitleText.text = "How to play";
			poppupTitleTextSpr.x = 192;
			poppupTitleTextSpr.y = 105;
			*/
			
			/*
			tabBackMask.graphics.beginFill(0xBBBB00, 0);
			tabBackMask.graphics.drawRect(0, 0, w, 60);
			tabBackMask.graphics.endFill();
			leaderBoardTab.addChild(tabBackMask);
			
			var advertBack:Sprite = new Sprite();
			advertBack.graphics.lineStyle(2,0xFFFFFF);
			advertBack.graphics.beginFill(0xFFFFFF,0.3);
			advertBack.graphics.drawRoundRect(0, 0, 460, 536, 25, 25);
			advertBack.graphics.endFill();
			this.advertContainer.addChild(advertBack);
			*/
			
			return banner;
		}
		
		public function getSelectActorRing
		(radius:Number, offset:Number):Sprite
		{
				var ring:Sprite = new Sprite();
				ring.name = "actor_glow";
				
				
				var myFilters:Array = [];
				myFilters.push(new GlowFilters().getWealthCardGlow());
				
				var wealthCardExternal:Sprite = new Sprite();
				wealthCardExternal.graphics.beginFill(0xBB00BB, 1);
				wealthCardExternal.graphics.drawCircle(0, 0, radius+offset);
				//wealthCardExternal.graphics.drawRoundRect(0, 0, spr1.width, spr1.width, 10, 10);
				wealthCardExternal.graphics.endFill();
				wealthCardExternal.filters = myFilters;
				wealthCardExternal.alpha=1;
				ring.addChild(wealthCardExternal);
				
				return ring;
				
				
		}
		
		public function getSelectBoxRing(w:Number=191, h:Number=296):Sprite
		{
				var ring:Sprite = new Sprite();
				ring.name = "actor_glow";
				
				/*
				var myFilters:Array = [];
				myFilters.push(new GlowFilters().getWealthCardGlow());
				*/
				var wealthCardExternal:Sprite = new Sprite();
				wealthCardExternal.graphics.beginFill(0xBB00BB, 1);
				wealthCardExternal.graphics.drawRoundRect(0, 0, w, h, 10, 10);
				wealthCardExternal.graphics.endFill();
				wealthCardExternal.filters = selectFilter;//myFilters;
				wealthCardExternal.alpha=1;
				ring.addChild(wealthCardExternal);
				
				return ring;
				
				
		}
		
		
	}
	
}