package src.app_classes.forms
{
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Graphics;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import src.app_classes.forms.AppForms;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import src.app_classes.tools.CompanySortCtrl;
	import src.app_classes.tools.Validation;
	import src.app_classes.GameFlow;
	import GlobalFunctions;
	import Config;
	import PlayerResources;
	
	import GeneralAnimations;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class LoginForm extends AppForms
	{
		
		
		/*[Embed (source="../../../res/trash/01.Zurich_GOL_LOGIN.jpg" )]
		protected var blockScreenImg:Class;*/
		
		
		private var gf:GameFlow;
		private var compSortCtrl:CompanySortCtrl;
		private var companyDropDown:Sprite;
		
		
		private var tfieldMarginBottom:int = 9;
		
		public var lf_emailAddress_tf:MovieClip;
		public var lf_password_tf:MovieClip;
		public var loginSwitchButton:Sprite;
		public var registerSwitchButton:Sprite;
		public var regSwitchBackground:Sprite;
		public var lgnSwitchBackground:Sprite;
		public var regButtonTextField:TextField;
		public var lgnButtonTextField:TextField;
		
		public var formFields:Sprite;
		public var form:Sprite;
		
		public var reg_button:Sprite;
		
		public var firstName_tf:MovieClip;
		public var lastName_tf:MovieClip;
		public var companyName_tf:MovieClip;
		public var emailAddress_tf:MovieClip;
		public var postCode_tf:MovieClip;
		public var password1_tf:MovieClip;
		public var password2_tf:MovieClip;
		
		private var register_pass:Boolean = false;
		
		
		private var animatedParts:Array = new Array();
		private var tttf:TextField;
		
		private var validatedItems:Array = new Array();
		
		
		private var formShowing:String = "loginButton";
		
		public var companiesArray:Array = 
		new Array();
		
		public function LoginForm(gf:GameFlow)
		{
			this.compSortCtrl = new CompanySortCtrl(this);
			this.gf=gf;
		}
		
		public function getLoginForm(showBackground:Boolean = false):Sprite
		{
			
			this.gf.getCompaniesList();
			
			this.form = new Sprite();
			
			//if(showBackground)
			//{
				//this.form.addChild(GlobalFunctions.getBackDrop());
			//}
			//this.form.addChild(new blockScreenImg());
			
					
			
			var loginDecision:Sprite = new Sprite();
			loginDecision.x = 340;
			loginDecision.y = 75;
			this.form.addChild(loginDecision);
			
			
			var g:Sprite = new Sprite();
			g.graphics.lineStyle(1.5, Config.APP_COLORS.DARK_BLUE, 1, true);
			g.graphics.beginFill(0x000000, 0.2);
			g.graphics.drawRoundRect(1.5, 1.5, 177, 57, 60, 60);
			g.graphics.endFill();
			loginDecision.addChild(g);
			
			var gMask:Sprite = new Sprite();
			gMask.graphics.beginFill(0xBB00BB);
			gMask.graphics.drawRect(0, 0, 150,90);
			gMask.graphics.endFill();
			loginDecision.addChild(gMask);
			
			g.mask = gMask;
			
			
			var f:Sprite = new Sprite();
			f.graphics.lineStyle(1.5, Config.APP_COLORS.DARK_BLUE, 1, true);
			f.graphics.beginFill(0x000000, 0.2);//Config.APP_COLORS.DARK_BLUE);
			f.graphics.drawRoundRect(1.5, 1.5, 177, 57, 60, 60);
			f.graphics.endFill();
			loginDecision.addChild(f);
			f.x = 120;
			
			var fMask:Sprite = new Sprite();
			fMask.graphics.beginFill(0xBB00BB);
			fMask.graphics.drawRect(0, 0, 150,90);
			fMask.graphics.endFill();
			loginDecision.addChild(fMask);
			fMask.x=150;
			
			f.mask = fMask;
			
			
			
			
			this.lgnSwitchBackground = new Sprite();
			this.lgnSwitchBackground.graphics.lineStyle(1.5, Config.APP_COLORS.DARK_BLUE, 1, true);
			this.lgnSwitchBackground.graphics.beginFill(Config.APP_COLORS.DARK_BLUE);
			this.lgnSwitchBackground.graphics.drawRoundRect(1.5, 1.5, 177, 57, 60, 60);
			this.lgnSwitchBackground.graphics.endFill();
			loginDecision.addChild(this.lgnSwitchBackground);
			this.lgnSwitchBackground.buttonMode = true;
			
			var loginButtonMask:Sprite = new Sprite();
			loginButtonMask.graphics.beginFill(0xBB00BB);
			loginButtonMask.graphics.drawRect(0, 0, 150,90);
			loginButtonMask.graphics.endFill();
			loginDecision.addChild(loginButtonMask);
			
			this.lgnSwitchBackground.mask = loginButtonMask;
			
			
			
			
			
			this.regSwitchBackground = new Sprite();
			this.regSwitchBackground.graphics.lineStyle(1.5, Config.APP_COLORS.DARK_BLUE, 1, true);
			this.regSwitchBackground.graphics.beginFill(Config.APP_COLORS.DARK_BLUE);
			this.regSwitchBackground.graphics.drawRoundRect(1.5, 1.5, 177, 57, 60, 60);
			this.regSwitchBackground.graphics.endFill();
			loginDecision.addChild(this.regSwitchBackground);
			this.regSwitchBackground.buttonMode = true;
			this.regSwitchBackground.x = 120;
			
			
			var registerButtonMask:Sprite = new Sprite();
			registerButtonMask.graphics.beginFill(0xBB00BB);
			registerButtonMask.graphics.drawRect(0, 0, 150,90);
			registerButtonMask.graphics.endFill();
			loginDecision.addChild(registerButtonMask);
			registerButtonMask.x=150;
			
			this.regSwitchBackground.mask = registerButtonMask;
			
			


			
			
			
			var ddd:Sprite = new Sprite();
			this.lgnButtonTextField = this.getLoginRegisterButtonText();
			loginDecision.addChild(ddd);
			ddd.addChild(this.lgnButtonTextField);
			this.lgnButtonTextField.text = "Login";
			ddd.x = 7;
			
			
			
			var eee:Sprite = new Sprite();
			this.regButtonTextField = this.getLoginRegisterButtonText();
			loginDecision.addChild(eee);
			eee.addChild(this.regButtonTextField);
			this.regButtonTextField.text = "Register";
			eee.x = 152;
			
			
			
			
			
			
			
			
			
			this.loginSwitchButton = new Sprite();
			this.loginSwitchButton.graphics.lineStyle(1.5, Config.APP_COLORS.DARK_BLUE, 1, true);
			this.loginSwitchButton.graphics.beginFill(0xBBCC00);
			this.loginSwitchButton.graphics.drawRoundRect(1.5, 1.5, 177, 57, 60, 60);
			this.loginSwitchButton.graphics.endFill();
			loginDecision.addChild(this.loginSwitchButton);
			this.loginSwitchButton.name = "loginButton";
			this.loginSwitchButton.buttonMode = true;
			this.loginSwitchButton.addEventListener(MouseEvent.MOUSE_OVER, this.toggleLoginDecisionButtons);
			this.loginSwitchButton.addEventListener(MouseEvent.MOUSE_OUT, this.toggleLoginDecisionButtons);
			
			
			var loginSwitchButtonMask:Sprite = new Sprite();
			loginSwitchButtonMask.graphics.beginFill(0xBB00BB);
			loginSwitchButtonMask.graphics.drawRect(0, 0, 150,90);
			loginSwitchButtonMask.graphics.endFill();
			loginDecision.addChild(loginSwitchButtonMask);
			
			this.loginSwitchButton.mask = loginSwitchButtonMask;
			this.loginSwitchButton.alpha = 0;
			
			
			
			this.registerSwitchButton = new Sprite();
			this.registerSwitchButton.graphics.lineStyle(1.5, Config.APP_COLORS.DARK_BLUE, 1, true);
			this.registerSwitchButton.graphics.beginFill(0xBBCC00);
			this.registerSwitchButton.graphics.drawRoundRect(1.5, 1.5, 177, 57, 60, 60);
			this.registerSwitchButton.graphics.endFill();
			loginDecision.addChild(this.registerSwitchButton);
			this.registerSwitchButton.name = "registerButton";
			this.registerSwitchButton.buttonMode = true;
			this.registerSwitchButton.addEventListener(MouseEvent.MOUSE_OVER, this.toggleLoginDecisionButtons);
			this.registerSwitchButton.addEventListener(MouseEvent.MOUSE_OUT, this.toggleLoginDecisionButtons);
			this.registerSwitchButton.x = 120;
			
			
			var registerSwitchButtonMask:Sprite = new Sprite();
			registerSwitchButtonMask.graphics.beginFill(0xBB00BB);
			registerSwitchButtonMask.graphics.drawRect(0, 0, 150,90);
			registerSwitchButtonMask.graphics.endFill();
			loginDecision.addChild(registerSwitchButtonMask);
			registerSwitchButtonMask.x=150;
			
			this.registerSwitchButton.mask = registerSwitchButtonMask;
			this.registerSwitchButton.alpha = 0;
		
			
			
			this.setSwitchButtonState("loginButton", "on");
			this.setSwitchButtonState("registerButton", "off");
			
			this.getLoginFields(this.form);
			
			
			return this.form;
		}
		
		public function animateDrop():void
		{
			
			GeneralAnimations.dropContainerRowsx
			("down", this.animatedParts, "tab3", null);
			
		}
		
		private function setYPositions(setMargin:Boolean, numberPosition:int):int
		{
			var marginBottom:int = 0;
			var ff:Boolean = true;
			if(setMargin){marginBottom = this.tfieldMarginBottom;}
			return (this.tf_height*numberPosition)+(marginBottom*(numberPosition));
		}
		
		private function getLoginFields(form:Sprite):void
		{
				
				
			this.formFields = new Sprite();
			this.formFields.name = "loginFormFields";
			this.formFields.x = 341;
			this.formFields.y = 176;
			
			
			
			
			
			var emailRow:Sprite = this._getLoginFormRow("Email Address", 1, "Please enter a valid email address");
			emailRow.name = "emailRow";
			this.formFields.addChild(emailRow);
			emailRow.y = 0;//this.setYPositions(true, 0);
			this.lf_emailAddress_tf = emailRow.getChildByName("txtBoxContainer")as MovieClip;
			(this.lf_emailAddress_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			
			var passRow:Sprite = this._getLoginFormRow("Password", 2, "Please enter a correct password", true);
			passRow.name = "passRow";
			this.formFields.addChild(passRow);
			passRow.y = 0;//this.setYPositions(true, 1);
			this.lf_password_tf = passRow.getChildByName("txtBoxContainer")as MovieClip;
			(this.lf_password_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			
			
			if(this.gf.res.positionLocal)
			{
				//temporarily place this here
				(this.lf_emailAddress_tf
				.getChildByName("field_txtBox") as TextField).text = "1@2.com";
				
				//temporarily place this here
				(this.lf_password_tf
				.getChildByName("field_txtBox") as TextField).text = "password";
			}
			
			
			
			
			this.animatedParts = 
			[{item:emailRow, Ypos:this.setYPositions(true, 0)},
			{item:passRow, Ypos:this.setYPositions(true, 1)}];
			
			
			
			this.reg_button = this.getFormButton();
			this.formFields.addChild(this.reg_button);
			this.reg_button.y = 197;
			this.reg_button.x = 59;
			
			
			this.formFields.addEventListener("addedToStage", this.gf.formAddedToScreen);
			
			
			
			form.addChild(this.formFields);
			
		}
		
		private function focusIn(evt:FocusEvent):void
		{
			this.compSortCtrl.clearCompanyDropDown();
		}
		
		private function focusOut(evt:FocusEvent):void
		{
			
			var theTxtField:TextField = (evt.target as TextField);
			var itemName:String = evt.target.parent.parent.name;
			
			
			switch(itemName)
			{
			
			/*
			passRow
			compNameRow
			*/
				case "fnRow":
				case "lnRow":
				//case "pcodeRow":
				case "passRow":
				case "passRow1":
				case "passRow2":
				case "compRow":
				{
					
					
					if(!Validation.is_text_blank(theTxtField.text))
					{
						(theTxtField.parent.parent
						.getChildByName("validYes") as Sprite).alpha = 1;
						(theTxtField.parent.parent
						.getChildByName("validNo") as Sprite).alpha = 0;
						
						if(itemName == "passRow1"||itemName == "passRow2")
						{
							var passTxt1:TextField = (this.password1_tf
							.getChildByName("field_txtBox") as TextField);
							
							var passTxt2:TextField = (this.password2_tf
							.getChildByName("field_txtBox") as TextField);
							
							if(passTxt1.text != passTxt2.text
							&& (!Validation.is_text_blank(passTxt1.text))
							&& (!Validation.is_text_blank(passTxt2.text)))
							{
								//passTxt2.textColor = 0xFFFF00;
								
								(this.password1_tf.parent
								.getChildByName("validYes") as Sprite).alpha = 0;
								(this.password2_tf.parent
								.getChildByName("validNo") as Sprite).alpha = 1;
								
								(this.password2_tf.parent
								.getChildByName("validYes") as Sprite).alpha = 0;
								(this.password1_tf.parent
								.getChildByName("validNo") as Sprite).alpha = 1;
								
				
				
							}
							else
							{
								if(passTxt1.text == passTxt2.text)
								{
									(this.password1_tf.parent
									.getChildByName("validYes") as Sprite).alpha = 1;
									(this.password2_tf.parent
									.getChildByName("validNo") as Sprite).alpha = 0;
									
									(this.password2_tf.parent
									.getChildByName("validYes") as Sprite).alpha = 1;
									(this.password1_tf.parent
									.getChildByName("validNo") as Sprite).alpha = 0;
								}	
								
							}
								
						}
						
					}
					else
					{
						(theTxtField.parent.parent
						.getChildByName("validYes") as Sprite).alpha = 0;
						(theTxtField.parent.parent
						.getChildByName("validNo") as Sprite).alpha = 1;
					}
					
					break;
				}
				
				case "emailRow":
				case "emRow":
				{
					if(Validation.isValidEmail(theTxtField.text))
					{
						//theTxtField.text = "valid";
						(theTxtField.parent.parent
						.getChildByName("validYes") as Sprite).alpha = 1;
						(theTxtField.parent.parent
						.getChildByName("validNo") as Sprite).alpha = 0;
			
					}
					else
					{
						//theTxtField.text = "invalid";
						(theTxtField.parent.parent
						.getChildByName("validYes") as Sprite).alpha = 0;
						(theTxtField.parent.parent
						.getChildByName("validNo") as Sprite).alpha = 1;
					}
					break;
				}
			}

		}
		
		private function getRegisterFields(form:Sprite):void
		{
			
			this.formFields = new Sprite();
			this.formFields.name = "registerFormFields";
			this.formFields.x = 341;
			this.formFields.y = 176;
			
			/*
			this.firstName_tf = this._getTextField();
			this.formFields.addChild(this.firstName_tf);
			this.firstName_tf.y=this.setYPositions(true, 0);
			
			
			var fnLabel:Sprite = this.getLabel("First Name");
			this.formFields.addChild(fnLabel);
			fnLabel.y = -7;
			*/
			
			var fnRow:Sprite = this._getLoginFormRow("First Name", 1, "Please enter your first name.");
			this.formFields.addChild(fnRow);
			fnRow.name = "fnRow";
			fnRow.y = 0;//this.setYPositions(true, 0);
			this.firstName_tf = fnRow.getChildByName("txtBoxContainer")as MovieClip;
			(this.firstName_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			(this.firstName_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusIn", this.focusIn);
			
			
			var lnRow:Sprite = this._getLoginFormRow("Surname", 2, "Please enter your last name.");
			this.formFields.addChild(lnRow);
			lnRow.name = "lnRow";
			lnRow.y = 0;//this.setYPositions(true, 1);
			this.lastName_tf = lnRow.getChildByName("txtBoxContainer")as MovieClip;
			(this.lastName_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			(this.lastName_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusIn", this.focusIn);
				
			
			var compNameRow:Sprite = this._getLoginFormRow("Company Name", 3, "Please enter your company");
			this.formFields.addChild(compNameRow);
			compNameRow.name = "compRow";//"fnRow";
			compNameRow.y = 0;//this.setYPositions(true, 2);
			this.companyName_tf = compNameRow.getChildByName("txtBoxContainer")as MovieClip;
			(this.companyName_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			(this.companyName_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusIn", this.focusIn);
				
				
			(this.companyName_tf
				.getChildByName("field_txtBox") as TextField).addEventListener
			(KeyboardEvent.KEY_UP, this.spacebarPressed);
			
			
			var emRow:Sprite = this._getLoginFormRow("Email Address", 4, "Please enter a valid email address");
			this.formFields.addChild(emRow);
			emRow.name = "emRow";
			emRow.y = 0;//this.setYPositions(true, 3);
			this.emailAddress_tf = emRow.getChildByName("txtBoxContainer")as MovieClip;
			(this.emailAddress_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			(this.emailAddress_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusIn", this.focusIn);
				
			
			
			var pcodeRow:Sprite = this._getLoginFormRow("Postcode", 5, "");
			this.formFields.addChild(pcodeRow);
			pcodeRow.name = "pcodeRow";
			pcodeRow.y = 0;//this.setYPositions(true, 4);
			this.postCode_tf = pcodeRow.getChildByName("txtBoxContainer")as MovieClip;
			(this.postCode_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			(this.postCode_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusIn", this.focusIn);
				
			
			
			var passRow1:Sprite = this._getLoginFormRow("Create Password", 6, "Please enter a correct password", true);
			this.formFields.addChild(passRow1);
			passRow1.name = "passRow1";
			passRow1.y = 0;//this.setYPositions(true, 5);
			this.password1_tf = passRow1.getChildByName("txtBoxContainer")as MovieClip;
			(this.password1_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			(this.password1_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusIn", this.focusIn);
				
			
			
			var passRow2:Sprite = this._getLoginFormRow("Confirm Password", 7, "Enter the same password again", true);
			this.formFields.addChild(passRow2);
			passRow2.name = "passRow2";
			passRow2.y = 0;//this.setYPositions(true, 6);
			this.password2_tf = passRow2.getChildByName("txtBoxContainer")as MovieClip;
			(this.password2_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusOut", this.focusOut);
			(this.password2_tf
				.getChildByName("field_txtBox") as TextField).addEventListener("focusIn", this.focusIn);
				
			
			
			
			
			this.animatedParts = 
			[{item:fnRow, Ypos:this.setYPositions(true, 0)},
			{item:lnRow, Ypos:this.setYPositions(true, 1)},
			{item:compNameRow, Ypos:this.setYPositions(true, 2)},
			{item:emRow, Ypos:this.setYPositions(true, 3)},
			{item:pcodeRow, Ypos:this.setYPositions(true, 4)},
			{item:passRow1, Ypos:this.setYPositions(true, 5)},
			{item:passRow2, Ypos:this.setYPositions(true, 6)}];
			
			
			
			
			
			this.companyDropDown = 
			this.compSortCtrl.getCompanyDropDown();
			this.formFields.addChild(this.companyDropDown);
			this.companyDropDown.y = 151;
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			this.reg_button = this.getFormButton();
			this.formFields.addChild(this.reg_button);
			this.reg_button.y = 441;
			this.reg_button.x = 59;
			
			
			this.formFields.addEventListener("addedToStage", this.gf.formAddedToScreen);
			
			this.clearAllValidatedMarks();
			
			form.addChild(this.formFields);
			
			
		}
		
		private function spacebarPressed(evt:KeyboardEvent):void
		{
			/*
			if(evt.keyCode == 32)
			{
				this.spinner_actor.removeEventListener
				(KeyboardEvent.KEY_DOWN, this.spacebarPressed);
				this.moveSpinner();
			}
			*/
			
			/*
				(this.firstName_tf
				.getChildByName("field_txtBox") as TextField).text =
				(evt.keyCode)+"";
			*/
			
			
			if(evt.keyCode!=38&&evt.keyCode!=40
			&&evt.keyCode!=13)
			{
				
				
				
				
			var textEntered:String = 
			((evt.target as TextField).text).toLowerCase();
			
			var arrayForList:Array = new Array();
			
			if(textEntered!="")
			{
					
				for(var i:int = 0; i<this.companiesArray.length; i++)
				{
					
					
					
								
					//this.companiesArray.substr(0,3);
					if((this.companiesArray[i].substr(0,textEntered.length).toLowerCase())==
					textEntered&&arrayForList.length<5)
					{
						//(this.lastName_tf
					//.getChildByName("field_txtBox") as TextField).text += 
					arrayForList.push(this.companiesArray[i]);
					
					}
					
					
				}
				
			}
			
			
			this.compSortCtrl.populateCompanyDropDown(arrayForList);
			
			
			}
			else
			{
				this.dealWithDownAndUp(evt.keyCode);
				
				/*
				(this.firstName_tf
				.getChildByName("field_txtBox") as TextField).text =
				(evt.keyCode)+"";
				*/
			}	
			
			
		}
		
		public function dealWithDownAndUp(keyCode:int):void
		{
			switch(keyCode)
			{
				case 13:
				{
					/*
					(this.firstName_tf
				.getChildByName("field_txtBox") as TextField).text = "watch";
					*/
					/*if(this.compSortCtrl.highlightedFound)
					{
						(this.firstName_tf
				.getChildByName("field_txtBox") as TextField).text = ""+
						this.compSortCtrl.getHighlightedCompanyName();
					}*/
					
					if(this.compSortCtrl.selectionMade)
					{
						/*
						(this.firstName_tf
				.getChildByName("field_txtBox") as TextField).text = "wicked";
						*/
						
						this.assignCompanyAndCloseDD
						(this.compSortCtrl.getHighlightedCompanyName());
						
					}
					
					break;
				}
				
				case 38:
				{
					this.compSortCtrl.positionMouseOverHighlight("up"); 
					break;
				}
				
				case 40:
				{
					this.compSortCtrl.positionMouseOverHighlight("down"); 
					break;
				}
			}
		}
		
		public function assignCompanyAndCloseDD(txt:String):void
		{
			(this.companyName_tf
				.getChildByName("field_txtBox") as TextField).text = txt;
						
			this.compSortCtrl.clearCompanyDropDown();
		}
		
		private function getFormRow():Sprite
		{
			return new Sprite();
		}
		
		private function toggleLoginDecisionButtons(evt:MouseEvent):void
		{
			var targetName:String = evt.target.name;
			switch(targetName)
			{
				case "registerButton":
				{
					
					if(evt.type == "mouseOut")
					{
						if(this.formShowing!=targetName)
						{
						this.setSwitchButtonState(evt.target.name, "off");
						}	
					}
					if(evt.type == "mouseOver")
					{
						this.setSwitchButtonState(evt.target.name, "on");
					}
					break;
				}
				
				case "loginButton":
				{
					
					if(evt.type == "mouseOut")
					{
						if(this.formShowing!=targetName)
						{
						this.setSwitchButtonState(evt.target.name, "off");
						}	
					}
					if(evt.type == "mouseOver")
					{
						this.setSwitchButtonState(evt.target.name, "on");
					}
					
					break;
				}
			}
		}
		
		
		private function setSwitchButtonState(type:String, state:String):void
		{
			var regTextFormat:TextFormat;
			var lgnTextFormat:TextFormat;
			
			switch(type)
			{
				case "registerButton":
				{
					
					if(state == "off")
					{
						this.regSwitchBackground.alpha = 0;
						regTextFormat = this.regButtonTextField.getTextFormat();
						regTextFormat.color = Config.APP_COLORS.DARK_BLUE;
						this.regButtonTextField.setTextFormat(regTextFormat);
			
					}
					if(state == "on")
					{
						this.regSwitchBackground.alpha = 1;
						regTextFormat = this.regButtonTextField.getTextFormat();
						regTextFormat.color = 0xFFFFFF;
						this.regButtonTextField.setTextFormat(regTextFormat);
					}
					break;
				}
				
				case "loginButton":
				{
					
					if(state == "off")
					{
						this.lgnSwitchBackground.alpha = 0;
						lgnTextFormat = this.lgnButtonTextField.getTextFormat();
						lgnTextFormat.color = Config.APP_COLORS.DARK_BLUE;
						this.lgnButtonTextField.setTextFormat(lgnTextFormat);
					}
					if(state == "on")
					{
						this.lgnSwitchBackground.alpha = 1;
						lgnTextFormat = this.lgnButtonTextField.getTextFormat();
						lgnTextFormat.color = 0xFFFFFF;
						this.lgnButtonTextField.setTextFormat(lgnTextFormat);
					}
					
					break;
				}
			}
		}
		
		public function setForm(type:String, tf:TextField):void
		{
			
			this.formFields.parent.removeChild(this.formFields);
			
			switch(type)
			{
				case "register":
				{
					this.formShowing = "registerButton";
					this.setSwitchButtonState("loginButton", "off");
					this.setSwitchButtonState("registerButton", "on");
					this.getRegisterFields(this.form);
					break;
				}
				
				case "login":
				{
					this.formShowing = "loginButton";
					this.setSwitchButtonState("loginButton", "on");
					this.setSwitchButtonState("registerButton", "off");
					this.getLoginFields(this.form);
					break;
				}
			
			}
		}
		
		public function validatonControl(itemToSet:Sprite):void
		{
			//Keep a track of these items as We will have to make them
			//invisible upon the click of the go button
			this.validatedItems.push(itemToSet);
			
			GeneralAnimations.basic_displayItemToggle(
				itemToSet,
				null,
				1);
			
			//(itemToSet.getChildByName("validNo") as Sprite).alpha=1;
		}
		
		public function clearAllValidatedMarks():void
		{
			for(var i:int=0; i<this.validatedItems.length; i++)
			{
				(this.validatedItems[i] as Sprite).alpha = 0;
			}
		}
		
		public function proceedToRegistration():Boolean
		{
			var registerFormValidated:Boolean = true;
			this.register_pass = false;
			
			if((this.firstName_tf.parent
			.getChildByName("validNo") as Sprite).alpha == 1
			||this.isRegTxtblank(this.firstName_tf))
			{registerFormValidated = false;}
			
			if((this.lastName_tf.parent
			.getChildByName("validNo") as Sprite).alpha == 1
			||this.isRegTxtblank(this.lastName_tf))
			{registerFormValidated = false;}
			
			if((this.companyName_tf.parent
			.getChildByName("validNo") as Sprite).alpha == 1
			||this.isRegTxtblank(this.companyName_tf))
			{registerFormValidated = false;}
			
			if((this.emailAddress_tf.parent
			.getChildByName("validNo") as Sprite).alpha == 1
			||this.isRegTxtblank(this.emailAddress_tf))
			{registerFormValidated = false;}
			
			/*if((this.postCode_tf.parent
			.getChildByName("validNo") as Sprite).alpha == 1
			||this.isRegTxtblank(this.postCode_tf))
			{registerFormValidated = false;}*/
			
			if((this.password1_tf.parent
			.getChildByName("validNo") as Sprite).alpha == 1
			||this.isRegTxtblank(this.password1_tf))
			{registerFormValidated = false;}
			
			if((this.password2_tf.parent
			.getChildByName("validNo") as Sprite).alpha == 1
			||this.isRegTxtblank(this.password2_tf))
			{registerFormValidated = false;}
			
			
			this.register_pass = registerFormValidated;
			
			return this.register_pass;
			
			/*
this.firstName_tf
this.lastName_tf
this.companyName_tf
this.emailAddress_tf
this.postCode_tf
this.password1_tf
this.password2_tf
			*/
		}
		
		private function isRegTxtblank(item:MovieClip):Boolean
		{
			var theReturn:Boolean = false;
			if(Validation.is_text_blank((item
				.getChildByName("field_txtBox") as TextField).text))
			{
				(item.parent
			.getChildByName("validNo") as Sprite).alpha = 1;
				theReturn = true;
			}
			else
			{
				(item.parent
			.getChildByName("validNo") as Sprite).alpha = 0;
			}
			return theReturn;
		}
	}
	
}