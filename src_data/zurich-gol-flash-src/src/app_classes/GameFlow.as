package src.app_classes
{
	
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.display.Stage;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.events.MouseEvent;
    import flash.utils.Timer;
	import flash.external.ExternalInterface;
	
	
	
	import Config;
	import src.app_classes.tools.GameActions;
	import src.app_classes.forms.LoginForm;
	import src.app_classes.tools.PictureLoader;
	import src.app_classes.tools.BackendInterface;
	import src.app_classes.tools.Resources;
	import src.app_classes.screens.WealthCardSelectScreen;
	import src.app_classes.screens.CarSelect;
	import src.app_classes.screens.PlayerSelect;
	import src.app_classes.PlayerUi;
	import src.app_classes.screens.res;
	import src.app_classes.GamePlay;
	import src.app_classes.forms.ContinueCurrentGameQuestion;
	import src.app_classes.forms.LogoutQuestion;
	import PlayerResources;
	import src.app_classes.screens.CardsHolding;
	import src.app_classes.animations.CardAnimation;
	import src.app_classes.forms.LoadingScreen;
	import src.app_classes.forms.PostLoadingScreen;
	import src.app_classes.screens.HowToPlay;
	import src.app_classes.screens.WealthCardSelect;
	import src.app_classes.screens.WealthCardReveal;
	import src.app_classes.actions.FeedActions;
	import GeneralAnimations;
	import GlobalFunctions;
	import src.app_classes.screens.TandC;
	import src.app_classes.screens.SpinToWinBackdrop;
	import src.app_classes.actions.PlayAgain;
	import src.app_classes.screens.Prizes;
	import src.app_classes.tools.BoardRepositioning;
	
	import src.app_classes.screens.GrandTotal;
	
	import src.app_classes.screens.LeaderBoard;
	import src.app_classes.tools.KeyboardControlTool;
	
	
	import src.app_classes.screens.TrashScreen;
	
	
	
	
	
	
	public dynamic class GameFlow extends GameActions
	{
		
		include "actions/FormActions.as";
		include "actions/LeaderBoardActions.as";
		

		private var lf:LoginForm;
		public var tf:TextField;
		public var res:Resources;
		
		private var bi:BackendInterface;
		private var play_again:PlayAgain;
		private var wcSelectScreen:WealthCardSelectScreen;
		
		private var CardsHoldingClass:CardsHolding;
		private var wcSelScreen:WealthCardSelectScreen;
		private var CarSelectScreen:CarSelect;
		private var PlayerSelectScreen:PlayerSelect;
		private var playerUiClass:PlayerUi;
		public var mainGamePlay:GamePlay;
		private var ldnScreen:LoadingScreen;
		private var lboard:LeaderBoard;
		private var htp:HowToPlay;
		private var feedActs:FeedActions;
		
		private var gamePlayAgain:Boolean = false;
		
		
		private var savedGameData:Object;
		
		public var gameRestored:Boolean = false;
		
		
		
		public function GameFlow(layers:appLayers,tf:TextField, 
		plui:PlayerUi, gp:GamePlay, res:Resources, feedAct:FeedActions)
		{
			this.feedActs = feedAct;
			this.mainGamePlay = gp;
			this.playerUiClass = plui;
			this.layers = layers;
			this.tf = tf;
			this.res = res;
			this.play_again = new PlayAgain(this, this.res, this.tf);
			
			//Now make sure that GamePlay has the 
			//current Instance of this GameFlow
			
			
			//Now go ahead, get an instance of the Backend Interface.
			this.bi = new BackendInterface(this.tf, this.finished_with_screen);
			
			this.setInitialState();
		}
		
		private function setInitialState():void
		{
			this.play_again.setNewGameEnvironment();
			this.mainGamePlay.updateLatestGameFlow(this.getInstance());
			this.mainGamePlay.initiateGameVariables();
		}
		
		public function restartGame():void
		{
			this.emptyLayer("forms");
			this.emptyLayer("spinToWin");
			this.gamePlayAgain = true;
			this.setInitialState();
			this.layers.getLayer("mainTrack").x = 0;
			this.layers.getLayer("mainTrack").y = 0;
			this.mainGamePlay.setCarTrackPos(1);
			this.play_again.clearUi();
			this.setReadyToPlayState();
			
			//clear car pegs, add actor peg
			this.removeExistingPegs();
			
			this.mainGamePlay
			.pegs.placeActorPeg(PlayerResources.sex);
			
			this.sendGoolgleAnaylticsInfo("User_played_again");
			
			//this.getCarTrack()._carContainer
		}
		
		private function removeExistingPegs():void
		{
			
			var numChildren:int = 
			this.getCarTrack()._carContainer.numChildren;
			
			var pegsToRemove:Array = new Array();
			
			for(var i:int = 0; i<numChildren; i++)
			{
				var childName:String = 
				this.getCarTrack()._carContainer
				.getChildAt(i).name;
				
				if(childName.indexOf("peg")>-1)
				{
					pegsToRemove.push(childName);
				}
			}
			
			for(var j:int = 0; j<pegsToRemove.length; j++)
			{
				
				
				
				this.getCarTrack()._carContainer
				.removeChild(
				this.getCarTrack()._carContainer
				.getChildByName(pegsToRemove[j])
				);
			}
			
		}
		
		private function getInstance():GameFlow
		{
			return this;
		}
		
		public function getMainXML():XML
		{
			return this.mainGamePlay.positionsXML;
		}
		
		private function afterLoadingScreenResourcesLoadeddd():void
		{
			
			//this.layers.getLayer("blockLayer").addChild(new BlockScreen());
			this.emptyLayer("interactionLayer");
			this.layers.getLayer("interactionLayer").alpha=1;
			this.layers.getLayer("interactionLayer").addChild(this.ldnScreen.getLoadingScreen());
			
		}
		
		public function loadFirstScreen():void
		{
			this.getScreen("POST_LOADING_SCREEN");
		}
		
		public function tandcs_clicked():void
		{
			this.emptyLayer("blockLayer");
			this.layers.getLayer("blockLayer").alpha=1;
		}
		
		public function openTandCs():void
		{
			//this.emptyLayer("forms");
			//this.layers.getLayer("forms").alpha=1;
			
			var tc:TandC = new TandC(this.tandcs_clicked);
			this.layers.getLayer("blockLayer").addChild(tc.getTacs());
		}
		
		private function getFeedData():void
		{
			this.bi.getFeed(this.afterReceivingFeed);
		}
		
		private function afterReceivingFeed(evt:Event):void
		{
			var feedData:Object = JSON.parse(evt.target.data);
			this.feedActs.populateFeed(feedData, this.layers);
			
			if(!this.gameRestored && !this.gamePlayAgain)
			{
				this.mainGamePlay.doAction(0);
			}
			else
			{
				if(this.gamePlayAgain)
				{
					this.mainGamePlay.doAction(0);
				}
				else
				{
					//If the user is logging back in and we are on square 128,
					//we will let the retire screen come back up
					if(this.gameRestored 
					&& PlayerResources.currentSquare == 128)
					{
						this.mainGamePlay.doAction(PlayerResources.currentSquare);
					}
					else
					{
					GeneralAnimations.animatePanelsIn(this.layers, this.panelsInPlace);
					}
				}
			}
		}
		
		private function setReadyToPlayState():void
		{
			
			
			this.getFeedData();
			//Hold off this for now
			//this.mainGamePlay.initiateCarTrackItems();
			
			//this.layers.getLayer("forms").alpha=1;
			//this.playerUiClass.y = 622;
			
		}
		
		public function onLoadingScreenComplete():void
		{
			this.getScreen("POST_LOADING_SCREEN");
		}
		
		public function savedDataReturned(savedData:Object):void
		{
			switch(savedData.savedGame.msg)
			{
				case "NO_SAVED_GAMES":
				{
					break;
				}
				
				case "SUCCESS":
				{
					this.restoreSavedGameVars(savedData.savedGame.data);
					break;
				}
			}
			
		}
		
		public function getScreen(screenName:String, showBackground:Boolean = false):void
		{
			switch(screenName)
			{
				case "LOGIN":
				{
					this.displayLoginForm(showBackground);
					break;
				}
				
				case "POST_LOADING_SCREEN":
				{
					this.sendGoolgleAnaylticsInfo("Game_Loaded");
					this.showPostLoadingScreen();
					
					//import src.app_classes.tools.BoardRepositioning;
					//Set the x and y position of the board. This will tell us
					//where to reposition the board when we are going to spin etc.
					//NOTE: On each move complete, we need to keep each of the
					//square's x and y position of the board as well so that we can
					//navigate back to that position.
					
					
					BoardRepositioning.centeredXpos = 
					this.mainGamePlay.carTrack_actor._board.x;
					
					BoardRepositioning.centeredYpos = 
					this.mainGamePlay.carTrack_actor._board.y;
					
					
					break;
				}
				
				case "PRIZES":
				{
					this.pauseGame();
					this.showPrizesScreen();
					break;
				}
				
				case "PLAYER_SELECT":
				{
					this.mainGamePlay.carTrack_actor.gotoAndStop(1);
					//PlayerResources.currentSquare = 0;
					//PlayerResources.salary = 0;
					this.showPlayerSelectScreen(0);
					break;
				}
				
				case "CAR_PICKER":
				{
					this.showCarPickerScreen(0);
					break;
				}
				
				case "WEALTH_SCREEN":
				{
					this.showWealthCardsScreen();
					break;
				}
				
				case "WEALTH_SCREEN_REVEAL":
				{
					this.showWealthCardsRevealScreen();
					break;
				}
				
				case "REVEAL_CARD_SELECT_1":
				{
					this.showRevealCardsSelectedScreen(1);
					break;
				}
				
				case "SHOW_SAVEDGAME_QUESTION":
				{
					this.showRevealCardsSelectedScreen(1);
					break;
				}
				
				case "SHOW_SPINNER":
				{
					this.mainGamePlay.displaySpinner();
					break;
				}
				
				case "HOW_TO_PLAY":
				{
					this.show_gf_how_to_play();
					break;
				}
			}
		}
		
		
		private function showPostLoadingScreen():void
		{
			/*this.emptyLayer("forms");
			layers.getLayer("forms").alpha=1;
			this.layers.getLayer("forms")
			.addChild(new PostLoadingScreen(this, this.postLoadingScreenLoaded).getPostLoadingScreen());
			*/
			
			layers.getLayer("pagesBackground")
			.addChild(GlobalFunctions.getBackDrop());
			
			(layers.getLayer("pagesLayer")
			.getChildByName("page1") as Sprite)
			.addChild(new PostLoadingScreen(this, this.postLoadingScreenLoaded).getPostLoadingScreen());
		}
		
		private function postLoadingScreenLoaded():void
		{
			this.emptyLayer("forms");
			layers.getLayer("forms").alpha=1;
			this.getScreen("LOGIN");
		}
		
		private function showPlayerSelectScreen(y:int):void
		{
			this.preparePlayerSelectScreen();
			this.prepareCarSelectScreen();
		}
		
		private function showRevealCardsSelectedScreen(phase:int):void
		{
			switch(phase)
			{
				case 1:
				{
					CardAnimation.closeCardSelectionScreen(this.layers, this.showRevealCardsSelectedScreen);
					break;
				}
				
				case 2:
				{
					
					this.emptyLayer("forms");
					
					var cardsRevealScreen:Sprite = this.CardsHoldingClass.getRevealCardsScreen()
					this.layers.getLayer("forms").addChild(cardsRevealScreen);
					//cardsRevealScreen.x = 188;
					//cardsRevealScreen.y = 228;
					
					CardAnimation.openCardSelectionScreen(this.layers);
					
					
					break;
				}
				
				case 3:
				{
					break;
				}
			}
		}
		
		public function showintro_WealthCardsScreen(showBackground:Boolean = true):void
		{
			this.displayPageScreen(
			new WealthCardSelect(this.mainGamePlay, this.wealth_Card_Selected, false)
			.getWealthCardSelectScreen());
		}
		
		public function showWealthCardsScreen(showBackground:Boolean = true):void
		{
			this.emptyLayer("forms");
			this.layers.getLayer("forms").alpha=1;
			this.layers.getLayer("forms")
			.addChild(new WealthCardSelect(this.mainGamePlay, this.wealth_Card_Selected, showBackground)
			.getWealthCardSelectScreen());
		}
		
		public function showWealthCardsRevealScreen(typeOfShowing:String = "default"):void
		{
			this.emptyLayer("forms");
			this.layers.getLayer("forms").alpha=1;
			this.layers.getLayer("forms")
			.addChild(new WealthCardReveal(this).getWealthCardRevealScreen(typeOfShowing));
		}
		
		public function showGrandTotal():void
		{

			this.emptyLayer("forms");
			this.layers.getLayer("forms").alpha=1;
			var gt:GrandTotal = new GrandTotal(this);
			this.layers.getLayer("forms").addChild(gt.getScreen());
		}
		
		public function wealth_Card_Selected(cardsSelected:Array):void
		{
			//save the wealthCards
			for(var i:int = 0; i<cardsSelected.length; i++)
			{
				PlayerResources.player_wealthCards
				.push(cardsSelected[i]);
			}
			
			//After saving the cards selected by the user, we will
			//now get the cards reveal screen.
			
			this.cardsRevealOkClicked();
			
		}
		
		public function ingame_wealth_Card_Selected(cardsSelected:Array):void
		{
			//save the wealthCards
			for(var i:int = 0; i<cardsSelected.length; i++)
			{
				PlayerResources.player_wealthCards
				.push(cardsSelected[i]);
			}
			
			//After saving the cards selected by the user, we will
			//now get the cards reveal screen.
			
			this.ingame_cardsRevealOkClicked();
			
		}
		
		public function cardsRevealOkClicked():void
		{
			//Now that we have seen our cards selected, we can now
			//clear the screen and let users play the game
			//this.emptyLayer("forms");
			//this.layers.getLayer("forms").alpha=1;
			//displayPageScreen
			
			//this.emptyLayer("pagesBackground");
					
			GlobalFunctions.deleteBothPages
			(this.layers.getLayer("pagesLayer") as Sprite);
			
			
			if(PlayerResources.currentSquare == 0)
			{this.setReadyToPlayState();}
			else
			{
				//Show the spinner now
				this.getScreen("SHOW_SPINNER");
			}
		}
		
		public function ingame_cardsRevealOkClicked():void
		{
			//Now that we have seen our cards selected, we can now
			//clear the screen and let users play the game
			this.emptyLayer("forms");
			this.layers.getLayer("forms").alpha=1;
			//displayPageScreen
			
			if(PlayerResources.currentSquare == 0)
			{this.setReadyToPlayState();}
			else
			{
				//Show the spinner now
				this.getScreen("SHOW_SPINNER");
			}
		}
		
		public function panelsInPlace():void
		{
			//Remove tab on states
			this.setAppropriateTab("", "off");
			
			
			//Get the title bar..
			var titleBar:Sprite = (this.layers.getLayer("titleLayer")
			.getChildByName("theTitle") as Sprite)
			.getChildByName("titleBar") as Sprite;
			
			//Animate the title bar into place.
			GeneralAnimations.positionTo
			(titleBar, this.letGameStartHere, 0, 0);
			
		}
		
		public function removeTitleBar(functionAfter:Function = null):void
		{
			//Get the title bar..
			var titleBar:Sprite = (this.layers.getLayer("titleLayer")
			.getChildByName("theTitle") as Sprite)
			.getChildByName("titleBar") as Sprite;
			
			var Ypos:Number = titleBar.height*-1
			
			//Animate the title bar into place.
			GeneralAnimations.positionTo
			(titleBar, functionAfter, 0, Ypos);
			
		}
		
		private function letGameStartHere():void
		{
			
			if(!this.gameRestored || this.gamePlayAgain)
			{
				this.gamePlayAgain = false;
				//this.mainGamePlay.doAction(0);
				this.mainGamePlay.performActionsAfterSelectingCards();
			}
			else
			{
				this.getScreen("SHOW_SPINNER");
			}
			
		}
		
		private function prepareCarSelectScreen():void
		{
			this.CarSelectScreen = new CarSelect(this.tf);
			this.CarSelectScreen.loadCarsImages();
		}
		
		public function saveGameState(gameCompleted:int = 0):void
		{
			//If the user is submitting a completed game we will
			//send Final_score_submitted event to google analytics. 
			if(gameCompleted==1)
			{
				this.sendGoolgleAnaylticsInfo("Final_score_submitted");
			}
			
			//In any event we will send a Game_saved event to google analytics.
			this.sendGoolgleAnaylticsInfo("Game_saved");
		
			//if(PlayerResources.currentSquare==128){gameCompleted = 1;}
			/////////////About to save game/////////////////
			//Attempt here to collect data for items we need
			//to save for this game.
			
			var dataToSave:Object = new Object();
			dataToSave.uid = PlayerResources.player_uid;
			dataToSave.car_square_postion = PlayerResources.currentSquare;
			dataToSave.cash_in_hand = PlayerResources.money;
			dataToSave.life_cards = PlayerResources.lifeCards;
			dataToSave.car_color = PlayerResources.player_car.toUpperCase();
			dataToSave.avatar = PlayerResources.player_avatar.substr(-1);
			dataToSave.pegs = PlayerResources.playerPegs;
			dataToSave.wheel_last_spin = "";
			dataToSave.wheel_last_rotation = "";
			dataToSave.needle_last_rotation = "";
			dataToSave.game_completed = gameCompleted;
			
			dataToSave.sex = PlayerResources.sex;
			//dataToSave.wealth_cards = PlayerResources.wealth_cards;
			dataToSave.salary = PlayerResources.salary;
			dataToSave.maximum_salary = PlayerResources.maximum_salary;
			dataToSave.salary_taxes = PlayerResources.salary_taxes;
			dataToSave.house_buying_price = PlayerResources.house_buying_price;
			dataToSave.house_selling_price = PlayerResources.house_selling_price;
			dataToSave.house_insurance = PlayerResources.house_insurance;
			dataToSave.lifePath = PlayerResources.lifePath;
			dataToSave.numOfChildren = PlayerResources.numOfChildren;
			dataToSave.job = PlayerResources.job;
			
			//Get the wealth cards
			var wealthcardsString:String = "";
			for(var i:int = 0; 
			i<PlayerResources.player_wealthCards.length; 
			i++)
			{
				wealthcardsString+=PlayerResources.player_wealthCards[i];
				if(i<(PlayerResources.player_wealthCards.length-1))
				{wealthcardsString+=",";}
			}
			
			dataToSave.wealth_cards = wealthcardsString;
			
			
			this.bi.saveGameState(dataToSave, gameCompleted);
			
		}
		
		public function leaderboardClicked():void
		{
		
			//Pause the game.
			this.pauseGame();
			
			this.lboard = new LeaderBoard(this);
			//this.layers.getLayer("blockLayer").addChild(new TrashScreen())
		}
		
		public function leaderboardUponGameEnd():void
		{
			this.layers.getLayer("forms").alpha=0;
			GlobalFunctions.emptyContainer(this.layers.getLayer("forms"));
			this.layers.getLayer("forms").alpha=1;
			
			//save game here
			PlayerResources.gamesavedAtEnd = true;
			this.saveGameState(1)
			
			//this.lboard = new LeaderBoard(this);
			//this.layers.getLayer("blockLayer").addChild(new TrashScreen())
		}
		
		public function logInClicked():void
		{
			this.getScreen("LOGIN");
		}
		
		public function logOutClicked():void
		{
			//CardAnimation.closeCardSelectionScreen(this.layers, this.showLogoutQuestion);
			this.showLogoutQuestion();
		}
		
		public function showLogoutQuestion():void
		{
			this.pauseGame();
			this.emptyLayer("blockLayer");
			this.layers.getLayer("blockLayer").alpha=1;
			var buttonNames:Object = {button1:"No, cancel", button2:"Yes and save"};
			this.layers.getLayer("blockLayer").addChild(new LogoutQuestion(this).getLogoutScreen(this.logoutDecisionMade, buttonNames, true));
		}
		
		public function logoutDecisionMade(logoutAnswer:String):void
		{
		
			this.setAppropriateTab("", "off");
		
			switch(logoutAnswer)
			{
				case "no":
				{
					//In case the user wants to change his mind, he
					//can go back to his game here.
					this.emptyLayer("blockLayer");
					this.resumeGame();
					break;
				}
				
				case "yes":
				{
					this.emptyLayer("forms");
					this.emptyLayer("blockLayer");
					
					this.saveGameState();
					
					this.removeTitleBar();
					
					this.getScreen("LOGIN", true);
					
					//this.ldnScreen = new LoadingScreen();
                    //this.ldnScreen.loadScreenImages(this.afterLoadingScreenResourcesLoaded);
					break;
				}
			}
			
			
			
			/*this.emptyLayer("forms");
			this.layers.getLayer("forms").alpha=1;
			this.layers.getLayer("forms").addChild(new LoadingScreen());*/
			
		}
		
		private function afterLoadingScreenResourcesLoaded():void
		{
			
			//ldnScreen
			this.emptyLayer("forms");
			this.layers.getLayer("forms").alpha=1;
			this.layers.getLayer("forms").addChild(this.ldnScreen.getLoadingScreen());
			
			this.saveGameState();
			
		}
		
		
		
		public function finished_with_screen(backendActivity:String, backEndMsg:Object):void
		{
			
			switch(backendActivity)
			{
				case "login":
				{
					this.dealWithLoginAction(backEndMsg);
					break;
				}
				
				case "saved_on_end":
				{
					//because this is saved at the end of the game, we
					//will load the leaderboard now.
					this.lboard = new LeaderBoard(this);
					break;
				}
				
				case "game_saved_at_end":
				{
					//because this is saved at the end of the game, we
					//will load the leaderboard now.
					this.lboard = new LeaderBoard(this);
					break;
				}
				
				default:
				{
					
				}
			}
		}
		
		private function loadWealthCardBack():void
		{
			var pl:PictureLoader = new PictureLoader(this.wealthCardLoaded);
			var img:String = "/resources/images/wealth_cards/wealth_card_back.jpg";
			pl.load_image("wealth_card",img,0);
		}
		
		private function wealthCardLoaded(item:PictureLoader,gg:Object):void
		{
			
			this.wcSelScreen =  new WealthCardSelectScreen(item.content,this.wealthCardSelected,this.tf);
			
			var wcSelCardBox:Sprite = this.wcSelScreen.getWealthCardScreen();
			this.layers.getLayer("forms").addChild(wcSelCardBox);
			wcSelCardBox.x=(980-(wcSelCardBox.width))/2;
			wcSelCardBox.y=75;
			
			//Now load the graphics needed for the next screen
			this.CardsHoldingClass = new CardsHolding(this.tf, this.wcSelScreen.getCardPositionIdentityArray(), this.cardScreenOkClicked);
			this.CardsHoldingClass.loadCardGraphics();
			
		}
		
		private function wealthCardSelected(evt:MouseEvent):void
		{
			
			if(this.CardsHoldingClass.numCardsSelected()<3)
			{
			
			var cardIdentityValue:Array = this.wcSelScreen.getCardPositionIdentityArray();
			
			//Get the card which was clicked on screen
			var whichOnscreenCardClicked:int = parseInt(evt.target.name.substring(21));
			
			//Each card has a random number. We can extract which number is on each card
			//here. Here is the number extracted when card is clicked
			var clickedCardValue:int = cardIdentityValue[whichOnscreenCardClicked];
			
			
			if(!this.CardsHoldingClass.isCardSelected(whichOnscreenCardClicked))
			{
				this.CardsHoldingClass.setCardSelected
				(
				whichOnscreenCardClicked,//card selected on screen
				clickedCardValue//value of card selected on screen
				);
				
				CardAnimation.cardSelectedAnimation(evt.target,"SELECTED");
				
				//Check to move on to new Screen
				if(this.CardsHoldingClass.numCardsSelected()==Config.NUM_WEALTH_CARDS_TO_SELECT)
				{
					this.showRevealCardsSelectedScreen(1);
					//keep this in the player resources for future use.
					PlayerResources.player_wealth_cards = this.CardsHoldingClass.getCardsSelected();
				}
			}
			else
			{
				this.CardsHoldingClass.removeCardSelected(whichOnscreenCardClicked);
				
				CardAnimation.cardSelectedAnimation(evt.target,"UNSELECTED");
			}
			
			
			
			}
			
			
		}
		/*
		private function emptyLayer(layername:String):void
		{
			while (layers.getLayer(layername).numChildren > 0) 
			{
				layers.getLayer(layername).removeChildAt(0);
			}
		}
		*/
		private function cardScreenOkClicked():void
		{
			CardAnimation.closeCardSelectionScreen(this.layers, this.tempFunction);
		}
		
		private function tempFunction(phase:int):void
		{
				
			this.emptyLayer("forms");
			layers.getLayer("forms").alpha = 1;
			this.emptyLayer("blockLayer");
		}
		
		private function showCarPickerScreen(phase:int):void
		{
				this.displayPageScreen
				(this.CarSelectScreen.getCarPickerScreen(this.carSelectedOnClick));
		}
		
		private function carSelectedOnClick():void
		{
		
			this.mainGamePlay.setPegs();
			this.mainGamePlay.setDefaultCar();
			//this.getScreen("WEALTH_SCREEN");
			
			this.showintro_WealthCardsScreen();
		}
		
		private function preparePlayerSelectScreen():void
		{
			this.PlayerSelectScreen = new PlayerSelect(tf, this.playerImagesLoadCompleted, this);
			this.PlayerSelectScreen.loadAvatarImages();
		}
		
		private function playerImagesLoadCompleted():void
		{
			this.displayPageScreen(this.PlayerSelectScreen.getSelectPlayerScreen());
		}
		
		public function playerIconSelected(evt:MouseEvent):void
		{
			this.changePlayerAvatar(evt.target.name);
			this.getScreen("CAR_PICKER");
		}
		
		private function changePlayerAvatar(avatarNameMain:String):void
		{
			PlayerResources.player_avatar = avatarNameMain;
			this.setPlayerSex();
					
			
			for(var i:int = 0; i<this.playerUiClass.UiResources.length; i++)
			{
				
			
			
				if(this.playerUiClass.UiResources[i].imgName=="med_"+PlayerResources.player_avatar)
				{
					var r:Bitmap = this.playerUiClass.UiResources[i].imgObj.content as Bitmap;
					this.playerUiClass.avatarSprite.removeChildAt(0);
					this.playerUiClass.avatarSprite.addChild(r);
				}
			}
		}
		
		private function setPlayerSex():void
		{
			var avatarNumber:int = 
			GlobalFunctions.changeAvatarNameToInt(PlayerResources.player_avatar);
			
			var sex:String = "m";
			
			switch(avatarNumber)
			{
				case 2:
				case 5:
				case 6:
				{ sex = "m" ;break;}
				
				case 1:
				case 3:
				case 4:
				{ sex = "f" ;break;}
				
			}
			
			PlayerResources.sex = sex;
		}
		
		public function sendFeedToDB(feedText:String):void
		{
			
			var avatarNumber:int = 
			GlobalFunctions.changeAvatarNameToInt(PlayerResources.player_avatar);
			
			var ft:Object = 
			{
			txt:feedText,
			uid:PlayerResources.player_uid,
			avatar:avatarNumber
			};
			
			this.bi.postFeed(this.afterPostingFeed, ft);
		}
		
		private function afterPostingFeed(evt:Event):void
		{
			//this.tf.text = evt.target.data;
		}
		
		public function updateUiText(majorUi:String, minorUi:String):void
		{
			
			this.res.uiMajorText.text = majorUi;
			this.res.uiMinorText.htmlText = minorUi;
			
		}
		
		public function updateUiMajorText(majorUi:String):void
		{
			this.res.uiMajorText.text = majorUi;
		}
		
		public function updateUiMinorText(minorUi:String):void
		{
			this.res.uiMinorText.htmlText = minorUi;
		}
		
		public function updateUiMoneyText(newMoney:Number):void
		{
			
			this.res.uiMoniesText.htmlText = 
			GlobalFunctions.formatMoneyToString(newMoney);
			
		}
		
		public function setAppropriateTab(tabName:String, state:String):void
		{
		
			if(state=="on")
			{
				
				switch(tabName)
				{
					case "prizes":{ this.res.prizesOnState.alpha = 1; break;}
					case "how_to_play":{ this.res.htpOnState.alpha = 1; break;}
					case "leaderboard":{ this.res.leadOnState.alpha = 1; break;}
					case "login":{ this.res.loginOnState.alpha = 1; break;}
				}
				
			}
			else
			{
				this.res.prizesOnState.alpha = 0;
				this.res.htpOnState.alpha = 0;
				this.res.leadOnState.alpha = 0;
				this.res.loginOnState.alpha = 0;
			}
			
		}
		
		public function getSpinner():MovieClip
		{
			return this.mainGamePlay.spinner_actor;
			//public var carTrack_actor
		}
		
		public function getCarTrack():MovieClip
		{
			return this.mainGamePlay.carTrack_actor;
		}
		
		public function getSpinnerTimer():Timer
		{
			return this.mainGamePlay.spinnerTimer;
		}
		
		public function getMainTimer():Timer
		{
			return this.mainGamePlay.mainTimer;
		}
		
		public function pauseGame():void
		{
			this.play_again.pauseGame();
		}
		
		public function resumeGame():void
		{
			this.play_again.resumeGame();
		}
		
		public function emptyLayerHelper(layerName:String):void
		{
			this.emptyLayer(layerName);
		}
		
		public function sendGoolgleAnaylticsInfo(analyticEventType:String):void
		{
		
			var eventString:String = "";
			var moreInfo:String = "";
			
			//eventString, moreInfo
			switch(analyticEventType)
			{
				case "Registered_User":
				{
					eventString = 
					PlayerResources.player_name+" "+PlayerResources.player_last_name;
					
					moreInfo = "Company - "+PlayerResources.player_company;
					break;
				}
				
				case "Completed_Game":
				{
					eventString = 
					"Square - "+PlayerResources.currentSquare+" "+PlayerResources.player_last_name;
					
					moreInfo = "Completed by - "+PlayerResources.player_name+
					" "+PlayerResources.player_last_name+" Score - "+PlayerResources.money;
					break;
				}
				
				case "Un_Completed_Game":
				{
					break;
				}
				
				case "Game_Loaded":
				{
					eventString = "Game loaded";
					moreInfo = "Someone has loaded our game.";
					break;
				}
				
				case "User_logged_in":
				{
					eventString = "logged in "+PlayerResources.player_name+" "+PlayerResources.player_last_name;
					
					moreInfo = "User "+PlayerResources.player_name+" "+PlayerResources.player_last_name+
								" has logged in";
					
					break;
				}
				
				case "Game_saved":
				{
					eventString = 
					"Score - "+PlayerResources.money;
					
					moreInfo = "Submitted by - "+PlayerResources.player_name+" "+PlayerResources.player_last_name;
					break;
				}
				
				case "Final_score_submitted":
				{
					eventString = 
					"Score - "+PlayerResources.money;
					
					moreInfo = "Submitted by - "+PlayerResources.player_name+" "+PlayerResources.player_last_name;
					break;
				}
				
				case "User_played_again":
				{
					eventString = 
					"Playing again";
					
					moreInfo = "Played by - "+PlayerResources.player_name+" "+PlayerResources.player_last_name;
					break;
				}
				
				case "User_clicked_advertisement":
				{
					eventString = 
					"Adertisement Clicked";
					
					moreInfo = "Advert - "+this.getMainXML().board_places
					.place[PlayerResources.currentSquare]
					.action.advert.advert_title_copy+
					"contact made by - "+PlayerResources.player_name+" "+PlayerResources.player_last_name;
					break;
				}
				
				case "Mansion_Retirement":
				{
					
					eventString = 
					"Mansion retirement chosen";
					
					moreInfo = "risky retirement choice made by - "+PlayerResources.player_name+" "+PlayerResources.player_last_name;
					
					break;
				}
				
				case "Country_Retirement":
				{
					
					eventString = 
					"Country estate retirement chosen";
					
					moreInfo = "safe retirement choice made by - "+PlayerResources.player_name+" "+PlayerResources.player_last_name;
					
					break;
				}
			}
			
			if (ExternalInterface.available)
			{
				ExternalInterface.call
				("sendTracking", analyticEventType, 
				eventString, moreInfo);
			}
			
			
		}
		
		
		
	}
}


	
/*
package src.app_classes
{	
	public class GameFlow.prototype.oneMore
	{
		public function oneMore()
		{
		
		}
		
		public function sugar():void
		{
		
		}
	}
}
*/