package src.app_classes.config
{
	
	import flash.filters.DropShadowFilter;
	import flash.filters.BitmapFilterQuality;
	
	public class DropShadows
	{
		
		private var color:Number = 0xFFFFFF;
        private var angle:Number = 60;
        private var alpha:Number = 0.8;
        private var blurX:Number = 2;
        private var blurY:Number = 2;
        private var distance:Number = 2;
        private var strength:Number = 0.65;
        private var inner:Boolean = false;
        private var knockout:Boolean = false;
        private var quality:Number = BitmapFilterQuality.HIGH;
		
		public function getDropShadow():DropShadowFilter
		{ 
			return new DropShadowFilter(this.distance,
                                        this.angle,
                                        this.color,
                                        this.alpha,
                                        this.blurX,
                                        this.blurY,
                                        this.strength,
                                        this.quality,
                                        this.inner,
                                        this.knockout);
		}
		
		public function getScreenTitleShadow():DropShadowFilter
		{ 
			//Make configurations to our drop shadow here
			return this.getDropShadow();
		}
		
		public function getWealthCardShadow():DropShadowFilter
		{ 
			this.color= 0x000000;
			this.angle = 90;
	        this.alpha= 0.8;
	        this.blurX= 5;
	        this.blurY= 5;
	        this.strength= 3;
	        this.inner = false;
	        this.knockout = true;
			//Make configurations to our drop shadow here
			return this.getDropShadow();
		}
		
		
    	
    	
	}
}