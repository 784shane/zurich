package src.app_classes.config
{
	
	public class TACText extends Array
	{
		
		public function getTACText():Array
		{
			var tacTextArray:Array = new Array();
			this.buildArray(tacTextArray);
			return tacTextArray;
		}
		
		private function buildArray(arr:Array):void
		{
			var contents:Array = new Array
			(
			["The Game of Life FT Adviser online game terms and conditions",
			"1.",
			"By entering this competition you agree to these terms and conditions; and to the use of your name in conjunction with the competition and publication of any list of names."],
			
			["",
			"2.",
			"The online UK adviser version of The Game of Life will run from 5 November 2013 to 6 December 2013 on the Financial Times Adviser online website."],
			
			["",
			"3.",
			"The game will be open to UK based financial advisers who register to play via the Financial Times Adviser online website."],
			
			["",
			"4.",
			"No employees, agents or suppliers of the Promoter, its retailer network and associated companies or their families will be eligible to take part in the competition."],
			
			["",
			"5.",
			"The Game of Life is free to enter - no purchase necessary."],
			
			["",
			"6.",
			"Entrants must be aged over 18."],
			
			["",
			"7.",
			"Only entries submitted using the functionality provided and as described will be considered."],
			
			["",
			"8.",
			"The winner of the individual prize will be the adviser with the highest score recorded on the individual players’ leader board, published on The Financial Times Adviser online website when the gameplay closes on Friday 6 December 2013 at 5.00 pm."],
			
			["",
			"9.",
			"The second placed individual winner will be the adviser with the second highest score recorded on the individual players’ leader board, published on The Financial Times Adviser online website when the gameplay closes on Friday 6 December 2013 at 5.00 pm.  "],
			
			["",
			"10.",
			"The winner of the team prize will be the registered adviser business entered into the ‘team’ gameplay category for The Game of Life, with the highest daily score recorded on the team leader board published on The Financial Times Adviser online website, when the gameplay closes on Friday 6 December 2013 at 5.00 pm."],
			
			["",
			"11.",
			"The winner of the individual prize will receive an iPad which is non-transferable."],
			
			["",
			"12.",
			"The second placed individual winner will receive an iPad mini which is non-transferable."],
			
			["",
			"13.",
			"The winner of the team prize will receive Red Letter Day vouchers to the value of £100 per person registered as a team player for the registered adviser business. This is up to a maximum value of £2,500 (25 people per adviser business team)."],
			
			["",
			"14.",
			"In the event of a draw, a prize will be awarded to the two first placed winners."],
			
			["",
			"15.",
			"The winners will be asked to supply their FCA registered number by way of proof that they are eligible to claim their prize."],
			
			["",
			"16.",
			"Zurich will not be liable for any events or circumstances beyond its control that make it impossible for the prize to be awarded to, or taken by the winner."],
			
			["",
			"17.",
			"In the event that any winner is unable to take up the prize for any reason they must notify the Promoter immediately. In this circumstance, the Promoter reserves the right to offer the prize to another entrant."],
			
			["",
			"18.",
			"If the Promoter subsequently discovers the winner is illegible, or foul play is suspected under these terms and conditions then the Promoter reserves the right to not award the prize and another participant will be selected at random as the winner."],
			
			["",
			"19.",
			"Winners accept that the prize could be cancelled or suspended for any reason whatsoever beyond the control of the Promoter."],
			
			["",
			"20.",
			"All entrants may be contacted by their/a Zurich consultant to discuss Zurich propositions."],
			
			["",
			"21.",
			"Zurich Intermediary Group Limited will attempt to contact the winner by telephone or post by 13 December 2013."],
			
			["",
			"22.",
			"Zurich Intermediary Group Limited reserves the right to choose an alternative winner if contact cannot be made with the original winner by 13 December 2013."],
			
			["",
			"23.",
			"All dates are non-negotiable."],
			
			["",
			"24.",
			"No cash alternatives will be offered for the prize."],
			
			["",
			"25.",
			"The winners must be prepared to appear in post promotion publicity."],
			
			["",
			"26.",
			"Details of the winning entry can be obtained from Anita Quinton,  Marketing Account Manager on 0500 546 546 from 18 December 2013"],
			
			["",
			"27.",
			"Zurich takes no responsibility for lost, damaged or delayed entries. Only entries that have been completed correctly will be considered as a valid entry."],
			
			["",
			"28.",
			"If for any reason a technical interruption, fault or site failure occurs, the Promoter does not take any responsibility for incomplete entries and any incomplete entries will not be considered valid."],
			
			["",
			"29.",
			"In event of any dispute, Zurich’s decision is final, and no correspondence will be entered into."],
			
			["",
			"30.",
			"Promoter is Zurich Intermediary Group Limited. The Grange, Bishops Cleeve, Cheltenham, GL52 8XX."],
			
			["",
			"31.",
			"Zurich reserves the right to contact all entrants regarding various product and service promotions or other related communications. Should entrants wish not to be contacted, they should write to the Zurich at the address as per Section 30. Entrants will have the opportunity to opt-out of future correspondence in any subsequent communications."],
			
			["",
			"32.",
			"These terms and conditions shall be governed by and construed in accordance with English law. The parties irrevocably agree that the courts of England and Wales shall have exclusive jurisdiction to deal with any dispute or claim that arises out of or in connection with these terms and conditions and the competition."]
			);
			
			
			for(var i:int = 0; i<contents.length; i++)
			{
				arr.push(contents[i]);
			}
		}
		
    	public function TACText()
		{
			
		}
    	
	}
}