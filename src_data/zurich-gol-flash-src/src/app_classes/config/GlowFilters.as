package src.app_classes.config
{
	
	import flash.filters.GlowFilter;
	import flash.filters.BitmapFilterQuality;
	
	public class GlowFilters
	{
		
		private var color:Number = 0x000000;
        private var alpha:Number = 1;
        private var blurX:Number = 10;
        private var blurY:Number = 10;
        private var strength:Number = 5;
        private var inner:Boolean = false;
        private var knockout:Boolean = false;
        private var quality:Number = BitmapFilterQuality.HIGH;
		
		private function getGlowFilter():GlowFilter
		{ 
			return new GlowFilter(
                                        this.color,
                                        this.alpha,
                                        this.blurX,
                                        this.blurY,
                                        this.strength,
                                        this.quality,
                                        this.inner,
                                        this.knockout);
		}
		
		public function getWealthCardGlow():GlowFilter
		{ 
			this.color= 0xFFFFFF;
	        this.alpha= 0.8;
	        this.strength=3;
	        this.knockout = true;
	        
			return this.getGlowFilter();
		}
		
		
    	
    	
	}
}