package src.app_classes
{
	import Config;
	import PlayerResources;
	import GlobalFunctions;
	
	
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.KeyboardEvent;
	import flash.events.Event;
	import flash.text.TextField;

    import flash.utils.Timer;
    import flash.events.TimerEvent;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.Back;

	
	
	import src.app_classes.actions.FeedActions;
	import src.app_classes.GameFlow;
	import src.app_classes.tools.GameActions;
	import src.app_classes.appLayers;
	import src.app_classes.tools.Resources;
	import src.app_classes.screens.Screens;
	import src.app_classes.tools.Pegs;
	import src.app_classes.animations.CardAnimation;
	import src.app_classes.AdvertControl;
	import src.app_classes.animations.MoneyAnimationCtrl;
	import GeneralAnimations;
	import src.app_classes.tools.BoardRepositioning;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class GamePlay extends GameActions
	{
		
		public var gf:GameFlow;
		private var moneyAnimationClass:MoneyAnimationCtrl;
		public var tf:TextField;
		public var positionsXML:XML;
		
		public var typeOfSpin:String = "regular";
		public var spinToWin_result:int;
		
		public var spinner_actor:MovieClip;
		public var carTrack_actor:MovieClip;
		private var numToMoveto:Number = 1;
		private var amtSpun:int = 1;
		
		private var frameToStopAt:int;
		private var alternativeStopAt:int = 0;
		//Dont change this from frame 1
		private var lastPlace:int = 0;
		
		private var nextPlace:int = 0;
		private var alternativeNextPlace:int = 0;
		
		//array to hold temporary stops
		public var game_pitstops:Array = new Array();
		private var passoverActionList:Array = new Array();
		
		//Advertisement
		private var adCtrl:AdvertControl;
		private var showAdvertisement:Boolean = false;
		
		private var screens:Screens;
		
		private var Res:Resources;
		private var lyrs:appLayers;
		public var pegs:Pegs;
		private var feedActs:FeedActions;
		
		
		
		private var delay:uint = 2000;
        private var repeat:uint = 1;
        public var mainTimer:Timer = new Timer(delay, repeat);
        public var spinnerTimer:Timer = new Timer(2500, 1);
		
		private var lastMousePosX:Number = 0;
		private var lastMousePosY:Number = 0;
		//private var lastMousePosY:Number = 0;
		
		
		
		//If chosenPath is 1 then keep on the regular path. As soon
		//as we have evaluated the chosenPath, remember to reset it back
		//to 1 incase we had set it to 2.
		public var chosenPath:int = 1;
		public var cancelSpin:Boolean = false;
		
		//When a popup is closed, we may have to perform extra actions before the advertisement.
		//If this was set upon popup close, we will test to see if this is true
		//And if so we will perform the action as stated in this.extraActionToPerform
		public var extraActionToPerform:String = "";
		public var performExtraActions:Boolean = false;
		public var lastExtraActionSquare:int = 10000;
		
		private var routeType:String = "regular";
		private var mergeRouteFrame:int = 0;
		private var mergeNormalFrame:int = 0;
		private var mergeRouteEnd:Boolean = false;
		private var alternativeRoutesToCheck:Array = new Array();
		
		
		
		
		private var rotationAdjusted:Boolean = false;
		private var spinnerActive:Boolean = false;
		private var count:int = 0;
		private var maxSpeed:int = 0;
		private var prevMaxSpeed:int = 0;
		private var rotationSpan:int = 0;
		private var lastDir:String = "right";
		private var theCount:int = 1;
		private var maxFrames:int = 50;
		private var regularSpan:int = 1;
		private var frameCount:int = 0;
		private var excessCount:int = 0;
		private var excessCountLimit:int = 50;
		private var lastRandomRotation:Number = 0;
		private var btw:MovieClip = new MovieClip();
		
		
		
        
		include "actions/AdvertisementActions.as";
		include "actions/SpintoWinActions.as";
		include "actions/WealthCardActions.as";
		include "actions/RouteActions.as";
		
		
		public function GamePlay(lyrs:appLayers, theRes:Resources, 
		feedAct:FeedActions, mainXML:XML, tf:TextField)
		{
			//TweenPlugin.activate([BlurFilterPlugin]);
			
			this.positionsXML = mainXML;
			this.pegs = new Pegs();
			this.feedActs = feedAct;
			this.feedActs.updateToGamePlayInstance(this);
			this.lyrs = lyrs;
			this.Res = theRes;
			this.tf = tf;
			this.adCtrl = new AdvertControl(this, this.lyrs, this.positionsXML);
			this.screens = new Screens(this, this.lyrs, this.Res);
			
		}
		
		public function initiateGameVariables():void
		{
				this.alternativeStopAt = 0;
				this.lastPlace = 0;
				this.nextPlace = 0;
		}
		
		public function updateLatestGameFlow(gf:GameFlow):void
		{
			this.gf = gf;
		}
		
		public function setActors(actor:MovieClip,stageName:String):void
		{
			switch(stageName)
			{
				case "carTrack":
				{
					//log the current spot on the board here.
					//this is kept in the PlayerResources class
					//When we want to know where the user is on the board,
					//all we have to do is call PlayerResources.currentSquare
					PlayerResources.currentSquare = this.lastPlace;
					
					//seeing that carTrack is ready, we initiate it here
					this.carTrack_actor = actor;
					
					this.initiateCarTrackItems();
					
					
					
					break;
				}
				
				case "spinner":
				{
					this.spinner_actor = actor;
					this.initiate_Spinner();
					break;
				}
				
				default:{}
			}
		}
		
		public function setCarTrackPos(pos:int):void
		{
			this.carTrack_actor.gotoAndStop(pos);
		}
		
		public function initiateCarTrackItems():void
		{
			
			//Define and Assign the MoneyAnimationCtrl here.
			//set Money Grow and Lose Actors
			
			this.moneyAnimationClass = 
			new MoneyAnimationCtrl(this.carTrack_actor.moneyGrow as MovieClip,
						this.carTrack_actor._MoneyLoseContainer as MovieClip);
						
			
			//Place
			this.pegs.setCar(this.carTrack_actor._carContainer);
			//this.pegs.placeActorPeg(PlayerResources.sex);
			//this.setDefaultCar();
		}
		
		public function setPegs():void
		{
			this.pegs.setCar(this.carTrack_actor._carContainer);
			this.pegs.placeActorPeg(PlayerResources.sex);
		}
		
		public function restorePegs(pegsData:String):void
		{
			
			var tempArray:Array=pegsData.split("|");
			
			var pegCount:int = 0;
			for(var i:int=0; i<tempArray.length; i++)
			{
				var str:String = tempArray[i];
				var str1:Array=str.split(",");
				
				var theSex:String;
			    var theX:int;
			    var theY:int;
			    
				for(var j:int=0; j<str1.length; j++)
				{
					
					if(j==0){ theSex = str1[j];}
					if(j==1){ theX = parseInt(str1[j]);}
					if(j==2){ theY = parseInt(str1[j]);}
					
				}
				
				if(theSex!="")
				{
					this.pegs.restoreAllPegs(theSex, pegCount);
					pegCount++;
				}	
			}
			
			
			/*
			var count:int = 0;
			var countArr:int = tempArray.length;
			for each(var email:String in tempArray) 
			{
			    
			    var theSex:String;
			    var theX:int;
			    var theY:int;
			    
			    var pegSecCount:int = 0;
			    var pegAttr:Array = email.split(",");
			    for each(var attr:String in pegAttr) 
				{
					if(pegSecCount==0){ theSex = attr;}
					if(pegSecCount==1){ theX = parseInt(attr);}
					if(pegSecCount==2){ theY = parseInt(attr); pegSecCount=0;}
					pegSecCount++;
				}
				
				this.pegs.restoreSinglePeg(theSex, theX, theY);
			    
			    if(count==(countArr-2)){break;}
			    count++;
			}
			
			
			this.pegs.resetSavedPegs(pegsData);
			*/
		}
		
		private function wax(evt:MouseEvent):void
		{
			
		}
		
		private function wwade(evt:Event):void
		{
			this.carTrack_actor._carContainer.alpha = 0.4;
		}
		
		public function setDefaultCar():void
		{
			var newCar:MovieClip;
			
			//We will remove any car that is present in the _car on stage
			//before we add another MovieClip to it
			for(var i:int = 0; i<this.carTrack_actor._carContainer._car.numChildren; i++)
			{
				this.carTrack_actor._carContainer._car.removeChildAt(i);
			}
			
			
			
			switch(PlayerResources.player_car)
			{
				case "YELLOW":
				{
					newCar = new CAR_YELLOW() as MovieClip;
					break;
				}
				
				case "BLUE":
				{
					newCar = new CAR_BLUE() as MovieClip;
					break;
				}
				
				case "GREEN":
				{
					newCar = new CAR_GREEN() as MovieClip;
					break;
				}
				
				case "RED":
				{
					newCar = new CAR_RED() as MovieClip;
					break;
				}
				
				case "ORANGE":
				{
					newCar = new CAR_ORANGE() as MovieClip;
					break;
				}
				
				case "PURPLE":
				{
					newCar = new CAR_PURPLE() as MovieClip;
					break;
				}
				
				default:
				{
					newCar = new CAR_ORANGE() as MovieClip;
				}
			}
			
			//Now add our new ColoredCar
			this.carTrack_actor._carContainer._car.addChild(newCar);
			
		}
		
		private function initiate_Spinner():void
		{
			//(this.spinner_actor.disp as TextField).text = "well";
			//this.spinner_actor.disp.text = "WELL";
			/*this.spinner_actor.disp.width=0;
			this.spinner_actor.disp.height=0;
			this.spinner_actor.disp.text="";*/
			this.spinner_actor.buttonMode = true;
			
			this.spinner_actor._360MC.gotoAndStop(1);
			
			//this.spinner_actor.addEventListener
			//(MouseEvent.MOUSE_DOWN,this.spinnerActorClicked);
			
			//this.spinner_actor.setCallback(this.spinComplete);
		}
		
		private function spinnerActorClicked(evt:MouseEvent):void
		{
			if(this.spinnerActive)
			{
				this.count = 0;
				this.maxSpeed = 0;
				this.prevMaxSpeed = 0;
				this.rotationSpan = 0;
				this.rotationAdjusted = false;
				
				this.lastMousePosX = evt.stageX;
				this.lastMousePosY = evt.stageY;
				
				//Apply the last rotation here.
				/*this.spinner_actor._360MC
				.wsr_wheelContainer._wheel_item.rotation = 
				this.lastRandomRotation;*/
				
				this.spinner_actor.removeEventListener
				(MouseEvent.MOUSE_DOWN,this.spinnerActorClicked);
				
				this.spinner_actor.addEventListener
				(MouseEvent.MOUSE_UP,this.whenSpinnerMouseUp);
				
				PlayerResources.mainStage.addEventListener
				(MouseEvent.MOUSE_MOVE, this.zzzz);
				//this.moveSpinner();
			}
		}
		
		private function whenSpinnerMouseUp(evt:MouseEvent):void
		{
			//first check to see if automatic spin chipped in.
			if(this.spinnerActive)
			{
				this.moveSpinner();
			}
			
			//this.spinner_actor.removeEventListener
			//(MouseEvent.MOUSE_DOWN,this.spinnerActorClicked)
			
			
		}
		
		private function re_adjustRotation():void
		{
			if(!this.rotationAdjusted)
			{
				this.rotationAdjusted = true;
				
				var leftOvers:Number = this.lastRandomRotation%36;
				
				var newRotation:int = 
				leftOvers>(36/2)?
				Math.ceil(this.lastRandomRotation/36)*36
				:Math.floor(this.lastRandomRotation/36)*36;
				
				this.spinner_actor._360MC
				.wsr_wheelContainer._wheel_item.rotation = 
				newRotation;

			}
		}
		
		private function zzzz(evt:MouseEvent):void
		{

			var addon:int = 0;
			var moveDiff:Number;
			var lastTurn:int;
			
			if(this.lastDir=="left"){
			lastTurn = this.spinner_actor._360MC.currentFrame-360;
			}else{	
			lastTurn = this.spinner_actor._360MC.currentFrame;
			}	
			
			var moveThisTurn:int = lastTurn;
			
			
			
			
			this.re_adjustRotation();
			
			
			
			if(evt.stageY>this.lastMousePosY)
			{
				
				this.lastDir = "right";
				
				//this.spinner_actor._360MC._needle_mc_left.alpha = 0;
				//(this.spinner_actor._360MC
				//.getChildByName("_needle_mc") as MovieClip).alpha = 1;
			
				moveDiff =
				evt.stageY - this.lastMousePosY;
				
				count+=moveDiff;
				moveThisTurn=lastTurn+Math.ceil(moveDiff);
				
				if(count>2){count = 0; }
				if(moveThisTurn>360){moveThisTurn=moveThisTurn-360;}
			
			}
			else
			{
				this.lastDir = "left";
				addon = 360;
				//this.spinner_actor._360MC._needle_mc_left.alpha = 1;
				
				//(this.spinner_actor._360MC
				//.getChildByName("_needle_mc") as MovieClip).alpha = 0;
			
				moveDiff =
				this.lastMousePosY - evt.stageY;
				
				count-=moveDiff;
				moveThisTurn=lastTurn-Math.ceil(moveDiff);
				if(count<0){count = 2; }
				if(moveThisTurn<1){moveThisTurn=360;}
				
			}
			
			if(Math.ceil(moveDiff)>maxSpeed){maxSpeed = Math.ceil(moveDiff);}
			
				
			this.spinner_actor._360MC.gotoAndStop(moveThisTurn+addon);
			
			
			this.lastMousePosY = evt.stageY;
			this.prevMaxSpeed = maxSpeed;
			
			
			
			if(maxSpeed > 50)
			{
				
				var rotSpan:Number = (moveThisTurn>lastTurn)?
				moveThisTurn-lastTurn:lastTurn-moveThisTurn;
				
				this.startAutomaticSpinner(rotSpan);
			
			
			}
			
		}

		private function getRandomRotationIncrement():int
		{
			var d:int = Math.floor(Math.random()*11);
			return 36*d;
		}
		
		
		private function moveSpinner():void
		{
			this.startAutomaticSpinner(60);
		}
		
		
		private function startAutomaticSpinner(rotSpan:Number):void
		{
		
			if(this.spinnerActive)
			{
			
				this.spinnerActive = false;
				
				this.theCount = 1;
				this.frameCount = 0;
				this.excessCount = 0;
				this.excessCountLimit = 50;
				
				PlayerResources.mainStage.removeEventListener
				(MouseEvent.MOUSE_MOVE, this.zzzz);
					
				this.rotationSpan = rotSpan;
					
				(this.btw).addEventListener("enterFrame",this.ef);
			
			}
			
		}
		
		
		private function ef(evt:Event):void
		{
			
			if(this.frameCount==5)
			{
				
				this.spinner_actor._360MC
				.wsr_wheelContainer._wheel_item.rotation = 0;
				
				this.spinner_actor._360MC.wsr_wheelContainer._wheel_item
				.rotation +=this.getRandomRotationIncrement(); 
			}
			
			if(this.frameCount!=this.maxFrames&&this.rotationSpan>0)
			{
				
				var addon:int;
				
				var perc:Number = (this.frameCount/this.maxFrames)*100;
				
				var adjustmentSpan:int = 
				Math.floor(((100-perc)/100)*this.rotationSpan);
				
				if(this.lastDir=="right")
				{theCount+=this.regularSpan+adjustmentSpan; addon = 0; }
				
				if(this.lastDir=="left")
				{theCount-=this.regularSpan+adjustmentSpan; addon = 360; }
				
				if(theCount>360){theCount=theCount-360;}
				if(theCount<1){theCount=360+theCount;}
				
				//if(this.lastDir = "left";
				
				this.spinner_actor._360MC.gotoAndStop(theCount+addon);
				
				this.frameCount++;
				if(this.frameCount>=this.maxFrames)
				{this.frameCount=this.maxFrames;}
				
			}
			else
			{
				
				/*
				this.spinner_actor.addEventListener
				(MouseEvent.MOUSE_DOWN,this.spinnerActorClicked);
				
				this.btw.removeEventListener("enterFrame",ef);
				
				this.spinComplete(8);
				*/
			
				if(excessCount<this.excessCountLimit)
				{
					var t:int;
					var addon1:int;
				
					if(this.lastDir=="right")
					{
						t = (this.spinner_actor._360MC.currentFrame) - 1;
						addon1 = 360;
					}
					
					if(this.lastDir=="left")
					{
						t = (this.spinner_actor._360MC.currentFrame) + 1;
						addon1 = 0;
					}
					
				
					if(t>360){t = t-360;}
					if(t<1){t = 360+t;}
					

					this.spinner_actor._360MC.gotoAndStop(t+addon1);
					
					
					excessCount++;
					
					
					
					if(excessCount>=this.excessCountLimit)
					{
					
					
						var increaseExcessContLimit:Boolean = false;
						
						if(this.lastDir=="left"&&this.spinner_actor._360MC
						._needle_mc.rotation<-26)
						{increaseExcessContLimit = true;}
						
						if(this.lastDir=="right"&&this.spinner_actor._360MC
						._needle_mc_left.rotation>27)
						{increaseExcessContLimit = true;}
						
						
						if(increaseExcessContLimit){this.excessCountLimit++;}
						
					}
					
					
				}
				else
				{
					
					var rot:Number = this.spinner_actor._360MC
						.wsr_wheelContainer._wheel_item.rotation;
						
					var currF:int = this.spinner_actor._360MC
					.currentFrame;
					
					currF=currF>360?currF-360:currF;
					
					rot = rot<0?rot+360:rot;
					
					var newRot:Number = currF+rot;
					newRot = newRot>360?newRot-360:newRot;
					
					this.lastRandomRotation = newRot;
					
					
						this.spinner_actor.addEventListener
						(MouseEvent.MOUSE_DOWN,this.spinnerActorClicked);
						
						this.btw.removeEventListener("enterFrame",ef);
						
						this.spinComplete(this.getNumberFromRotation(newRot));
					
				}
				
				
			}	
			
			
		}
		
		private function getNumberFromRotation(currFrame:Number):int
		{
			var spun:int = 1;
			switch(this.lastDir)
			{
				case "right":
				{
					currFrame = currFrame+360;
					if(currFrame>=361 && currFrame<=396){ spun=10; }
					if(currFrame>=397 && currFrame<=432){ spun=9; }
					if(currFrame>=433 && currFrame<=468){ spun=8; }
					if(currFrame>=469 && currFrame<=504){ spun=7; }
					if(currFrame>=505 && currFrame<=539){ spun=6; }
					if(currFrame>=540 && currFrame<=576){ spun=5; }
					if(currFrame>=577 && currFrame<=613){ spun=4; }
					if(currFrame>=614 && currFrame<=648){ spun=3; }
					if(currFrame>=649 && currFrame<=684){ spun=2; }
					if(currFrame>=685 && currFrame<=720){ spun=1; }
					
					break;
				}
				
				
				case "left":
				{
					if(currFrame>=36 && currFrame<=67){ spun=10; }
					if(currFrame>=72 && currFrame<=103){ spun=9; }
					if(currFrame>=108 && currFrame<=139){ spun=8; }
					if(currFrame>=144 && currFrame<=175){ spun=7; }
					if(currFrame>=180 && currFrame<=211){ spun=6; }
					if(currFrame>=215 && currFrame<=247){ spun=5; }
					if(currFrame>=252 && currFrame<=283){ spun=4; }
					if(currFrame>=288 && currFrame<=319){ spun=3; }
					if(currFrame>=324 && currFrame<=355){ spun=2; }
				
					
					break;
				}
			}
			
			
			return spun;
		}
		
		private function getRotationResults():int
		{
		
			
			var spun:int = 1;
			
			var newNum:int;
			
			var currFrame:int = 
			this.spinner_actor._360MC.currentFrame;
			
			var interiorwheelRotation:Number = 
			this.spinner_actor._360MC
						.wsr_wheelContainer._wheel_item.rotation;
			
			//Together, this is the rotation of the exterior container 
			//and the interior wheel.
			this.lastRandomRotation = 
			this.spinner_actor._360MC
						.wsr_wheelContainer._wheel_item.rotation+
			this.spinner_actor._360MC
						.wsr_wheelContainer.rotation;
			
			
			var calc:Number;
			var calcFrm:Number;
			
			calcFrm = currFrame>360?currFrame-360:currFrame;
			calcFrm = currFrame<0?currFrame+360:currFrame;
			
			calc = calcFrm+interiorwheelRotation;
			calc = calc>360?calc-360:calc;
			calc = calc<0?calc+360:calc;
						
						
			var adj:Number;
						
			
			if(this.lastDir=="right")
			{
				if(currFrame>=361 && currFrame<=396){ spun=10; }
				if(currFrame>=397 && currFrame<=432){ spun=9; }
				if(currFrame>=433 && currFrame<=468){ spun=8; }
				if(currFrame>=469 && currFrame<=504){ spun=7; }
				if(currFrame>=505 && currFrame<=539){ spun=6; }
				if(currFrame>=540 && currFrame<=576){ spun=5; }
				if(currFrame>=577 && currFrame<=613){ spun=4; }
				if(currFrame>=614 && currFrame<=648){ spun=3; }
				if(currFrame>=649 && currFrame<=684){ spun=2; }
				if(currFrame>=685 && currFrame<=720){ spun=1; }
				
				
				if(interiorwheelRotation<1)
				{interiorwheelRotation = 360 + interiorwheelRotation;}
				
				
				newNum = spun - Math.ceil(interiorwheelRotation/36);
				newNum = newNum>10?newNum-10:newNum;
				newNum = newNum<1?10+newNum:newNum;

			}
			else
			{
			
				if(currFrame>=36 && currFrame<=67){ spun=10; }
				if(currFrame>=72 && currFrame<=103){ spun=9; }
				if(currFrame>=108 && currFrame<=139){ spun=8; }
				if(currFrame>=144 && currFrame<=175){ spun=7; }
				if(currFrame>=180 && currFrame<=211){ spun=6; }
				if(currFrame>=215 && currFrame<=247){ spun=5; }
				if(currFrame>=252 && currFrame<=283){ spun=4; }
				if(currFrame>=288 && currFrame<=319){ spun=3; }
				if(currFrame>=324 && currFrame<=355){ spun=2; }
				
				
				
				if(interiorwheelRotation<1)
				{interiorwheelRotation = 360 + interiorwheelRotation;}
				
				
				newNum = spun - Math.ceil(interiorwheelRotation/36);
				newNum = newNum>10?newNum-10:newNum;
				newNum = newNum<1?10+newNum:newNum;
				
				
			
			}
			
			
			
				adj = 
				this.lastRandomRotation<0?
				360+this.lastRandomRotation:
				this.lastRandomRotation;
				
				
			return newNum;
						
		}
	
		
		public function restoreSavedPositions
		(
		car_square_postion:String,
		car_color:String,
		wheel_last_spin:String,
		wheel_last_rotation:String,
		needle_last_rotation:String
		):void
		{
			
			
		
			if(this.carTrack_actor)
			{
				
			//this.numToMoveto
			this.lastPlace = parseInt(car_square_postion);
			//this.numToMoveto = this.numToMoveto + this.lastPlace;
			this.nextPlace = this.lastPlace;//+1;
			PlayerResources.currentSquare = this.lastPlace;
			this.carTrack_actor.gotoAndStop(this.convertNumToFrame(this.lastPlace));
			
			PlayerResources.player_car = car_color;
			
			
			
			
			this.setDefaultCar();
			
			}
			
			if(this.spinner_actor)
			{
			//this.carTrack_actor.gotoAndStop(this.convertNumToFrame(parseInt(car_square_postion)));
			}	
		}
		/*
		private function moveSpinner():void
		{
			if(this.spinner_actor.alpha==0)
			{this.spinner_actor.alpha=1;}
			else
			{
			this.spinner_actor.take_a_spin();
			}
		}
		*/
		private function spacebarPressed(evt:KeyboardEvent):void
		{
			this.lastDir = "right";
			
			if(evt.keyCode == 32)
			{
				this.spinner_actor.removeEventListener
				(KeyboardEvent.KEY_DOWN, this.spacebarPressed);
				this.moveSpinner();
			}
		}
		
		private function repositionCarBeforeMove():void
		{
			GeneralAnimations.positionTo
					(this.lyrs.getLayer("mainTrack"), 
					this.carTrackBackInPosition,
					0,0,2.5,2);
		}
		
		private function carTrackBackInPosition():void
		{
			GeneralAnimations.animatePanelsIn(this.lyrs, 
			this.afterPanelsRemoved_afterSpinnerTween);
		}
		
		public function spinComplete(num:Number):void
		{
			
			switch(this.typeOfSpin)
			{
				case "regular":
				{
					
					//num = 1;
					this.numToMoveto = num;
					
					GeneralAnimations.basic_displayItemToggle
					(this.lyrs.getLayer("spinToWin"),this.onClearSpinningScreen,0);
					
					/*
					TweenLite.to(this.spinner_actor, 0.5, 
					{delay:1, alpha:0, ease:Back.easeOut, 
					onComplete:onFinishTween, onCompleteParams:[5,this.spinner_actor]});
					*/
					
					
					break;
				}
				
				case "enhance_pension":
				{
					
					/*GeneralAnimations.basic_displayItemToggle
					(this.spinner_actor,null,0);*/
					
					this.spinToWin_result = num;
					
					
					GeneralAnimations.basic_displayItemToggle
					(
					this.lyrs.getLayer("spinToWin"),
					this.enhance_spinToWinClose,
					0
					);
					
					
					
					
					break;
				}
				
				case "retirement_spin":
				{
					/*GeneralAnimations.basic_displayItemToggle
					(this.spinner_actor,null,0);*/
					
					this.spinToWin_result = num;
					
					GeneralAnimations.basic_displayItemToggle
					(
					this.lyrs.getLayer("spinToWin"),
					this.retire_spinToWinClose,
					0
					);
					
					//this.manageRetireSpinMoney(num);
					//this.doSpinTowinActivities(num);
					break;
				}
				
				case "spin_to_win":
				{
				
					//We moved the panels earlier so that we could show
					//the items on the spin to win panel,
					//we can now put it back.
					this.placePanels_ForSpinToWin();
					
					//change this back immmediately to regular before
					//we forget to do so.
					this.typeOfSpin = "regular";
					//Now conduct Spin to win acivities
					this.doSpinTowinActivities(num);
					
					break;
				}
				
				default:
				{
					//change this back immmediately to regular before
					//we forget to do so.
					this.typeOfSpin = "regular";
					//Now conduct Spin to win acivities
					this.doSpinTowinActivities(num);
				}
			}
			
			/*
			//spinToWin
			if(this.typeOfSpin == "regular")
			{
				
				this.numToMoveto = num;
			
				GeneralAnimations.basic_displayItemToggle
				(this.lyrs.getLayer("spinToWin"),this.onClearSpinningScreen,0);
				
				TweenLite.to(this.spinner_actor, 0.5, 
				{delay:1, alpha:0, ease:Back.easeOut, 
				onComplete:onFinishTween, onCompleteParams:[5,this.spinner_actor]});
				
			}
			else
			{
				//change this back immmediately to regular before
				//we forget to do so.
				this.typeOfSpin = "regular";
				//Now conduct Spin to win acivities
				this.doSpinTowinActivities(num);
			}
			*/
		}
		
		private function onClearSpinningScreen():void
		{
			GlobalFunctions.emptyContainer(this.lyrs.getLayer("spinToWin"));
			this.lyrs.getLayer("spinToWin").alpha = 1;
			
			this.repositionCarBeforeMove();
		}
		
		private function doSpinTowinActivities(spinToWinNumber:int):void
		{
			
			switch(this.typeOfSpin)
			{
				
				case "retirement_spin":
				{
			
					this.spinToWin_result = spinToWinNumber;
					
					/*GeneralAnimations.basic_displayItemToggle
					(this.spinner_actor,null,0);*/
					
					GeneralAnimations.basic_displayItemToggle
					(
					this.lyrs.getLayer("spinToWin"),
					this.manageRetireSpinMoney,
					0
					);
					
					break;
				
				}	
				
				default:
				{
			
					this.spinToWin_result = spinToWinNumber;
					
					/*GeneralAnimations.basic_displayItemToggle
					(this.spinner_actor,null,0);*/
					
					GeneralAnimations.basic_displayItemToggle
					(
					this.lyrs.getLayer("spinToWin"),
					this.spinToWinClose,
					0
					);
				
				}	
			
			}	
			
		}
		
		public function carTrackclicked(evt:MouseEvent):void
		{
			
			if(this.gf.res.positionLocal)
			{
				//getReadyGoMoveCar();
			}
			


		}
		
		public function displaySpinner():void
		{
			
			
			
			//if(!this.gf.res.positionLocal)
			{
			
				this.typeOfSpin = "regular";
				
				//Just check to make sure that there are no more moves that needs to be completed
				//We check this.game_pitstops array to confirm this.
				//this.spinType
				if(this.game_pitstops.length<1 || PlayerResources.currentSquare != 128)
				{
					this.spinnerTimer.start();
					//this.mainTimer.addEventListener(TimerEvent.TIMER, timerHandler);
					this.spinnerTimer.addEventListener(TimerEvent.TIMER_COMPLETE, spinnerReadyToShow);
				}
				
			}
			
		}
		
		private function spinnerReadyToShow(evt:TimerEvent):void
		{
			//Animate the title bar into place.
			this.gf.removeTitleBar(this.removePanels_ShowSpinner);
		}
		
		private function removePanels_ShowSpinner():void
		{
			GeneralAnimations.animatePanelsOut
			(this.lyrs, this.afterPanelsRemoved_ShowSpinner);
		}
		
		public function removePanels_ForSpinToWin():void
		{
			GeneralAnimations.animatePanelsOut_ForSpinToWin
			(this.lyrs, null);
			//animatePanelsIn_ForSpinToWin
		}
		
		public function placePanels_ForSpinToWin():void
		{
			GeneralAnimations.animatePanelsIn_ForSpinToWin
			(this.lyrs, null);
		}
		
		private function afterPanelsRemoved_ShowSpinner():void
		{
		
			//After the panels have been remove, we now cut to the
			//spinner at the spinning position
			
			//Actually, just realise that its not the mainTrack that
			//we should be moving but whatever layer we are moving
			//inside the swc. Open the swc and find out exactly what 
			//we are moving.
			
			/*
			this.layers.getLayer("mainTrack").x = 
			BoardRepositioning.squareXpos;
			this.layers.getLayer("mainTrack").y = 
			BoardRepositioning.squareYpos;
			*/
			

			
			
					
					
					this.lyrs.getLayer("mainTrack").x = 
					32-this.carTrack_actor._board.x;
					
					
					this.lyrs.getLayer("mainTrack").y = 
					(160-this.carTrack_actor._board.y)-20;
					
					this.spinner_actor.y = 197;
					
					
					GeneralAnimations.positionTo
					(this.spinner_actor, null,
					this.spinner_actor.x, 213.9);
					
					
					
					
					
					
					PlayerResources.mainStage.addEventListener
					(KeyboardEvent.KEY_DOWN, this.spacebarPressed);
					
					this.spinner_actor.addEventListener
					(MouseEvent.MOUSE_DOWN,this.spinnerActorClicked);
					
					
					this.spinnerActive = true;
					
					PlayerResources.mainStage.focus = this.spinner_actor;
					
					if(this.typeOfSpin=="regular")
					{
						this.screens.placeSpinnerScreen();
					}
					else
					{
						this.screens.placeSpinToWinScreen(this.typeOfSpin);
					}
					
					
		}
		
		private function onFinishTween(param1:Number, param2:MovieClip):void
		{
			GeneralAnimations.animatePanelsIn(this.lyrs, 
			this.afterPanelsRemoved_afterSpinnerTween);
		}
		
		private function afterPanelsRemoved_afterSpinnerTween():void
		{
			//Get the title bar..
			var titleBar:Sprite = (this.lyrs.getLayer("titleLayer")
			.getChildByName("theTitle") as Sprite)
			.getChildByName("titleBar") as Sprite;
			
			
			//Animate the title bar into place.
			GeneralAnimations.positionTo
			(titleBar, this.afterTitleBarRInPlace_afterSpinnerTween, 0, 0);
		}
		
		private function afterTitleBarRInPlace_afterSpinnerTween():void
		{
			getReadyGoMoveCar();
		}
		
		private function getReadyGoMoveCar():void
		{
			//temporary
			//this.nextPlace = 133;
			//this.alternativeNextPlace
		
			//Text to see if the car is on a regular path or
			//an alternative path.
			//Remember if its on a regular path, we will be making a decision
			//to send the user to an alternative route and also set the 
			//this.routeType variable as alternative.
			
			
			
			if(this.chosenPath == 1&&PlayerResources.currentSquare!=0)
			{
			this.regularOrAlternative();
			}
			else
			{
				
				//if(PlayerResources.currentSquare==0){PlayerResources.currentSquare=1;}
				
				
				//If chosen path is 2, we need to set it back
				//immediately to 1 as we have used it above.
				//We only need it if the car is on the spot just 
				//before the intersection.
				this.chosenPath = 1;
				if(PlayerResources.currentSquare!=0&&PlayerResources.currentSquare!=57)
				{
				this.routeType = "alternative";
				}
				else
				{
				//this.frameToStopAt = 30;
				}	
			}
			
			
			
			switch(this.routeType)
			{
				case "alternative":
				{
					/*if(PlayerResources.currentSquare == 0 )
					{
							//3385
							this.frameToStopAt = 3385;
							this.carTrack_actor
							.addEventListener("enterFrame", this.carRunning);
							this.carTrack_actor.gotoAndPlay(3360);
					}
					else
					{
					*/
					
						//first thing we need to do since this is an alternative
						//route is to jump to car to the identical spot poising
						//it to go forward on the new route.
						var gohere:Number = this.getAlternativeRouteJumpSpots();
						this.carTrack_actor
						.gotoAndStop(gohere);
						
						
						//this.carTrack_actor.play();
						//this is what we just spun.
						//this.numToMoveto
						this.getXMLSpotsToCheck();
						
						//Now, get the elements in the XML that we need to check
						//for stops.
						//This function will replace the one which places stuff into
						//the game_pitstops array
						
						
						
						if(this.alternativeRoutePittstops())
						{
							
							
							if(this.game_pitstops.length>0)
							{
								this.nextPlace=this.game_pitstops[0];
								this.moveCar();
							}
							
							//this.makeAlternativeCarMove();
						}
						else
						{
							//If we are not going to make a few stops then we
							//will continue with our car move with this 
							//function call below
							//this.setNextPlaceAndMove(this.numToMoveto);
							this.carTrack_actor
							.addEventListener("enterFrame", this.carRunning);
				//this.carTrack_actor.gotoAndPlay(this.convertNumToFrame(this.lastPlace));
							this.carTrack_actor.play();
						
						}
						
						
						
						
					//}
					
					
			
			
					break;
				}
				
				case "regular":
				{
					//routeType
					//this.carTrack_actor.moneyGrow.alpha=0;
					
					
					if(PlayerResources.currentSquare == 0 )
					{
							//3385
							this.nextPlace = 1;
							this.lastPlace = 1;
							this.cancelSpin = false;
							this.frameToStopAt = 30;
							this.carTrack_actor
							.addEventListener("enterFrame", this.carRunning);
							this.carTrack_actor.gotoAndPlay(1);
					}
					else
					{
						
							if(/*(this.nextPlace+this.numToMoveto)<128 && */this.makeImportantStops(this.numToMoveto))
							{
								this.makeAlternativeCarMove();
							}
							else
							{
								//If we are not going to make a few stops then we
								//will continue with our car move with this 
								//function call below
								this.setNextPlaceAndMove(this.numToMoveto);
							}
							
					}	
			
					break;
				}
			}
			
			
			
			
		
			 
			
			
			
		}
		
		private var counter:int = 0;
		private function moveCar():void
		{
			
			this.Res.uiMajorText.text = "";
			this.Res.uiMinorText.text = "";
			
			
			counter++;
			
			
			
			
			//if(!this.cancelSpin)
			//{
				
				
				if(this.nextPlace>128||this.nextPlace==128)
				{/*this.lastPlace=1;*/ this.nextPlace=128;}
				
				this.frameToStopAt = this.convertNumToFrame(this.nextPlace);
					
					
				this.carTrack_actor.addEventListener("enterFrame", this.carRunning);
				this.carTrack_actor.gotoAndPlay(this.convertNumToFrame(this.lastPlace));
				
				this.lastPlace=this.nextPlace;
			
			/*
			//}
			//else
			//{
				
				this.cancelSpin = false;
				//If we are to cancel the remaining moves, we will
				//clear the arrays and display the spinner once more
				//for we have to make a new spin.
				this.game_pitstops = new Array();
				this.passoverActionList = new Array();
				
				//Display the spinner again.
				this.displaySpinner();
				
				//Seeing that we are canceling the spin we need
				//to make sure that the next place takes the
				//position of the current position.
				//Also, we need to set the Player's current
				//position.
				this.nextPlace = this.lastPlace;
				PlayerResources.currentSquare = this.lastPlace;
				
			//}
			*/
			
			
			
			
		}
		
		private function carRunning(evt:Event):void
		{
			
			
			
			/*
			if(this.carTrack_actor.currentFrame == this.frameToStopAt)
			{
				this.carTrack_actor.removeEventListener("enterFrame",this.carRunning);
				this.carTrack_actor.stop();
				
				this.onCarMoveCompleted();
				
			}
			*/
			
			var ii:int = 0;
			var xmlPosition:int;
			
			
			if(this.routeType == "alternative")
			{
				
				//We need to know if we are going to switch back to the normal
				//route when the car gets to the intersection.
				//We will monitor if the car comes to the intersection
				if(this.mergeRouteEnd)
				{
					if(this.carTrack_actor.currentFrame == this.mergeRouteFrame)
					{
						this.carTrack_actor.gotoAndPlay
						(this.mergeNormalFrame);
						
						this.mergeRouteEnd = false;
					}
				}
				
				
				if(this.passoverActionList.length>0)
				{
				
					for(ii; ii<this.passoverActionList.length; ii++)
					{
						if(this.carTrack_actor.currentFrame == this.passoverActionList[ii].frame)
						{
							//this.passoverActionList[0].xmlIndex
							//this.positionsXML.board_places.place[this.passoverActionList[0].xmlIndex].action.type
							//var actionType:String = this.positionsXML.board_places.place[this.passoverActionList[0].xmlIndex].action.type;
							xmlPosition = this.passoverActionList[ii].xmlIndex;
							this.performPassoverAction(xmlPosition);
						}
						break;
					}
				
				}
			
			
			
				
				//Look out for the completion of this move
				if(this.carTrack_actor.currentFrame == this.frameToStopAt)
				{
					
					this.carTrack_actor.removeEventListener("enterFrame",this.carRunning);
					this.carTrack_actor.stop();
					
					this.onAlternativeCarMoveCompleted();
					this.nextPlace = this.lastPlace;
					
					//place this here temporarily
					this.cancelSpin = false;//Oh I just realized. 
					//This cancel spin got assigned true because
					//there maybe was a two card popup
				}
			}
			else
			{
				
				
				if(this.passoverActionList.length>0)
				{
				
					for(ii; ii<this.passoverActionList.length; ii++)
					{
						if(this.carTrack_actor.currentFrame == this.passoverActionList[ii].frame)
						{
							//this.passoverActionList[0].xmlIndex
							//this.positionsXML.board_places.place[this.passoverActionList[0].xmlIndex].action.type
							//var actionType:String = this.positionsXML.board_places.place[this.passoverActionList[0].xmlIndex].action.type;
							xmlPosition = this.passoverActionList[ii].xmlIndex;
							this.performPassoverAction(xmlPosition);
						}
						break;
					}
				
				}
				
				
				if(this.carTrack_actor.currentFrame == this.frameToStopAt)
				{
					
					this.carTrack_actor.removeEventListener("enterFrame",this.carRunning);
					this.carTrack_actor.stop();
					
					this.onCarMoveCompleted();
					
				}
			}
			
		}
		
		private function setNextPlaceAndMove(numTo:Number):void
		{
			
			this.nextPlace+=Math.floor(numTo);
			this.moveCar();
			
		}
		
		private function makeAlternativeCarMove():void
		{
			if(this.game_pitstops.length>0)
			{
				this.nextPlace=this.game_pitstops[0];
				this.moveCar();
			}
		}
		
		public function onAlternativeCarMoveCompleted():void
		{
			
			//After user has moved, we log their current
			//spot on the board here.
			PlayerResources.currentSquare = this.lastPlace;
			
			var spot:int;
			if(this.game_pitstops.length>0)
			{
				spot = this.game_pitstops[0];
				this.game_pitstops.shift();
				this.doAction(spot);
			}
			else
			{
				this.doAction(PlayerResources.currentSquare);
			}
			
		}
		
		public function onCarMoveCompleted():void
		{
			
			//After user has moved, we log their current
			//spot on the board here.
			PlayerResources.currentSquare = this.lastPlace;
			
			var spot:int;
			if(this.game_pitstops.length>0)
			{
				spot = this.game_pitstops[0];
				this.game_pitstops.shift();
				this.doAction(spot);
			}
			else
			{
				spot = this.nextPlace;
				this.doAction(spot);
			}
			
		}
		
		private function convertNumToFrame(num:int):int
		{
			return parseInt(this.positionsXML.board_places.place[(num)].@frame);
		}
		
		private function makeImportantStops(myRoll:int):Boolean
		{
			var lastPlaceSaved:Boolean = false;
			var placeRollInPittStop:Boolean = false;
			var startCheckAt:int = (this.lastPlace+1);
			
			for(var i:int = startCheckAt; i<(startCheckAt+(myRoll-1)); i++)
			{
			
				if
				(
				   this.positionsXML.board_places.place[i].action.briefstop != undefined
				   &&
				   this.positionsXML.board_places.place[i].action.briefstop == true
				)
				{
					
					this.game_pitstops.push(i);
					placeRollInPittStop = true;
					if((this.nextPlace+myRoll)==i)
					{
						lastPlaceSaved = true;
					}
				}
				
				if
				(
				   this.positionsXML.board_places.place[i].action.passover_action != undefined
				   &&
				   this.positionsXML.board_places.place[i].action.passover_action == true
				)
				{
					var poa:Object = new Object();
					poa.xmlIndex = i;
					poa.frame =  this.positionsXML.board_places.place[i].@frame;
					this.passoverActionList.push(poa);
					//placeRollInPittStop = true;
				}
				
			}
			
			if(!lastPlaceSaved && placeRollInPittStop)
			{
				this.game_pitstops.push(this.nextPlace+myRoll);
			}
			
			
			return placeRollInPittStop;
		}
		
		private function performPassoverAction(pos:int):void
		{
			//we have used the first element of this array to identify
			//the passover action on the frame. Lets remove the first element
			//now as we no longer need it
			this.passoverActionList.shift();
			this.doAction(pos);
		}
		
		public function doAction(pos:int):void
		{
			
			
			var mainText:String = "";
			var subText:String = "";
			var popupFormat:String = "";
			var display_type:String = "";
			
			
			var displayUiText:Boolean = true;
			
			
			
			
			var actionType:String = this.positionsXML.board_places.place[pos].action.type;
			
			//Check to see if an avert is imminent. If so we need to
			//set a boolean so we can take care of this after the action is completed.
			this.showAdvertisement = false;
			if(this.positionsXML.board_places.place[pos].action.advert.length() > 0)
			{
				this.showAdvertisement = true;
			}
			
			//While performing action, look out for monies tag.
			//If the monies tag is available then we need to deal with
			//this new money
			if(this.positionsXML.board_places.place[pos].action.monies.length() > 0)
			{
				//we want to determine if to use the existing subtext or if we should use
				//alternative text if we have a suitable wealth card, therefore, we will
				//check to see if we need a wealth card for using monies and if we 
				//have the appropriate wealth card.
				displayUiText = this.dealWithMonies(pos);
			}
			
			
			
			if(this.positionsXML.board_places.place[pos].action.pegs.length() > 0)
			{
				this.dealWithPegs(pos);
			}
			
			
			//Check here if Feed Should be posted
			if(this.feedActs.readyToUpdate())
			{
				//We look here to see if the XML has a feed tag.
				//If it does, we will send the feed text to the database and 
				//save it as an action performed by this user.
				if(this.positionsXML.board_places.place[pos].action.feedText.length() > 0)
				{
					this.gf.sendFeedToDB(this.positionsXML.board_places.place[pos].action.feedText);
				}
			}
			
			
			if(actionType!="popup")
			{
					//no more actions. 
					//Drop feed now if any needs to be dropped.
					//Wait for this action to complete before giving user the
					//option to spin again
					
					//Check here if We should drop feed now
					if(this.feedActs.dropFeedNow())
					{
						//We look here to see if the if feed is ready to dropped.
						//If its time to drop a feed, we will access the control
						//feed panel and perform that dropping action now.
						//this.feedActs.this.fBar
						this.feedActs.performFeedInsertion();
					}
					
			}
			
			
			
					
					
			
			
			
			switch(actionType)
			{
				
				
				//A person may be paid whether or not he lands on this square. In the case that the person
				//passes this way we will do this action here
				case "pay":
				{
					var payIncrease:Number = 0;
					//Deal with the increase here, if there is an increase element
					if(this.positionsXML.board_places.place[pos].action.increase.length() > 0)
					{
						payIncrease = parseFloat(this.positionsXML.board_places.place[pos].action.increase);
					}
					
					//Calculate the user's amount of money
					var moneywithIncrease:Number = PlayerResources.salary+payIncrease;
					
					//Test to see if this increase will make user surpass his maximum
					//salary. If its going to surpass, then let the user have his 
					//maximum salary only
					PlayerResources.salary = 
					moneywithIncrease>PlayerResources.maximum_salary?
					PlayerResources.salary:moneywithIncrease;
					
					
					//Now increase the user's cash in hand
					var newMoney:Number = PlayerResources.money+PlayerResources.salary;
					PlayerResources.money = newMoney;
					
					
					
					this.Res.uiMajorText.text = this.positionsXML.board_places.place[pos].action.mainText;
					this.Res.uiMinorText.htmlText = this.positionsXML.board_places.place[pos].action.subText;
					
					if(this.positionsXML.board_places.place[pos].action.subText == "")
					{
						this.Res.uiMinorText.htmlText = "Receive "+
						GlobalFunctions.formatMoneyToString(PlayerResources.salary);
					}
					
					//this.Res.uiMoniesText.htmlText = GlobalFunctions.formatMoneyToString(newMoney);
					
					//Show animation of gaining money on car
					this.moneyAnimationClass.animateMoneyGain();
					
					
					
					
					//deal with Advertisement
					if(this.showAdvertisement)
					{this.dealWithAdvert(pos, this.mainFunctionAfterAdvertisement);}
					else
					{
					//We are now here at the pay day. We want to find out if this is the last of the
					//actions. Lets check game_pitstops array to see if it has elements. If it has we will\
					//not do anything. If it does not, we will bring the spinner up.
					//game_pitstops
						if(this.game_pitstops.length<1
						&& this.positionsXML.board_places.place[pos].@frame == this.frameToStopAt)
						{
							this.displaySpinner();
						}
					
					}
					
					break;
				}
				
				
				case "popup":
				{
					popupFormat = this.positionsXML.board_places.place[pos].action.format;
					//this.Res.uiMinorText.text = "the spot is "+popupFormat;
						
					mainText = this.positionsXML.board_places.place[pos].action.mainText;
					subText = this.positionsXML.board_places.place[pos].action.subText;
					
					if(this.positionsXML.board_places.place[pos].action.format.@display_type)
					{
						display_type = this.positionsXML.board_places.place[pos].action.format.@display_type;
					}
						
					this.displayPopup(mainText, subText, popupFormat, display_type);
					
					
					
					break;
				}
				
				case "marriage":
				{
					
					this.Res.uiMajorText.text = this.positionsXML.board_places.place[pos].action.mainText;
					this.Res.uiMinorText.text = this.positionsXML.board_places.place[pos].action.subText;
					
					
					this.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.cancelSpin;
						
					if(this.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.cancelSpin.length() > 0)
					{
						if( this.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.cancelSpin == 1 )
						{
							this.cancelSpin = true;
						}
					}
					
					
					
					//deal with Advertisement
					if(this.showAdvertisement){this.dealWithAdvert(pos, this.screenClosed);}
					//else
					//{this.displaySpinner();}
					
					
					
					break;
				}
				
				case "life":
				{
					//increment the life cards
					PlayerResources.lifeCards++;
					
					this.Res.uiLifeText.text = GlobalFunctions.formatLifeCardString(PlayerResources.lifeCards);
					this.Res.uiMajorText.text = this.positionsXML.board_places.place[pos].action.mainText;
					this.Res.uiMinorText.text = this.positionsXML.board_places.place[pos].action.subText;
					
					//deal with Advertisement
					if(this.showAdvertisement){this.dealWithAdvert(pos, this.mainFunctionAfterAdvertisement);}
					else
					{this.displaySpinner();}
					
					break;
				}
				
				case "spin":
				{

					var spinType:String = "spin_to_win";
					if(this.positionsXML.board_places.place[pos].action.type.@variety)
					{
					spinType = this.positionsXML.board_places.place[pos].action.type.@variety;
					}
					
					this.typeOfSpin = spinType;
					
					//this.screens.placeSpinToWinScreen(spinType);
					
					this.Res.uiMajorText.text = 
					this.positionsXML.board_places.place[pos].action.mainText;
					this.Res.uiMinorText.text = 
					this.positionsXML.board_places.place[pos].action.subText;
					
					this.afterPanelsRemoved_ShowSpinner();
					
					//deal with Advertisement
					//if(this.showAdvertisement)
					//{this.dealWithAdvert(pos, this.mainFunctionAfterAdvertisement);}
					
					break;
				}
				
				case "move":
				{
					/*
					delayThenTakeFurtherAction
					*/
					this.Res.uiMajorText.text = this.positionsXML.board_places.place[pos].action.mainText;
					this.Res.uiMinorText.text = this.positionsXML.board_places.place[pos].action.subText;
					this.delayThenTakeFurtherAction("moveCarToSquare");
					
					break;
				}
				
				case "tax":
				{
					//Paid taxes
					this.manageMonies("MINUS", PlayerResources.salary_taxes );
					//Subtract taxes from the amount cash in hand
					this.update_moneyText(PlayerResources.money);
					
					this.Res.uiMajorText.text = this.positionsXML.board_places.place[pos].action.mainText;
					//We are making sure that we complete the message in the Ui
					//Append the taxes we are paying to the string on the xml.
					this.Res.uiMinorText.text = 
					this.positionsXML.board_places.place[pos].action.subText
					+GlobalFunctions.formatNumberToMoneyString(PlayerResources.salary_taxes);
					
					this.displaySpinner();
					
					break;
				}
				
				case "pay_per_child":
				case "gain_per_child":
				{
					
					if(this.positionsXML.board_places
					.place[pos].action.pay_per_child_monies.length()>0)
					{
						var moneyAction:String = (this.positionsXML.board_places
					.place[pos].action.pay_per_child_monies.@action).toUpperCase();
					
					var ppc_money:Number = this.positionsXML.board_places
					.place[pos].action.pay_per_child_monies;
					
					var ppc_payment_monies:Number = 
					ppc_money*PlayerResources.numOfChildren;
					
					this.manageMonies(moneyAction, ppc_payment_monies );
					
					}
					
					//Subtract taxes from the amount cash in hand
					this.update_moneyText(PlayerResources.money);
					
					this.Res.uiMajorText.text = 
					this.positionsXML.board_places.place[pos].action.mainText;
					//We are making sure that we complete the message in the Ui
					//Append the taxes we are paying to the string on the xml.
					this.Res.uiMinorText.text = 
					this.positionsXML.board_places.place[pos].action.subText;
					
					this.displaySpinner();
					
					break;
				}
				
				default:
				{
				
					if(displayUiText)
					{
						this.Res.uiMajorText.text = 
						this.positionsXML.board_places.place[pos].action.mainText;
						this.Res.uiMinorText.text = 
						this.positionsXML.board_places.place[pos].action.subText;
					}	
					//deal with Advertisement
					if(this.showAdvertisement){this.dealWithAdvert(pos, this.mainFunctionAfterAdvertisement);}
		 			else
					//Once it gets here, if there is no advertisement, we will display spinner.
					//Actually, we need to create a boolean above to test to see if a spinner was displayed
					//That's important.
					{
					//Only if there is no other action to be taken.
					//In this case, if we are displaying items in the Ui here means we are not
					//doing the money pop-up. We have to be careful that the spinner don't come up
					//when there's an action to be taken.
					if(displayUiText){this.displaySpinner();}
					}
					
					break;
				}
			}
			
			
			this.Res.uiMoniesText.htmlText = GlobalFunctions.formatMoneyToString(PlayerResources.money);
			
			/*
			//this.screenAnimatedOff(0);
			this.mainTimer.start();
            this.mainTimer.addEventListener(TimerEvent.TIMER, timerHandler);
            this.mainTimer.addEventListener(TimerEvent.TIMER_COMPLETE, completeHandler);
            */
		}
		
		public function afterSpendingWealthCard():void
		{
			
			this.lyrs.getLayer("forms").alpha=0;
			GlobalFunctions.emptyContainer(this.lyrs.getLayer("forms"));
			this.lyrs.getLayer("forms").alpha=1;
			
			//Update the Ui
			this.Res.uiMajorText.text = 
			this.positionsXML.board_places.place[PlayerResources.currentSquare].action.mainText;
			this.Res.uiMinorText.text = 
			this.positionsXML.board_places.place[PlayerResources.currentSquare]
			.action.wealthCards.alternative_copy.subText;
			
			//Call spinner now.
			this.displaySpinner();
		}
		
		private function mainFunctionAfterAdvertisement():void
		{
			//Call spinner now.
			this.displaySpinner();
		}
		
		public function decideToSpendWealthCardLater(evt:MouseEvent):void
		{
			
			this.lyrs.getLayer("forms").alpha=0;
			GlobalFunctions.emptyContainer(this.lyrs.getLayer("forms"));
			this.lyrs.getLayer("forms").alpha=1;
			
			this.moneyAnimationClass.animateMoneyLose();
			//We will subtract monies in our resource here
			this.manageMonies("MINUS", 
			parseFloat(this.positionsXML.board_places
			.place[PlayerResources.currentSquare].action.monies) );
			
			
			//Update the Ui
			this.Res.uiMajorText.text = 
			this.positionsXML.board_places.place[PlayerResources.currentSquare].action.mainText;
			this.Res.uiMinorText.text = 
			this.positionsXML.board_places.place[PlayerResources.currentSquare].action.subText;
			
			
			//Call spinner now.
			this.displaySpinner();
		}
		
		private function dealWithMonies(pos:int):Boolean
		{
			var theReturn:Boolean = true;
			var cardsNeeded:Object
			var haveCards:Boolean = false;
			
			
			if(this.positionsXML.board_places.place[pos].action.wealthCards.length() > 0)
			{
				
				//Ill first get the cards I need
				cardsNeeded = this.getCardsNeeded(pos);
				
				//Then ill check to see if I have the cards.
				haveCards = this.doIHaveWealthCard(cardsNeeded);
				if(haveCards){theReturn = false;}
				
			}
			
			
			var moniesAction:String = this.positionsXML.board_places.place[pos].action.monies.@action;
			switch(moniesAction.toUpperCase())
			{
				case "PLUS":
				{
					this.moneyAnimationClass.animateMoneyGain();
					//We will add monies in our resource here
					this.manageMonies("PLUS", parseFloat(this.positionsXML.board_places.place[pos].action.monies) );
					break;
				}
				
				case "MINUS":
				{
					//If I do have the cards, then Ill take the appropriate action.
					if(haveCards)
					{
						switch(cardsNeeded.decisionStyle)
						{
							case "manual":
							{
								
								//showWealthCardsRevealScreen
								//test here temporarily
								this.gf.showWealthCardsRevealScreen("useable_cards");
								
								break;
							}
							
							case "automatic":
							{
								//Since we are automatically taking care of this action, 
								//we will update the UI and the user will be exempted from paying
								//any monies.
								this.Res.uiMajorText.text = this.positionsXML.board_places.place[pos].action.mainText;
								this.Res.uiMinorText.text = 
								this.positionsXML.board_places.place[pos]
								.action.wealthCards.alternative_copy.subText;
								
								//End of process
								//show spinner
								this.displaySpinner();
								
								break;
							}
						}
					}
					else
					//If I dont have the cards, then Ill continue as usual.
					{
					this.moneyAnimationClass.animateMoneyLose();
					//We will subtract monies in our resource here
					this.manageMonies("MINUS", parseFloat(this.positionsXML.board_places.place[pos].action.monies) );
					}	
					break;
				}
			}
			
			this.update_moneyText(PlayerResources.money);
			
			return theReturn;
		}
		
		private function hhccc():void
		{
			
		}
		
		public function update_moneyText(moneyText:Number):void
		{
			this.Res.uiMoniesText.htmlText = GlobalFunctions.formatMoneyToString(moneyText);
		}
		
		public function manageMonies(action:String, amt:*):void
		{
			switch(action)
			{
				case "PLUS":
				{
					//We will add monies in our resource here
					PlayerResources.money+=amt;
					break;
				}
				
				case "MINUS":
				{
					//We will subtract monies in our resource here
					PlayerResources.money-=amt;
					break;
				}
			}
		}
		
		private function dealWithPegs(pos:int):void
		{
			
			
			var variety:String = 
			this.positionsXML.board_places.place[pos].action.pegs.@variety;
			if(this.pegs.numberOfPegsAdded<6||variety=="married")
			{
				this.pegs.placePeg(variety);
			}
				
		}
		
		private function timerHandler(evt:TimerEvent):void
		{
			
		}
		
		private function completeHandler(evt:TimerEvent):void
		{
			//this.screenAnimatedOff(0);
		}
		
		private function displayPopup
		(
		mainMsg:String, subMsg:String, 
		theFormat:String, display_type:String
		):void
		{
			
		   
			switch(theFormat)
			{
				
				case "two_option_square":
				{
					//Need to know when this screen is ready for 
					//we might need to interact with it
					//For example we might want to add text to this screen
					//therefore - we send a callback here
					this.screens.place_twoCardScreen(mainMsg, display_type, this.whenPopupDisplayed);
			
					break;
				}
				
				case "retire_square":
				{
					//Need to know when this screen is ready for 
					//we might need to interact with it
					//For example we might want to add text to this screen
					//therefore - we send a callback here
					this.screens.place_retireScreen(this.whenRetireChoiceMade);
			
					break;
				}
				
				case "carousel_option_square":
				{
					//Need to know when this screen is ready for 
					//we might need to interact with it
					//For example we might want to add text to this screen
					//therefore - we send a callback here
					this.screens.place_threeCardScreen(mainMsg, display_type, this.whenPopupDisplayed);
			
					break;
				}
				
				case "share_the_wealth":
				{
					//Need to know when this screen is ready for 
					//we might need to interact with it
					//For example we might want to add text to this screen
					//therefore - we send a callback here
					var squareTitle:String = 
					this.positionsXML.board_places.place[PlayerResources.currentSquare]
					.action.mainText;
					
					
					this.screens.placeWealthCardSelectScreen(squareTitle);
			
					break;
				}
				
				default:
				{
					this.Res.uiMajorText.text = mainMsg;
					this.Res.uiMinorText.text = subMsg;
				}
				
			};
		}
		
		public function whenRetireChoiceMade(choice:int):void
		{
			//Now we know what the user has chosen upon retiring. 
			//If the user chooses a one, 
			//If the user chooses a two we will bring up a special edition
			//of the spin to win. In this edition, when the spin to win closes,
			//we will then call the grand total screen.
			if(choice == 0)
			{
				//Calculate for a 100000 payment added on
				PlayerResources.money = PlayerResources.money+100000;
				
				this.gf.sendGoolgleAnaylticsInfo("Country_Retirement");
				
				this.gf.showGrandTotal();
			}
			else
			{
			
				GlobalFunctions.emptyContainer(this.lyrs.getLayer("forms"));
				
				//risky path, we may lose or add some money here
				this.typeOfSpin = "retirement_spin";
				
				this.gf.sendGoolgleAnaylticsInfo("Mansion_Retirement");
				
				this.afterPanelsRemoved_ShowSpinner();
						
						
			}
			
			
		}
		
		
		public function wealthCardSelected(obj:Array):void
		{
			this.gf.wealth_Card_Selected(obj);
		}
		
		
		public function ingame_wealthCardSelected(obj:Array):void
		{
			this.gf.ingame_wealth_Card_Selected(obj);
		}
		
		private function whenPopupDisplayed():void
		{
			
		}
		
		public function screenClosed():void
		{
			CardAnimation.closeCardSelectionScreen(this.lyrs, this.screenAnimatedOff);
		}
		
		private function screenAnimatedOff(i:int):void
		{
		
			
			for(var ss:int = 0 ; ss<lyrs.getLayer("forms").numChildren; ss++)
			{
				this.lyrs.getLayer("forms").removeChildAt(0);
			}
			this.lyrs.getLayer("forms").alpha = 1;
			
			
			//this.Res.uiMinorText.text = this.game_pitstops.length+""//this.positionsXML.board_places.place[spot].@frame;
			
			
			if(this.game_pitstops.length>0)
			{
				
				
				//If there are more, go ahead and do them unless cancelSpin is false
				if(this.cancelSpin)
				{
					
					this.cancelSpin = false;
					//If we are to cancel the remaining moves, we will
					//clear the arrays and display the spinner once more
					//for we have to make a new spin.
					this.game_pitstops = new Array();
					this.passoverActionList = new Array();
					
					//Seeing that we are canceling the spin we need
					//to make sure that the next place takes the
					//position of the current position.
					//Also, we need to set the Player's current
					//position.
					this.nextPlace = this.lastPlace;
					PlayerResources.currentSquare = this.lastPlace;
					
					
					
					if(this.performExtraActions)
					{
						this.performActionAfterPopupClosed();
					}
					else
					{
						//Display the spinner again.
						this.displaySpinner();
					}
				}
				else
				{
		
					
				
					this.nextPlace = //this.game_pitstops[0];
					this.game_pitstops[this.game_pitstops.length-1];
					
					if(this.performExtraActions)
					{
						this.performActionAfterPopupClosed("actionA");
					}
					else
					{
						//deal with Advertisement
						if(this.showAdvertisement)
						{
							this.dealWithAdvert(PlayerResources.currentSquare, this.moveCar);
						}
						else
						{ 
							this.moveCar();
						}
					}
					
				}	
				
			
			}
			else
			{
				
				//Ignore everything if current square is 0//Game is at the 
				//beginning.
				if(PlayerResources.currentSquare == 0 && PlayerResources.salary==0)
				{
					this.gf.emptyLayerHelper("pagesBackground");
					GeneralAnimations.animatePanelsIn(this.lyrs, this.gf.panelsInPlace);
			
				}
				else
				{
					
					if(this.performExtraActions)
					{
						this.performActionAfterPopupClosed("actionB");
					}
					else
					{
						if(this.showAdvertisement)
						{this.dealWithAdvert(PlayerResources.currentSquare, this.mainFunctionAfterAdvertisement);}
						else
						{
							//Call spinner now.
							this.displaySpinner();
						}
					}
				
				}
				
				
			
			}	
		}
		
		public function performActionsAfterSelectingCards():void
		{
		
			if(this.chosenPath == 2 )
					{
						//Display a jobs carousel here;
						PlayerResources.lifePath = "NON_GRADUATE";
						
							this.frameToStopAt = 3385;
							this.carTrack_actor
							.addEventListener("enterFrame", this.carRunning);
							this.carTrack_actor.gotoAndPlay(3360);
							
							this.nextPlace = 129;
							this.lastPlace = 129;
							PlayerResources.currentSquare = 129;
							
							
							if(this.performExtraActions)
							{
								this.performActionAfterPopupClosed();
							}
							
							
						
					}
					else
					{
						//"NON_GRADUATE";
						PlayerResources.lifePath = "GRADUATE";
						
						if(this.performExtraActions)
						{
							this.performActionAfterPopupClosed();
						}
						else
						{
							this.getReadyGoMoveCar();
						}
						
					}
					
		}
		
		private function performActionAfterPopupClosed(actionFunction:String = ""):void
		{
			//Set this back to false immediately. We dont want this hanging
			//around to disrupt the functionality.
			this.performExtraActions = false;
			
			//We don't want to keep repeating extra actions at this square,
			//so we will keep a note of this square as we just perform the action.
			this.lastExtraActionSquare = PlayerResources.currentSquare;
			
			
			var extraOptionType:String = "";
			
			
			if(this.chosenPath == 1 )
			{
				extraOptionType = 
				this.positionsXML.board_places
				.place[this.lastExtraActionSquare]
				.action.options.option1.extra;
			}
			
			if(this.chosenPath == 2 )
			{
				extraOptionType = 
				this.positionsXML.board_places
				.place[this.lastExtraActionSquare]
				.action.options.option2.extra;
			}
			
			switch(extraOptionType)
			{
				case "job":
				{
				
					this.screens.place_threeCardScreen
					("Change your job", "jobs", this.whenPopupDisplayed);
				
					break;
				}
				
				case "payrise":
				{
					
					//Add this amount to the player's salary
					var salaryIncrease:Number = this.positionsXML.board_places
					.place[this.lastExtraActionSquare]
					.action.options['option'+this.chosenPath].extra.@monies;
					
					this.gf.updateUiMajorText("Pay rise");
					this.gf.updateUiMinorText("Pay increase "+GlobalFunctions.formatMoneyToString(salaryIncrease));
					
					PlayerResources.salary +=salaryIncrease;
					
					
					switch(actionFunction)
					{
						case "actionA":
						{
							if(this.showAdvertisement)
							{
								this.dealWithAdvert(PlayerResources.currentSquare, this.moveCar);
							}
							else
							{
								this.moveCar();
							}
						}
						
						case "actionB":
						{
							if(this.showAdvertisement)
							{
								this.dealWithAdvert(PlayerResources.currentSquare, 
								this.mainFunctionAfterAdvertisement);
							}
							else
							{
								this.displaySpinner();
							}
						}
					}
					
					if(PlayerResources.currentSquare==57){this.displaySpinner();}
					
		
					break;
				}
			}
			
			
		}
		
		private function delayThenTakeFurtherAction(typeOfDelay:String):void
		{
			
			switch(typeOfDelay)
			{
				case "moveCarToSquare":
				{
					this.mainTimer = new Timer(2000, 1);
		            this.mainTimer.addEventListener(TimerEvent.TIMER, xxtimerHandler);
		            this.mainTimer.addEventListener(TimerEvent.TIMER_COMPLETE, xxcompleteHandler);
					break;
				}
			}
			
			this.mainTimer.start();
			
		}
		
		private function xxtimerHandler(evt:TimerEvent):void
		{
			this.moveToSquare();
		}
		
		private function xxcompleteHandler(evt:TimerEvent):void
		{
			//this.screenAnimatedOff(0);
		}
		
		private function moveToSquare():void
		{
			
			//this will simply move the car on to a future square
			//22 is the Get married square
			var moveto:int = 
			parseInt(this.positionsXML.board_places.
			place[PlayerResources.currentSquare].action.moveto);
			
			var newSquareNum:int = moveto - PlayerResources.currentSquare;
			
			this.setNextPlaceAndMove(newSquareNum);
			
		}
		
		public function requestGamePause():void
		{
			this.gf.pauseGame();
		}
		
		public function requestGameResumption():void
		{
			this.gf.resumeGame();
		}
	}
}