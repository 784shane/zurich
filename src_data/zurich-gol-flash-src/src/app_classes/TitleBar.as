package src.app_classes
{
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import Config;
	import src.app_classes.forms.AppForms;
	import PlayerResources;
	import src.app_classes.tools.Resources;
	import src.app_classes.GameFlow;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class TitleBar extends Sprite
	{
		
		private var gameFlow:GameFlow;
		private var ResClass:Resources;
		public var theBar:Sprite;
		//private var callBack_whenAddedToStage:Function;
		
		[Embed (source="../../res/logo/title_logo.png" )]
		protected var titleLogo:Class;
		
		
		private var marginsSideTitleText:Number = 20;
		
		
		public function TitleBar(res:Resources)
		{
			
			this.name = "theTitle";
			this.ResClass = res;
			
			//Add strip of blue around the edges of the sprite
			var borderLine:Sprite = new Sprite();
			//we are not adding a fill here as we want a hole in the
			//middle of this sprite
			borderLine.graphics.lineStyle(2, Config.APP_COLORS.DARK_BLUE);
			//borderLine.graphics.lineStyle(4, 0xBB0000);
			borderLine.graphics.drawRect(1, 1, 
										Config.SCREEN_CONFIG.width-2, 
										Config.SCREEN_CONFIG.height-2);
			borderLine.graphics.endFill();
			this.addChild(borderLine);
			
			
			this.theBar = this.buildTitleBar()
			this.addChild(this.theBar);
			//titleBar.y = 400;
		}
		
		private function buildTitleBar():Sprite
		{
			var titleBar:Sprite = new Sprite();
			titleBar.name = "titleBar";
			
			//This is the background for the title bar
			var blueStrip:Sprite = new Sprite();
			blueStrip.graphics.beginFill(Config.APP_COLORS.DARK_BLUE, 1);
			//blueStrip.graphics.beginFill(0xBB0000);
			blueStrip.graphics.drawRect(0, 0, Config.SCREEN_CONFIG.width, 50);
			blueStrip.graphics.endFill();
			titleBar.addChild(blueStrip);
			
			
			var titleLogo:Bitmap = new titleLogo() as Bitmap;
			titleBar.addChild(titleLogo);
			titleLogo.x=24;
			titleLogo.y=4;
			
			
			
			var titleButtons:Sprite = this.getTextsInTitle();
			titleBar.addChild(titleButtons);
			/*titleButtons.x=24;
			titleButtons.y=3;*/
			
			
			
			return titleBar;
		}
		
		private function getTextsInTitle():Sprite
		{
			var aF:AppForms = new AppForms();
			var titleTextSpr:Sprite = new Sprite();
			
			
			
			
			var prizesSpr:Sprite = 
			getTitleButton("PRIZES");
			titleTextSpr.addChild(prizesSpr);
			prizesSpr.x = 364;
			prizesSpr.name = "title_prizes";
			
			this.ResClass.prizesOnState = 
			(prizesSpr.getChildByName("buttonOn") as Sprite);
			
			
			
			var howToPlay:Sprite = 
			getTitleButton("HOW TO PLAY");
			titleTextSpr.addChild(howToPlay);
			howToPlay.x = 518;
			howToPlay.name = "title_howToPlay";
			
			this.ResClass.htpOnState = 
			(howToPlay.getChildByName("buttonOn") as Sprite);
			
			
			
			var leaderBoard:Sprite = 
			getTitleButton("LEADERBOARD");
			titleTextSpr.addChild(leaderBoard);
			leaderBoard.x = 672;
			leaderBoard.name = "title_leaderboard";
			this.ResClass.leadOnState = 
			(leaderBoard.getChildByName("buttonOn") as Sprite);
			
			
			
			this.ResClass.lgnTextBoxSprite = 
			getTitleButton("LOGIN");
			titleTextSpr.addChild(this.ResClass.lgnTextBoxSprite);
			this.ResClass.lgnTextBoxSprite.x = 826;
			this.ResClass.lgnTextBoxSprite.name = "lgnTextBox";
			this.ResClass.loginOnState = 
			(this.ResClass.lgnTextBoxSprite.getChildByName("buttonOn") as Sprite);
			//lgnTextBox
			
			
			
			prizesSpr.addEventListener(MouseEvent.CLICK, this.buttonClicked);
			prizesSpr.addEventListener(MouseEvent.MOUSE_OVER, this.mousedEvents);
			prizesSpr.addEventListener(MouseEvent.MOUSE_OUT, this.mousedEvents);
			
			howToPlay.addEventListener(MouseEvent.CLICK, this.buttonClicked);
			howToPlay.addEventListener(MouseEvent.MOUSE_OVER, this.mousedEvents);
			howToPlay.addEventListener(MouseEvent.MOUSE_OUT, this.mousedEvents);
			
			leaderBoard.addEventListener(MouseEvent.CLICK, this.buttonClicked);
			leaderBoard.addEventListener(MouseEvent.MOUSE_OVER, this.mousedEvents);
			leaderBoard.addEventListener(MouseEvent.MOUSE_OUT, this.mousedEvents);
			
			//I am using a dictionary to keep track of my listeners here
			//Here, I am keeping this buttonClicked listener in the 
			//Listener Dictionary
			//logoutClicked
			this.ResClass.listenerDictionary.setCLICKListener("logout",this.logoutClicked);
			this.ResClass.listenerDictionary.setCLICKListener("login",this.buttonClicked);
			this.ResClass.lgnTextBoxSprite.addEventListener(MouseEvent.CLICK, 
			this.ResClass.listenerDictionary.getCLICKListener("login")
			);
			this.ResClass.lgnTextBoxSprite
			.addEventListener(MouseEvent.MOUSE_OVER, this.mousedEvents);
			this.ResClass.lgnTextBoxSprite
			.addEventListener(MouseEvent.MOUSE_OUT, this.mousedEvents);
			
			return titleTextSpr;
		}
		
		public function getTitleButton(buttonText:String):Sprite
		{
			var aF:AppForms = new AppForms();
			var howToPlay:Sprite = new Sprite();
			//titleTextSpr.addChild(howToPlay);
			
			
			var howToPlayBack:Sprite = new Sprite();
			howToPlayBack.graphics.beginFill(0x232c60,1);
			howToPlayBack.graphics.drawRect
			(0, 0, 154, 50);
			howToPlayBack.graphics.endFill();
			howToPlay.addChild(howToPlayBack);
			howToPlayBack.name = "buttonMOver";
			howToPlayBack.alpha = 0;
			
			
			var buttonOn:Sprite = new Sprite();
			buttonOn.graphics.beginFill(0x232c60,1);
			buttonOn.graphics.drawRect
			(0, 0, 154, 50);
			buttonOn.graphics.endFill();
			howToPlay.addChild(buttonOn);
			buttonOn.name = "buttonOn";
			buttonOn.alpha = 0;
			
			
			var htpTextBox:TextField = aF.getTitleText();
			howToPlay.addChild(htpTextBox);
			htpTextBox.text = buttonText;
			htpTextBox.name = "txtBox"
			//Indent the text a bit
			htpTextBox.x = (154-htpTextBox.width)/2;//this.marginsSideTitleText;
			htpTextBox.y = 11;
			howToPlay.buttonMode = true;
			howToPlay.mouseChildren = false;
			
			
			return howToPlay;
			
		}
		
		public function getTitleBar():Sprite
		{
			return this;
		}
		
		public function setGameFlowInstance(gmFlow:GameFlow):void
		{
			this.gameFlow = gmFlow;
		}
		
		private function mousedEvents(evt:MouseEvent):void
		{
			switch(evt.type)
			{
				case "mouseOver":
				{
					(evt.target.getChildByName("buttonMOver") as Sprite)
					.alpha = 1;
					break;
				}
				
				case "mouseOut":
				{
					(evt.target.getChildByName("buttonMOver") as Sprite)
					.alpha = 0;
					break;
				}
			}
		}
		
		private function buttonClicked(evt:MouseEvent):void
		{
			switch(evt.target.name)
			{
				
				case "title_prizes":
				{
					this.gameFlow.getScreen("PRIZES");
					break;
				}
				
				case "title_howToPlay":
				{
					this.gameFlow.howtoplayClicked();
					break;
				}
				
				case "title_leaderboard":
				{
					this.gameFlow.leaderboardClicked();
					break;
				}
				
				case "title_login":
				{
					this.gameFlow.logInClicked();
					break;
				}
				
				default:{}
			}
		}
		
		private function logoutClicked(evt:MouseEvent):void
		{
			this.gameFlow.logOutClicked();
		}	
		
	}
}