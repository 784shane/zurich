package src.app_classes.screens
{
	
	import flash.text.TextField;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	
	import src.app_classes.forms.AppForms;
	import Config;
	import src.app_classes.tools.PictureLoader;
	import GlobalFunctions;
	import src.app_classes.GameFlow;
	
	//temp
	//import src.app_classes.tools.KeyboardControlTool;
	
	public class PlayerSelect extends AppForms
	{
		
		private var tf:TextField;
		private var gf:GameFlow;
		private var playerAvatarsLoaded:Array = new Array();
		private var noOfImagesToLoad:int = 0;
		private var imagesLoaded:int = 0;
		private var allImagesLoaded:Boolean = false;
		
		private var onLoadCompleted:Function;
		public function PlayerSelect(tf:TextField, loadCompleteFunction:Function, gf:GameFlow)
		{
			this.onLoadCompleted = loadCompleteFunction;
			this.tf = tf;
			this.gf = gf;
		}
		
		public function loadAvatarImages():void
		{
			var imagesToLoad:Array = Config.avatarImages.selectPlayerImages;
			this.noOfImagesToLoad = imagesToLoad.length;
			
			for(var i:int=0; i<this.noOfImagesToLoad; i++)
			{
				var pl:PictureLoader = new PictureLoader(this.carImagesLoaded);
				var img:String = imagesToLoad[i].src;
				pl.load_image(imagesToLoad[i].name,img,0);
			}
		}
		
		private function carImagesLoaded(cardReturned:PictureLoader, f:Object):void
		{
			
				var imgObj:Object = new Object();
				imgObj.avatar = cardReturned.content;
				imgObj.id = f.id;
				this.playerAvatarsLoaded.push(imgObj);
			
				
			if(this.imagesLoaded==(this.noOfImagesToLoad-1))
			{ 
				//this.tf.text = this.playerAvatarsLoaded.length.toString();
				this.allImagesLoaded = true;
				this.onLoadCompleted();
			}
			
			this.imagesLoaded++;
		}
		
		public function getSelectPlayerScreen():Sprite
		{
			var screen:Sprite = new Sprite();
			//screen.addChild(GlobalFunctions.getBackDrop());
			
			
			
			var poppupTitleTextSpr:Sprite = new Sprite();
			var poppupTitleText:TextField = this.popUpTitleText();
			poppupTitleTextSpr.addChild(poppupTitleText);
			poppupTitleText.text = "Select your player";
			poppupTitleTextSpr.x = 201;
			poppupTitleTextSpr.y = 151;
			screen.addChild(poppupTitleTextSpr);
			
			//var ff:TextField = new TextField();
			
			
			
			
			
			
			var imagesToLoad:Array = Config.avatarImages.selectPlayerImages;
			
			for(var i:int = 0; i<this.playerAvatarsLoaded.length; i++)
			{
				var spr1:Sprite = new Sprite();
				var ff:Bitmap = this.playerAvatarsLoaded[i].avatar;
				spr1.addChild(ff);
				
				
				screen.addChild(spr1);
				
				for(var j:int = 0; j<imagesToLoad.length; j++)
				{
					if(imagesToLoad[j].name == this.playerAvatarsLoaded[i].id)
					{
						spr1.x = imagesToLoad[j].positions.x;
						spr1.y = imagesToLoad[j].positions.y;
						break;
					}
				}
				
				//new KeyboardControlTool(spr1);
				spr1.name = this.playerAvatarsLoaded[i].id;
				spr1.addEventListener(MouseEvent.CLICK, this.mousedActor);
				spr1.addEventListener(MouseEvent.MOUSE_OVER, this.mousedActor);
				spr1.addEventListener(MouseEvent.MOUSE_OUT, this.mousedActor);
				spr1.mouseChildren = false;
				spr1.buttonMode = true;
				
				var wealthCardExternal:Sprite = getSelectActorRing(65, 3);
				spr1.addChild(wealthCardExternal);
				wealthCardExternal.x = 65;
				wealthCardExternal.y = 65;
				wealthCardExternal.alpha=0;
				
				
			
			
				
				
			}
			
			return screen;
		}
		
		private function mousedActor(evt:MouseEvent):void
		{
			
			switch(evt.type)
			{
				case "mouseOver":
				{
					((evt.target)
					.getChildByName("actor_glow") as Sprite)
					.alpha = 1;
					break;
				}
				
				case "mouseOut":
				{
					((evt.target)
					.getChildByName("actor_glow") as Sprite)
					.alpha = 0;
					break;
				}
				
				case "click":
				{
					((evt.target) as Sprite).y -=10;
					this.gf.playerIconSelected(evt);
					//spr1.addEventListener(MouseEvent.CLICK, 
					//this.gf.playerIconSelected);
					break;
				}
			}
			
		}
	
	}
	
}