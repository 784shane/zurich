package src.app_classes.screens
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	
	import src.app_classes.appLayers;
	import src.app_classes.tools.Resources;
	import src.app_classes.screens.Cards.TwoCardScreen;
	import src.app_classes.screens.SpinToWinScreen;
	import PlayerResources;
	import src.app_classes.screens.Cards.RetireScreen;
	
	import src.app_classes.screens.SpinToWinBackdrop;
	
	import src.app_classes.GamePlay;
	import GlobalFunctions;
	
	public class Screens
	{
		
		private var layers:appLayers;
		private var Res:Resources;
		private var twoCdScn:TwoCardScreen;
		private var counter:int = 0;
		private var tff:TextField;
		private var loadScreenFromResource:Boolean = false;
		private var TwoScreenMsg:String;
		private var retireScrn:RetireScreen;
		
		private var whenPopupDisplayed:Function;

		private var gp:GamePlay;
		private var stw:SpinToWinBackdrop;
		
		public function Screens(gp:GamePlay, lyrs:appLayers, theRes:Resources)
		{
			this.retireScrn = new RetireScreen();
			this.Res = theRes;
			this.layers = lyrs;
			this.gp = gp;
			this.stw = new SpinToWinBackdrop(this.gp);
		}
		
		public function place_twoCardScreen(msg:String, display_type:String, whenScreenReady:Function):void
		{
			
			
			this.TwoScreenMsg = msg;
			this.whenPopupDisplayed = whenScreenReady;
			
			
			this.twoCdScn = new TwoCardScreen(this.gp, this.Res, this.TwoCardScreens_Loaded, "two-card", msg, display_type);
			
			
		}
		
		public function place_threeCardScreen(msg:String, display_type:String, whenScreenReady:Function):void
		{
			this.TwoScreenMsg = msg;
			//When screen is ready we will let alert this callback function
			this.whenPopupDisplayed = whenScreenReady;
			
			if(this.Res.threeCardScreenBox == null)
			{
				this.loadScreenFromResource = false;
				this.twoCdScn = new TwoCardScreen(this.gp, this.Res, this.ThreeCardScreens_Loaded, "three-card", msg, display_type);
			}
			else
			{
				//If the resource already contains an instance of two screen
				//we will just load it
				this.loadScreenFromResource = true;
				this.ThreeCardScreens_Loaded();
			}
			
		}
		
		public function place_retireScreen(whenRetireChoiceMade:Function):void
		{
			
			/*
			this.layers.getLayer("forms").alpha =1;
			var tf:TextField = new TextField();
			*/
			this.layers.getLayer("forms").addChild(retireScrn.getRetireScreen(whenRetireChoiceMade));
		}
		
		public function placeWealthCardSelectScreen(screenTitle:String):void
		{
			
			
			
			//this.emptyLayer("forms");
			//this.layers.getLayer("forms").alpha=1;
			this.layers.getLayer("forms")
			.addChild(new WealthCardSelect(this.gp, this.gp.ingame_wealthCardSelected,true)
			.getWealthCardSelectScreen(screenTitle, 1));
			
			////this.gf.showWealthCardsScreen(squareTitle);this.wealth_Card_Selected
			
			//We need to select one card here, but for testing purposes lets select three
			//for now
			//.getWealthCardSelectScreen(screenTitle, 3));
			
			//This two card screen is added to the form layer here
			//this.layers.getLayer("forms").addChild(this.twoCdScn.getTwoCardScreen(gp.screenClosed));
		}
		
		private function wealth_Card_Selected(obj:Object):void
		{
			
		}
		
		private function ThreeCardScreens_Loaded():void
		{
			//This two card screen is added to the form layer here
			this.layers.getLayer("forms").addChild(this.twoCdScn.getTwoCardScreen(gp.screenClosed));
		}
		
		private function TwoCardScreens_Loaded():void
		{
			//We will get the twoCardScreen Sprite here
			//We are saving it in one of our resources for we do not
			//want to load this again but load it from the resources
			//instead.
			if(!this.loadScreenFromResource)
			{
			this.Res.twoCardScreenBox = this.twoCdScn.getTwoCardScreen(gp.screenClosed);
			}
			
			//this.Res.TwoCardScreenMajorText.text = this.TwoScreenMsg;
			
			this.Res.twoCardScreenBox.addEventListener("addedToStage", CardScreenAdded);
			
			//This two card screen is added to the form layer here
			this.layers.getLayer("forms").addChild(this.Res.twoCardScreenBox);
			
		}
		
		private function CardScreenAdded(evt:Event):void
		{
			this.whenPopupDisplayed();
		}
		
		
		public function getSavedTwoCardScreen():Sprite
		{
			return this.Res.twoCardScreenBox;
		}
		
		public function placeSpinToWinScreen(typeOfSpin:String = "spinToWin"):void
		{
			GlobalFunctions.emptyContainer(this.layers.getLayer("spinToWin"));
			this.layers.getLayer("spinToWin").alpha=1;
			this.layers.getLayer("spinToWin").addChild(this.stw.buildSpinToWinBackdrop(typeOfSpin));
			
		}
		
		public function placeSpinnerScreen():void
		{
			//This will launch the spinner for the game
			GlobalFunctions.emptyContainer(this.layers.getLayer("spinToWin"));
			this.layers.getLayer("spinToWin").alpha=1;
			this.layers.getLayer("spinToWin").addChild(this.stw.buildSpinToWinBackdrop("regular"));	
			
		}
	
	}
	
}