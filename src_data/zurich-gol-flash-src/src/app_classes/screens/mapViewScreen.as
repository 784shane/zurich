package src.app_classes.screens
{
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	
	import src.app_classes.GamePlay;
	import src.app_classes.tools.Resources;
	import GlobalFunctions;
	import PlayerResources;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class mapViewScreen extends MovieClip
	{
		
		[Embed (source="../../../res/mapView/cross.png" )]
		protected var mapViewCross:Class;
		
		private var resClass:Resources;
		
		private var gp:GamePlay;
		
		public function mapViewScreen(gp:GamePlay, res:Resources)
		{
			this.resClass = res;
			this.gp = gp;
		}
		
		public function getMapViewScreen():MovieClip
		{
			
			this.addChild(GlobalFunctions.getBackDrop());
			
			if(this.resClass.mapViewMC == null)
			{
			this.resClass.mapViewMC = new mapView() as MovieClip;
			}
			
			this.addChild(this.resClass.mapViewMC);
			this.resClass.mapViewMC.addEventListener(Event.ENTER_FRAME,this.mv_frameRunning);
			this.resClass.mapViewMC.x = 490;
			this.resClass.mapViewMC.y = 398.5;
			
			var bm:Bitmap = new mapViewCross() as Bitmap;
			var bmm:Sprite = new Sprite();
			bmm.addChild(bm);
			this.addChild(bmm);
			bmm.buttonMode = true;
			bmm.x = 920;
			bmm.y = 169;
			
			bmm.addEventListener(MouseEvent.CLICK,this.crossClicked);			
			
			return this;
		}
		
		private function mv_frameRunning(evt:Event):void
		{
			evt.target.stop();
			evt.target.removeEventListener(Event.ENTER_FRAME,this.mv_frameRunning);
			evt.target.gotoAndStop(PlayerResources.currentSquare);
		}
		
		private function crossClicked(evt:MouseEvent):void
		{
			//resume game
			this.gp.requestGameResumption();
			
			this.parent.removeChild(this);
		}
	
	}
	
}