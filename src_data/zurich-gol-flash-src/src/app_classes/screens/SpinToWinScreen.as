package src.app_classes.screens
{
	import flash.display.Sprite;
	import GlobalFunctions;
	import src.app_classes.tools.KeyboardControlTool;
	
	public class SpinToWinScreen extends Sprite
	{
		
		[Embed (source="../../../res/trash/spinToWin/spintowin_labelGraphic.png" )]
		protected var spintowinGraphic:Class;
		
		[Embed (source="../../../res/trash/spinToWin/spintowin_Spinner.png" )]
		protected var spintowinSpinner:Class;
		
		public function SpinToWinScreen()
		{
			this.addChild(this.buildSpinToWinScreen());
		}
		
		private function buildSpinToWinScreen():Sprite
		{
			
			//Add backdrop to this screen
			this.addChild(GlobalFunctions.getBackDrop());
			
			var spinToWinContainer:Sprite = new Sprite();
			
			var spinToWinGraphic:Sprite = new Sprite();
			spinToWinGraphic.addChild(new spintowinGraphic());
			spinToWinContainer.addChild(spinToWinGraphic);
			spinToWinGraphic.x = 224;
			spinToWinGraphic.y = 155;
			
			var spintowinSpinnerVar:Sprite = new Sprite();
			spintowinSpinnerVar.addChild(new spintowinSpinner());
			spinToWinContainer.addChild(spintowinSpinnerVar);
			spintowinSpinnerVar.x = 300;
			spintowinSpinnerVar.y = 236;
			
			//new KeyboardControlTool(spintowinSpinnerVar);
			
			return spinToWinContainer;
		}
	
	}
	
}