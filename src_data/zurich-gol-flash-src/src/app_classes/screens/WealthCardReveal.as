package src.app_classes.screens
{
	
	import flash.text.TextField;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	
	import src.app_classes.forms.AppForms;
	import GlobalFunctions;
	import GeneralAnimations;
	import PlayerResources;
	import src.app_classes.GameFlow;
	
	import src.app_classes.tools.KeyboardControlTool;
			
	
	public class WealthCardReveal extends AppForms
	{
		
		[Embed (source="../../../res/wealthCards/wealthCards/0.png" )]
		protected var shareWealth1:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/1.png" )]
		protected var shareWealth2:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/2.png" )]
		protected var shareWealth3:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/3.png" )]
		protected var shareWealth4:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/4.png" )]
		protected var shareWealth5:Class;
		
		//../../../res/wealthCards/possible_cards/shareWealth3.png
		
		
		
		
		private var gf:GameFlow;
		
		private var showingType:String = "default";
		
		private var wealthCardRemoved:Sprite;
		
		private var numberRemainingAfterWCSpent:int = 0;
		
		private var carouselCardsShifted:int = 0;
		
		public function WealthCardReveal(gf:GameFlow)
		{
			this.gf = gf;
		}	
		
		public function getWealthCardRevealScreen(typeOfShowing:String = "default"):Sprite
		{
			
			this.showingType = typeOfShowing;
			
			var revealScreen:Sprite = new Sprite();
			
			revealScreen.addChild(GlobalFunctions.getBackDrop());
			
			//place screen title.
			var titleSpr:Sprite = this.getScreenTitleText();
			revealScreen.addChild(titleSpr);
			titleSpr.x = 190;
			titleSpr.y = 156;
			
			
			//PlayerResources.player_wealthCards
			
			//Place carousel here now.
			var carousel:Sprite = getPopupCarousel(this.carouselSprites(), this.showingType)
			revealScreen.addChild(carousel);
			carousel.x = 148;
			carousel.y = 231;
			
			//Add the ok Button
			var okButton:Sprite = getWealthRevealButton();
			revealScreen.addChild(okButton);
			okButton.x = 401;
			okButton.y = 569;
			
			//new KeyboardControlTool(okButton);
			
			return revealScreen;
		}	
		
		public function carouselSprites():Array
		{
			//temporarily extend the number of wealth cards to test shuffle;
			//PlayerResources.player_wealthCards = new Array(4, 1, 0, 3, 2, 1);
			
			//We will be using a new array called wealthCards here since
			//we might have to shuffle it if we only need to highlight
			//only the usable ones.
			var wealthCards:Array = PlayerResources.player_wealthCards;
			var cardsNeeded:Array;
			if(this.showingType == "useable_cards")
			{
				cardsNeeded = this.checkForNeededCards();
				//Shuffle wealth cards so we can have the useable cards
				//to the front of the line.
				wealthCards = this.reshuffleWealthCards(wealthCards, cardsNeeded);
				//Now that we have shuffled the cards, we will replace the old
				//card arrangement with this new arrangement.
				PlayerResources.player_wealthCards = wealthCards;
				
			}
			
			
			
			var carouselArr:Array = new Array();
			
			for(var i:int = 0; i<wealthCards.length; i++)
			{
				
				
				
				var disableCards:Boolean = true;
				
				if(this.showingType == "useable_cards")
				{
					for(var cn:int = 0; cn<cardsNeeded.length; cn++)
					{
						if(GlobalFunctions.getWealthCardNameFromNumber(wealthCards[i])
						== cardsNeeded[cn])
						{
							disableCards = false;
							break;
						}
					}	
				}
				
				
				//Get the card index thats stored in the saved array
				var cardIndex:int = wealthCards[i];
				var cards:Sprite = new Sprite();
				cards.addChild(this.getAppropriateCaourselImg(cardIndex));
				cards.buttonMode = true;
				
				if(this.showingType == "useable_cards")
				{
					//We want certain stuff to be applied to the wealthCardsToBeSpent
					//here. For example, some will be highlighted when moused over, clickable,
					//etc.
					
					if(!disableCards)
					{
						cards.name = "wealthCard_"+cardIndex;
						cards.addEventListener(MouseEvent.CLICK, this.wealthCardToSpend);
					}
					else
					{
						cards.alpha = 0.2; 
						cards.buttonMode = false;
					}
				}
				
				
				carouselArr.push(cards);	
			}
			
			return carouselArr;
		}
		
		private function reshuffleWealthCards(wc:Array, cardsNeeded:Array):Array
		{
			var cardsWehaveFirst:Array = new Array();
			var otherCardsWehave:Array = new Array();
			
			for(var i:int = 0; i<wc.length; i++)
			{
				var placeInNewArray:Boolean = true;
				
				for(var cn:int = 0; cn<cardsNeeded.length; cn++)
				{
					if(GlobalFunctions.getWealthCardNameFromNumber(wc[i])
						== cardsNeeded[cn])
					{
						cardsWehaveFirst.push(wc[i]);
						placeInNewArray = false;
						break;
					}
				}
				
				if(placeInNewArray)
				{
					otherCardsWehave.push(wc[i]);
				}
				
			}
			
			//Merge the cards we have with the other cards so that we
			//can shuffle the cards with the needed cards(cards to be highlighted, first)
			//then all other cards after.
			return cardsWehaveFirst.concat(otherCardsWehave);
			
		}
		
		
		private function checkForNeededCards():Array
		{
			
			var cardsNeeded:Array = new Array();
			var theXML:XML = this.gf.getMainXML();
			
			
			
			var presentSpot:int = PlayerResources.currentSquare;
			
			for(var i:int = 0; 
			i<theXML.board_places.place[presentSpot]
			.action.wealthCards.cardOptions.card.length();
			i++)
			{
				cardsNeeded.push(theXML.board_places.place[presentSpot]
				.action.wealthCards.cardOptions.card[i]);
			}
			
			return cardsNeeded;
			
		}
		
		private function getAppropriateCaourselImg(imgNo:int):Bitmap
		{
			switch(imgNo)
			{
				case 0:
				{
					return new shareWealth1() as Bitmap;
					break;
				}
				
				case 1:
				{
					return new shareWealth2() as Bitmap;
					break;
				}
				
				case 2:
				{
					return new shareWealth3() as Bitmap;
					break;
				}
				
				case 3:
				{
					return new shareWealth4() as Bitmap;
					break;
				}
				
				case 4:
				{
					return new shareWealth5() as Bitmap;
					break;
				}
				
				default:
				{
					return new shareWealth2() as Bitmap;
				}
			}
			
			return new Bitmap();
		}
		
		private function getScreenTitleText():Sprite
		{
			var poppupTitleTextSpr:Sprite = new Sprite();
			var poppupTitleText:TextField = this.popUpTitleText();
			poppupTitleTextSpr.addChild(poppupTitleText);
			
			if(this.showingType == "useable_cards")
			{
				poppupTitleText.text = "Choose a card.";
			}
			else
			{
				poppupTitleText.text = "You have chosen . . .";
			}	
			
			return poppupTitleTextSpr
		}
		
		private function getWealthRevealButton():Sprite
		{
			
			var buttonTxt:String = "Ok";
			
			if(this.showingType == "useable_cards"){buttonTxt = "Use later";}
			
			var OkButton:Sprite = getFormButton(buttonTxt);
			
			if(this.showingType == "useable_cards")
			{OkButton.addEventListener(MouseEvent.CLICK, this.gf.mainGamePlay.decideToSpendWealthCardLater);}
			else
			{OkButton.addEventListener(MouseEvent.CLICK, this.OkButtonClicked);}
			
			return OkButton;	
		}
		
		private function OkButtonClicked(evt:MouseEvent):void
		{
			this.gf.cardsRevealOkClicked();
		}
		
		private function wealthCardToSpend(evt:MouseEvent):void
		{
			
			var indexUsed:String = evt.currentTarget.name.substr(11);
			
			//wealthCard_
			//We need to remove this from the card display on screen.
			//We must also, remove it from the resouces.
			
			//Remove it from the PlayerResources array. Remember to only remove
			//one instance of it
			for(var i:int = 0; i<PlayerResources.player_wealthCards.length; i++)
			{
				if(PlayerResources.player_wealthCards[i] == indexUsed)
				{
					//remove item from array
					PlayerResources.player_wealthCards.splice(i,1);
					break;
				}
			}
			
			
			//Now that we have removed the item from the array, lets remove the item from
			//the screen. We will first make it invisible. One that is done, we will
			//reposition it on screen
			this.wealthCardRemoved = evt.currentTarget as Sprite;
			
			GeneralAnimations.basic_displayItemToggle
			(this.wealthCardRemoved, 
			this.wealthCardRemovedFromScreen,
			0);
			
			
		}
		
		private function wealthCardRemovedFromScreen():void
		{
			//get Container
			var parentOfRemovedSpr:Sprite = this.wealthCardRemoved.parent as Sprite;
			
			//Now that we have erased the card, lets remove it from the parent container.
			parentOfRemovedSpr.removeChild(this.wealthCardRemoved);
			
			
			
			
			
			this.numberRemainingAfterWCSpent = parentOfRemovedSpr.numChildren;
			var newXpos:Number = 0;
			switch(this.numberRemainingAfterWCSpent)
			{
				case 1:{newXpos = (595/2)-(185/2); break;}
				case 2:{newXpos = (595-((185*2)+20))/2; break;}
			}
			
			var sprSpace:Number = 20;
			for(var i:int = 0; i<this.numberRemainingAfterWCSpent; i++)
			{
				var actorSprite:Sprite = 
				(parentOfRemovedSpr.getChildAt(i) as Sprite);
				
				GeneralAnimations.positionTo(
				actorSprite, 
				this.wealthCardsRepositioned,
				newXpos,
				actorSprite.y);
				
				newXpos+=actorSprite.width+sprSpace;
			}
			
		}
		
		private function wealthCardsRepositioned():void
		{
			if(this.carouselCardsShifted==(this.numberRemainingAfterWCSpent-1))
			{
				
				//this.gf.tf.text = "done now";
				//this.OkButtonClicked();
				this.gf.mainGamePlay.afterSpendingWealthCard();
			}
			this.carouselCardsShifted++;
		}
		
	
	}
	
}