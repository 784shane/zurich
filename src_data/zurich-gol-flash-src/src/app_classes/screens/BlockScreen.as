package src.app_classes.screens
{
	//import flash.display.Bitmap;
	import flash.display.Sprite;
	
	public class BlockScreen extends Sprite
	{
		
		[Embed (source="../../../res/trash/blockScreen.jpg" )]
		protected var blockScreenImg:Class;
		
		public function BlockScreen()
		{
			this.addChild(new blockScreenImg());
		}
	
	}
	
}