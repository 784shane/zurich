package src.app_classes.screens
{
	import flash.text.TextField;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import mx.collections.ArrayList;
	import flash.display.Bitmap;
	
	
	import src.app_classes.forms.AppForms;
	import GlobalFunctions;
	import Config;
	import src.app_classes.tools.PictureLoader;
	import PlayerResources;
	//temp
	//import src.app_classes.tools.KeyboardControlTool;
	
	public class CarSelect extends AppForms
	{
		
		//[Embed (source="choosecar.jpg" )]
		private var carScreen:Class;
		
		
		private var carRing:Object;
		private var carsArray:Array = new Array();
		
		private var tf:TextField;
		private var noOfCarsToLoad:int;
		private var carsloaded:int = 0;
		private var allCarImagesLoaded:Boolean = false;
		private var notifyOnCarSelection:Function;
		
		public function CarSelect(tf:TextField)
		{
			this.tf = tf;
		
		}
		
		public function loadCarsImages():void
		{
			
			var carsToLoad:ArrayList = Config.carSelectImagesToLoad;
			this.noOfCarsToLoad = carsToLoad.length;
			
			for(var i:int=0; i<carsToLoad.length; i++)
			{
				var pl:PictureLoader = new PictureLoader(this.carImagesLoaded);
				var img:String = carsToLoad.getItemAt(i).src;
				pl.load_image(carsToLoad.getItemAt(i).car,img,0);
			}
			
		}
		
		private function carImagesLoaded(cardReturned:PictureLoader, f:Object):void
		{
			
			if(f.id == "car_ring"){this.carRing = cardReturned; }
			else
			{
				var carObj:Object = new Object();
				carObj.carItem = cardReturned.content;
				carObj.id = f.id;
				this.carsArray.push(carObj);
			}
				
			if(this.carsloaded==(this.noOfCarsToLoad-1))
			{ 
				this.allCarImagesLoaded = true;
			}
			
			this.carsloaded++;
		}
		
		public function getCarPickerScreen(carSelectedOnClick:Function):Sprite
		{
			
			this.notifyOnCarSelection = carSelectedOnClick;
			
			var carsToLoad:ArrayList = Config.carSelectImagesToLoad;
			
			var spr1:Sprite = new Sprite();
			
			//spr1.addChild(GlobalFunctions.getBackDrop());
			
			
			var poppupTitleTextSpr:Sprite = new Sprite();
			var poppupTitleText:TextField = this.popUpTitleText();
			poppupTitleTextSpr.addChild(poppupTitleText);
			poppupTitleText.text = "Select a car";
			poppupTitleTextSpr.x = 193;
			poppupTitleTextSpr.y = 151;
			spr1.addChild(poppupTitleTextSpr);
			
			
			
			
			var spr:Sprite = new Sprite();
			spr1.addChild(spr);
			
			for(var i:int=0; i<this.carsArray.length; i++)
			{
				var g:Sprite = getCarIcon(i);
				spr.addChild(g);
				
				for(var ctl:int=0; ctl<carsToLoad.length; ctl++)
				{
					if(carsToLoad.getItemAt(ctl).car != "car_ring" 
					&& this.carsArray[i].id == carsToLoad.getItemAt(ctl).car)
					{
						g.x = carsToLoad.getItemAt(ctl).pos.x;
						g.y = carsToLoad.getItemAt(ctl).pos.y;
						break;
					}
					
				}

			}
			
			spr.x=236;
			spr.y=234;
			return spr1;
		}
		
		private function getCarIcon(i:int):Sprite
		{
			var spr:Sprite = new Sprite();
			
			
			
				
			var wealthCardExternal:Sprite = getSelectActorRing(65, 3);
			spr.addChild(wealthCardExternal);
			wealthCardExternal.x = 89;
			wealthCardExternal.y = 68;
			wealthCardExternal.alpha=0;
				
				
			//spr.addChild(carsArray[i].carObj);
			var carBackground:Bitmap = new Bitmap(Bitmap(this.carRing.content).bitmapData);
			var coloredCar:Bitmap = carsArray[i].carItem;
			
			spr.addChild(carBackground);
			spr.addChild(coloredCar);
			//coloredCar.x = -20;
			coloredCar.y = 36;
			carBackground.x = 20;
			
			spr.buttonMode = true;
			spr.name = carsArray[i].id;
			//spr.addEventListener(MouseEvent.CLICK, this.carSelected);
			spr.addEventListener(MouseEvent.CLICK, this.mousedActor);
			spr.addEventListener(MouseEvent.MOUSE_OVER, this.mousedActor);
			spr.addEventListener(MouseEvent.MOUSE_OUT, this.mousedActor);
			spr.mouseChildren = false;
			return spr;
		}
		
		private function mousedActor(evt:MouseEvent):void
		{
			
			switch(evt.type)
			{
				case "mouseOver":
				{
					((evt.target)
					.getChildByName("actor_glow") as Sprite)
					.alpha = 1;
					break;
				}
				
				case "mouseOut":
				{
					((evt.target)
					.getChildByName("actor_glow") as Sprite)
					.alpha = 0;
					break;
				}
				
				case "click":
				{
					((evt.target) as Sprite).y -=10;
					this.carSelected(evt);
					//spr1.addEventListener(MouseEvent.CLICK, 
					//this.gf.playerIconSelected);
					break;
				}
			}
			
		}
		
		private function carSelected(evt:MouseEvent):void
		{
			PlayerResources.player_car = evt.target.name;
			this.notifyOnCarSelection();
		}

		
	
	}
	
}