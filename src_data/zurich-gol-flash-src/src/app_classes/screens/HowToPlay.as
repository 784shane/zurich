package src.app_classes.screens
{
	import flash.text.TextField;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	
	
	import src.app_classes.forms.AppForms;
	import Config;
	import src.app_classes.tools.ClassImageLoader;
	import GlobalFunctions;
	import src.app_classes.GameFlow;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class HowToPlay extends Sprite
	{
		private var loadScreenResources:Array = new Array();
		private var afterResourcesLoaded:Function;
		private var funcWhenExitButtonClicked:Function;
		
		private var aF:AppForms;
		private var classImgLdr:ClassImageLoader;
		
		private var gf:GameFlow;
		
		private var cccB:Sprite;
		
		public function HowToPlay(gf:GameFlow, whenExitClicked:Function )
		{
			this.gf = gf;
			this.funcWhenExitButtonClicked = whenExitClicked;
			this.aF = new AppForms();
		}
		
		public function loadScreenImages(functionAfterResLoaded:Function):void
		{
			
			this.classImgLdr = new ClassImageLoader();
			this.afterResourcesLoaded = functionAfterResLoaded;
			this.classImgLdr.saveImages
			(
				this.calledUponImagesSaved,
				Config.howToPlayImages.images
			);
		}
		
		private function calledUponImagesSaved(imagesSaved:Array):void
		{
			this.loadScreenResources = imagesSaved;
			this.afterResourcesLoaded();
		}
		
		public function getHowToPlayScreen(displayBackground:Boolean):Sprite
		{
			this.gf.setAppropriateTab("", "off");
			this.gf.setAppropriateTab("how_to_play", "on");
			
			if(displayBackground)
			{
				this.addChild(GlobalFunctions.getBackDrop());
			}
			
			var poppupTitleTextSpr:Sprite = new Sprite();
			var poppupTitleText:TextField = aF.popUpTitleText();
			poppupTitleTextSpr.addChild(poppupTitleText);
			this.addChild(poppupTitleTextSpr);
			
			poppupTitleText.text = "How to play";
			poppupTitleTextSpr.x = 192;
			poppupTitleTextSpr.y = 105;
			
			for(var i:int=0; i<this.loadScreenResources.length; i++ )
			{
				
				var s:Sprite = new Sprite();
				var bmp:Bitmap = this.loadScreenResources[i].imgObj.content as Bitmap;
				s.addChild(bmp)
				this.addChild(s);
				
				for(var lsc:int = 0; lsc<Config.howToPlayImages.images.length; lsc++)
				{
					if(this.loadScreenResources[i].imgName==Config.howToPlayImages.images[lsc].name)
					{
						s.x = Config.howToPlayImages.images[lsc].positions.x;
						s.y = Config.howToPlayImages.images[lsc].positions.y;
						break;
					}
				}
				
				
				
			}
			
			var htpTxtTable:Sprite = this.howToPlayTextTable(
			"Spinning &amp; Moving",
			"To move around the board, "+
			"you must click and drag on the wheel. You can also press the Space bar."
			);
			this.addChild(htpTxtTable);
			htpTxtTable.x = 175;
			htpTxtTable.y = 350;
			
			var htpTxtTable1:Sprite = this.howToPlayTextTable(
			"Life Tiles",
			"You gain LIFE Tiles when landing on "+
			"LIFE spaces. Each tile has a cash value which is added to your total."
			);
			this.addChild(htpTxtTable1);
			htpTxtTable1.x = 333;
			htpTxtTable1.y = 350;
			
			var htpTxtTable2:Sprite = this.howToPlayTextTable(
			"Share the Wealth cards",
			"These provide players with benefits. "+
			"Select three at the start of the game and acquire more throughout."
			);
			this.addChild(htpTxtTable2);
			htpTxtTable2.x = 493;
			htpTxtTable2.y = 350;
			
			var htpTxtTable3:Sprite = this.howToPlayTextTable(
			"Retirement",
			"Take the Risky route, and retire in the Mansion, "+
			"or take the Safe route and stay in the Countryside Acres."
			);
			this.addChild(htpTxtTable3);
			htpTxtTable3.x = 652;
			htpTxtTable3.y = 350;
			
			
			
			
			
			//Add the OK Button
			this.addChild(this.getHowToPlayButton());
			
			
			
			var tandCSprite:Sprite = new Sprite();
			tandCSprite.buttonMode = true;
			tandCSprite.mouseChildren = false;
			
			var advertLinkTxt:TextField = aF.bodyHeaderTextBox();
			var advertLinkTxtFormat:TextFormat = advertLinkTxt.getTextFormat();
			advertLinkTxtFormat.size = 15;
			advertLinkTxtFormat.bold = false;
			advertLinkTxt.defaultTextFormat = advertLinkTxtFormat;
			tandCSprite.addChild(advertLinkTxt);
			//advertLinkTxt.y = advertBodyTxt.y+advertBodyTxt.textHeight+12;
			advertLinkTxt.width = 460;
			//headTxt.background = true;
			advertLinkTxt.wordWrap = true;
			advertLinkTxt.autoSize = TextFieldAutoSize.CENTER;
			advertLinkTxt.htmlText = 'Terms and Conditions';
			
			
			this.addChild(tandCSprite);
			tandCSprite.addEventListener(MouseEvent.MOUSE_DOWN, this.openTandCs);
			
			//new KeyboardControlTool(tandCSprite);
			tandCSprite.x= 263;
			tandCSprite.y= 666;
			
			
			
			return this;
				
		}
		
		private function openTandCs(evt:MouseEvent):void
		{
			this.gf.openTandCs();
		}
		
		private function howToPlayTextTable(htpTxtTitle:String, htpTxtBody:String):Sprite
		{
			var htpTable:Sprite = new Sprite();
			htpTable.mouseChildren = false;
			var headTxt:TextField = aF.bodyHeaderTextBox();
			
			
			var tff:TextFormat = headTxt.getTextFormat();
			tff.size = 21;
			headTxt.defaultTextFormat = tff;
			
			
			htpTable.addChild(headTxt);
			headTxt.x = 15;
			headTxt.width = 120;
			//headTxt.background = true;
			headTxt.wordWrap = true;
			headTxt.autoSize = TextFieldAutoSize.CENTER;
			headTxt.htmlText = htpTxtTitle;
			
			
			var paraTxt:TextField = aF.paragraphTextBox();
			htpTable.addChild(paraTxt);
			
			var tformat:TextFormat = paraTxt.getTextFormat();
			tformat.align = "center";
			paraTxt.defaultTextFormat = tformat;
			
			paraTxt.autoSize = TextFieldAutoSize.CENTER;
			paraTxt.htmlText = htpTxtBody;
			paraTxt.y = 84;
			paraTxt.width = 148;
			
			return htpTable;
		}
		
		private function getHowToPlayButton():Sprite
		{
			var continueButton:Sprite = this.aF.getFormButton("OK, let's Play!");
			//advertScrn.addChild(continueButton);
			continueButton.x = 401;
			continueButton.y = 590;
			
			continueButton.addEventListener(MouseEvent.CLICK, this.continueButtonClicked);
			
			//this.cccB = continueButton;
			return continueButton;
		}
		
		private function continueButtonClicked(evt:MouseEvent):void
		{
			this.gf.setAppropriateTab("", "off");
			this.funcWhenExitButtonClicked();
		}
		
		public function whenAddedToStage(evt:Event):void
		{
			//this.cccB.x=0;
		}	
		
	
	}
	
}