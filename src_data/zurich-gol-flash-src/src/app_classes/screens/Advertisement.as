package src.app_classes.screens
{
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	
	import src.app_classes.forms.AppForms;
	import src.app_classes.GamePlay;
	import Config;
	import GlobalFunctions;
	import GeneralAnimations;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class Advertisement extends Sprite
	{
		
		[Embed (source="../../../res/logo/zurich-logo.png" )]
		protected var zurichLogo:Class;
		
		private var aF:AppForms;
		private var afterAdClosed:Function;
		private var continueButton:Sprite;
		private var advertContainer:Sprite;
		private var animationSpeed:Number = 0.75;
		private var gp:GamePlay;
		//private var classImgLdr:ClassImageLoader;
		
		public function Advertisement(gp:GamePlay, aftAdClosed:Function = null)
		{	
			this.gp = gp;
			this.afterAdClosed = aftAdClosed;
			this.aF = new AppForms();
		}
		
		public function getAdvertScreen(titleCopy:String, bodyCopy:String):Sprite
		{
			var advertScrn:Sprite = new Sprite();
			
			advertScrn.addChild(GlobalFunctions.getBackDrop());
			
			
			this.advertContainer = new Sprite();
			this.advertContainer.name = "advertContainer";
			advertScrn.addChild(this.advertContainer);
			
			
			var advertBack:Sprite = new Sprite();
			advertBack.graphics.lineStyle(2,0xFFFFFF);
			advertBack.graphics.beginFill(0xFFFFFF,0.3);
			advertBack.graphics.drawRoundRect(0, 0, 460, 536, 25, 25);
			advertBack.graphics.endFill();
			this.advertContainer.addChild(advertBack);
			
			
			var logoSpr:Sprite = new Sprite();
			var zurichLogoBitmap:Bitmap = new zurichLogo() as Bitmap;
			logoSpr.addChild(zurichLogoBitmap);
			this.advertContainer.addChild(logoSpr);
			logoSpr.x = 143;
			logoSpr.y = 128;
			
			
			
			
			var headTxt:TextField = aF.bodyHeaderTextBox();
			var tff:TextFormat = headTxt.getTextFormat();
			tff.size = 18;
			headTxt.defaultTextFormat = tff;
			this.advertContainer.addChild(headTxt);
			headTxt.y = 306;
			headTxt.width = 460;
			//headTxt.background = true;
			headTxt.wordWrap = true;
			headTxt.autoSize = TextFieldAutoSize.CENTER;
			headTxt.text = titleCopy;
			
			
			
			
			var advertBodyTxt:TextField = aF.paragraphTextBox();
			var advertBodyTxtFormat:TextFormat = advertBodyTxt.getTextFormat();
			advertBodyTxtFormat.size = 15;
			advertBodyTxtFormat.align = "center";
			advertBodyTxt.defaultTextFormat = advertBodyTxtFormat;
			this.advertContainer.addChild(advertBodyTxt);
			advertBodyTxt.x = 70;
			advertBodyTxt.y = headTxt.y+headTxt.textHeight+12;
			advertBodyTxt.width = 320;
			//advertBodyTxt.background = true;
			advertBodyTxt.wordWrap = true;
			advertBodyTxt.autoSize = TextFieldAutoSize.CENTER;
			advertBodyTxt.text = bodyCopy;/*"Protect your clients' personal, funeral and inheritance "+
			"tax needs with our whole of life contract, which includes waiver, indexation "+
			"and guaranteed insurability options.";*/
			
			
			
			
			var advertLinkTxt:TextField = aF.bodyHeaderTextBox();
			var advertLinkTxtFormat:TextFormat = advertLinkTxt.getTextFormat();
			advertLinkTxtFormat.size = 15;
			advertLinkTxt.defaultTextFormat = advertLinkTxtFormat;
			this.advertContainer.addChild(advertLinkTxt);
			advertLinkTxt.y = advertBodyTxt.y+advertBodyTxt.textHeight+12;
			advertLinkTxt.width = 460;
			//headTxt.background = true;
			advertLinkTxt.wordWrap = true;
			advertLinkTxt.autoSize = TextFieldAutoSize.CENTER;
			advertLinkTxt.htmlText = 'Find out more <a target="_blank" '+
			'href="http://www.zurich4Protection.co.uk" title="Zurich4Protection">'+
			'Zurich4Protection.co.uk</a>';
			advertLinkTxt.addEventListener(MouseEvent.MOUSE_DOWN, this.gp.advertContactMade);
			
			
			
			
			
			
			
			this.continueButton = this.aF.getFormButton("Continue");
			this.continueButton.name = "continueButton";
			advertScrn.addChild(this.continueButton);
			this.continueButton.x = 401;
			this.continueButton.y = 
			Config.SCREEN_CONFIG.height+this.continueButton.height;//577;
			
			this.continueButton.addEventListener(MouseEvent.CLICK, this.continueButtonClicked);
			
			this.advertContainer.x = 260;
			this.advertContainer.y = this.advertContainer.height*-1;
			//We will test for when the screen is added to the stage, then bring in the
			//animation then reveal the button afterwards.
			advertScrn.addEventListener("addedToStage", this.rollInAd);
			
			
			//new KeyboardControlTool(this.advertContainer);
			
			
			return advertScrn;
		}
		
		private function rollInAd(evt:Event):void
		{
			GeneralAnimations.positionTo
			(this.advertContainer, this.afterAdPositioned, 260, 0, this.animationSpeed);
		}
		
		private function afterAdPositioned():void
		{
			GeneralAnimations.positionTo
			(this.continueButton, null, 401, 577, this.animationSpeed);
		}
		
		private function continueButtonClicked(evt:MouseEvent):void
		{
			this.removeButton();
		}
		
		private function removeButton():void
		{
			GeneralAnimations.positionTo(this.continueButton, this.onButtonremoved, 401, 
			Config.SCREEN_CONFIG.height+this.continueButton.height, this.animationSpeed);
		}
		
		private function onButtonremoved():void
		{
			GeneralAnimations.positionTo(this.advertContainer, this.onAdremoved, 260, 
			this.advertContainer.height*-1, this.animationSpeed);
		}
		
		private function onAdremoved():void
		{
			this.afterAdClosed();
		}
		
	
	}
	
}