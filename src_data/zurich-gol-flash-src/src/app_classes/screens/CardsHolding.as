package src.app_classes.screens
{
	
	import mx.collections.ArrayList;
	import flash.text.TextField;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import src.app_classes.tools.PictureLoader;
	import flash.display.Bitmap;
	import Config;
	
	
	import GlobalFunctions;
	import src.app_classes.forms.DefineUserFormItems;
	import PlayerResources;
	
	public class CardsHolding
	{
		private var cardsSelected:ArrayList = new ArrayList();
		//the different types of wealth cards(images) templates loaded
		//one will be insurance, one payout etc
		private var cardTemplates:ArrayList = new ArrayList();
		
		private var total_possible_cards:int = 0;
		private var possible_cards:int = 0;
		private var tf:TextField;
		private var cardsLoaded:Boolean = false;
		private var callGameFlowOnOkClicked:Function;
		
		public function CardsHolding(tf:TextField, cardsPositionArray:Array, notifyGameFlowOnOkClicked:Function)
		{
			this.callGameFlowOnOkClicked = notifyGameFlowOnOkClicked;
			this.tf = tf;
			//this.tf.text = (cardsPositionArray.length).toString();
		}
		
		public function loadCardGraphics():void
		{
				
				
			
			var cardsToLoad:ArrayList = Config.possible_cards_toLoad;
			this.total_possible_cards = cardsToLoad.length;
			
			for(var i:int=0; i<cardsToLoad.length; i++)
			{
				var pl:PictureLoader = new PictureLoader(this.possible_cards_loaded);
				var img:String = cardsToLoad.getItemAt(i).src;
				pl.load_image(cardsToLoad.getItemAt(i).cardName,img,0);
			}
			
		}
		
		private function possible_cards_loaded(cardReturned:PictureLoader, f:Object):void
		{
			
			var cardObj:Object = new Object;
			cardObj.cardType = f.id;
			cardObj.card = cardReturned.content;
			
			this.cardTemplates.addItem(cardObj);
			
			if(this.possible_cards==(this.total_possible_cards-1)){ this.cardsLoaded = true; }
			
			this.possible_cards++;
		}
		
		public function setCardSelected
		(
			whichOnscreenCardClicked:int,//card selected on screen
			clickedCardValue:int//value of card selected on screen
		):void
		{
			var f:Object = new Object;
			f.cardScreenNumber = whichOnscreenCardClicked;
			f.clickedCardValue = clickedCardValue;
			this.cardsSelected.addItem(f);
		}
		
		public function getCardsSelected():ArrayList
		{
			return this.cardsSelected;
		}
		
		public function removeCardSelected
		(
			cardClickedOnScreen:int//card selected on screenclickedCardValue:int//value of card selected on screen
		):void
		{
			for(var i:int = 0; i<this.cardsSelected.length; i++)
			{
				if(this.cardsSelected.getItemAt(i).cardScreenNumber==cardClickedOnScreen)
				{
					this.cardsSelected.removeItemAt(i);
					break;
				}
			}
		}
		
		public function isCardSelected(cardClickedOnScreen:int):Boolean
		{
			for(var i:int = 0; i<this.cardsSelected.length; i++)
			{
				if(this.cardsSelected.getItemAt(i).cardScreenNumber==cardClickedOnScreen)
				{
					return true;
					break;
				}
			}
			
			return false;
		}
		
		public function getValue():String
		{
			return this.cardsSelected.getItemAt(0).cardScreenNumber + " num - val " + this.cardsSelected.getItemAt(0).clickedCardValue;
		}
		
		public function numCardsSelected():int
		{
			return this.cardsSelected.length;
		}
		
		public function getRevealCardsScreen():Sprite
		{
			
			PlayerResources.player_car = "red";
			
			var rvlCardSprite:Sprite = new Sprite();
			
			rvlCardSprite.addChild(GlobalFunctions.getBackDrop());
			
			
			var rvlCardSpriteContents:Sprite = new Sprite();
			
			rvlCardSprite.addChild(rvlCardSpriteContents);
			rvlCardSpriteContents.x = 188;
			rvlCardSpriteContents.y = 228;
			
			
			
			for(var i:int; i<3; i++)
			{
				var spr:Sprite = new Sprite();
				
				spr.addChild(new Bitmap(Bitmap(this.cardTemplates.getItemAt(this.cardsSelected.getItemAt(i).clickedCardValue).card).bitmapData));
				rvlCardSpriteContents.addChild(spr);
				spr.x = (i*12)+ (i*193);
			}
			
			var okButton:Sprite = new DefineUserFormItems().getWealthCardOkButton();
			rvlCardSpriteContents.addChild(okButton);
			okButton.x = 200;
			okButton.y = 330;
			
			okButton.addEventListener(MouseEvent.CLICK,wealthCardsOKClicked);
			
			return rvlCardSprite;
		}
		
		private function wealthCardsOKClicked(evt:MouseEvent):void
		{
			this.callGameFlowOnOkClicked();
		}
	
	}
	
}