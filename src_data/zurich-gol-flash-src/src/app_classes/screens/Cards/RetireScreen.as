package src.app_classes.screens.Cards
{
	
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.display.Bitmap;
	import src.app_classes.screens.Cards.CardScreens;
	import src.app_classes.screens.CardContents.TwoCards;
	import src.app_classes.tools.RibbonText;
	
	import src.app_classes.tools.KeyboardControlTool;
	import GlobalFunctions;
	import PlayerResources;
	import GeneralAnimations;
	
	public class RetireScreen extends CardScreens
	{
		
		[Embed (source="../../../../res/trash/retireScreen.jpg" )]
		protected var retireScreen:Class;
		
		private var twoScreensRes:TwoCards;
		
		private var whenRetireChoiceMade:Function;
		
		
		private var cardData:Array = 
		[{pos:{x:290, y:316},img:"countryestate", main_name:"Country Estate", back_name:"safe", facing:"front", infoText:"Play it safe"},
		{pos:{x:505, y:316},img:"retiremansion", main_name:"Luxury Mansion", back_name:"risk", facing:"front", infoText:"Take a risk"}];
		
		public function RetireScreen()
		{
			this.twoScreensRes = new TwoCards();
		}
		
		public function getRetireScreen(whenChoiceMade:Function):Sprite
		{
			this.whenRetireChoiceMade = whenChoiceMade;
			
			var retire:Sprite = new Sprite();
			
			//var bm:Bitmap = new retireScreen() as Bitmap;
			//retire.addChild(bm);
			retire.addChild(GlobalFunctions.getBackDrop());
			
			
			
			var activityTxtSpr:Sprite = new Sprite();
			var lrgTxt:TextField = this.getActivityText();		
			activityTxtSpr.addChild(lrgTxt);
			lrgTxt.text = "CONGRATULATIONS YOUR SCORE IS";
			//lrgTxt.background = true;
			lrgTxt.width = 510;
			lrgTxt.height = 53;
			retire.addChild(activityTxtSpr);
			activityTxtSpr.x = 237;
			activityTxtSpr.y = 112;
			
			
			
			var retireRibbon:Sprite = new RibbonText().getRibbonText((PlayerResources.money).toString());
			retire.addChild(retireRibbon);
			retireRibbon.x = 236;
			retireRibbon.y = 172;
			
			
			for(var i:int = 0; i<2; i++)
			{
				
				var rotationSprContainer:Sprite = new Sprite();
				var rotationSpr:MovieClip = new _cardRotationSpr() as MovieClip;
				rotationSpr.addEventListener("addedToStage", cardSprAddedToScreen);
				rotationSpr.name="rotationSpr"+i;
				rotationSpr.buttonMode = true;
				rotationSpr.mouseChildren = false;
				rotationSprContainer.addChild(rotationSpr);
				retire.addChild(rotationSprContainer);
				
				rotationSprContainer.x = cardData[i].pos.x+96.5;
				rotationSprContainer.y = cardData[i].pos.y;
			}
			
			
			//getTwoCardText
			var CardLabelText:TextField = 
			getTwoCardText();
			retire.addChild(CardLabelText);
			CardLabelText.text = "Choose where you would like to retire...";
			CardLabelText.x = 300;
			CardLabelText.y = 645;
			CardLabelText.width = 400;
			//CardLabelText.background = true;
			
			//new KeyboardControlTool(CardLabelText);
			
			
			return retire;
		}
		
		private function hhhh(evt:MouseEvent):void
		{
			var cardId:int = parseInt(evt.currentTarget.name.substr(9));
			
			var cardMC:MovieClip = 
			evt.currentTarget.parent.getChildByName("rotationSpr"+cardId) as MovieClip;
			cardMC.addEventListener("enterFrame", playBackward);
			cardMC.gotoAndPlay(20);
		}
		
		private function cardClicked(evt:MouseEvent):void
		{
			var cardId:int = this.getCardId(evt.target.name);
			
			if(cardData[cardId].facing == "front")
			{
				evt.currentTarget.addEventListener("enterFrame", playForward);
				evt.currentTarget.gotoAndPlay(1);
			}	
			else
			{
				//We now know that the card is facing to the front.
				//With this being the case, we will ignite the choice
				//and put the transistion in motion.
				this.whenRetireChoiceMade(cardId);
			}	
			
			//cardData[cardId].facing == "front";
		}
		
		private function cardSprAddedToScreen(evt:Event):void
		{
			evt.target.gotoAndStop(1);
			
			var cardId:int = this.getCardId(evt.target.name);
			
			var front_card:Sprite = _aSingleCard
			(cardId,
				this.twoScreensRes.getTwoCardImage(cardData[cardId].img),
				cardData[cardId].main_name, "endCard", cardData[cardId].infoText
			);
			
			evt.target._crRotationCards.rotationCard1.addChild(front_card);
			front_card.x = -93;
			front_card.y = 3;
			
			var backCard:Sprite = _aSingleBackCard(cardData[cardId].back_name);
			evt.currentTarget._crRotationCards.rotationCard2.addChild(backCard);
			backCard.x = -97;
			backCard.y = 0;
			
			//backButton
			//var backButton:Sprite = getChildByName("backButton") as Sprite;
			//backButton.addEventListener(MouseEvent.CLICK, this.cardClicked);
			
			/*var cardBackx:Sprite = new Sprite();
				var ico:Bitmap = new infoIcon() as Bitmap;
				cardBackx.addChild(ico);
				templHolder.addChild(cardBackx);
				cardBackx.x = 157;
				cardBackx.y = 14;
				cardBackx.name = "backButton";*/
				
			//place Back Call_to _action button
			var backButtonSpr:Sprite = new Sprite();
			backButtonSpr.graphics.beginFill(0xBBCC00);
			backButtonSpr.graphics.drawRect(0, 0, 23, 23);
			backButtonSpr.graphics.endFill();
			evt.target.parent.addChild(backButtonSpr);
			backButtonSpr.x = 60.5;
			backButtonSpr.y = 13;
			backButtonSpr.name="backHover"+cardId;
			backButtonSpr.alpha = 0;
			backButtonSpr.buttonMode = true;
			backButtonSpr.addEventListener(MouseEvent.CLICK, this.hhhh);
				
				
			evt.target.gotoAndStop(1);
			evt.target.addEventListener(MouseEvent.CLICK, this.cardClicked);
		}
		
		private function playBackward(evt:Event):void
		{
			
			if(evt.target.currentFrame==40)
			{
				evt.target.stop();
				evt.target.removeEventListener("enterFrame", playBackward);
				this.setCardState(evt.target.name);
			}	
		}
		
		private function playForward(evt:Event):void
		{
			if(evt.target.currentFrame==20)
			{
				evt.target.stop();
				evt.target.removeEventListener("enterFrame", playForward);
				this.setCardState(evt.target.name);
			}
		}
		
		private function setCardState(cardName:String):void
		{
			var cardId:int = this.getCardId(cardName);
			if(cardData[cardId].facing == "front")
			{
				cardData[cardId].facing = "back";
			}
			else
			{
				cardData[cardId].facing = "front";
			}
			
		}
		
		private function getCardId(cardName:String):int
		{
			return parseInt(cardName.substr(11));
		}	
	
	}
	
}