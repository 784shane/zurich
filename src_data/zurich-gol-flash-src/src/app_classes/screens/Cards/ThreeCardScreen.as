package src.app_classes.screens.Cards
{
	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	
	import Config;
	import src.app_classes.screens.Cards.CardScreens;
	import src.app_classes.tools.PictureLoader;
	import PlayerResources;
	import src.app_classes.forms.PopUpForms;
	import src.app_classes.tools.Resources;
	
	//temp
	import src.app_classes.tools.KeyboardControlTool;
	
	public class ThreeCardScreen extends CardScreens
	{
		
		public var tt:TextField;
		
		private var resClass:Resources;
		
		private var numOfResourcesToLoad:int = 0;
		private var afterResourcesLoaded:Function;
		private var TwoCardResources:Array = new Array();
		private var x2:Array = new Array();
		
		private var WhenDecisionIsMade:Function;
		
		private var PopupFormClass:PopUpForms;
		
		public function ThreeCardScreen(ResourcesClass:Resources, afterResLoadedFunction:Function)
		{
			this.resClass = ResourcesClass;
			this.PopupFormClass = new PopUpForms();
			this.afterResourcesLoaded = afterResLoadedFunction;
			this.loadResources();
			
		}
		
		private function loadResources():void
		{
			for(var i:int = 0; i<Config.twoScreenResources.collegeOrCareer.length; i++)
			{
				
				var pl:PictureLoader = new PictureLoader(this.resourcesLoaded);
				var img:String = Config.twoScreenResources.collegeOrCareer[i].src;
				pl.load_image(Config.twoScreenResources.collegeOrCareer[i].name,img,0);
				
			}
		}
		
		private function resourcesLoaded(cardReturned:PictureLoader, f:Object):void
		{
			
			var pp:Object = new Object();
			pp.imgName = f.id;
			pp.imgObj = cardReturned;
			this.TwoCardResources.push(pp);
			
			if(this.numOfResourcesToLoad == (Config.twoScreenResources.collegeOrCareer.length-1))
			{
				this.afterResourcesLoaded();
			}
			
			this.numOfResourcesToLoad++;
			
		}
		
		public function getTwoCardScreen(whenDecsMade:Function):Sprite
		{
			this.WhenDecisionIsMade = whenDecsMade;
			this.buildTwoCardScreen();
			return this;
		}
		
		private function buildTwoCardScreen():void
		{
			
			//add temporary back here
			var backDrop:Sprite = new Sprite();
			backDrop.graphics.beginFill(0xFFFFFF);
			backDrop.graphics.drawRect(0, 0, Config.SCREEN_CONFIG.width, Config.SCREEN_CONFIG.height);
			backDrop.graphics.endFill();
			this.addChild(backDrop);
			backDrop.alpha = 0.7;
			
			var twoScreenHolder:Sprite = new Sprite();
			this.addChild(twoScreenHolder);
			
			
			var newSprx:Sprite = new Sprite();
			//Keeping a recod of trhis text box in Resources Class
			this.resClass.TwoCardScreenMajorText = this.PopupFormClass.getMajorUiTextField();//new TextField();
			this.resClass.TwoCardScreenMajorText.width = 460;
			this.resClass.TwoCardScreenMajorText.height = 40;
			newSprx.addChild(this.resClass.TwoCardScreenMajorText);
			this.addChild(newSprx);
			this.resClass.TwoCardScreenMajorText.text = "Choose your career path";
			//this.resClass.uiMajorText
			
			var format1:TextFormat = this.resClass.TwoCardScreenMajorText.getTextFormat();
			format1.color = 0x3300CC;
			format1.size = 24;
			format1.bold = true;
			format1.align = TextFormatAlign.CENTER;
			this.resClass.TwoCardScreenMajorText.setTextFormat(format1);
			this.resClass.TwoCardScreenMajorText.defaultTextFormat = format1;
			
			
			//new KeyboardControlTool(newSprx);
			
			newSprx.x=260;
			newSprx.y=221;
			
			
			
			
			
			
			
			
			var newSpr:Sprite = new Sprite();
			var tx:TextField = this.PopupFormClass.getMinorUiTextField();//new TextField();
			tx.width = 60;
			tx.height = 40;
			newSpr.addChild(tx);
			this.addChild(newSpr);
			tx.text = "or";
			//this.resClass.uiMajorText
			
			var format:TextFormat = new TextFormat();//tf.getTextFormat();
			format.color = 0x3300CC;
			format.size = 24;
			format.bold = true;
			format.align = TextFormatAlign.CENTER;
			tx.setTextFormat(format);
			tx.defaultTextFormat = format;
			
			
			//new KeyboardControlTool(newSpr);
			
			newSpr.x=460;
			newSpr.y=408;
			
			
			
			
			
			
			
			for(var i:int = 0; i<Config.twoScreenResources.collegeOrCareer.length; i++)
			{
				
				var twoCard:Sprite = new Sprite();
				//making sure that iteams in this sprite isnt clickable
				//since the MouseEvent listener is on the parent mouse and not
				//any of its children.
				twoCard.mouseChildren = false;
				//this displat object already is mouse enabled so no need for this
				//twoCard.mouseEnabled = true; 
				
				twoCard.buttonMode = true;
				twoCard.name = Config.twoScreenResources.collegeOrCareer[i].name;
				twoCard.addEventListener("addedToStage" ,this.twoCardScr_AddedToStage);
				//twoCard.addEventListener(MouseEvent.CLICK ,this.f);
				
				var cdtempl:Bitmap = new this.cardTemplateImg() as Bitmap;
				var d:Sprite = new Sprite();
				twoCard.addChild(cdtempl);
				
				
				var xx:Sprite = new Sprite();
				twoCard.addChild(xx);
				xx.y = 83;
				xx.x = 31;
				
				for(var j:int=0; j<this.TwoCardResources.length; j++)
				{
					if( Config.twoScreenResources.collegeOrCareer[i].name == this.TwoCardResources[j].imgName )
					{
						var obj:Bitmap = this.TwoCardResources[j].imgObj.content;
						xx.addChild(obj);
				
						twoCard.x = Config.twoScreenResources.collegeOrCareer[i].positions.x;//(i*240);
						twoCard.y = Config.twoScreenResources.collegeOrCareer[i].positions.y;
					 break;
					}	
				}

			twoScreenHolder.addChild(twoCard);
				
			}
			
			
			twoScreenHolder.x = 260;
			twoScreenHolder.y = 282;
			
		}
		
		private function twoCardScr_AddedToStage(evt:Event):void
		{
			evt.target.addEventListener(MouseEvent.CLICK ,this.f);
		}
		
		private function f(evt:MouseEvent):void
		{
			PlayerResources.playerRoutes.careerOrCollege = evt.target.name;
			this.WhenDecisionIsMade();
		}
		
	
	}
	
}