package src.app_classes.screens.Cards
{
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.display.Bitmap;
	import GlobalFunctions;
	import Config;
	import src.app_classes.forms.AppForms;
	
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class CardScreens extends AppForms
	{
		
		[Embed (source="../../../../res/cardImages/infoIcon.png" )]
		protected var infoIcon:Class;
		
		[Embed (source="../../../../res/cardTemplate.png" )]
		protected var cardTemplateImg:Class;
		
		[Embed (source="../../../../res/cardTemplateBack.png" )]
		protected var cardTemplateBackImg:Class;
		
		[Embed (source="../../../../res/trash/cards/risk_retire.png" )]
		protected var risk_retire:Class;
		
		[Embed (source="../../../../res/trash/cards/safe_retire.png" )]
		protected var safe_retire:Class;
		
		[Embed (source="../../../../res/cardBack.png" )]
		protected var cardBack:Class;
		
		[Embed (source="../../../../res/trash/07.Zurich_GOL_CAREER_PATH.jpg" )]
		protected var trashScreen:Class;
		
		private var cardPos:Object = 
		{
		defaultCard:{img:{x:31, y:83},txt:{x:5, y:216}}, 
		endCard:{img:{x:31, y:116},txt:{x:5, y:72}}
		};
		
		public function CardScreens()
		{
			
		}
		
		public function getCardBackDrop():Sprite
		{
			return GlobalFunctions.getBackDrop();
		}
		
		public function aCarouselButton():Sprite
		{
			/*
			var carouselButton:Sprite = new Sprite();
			//carouselButton.graphics.beginFill(0xBBCC00);
			//carouselButton.graphics.drawRect(0, 0, 20, 50);
			//carouselButton.graphics.endFill();
			this.addChild(carouselButton);
			//carouselButton.alpha = 0.7;
			
			return carouselButton;
			*/
			
			return new Sprite();
		}
		
		public function _aSingleCard
		(i:int, obj:Bitmap, cardText:String, 
		cardStyle:String="endCard", infoTextString:String="endCard"):Sprite
		{
				var twoCard:Sprite = new Sprite();
				//making sure that iteams in this sprite isnt clickable
				//since the MouseEvent listener is on the parent mouse and not
				//any of its children.
				//twoCard.mouseChildren = false;
				//this displat object already is mouse enabled so no need for this
				twoCard.mouseEnabled = true; 
				
				twoCard.buttonMode = true;
				twoCard.name = Config.twoScreenResources.collegeOrCareer[i].name;
				
				//temporarily take this out
				//twoCard.addEventListener("addedToStage" ,this.twoCardScr_AddedToStage);
				
				var cdtempl:Bitmap = new this.cardTemplateImg() as Bitmap;
				var templHolder:Sprite = new Sprite();
				templHolder.addChild(cdtempl);
				twoCard.addChild(templHolder);
				
				
				var cardPic:Sprite = new Sprite();
				twoCard.addChild(cardPic);
				cardPic.y = 83;
				cardPic.x = 31;
				
				if(cardStyle == "default")
				{
				cardPic.x = cardPos.defaultCard.img.x;
				cardPic.y = cardPos.defaultCard.img.y;
				
				twoCard.x = 
				Config.twoScreenResources.collegeOrCareer[i].positions.x;//(i*240);
				twoCard.y = 
				Config.twoScreenResources.collegeOrCareer[i].positions.y;
				
				
				}
				else
				{
				cardPic.x = cardPos.endCard.img.x;
				cardPic.y = cardPos.endCard.img.y;
				}
				
				
				cardPic.addChild(obj);
				
				
				
						
							
				
				
			//create the textbox for the career name
			var sprxx:Sprite = new Sprite();
			var careerName1:TextField = getTwoCardText();
			sprxx.addChild(careerName1);
			twoCard.addChild(sprxx);
			careerName1.text = cardText;//this.getAppropriateTwoCardText(i);
			//careerName1.background = true;
			//careerName1.backgroundColor = 0xFF0000;
			careerName1.width = 175;
			
			if(cardStyle == "default")
			{
				sprxx.x = cardPos.defaultCard.txt.x;
				sprxx.y = cardPos.defaultCard.txt.y;
			}
			else
			{
				sprxx.x = cardPos.endCard.txt.x;
				sprxx.y = cardPos.endCard.txt.y;
				
				
				//addinfo text
				var infoImgHolder:Sprite = new Sprite();
				twoCard.addChild(infoImgHolder);
				infoImgHolder.addChild(new infoIcon() as Bitmap);
				infoImgHolder.x=32;
				infoImgHolder.y=249;
				
				var infoTextSpr:Sprite = new Sprite();
				twoCard.addChild(infoTextSpr);
				var infoText:TextField = getTwoCardText(false);//new TextField();
				infoTextSpr.addChild(infoText);
				var infoTextSprTF:TextFormat = infoText.getTextFormat();
				infoTextSprTF.color = Config.APP_COLORS.FONT_GREY;
				infoTextSprTF.size = 18;
				infoText.defaultTextFormat = infoTextSprTF;
				infoText.text = infoTextString;
				infoTextSpr.x = 48;
				infoTextSpr.y = 246;
				
				
				
			}
			
			
			if(cardStyle == "default")
			{
				/*var tfft:TextField = new TextField();
				twoCard.addChild(tfft);
				tfft.text = "assesment";*/
				
				var outsideGlow:Sprite = getSelectBoxRing(191, 296);
				outsideGlow.name = "outsideGlow";
				twoCard.addChild(outsideGlow);
				outsideGlow.alpha = 0;
				outsideGlow.x = -3;
				outsideGlow.y = -3;
				
				twoCard.addEventListener
				(MouseEvent.MOUSE_OVER, this.carouselItemMoused);
				twoCard.addEventListener
				(MouseEvent.MOUSE_OUT, this.carouselItemMoused);
			}	
			/*
			
				var outsideGlow:Sprite = getSelectBoxRing(191, 296);
				outsideGlow.name = "outsideGlow";
			*/
			
			
			return twoCard;
			
		}
		
		
	
	private function carouselItemMoused(evt:MouseEvent):void
	{
		switch(evt.type)
		{
			case "mouseOver":
			{
				((evt.currentTarget as Sprite)
				.getChildByName("outsideGlow") as Sprite)
				.alpha = 1;
				break;
			}
			
			case "mouseOut":
			{
				((evt.currentTarget as Sprite)
				.getChildByName("outsideGlow") as Sprite)
				.alpha = 0;
				break;
			}
		}
	}
		
		public function _aSingleBackCard(cardBackType:String):Sprite
		{
				var cardBack:Sprite = new Sprite();
				
				//cardCloseButton
			
				var cdtempl:Bitmap = this.getAppropriateCardBack(cardBackType);
				var templHolder:Sprite = new Sprite();
				templHolder.addChild(cdtempl);
				cardBack.addChild(templHolder);
				
				
				var cardBackx:Sprite = new Sprite();
				var ico:Bitmap = new cardCloseButton() as Bitmap;
				cardBackx.addChild(ico);
				//We dont need this to show anymore so lets
				//make it invisible
				ico.alpha = 0;
				templHolder.addChild(cardBackx);
				cardBackx.x = 157;
				cardBackx.y = 14;
				cardBackx.name = "backButton";
				
			
				
				
				return cardBack;
		}
		
		private function getAppropriateCardBack(cardBackType:String):Bitmap
		{
			
			switch(cardBackType)
			{
				case "safe":
				{
					return new this.safe_retire() as Bitmap;
					break;
				}
				
				case "risk":
				{
					return new this.risk_retire() as Bitmap;
					break;
				}
			}
				
				return new this.risk_retire() as Bitmap;
				
		}
		
		public function _aSingleBackCardx():Sprite
		{
				var cardBack:Sprite = new Sprite();
			
				var cdtempl:Bitmap = new this.risk_retire() as Bitmap;
				var templHolder:Sprite = new Sprite();
				templHolder.addChild(cdtempl);
				cardBack.addChild(templHolder);
				
			return cardBack;
		}
		
		/*
		private function createCardBox():Sprite
		{
			
			var box:Sprite = new Sprite();
			
			var boxBack:Sprite = new Sprite();
			boxBack.graphics.beginFill(0xBBCC00);
			boxBack.graphics.drawRoundRect(0, 0, 84, 32, 6, 6);
			boxBack.graphics.endFill();
			box.addChild(boxBack);
			
			var tt:TextField = new TextField();
			box.addChild(tt);
			tt.text = "back";
			
			return box;
		}
		*/
	
	}
	
}