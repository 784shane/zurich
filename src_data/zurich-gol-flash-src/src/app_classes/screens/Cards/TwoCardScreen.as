package src.app_classes.screens.Cards
{
	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	
	import src.app_classes.GamePlay;
	import Config;
	import src.app_classes.animations.CardAnimation;
	import src.app_classes.screens.Cards.CardScreens;
	import src.app_classes.tools.PictureLoader;
	import PlayerResources;
	import src.app_classes.forms.PopUpForms;
	import src.app_classes.tools.Resources;
	import src.app_classes.forms.AppForms;
	import src.app_classes.screens.CardContents.Jobs;
	import src.app_classes.screens.CardContents.Houses;
	import src.app_classes.screens.CardContents.TwoCards;
	
	
	//temp
	import src.app_classes.tools.KeyboardControlTool;
	
	public class TwoCardScreen extends CardScreens
	{
		
		[Embed (source="../../../../res/trash/07.Zurich_GOL_CAREER_PATH.jpg" )]
		protected var twoCardScrn:Class;
		
		[Embed (source="../../../../res/09.Zurich_GOL_SELECT_A_PROPERTY.jpg" )]
		protected var xcardTemplateImg:Class;
		
		//[Embed (source="../../../../res/career.png" )]
		//[Embed (source="../../../../res/cardBack.png" )]
		[Embed (source="../../../../res/masonette.png" )]
		protected var masonette:Class;
		
		[Embed (source="../../../../res/cardImages/carousel/buttonLeftOff.png" )]
		protected var buttonLeftOff:Class;
		
		[Embed (source="../../../../res/cardImages/carousel/buttonLeftOn.png" )]
		protected var buttonLeftOn:Class;
		
		[Embed (source="../../../../res/cardImages/carousel/buttonRightOn.png" )]
		protected var buttonRightOn:Class;
		
		[Embed (source="../../../../res/cardImages/carousel/buttonRightOff.png" )]
		protected var buttonRightOff:Class;
		
		
		public var contentsData:*;
		
		public var cardFormat:String;
		
		public var displayType:String;
		
		private var resClass:Resources;
		
		private var aF:AppForms;
		private var numOfResourcesLoaded:int = 0;
		private var afterResourcesLoaded:Function;
		private var CardResources:Array = new Array();
		private var x2:Array = new Array();
		
		private var WhenDecisionIsMade:Function;
		
		private var PopupFormClass:PopUpForms;
		
		private var carouselSlider:Sprite = new Sprite();
		
		private var gp:GamePlay;
		
		private var twoScreensRes:TwoCards;
		
		private var popupTitle:String;
		
		private var jobNumberRemoved:int = 0;
		
		private var doJobCheck:Boolean = false;
		
		public function TwoCardScreen
		(gp:GamePlay, ResourcesClass:Resources, 
		afterResLoadedFunction:Function, 
		cardFormat:String, msg:String, displ_type:String)
		{
			
			this.popupTitle = msg;
			this.gp = gp;
			this.gp.cancelSpin = false;
			this.aF = new AppForms();
			//this.addChild(new xcardTemplateImg());
			
			
			this.displayType = displ_type;
			this.cardFormat = cardFormat;
			this.resClass = ResourcesClass;
			this.PopupFormClass = new PopUpForms();
			this.afterResourcesLoaded = afterResLoadedFunction;
			this.loadResources();
			
		}
		
		private function loadResources():void
		{
			
			switch(this.cardFormat)
			{
				case "two-card":
				{
					this.setTwoScreenResources();
					break;
				}
				
				case "three-card":
				{
					this.setThreeScreenResources();
					break;
				}
				
				case "retire":
				{
					this.afterResourcesLoaded();
					break;
				}
			}
		}
		
		private function setTwoScreenResources():void
		{
			
			for(var i:int = 0; i<Config.twoScreenResources.collegeOrCareer.length; i++)
			{
				
				var pl:PictureLoader = new PictureLoader(this.resourcesLoaded);
				var img:String = Config.twoScreenResources.collegeOrCareer[i].src;
				pl.load_image(Config.twoScreenResources.collegeOrCareer[i].name,img,0);
				
			}
		}
		
		private function setThreeScreenResources():void
		{
			
			for(var i:int = 0; i<Config.threeScreenResources.collegeOrCareer.length; i++)
			{
				
				var pl:PictureLoader = new PictureLoader(this.resourcesLoaded);
				var img:String = Config.threeScreenResources.collegeOrCareer[i].src;
				pl.load_image(Config.threeScreenResources.collegeOrCareer[i].name,img,0);
				
			}
			
		}
		
		private function resourcesLoaded(cardReturned:PictureLoader, f:Object):void
		{
			
			var pp:Object = new Object();
			pp.imgName = f.id;
			pp.imgObj = cardReturned;
			this.CardResources.push(pp);
			
			if(this.testCardResourcesLoaded(this.numOfResourcesLoaded))
			{
				this.afterResourcesLoaded();
			}
			
			this.numOfResourcesLoaded++;
			
		}
		
		private function testCardResourcesLoaded(numLoaded:int):Boolean
		{
			
			switch(this.cardFormat)
			{
				case "two-card":
				{
					if(numLoaded == 
					(Config.twoScreenResources.collegeOrCareer.length-1))
					{
						return true;
					}
					break;
				}
				
				case "three-card":
				{
					if(numLoaded == 
					(Config.threeScreenResources.collegeOrCareer.length-1))
					{
						return true;
					}
				break;
				}
			}
			
			return false;
			
		}
		
		public function getTwoCardScreen(whenDecsMade:Function):Sprite
		{
			this.WhenDecisionIsMade = whenDecsMade;
			var theTwoCard:Sprite;
			
			switch(this.cardFormat)
			{
				case "two-card":
				{
					theTwoCard = this.buildTwoCardScreen();
					break;
				}
				
				case "three-card":
				{
					theTwoCard = this.buildthreeCardScreen();
				break;
				}
				
				case "retire":
				{
					theTwoCard = this.buildRetireScreen();
				break;
				}
				
			}
			
			return theTwoCard;
		}
		
		private function buildRetireScreen():Sprite
		{
			return getCardBackDrop();
		}
		
		private function buildTwoCardScreen():Sprite
		{
			
			var theTwoCard:Sprite = new Sprite();
			
			//this will hold the resouces for the twoScreens
			this.twoScreensRes = new TwoCards();
			
			//add temporary back here
			theTwoCard.addChild(getCardBackDrop());
			
			//theTwoCard.addChild(new twoCardScrn());
			
			var twoScreenHolder:Sprite = new Sprite();
			theTwoCard.addChild(twoScreenHolder);
			
			
			
			var poppupTitleTextSpr:Sprite = new Sprite();
			this.resClass.TwoCardScreenMajorText = aF.popUpTitleText();
			this.resClass.TwoCardScreenMajorText.width = 980;
			poppupTitleTextSpr.addChild(this.resClass.TwoCardScreenMajorText);
			theTwoCard.addChild(poppupTitleTextSpr);
			
			this.resClass.TwoCardScreenMajorText.text = this.popupTitle;
			
			poppupTitleTextSpr.x = 0;//179;
			poppupTitleTextSpr.y = 198;
			//poppupTitleTextSpr.y = 162;
			
			
			var newSpr:Sprite = new Sprite();
			var tx:TextField = aF.popUpTitleText();
			newSpr.addChild(tx);
			theTwoCard.addChild(newSpr);
			tx.width = 78;
			//tx.background = true;
			tx.text = "or";
			newSpr.x = 450;//191;
			newSpr.y = 407;
			
			
			
			for(var i:int = 0; i<Config.twoScreenResources.collegeOrCareer.length; i++)
			{
				var twoCardSingle:Sprite = this.getDefaultTwoTemplateCard(i);
				twoCardSingle.mouseChildren = false;
				//twoCardSingle.name = "two_card"+(1+i);
				
				twoScreenHolder.addChild(twoCardSingle);
				twoCardSingle.addEventListener("addedToStage" ,this.twoCardScr_AddedToStage);
				
				
				//Add name here
				if(i==0)
				{
					twoCardSingle.x = 0;
					twoCardSingle.name = 
					"two_card1"
					/*
					twoCardSingle.name = 
					"two_card"+this.gp.positionsXML.board_places
					.place[PlayerResources.currentSquare]
					.action.options.option1.path;
					*/
				}
				else
				{
					twoCardSingle.x = 274;
					twoCardSingle.name = 
					"two_card2"
					/*
					twoCardSingle.name = 
					"two_card"+this.gp.positionsXML.board_places
					.place[PlayerResources.currentSquare]
					.action.options.option2.path;
					*/
				}
			}
			
			
			twoScreenHolder.x = 260;
			twoScreenHolder.y = 282;
			
			return theTwoCard;
			
			
		}
		
		private function getDefaultTwoTemplateCard(i:int):Sprite
		{
			return _aSingleCard(
			//the index
			i,
			//the Bitmap
			this.twoScreensRes.getTwoCardImage(this.getAppropriateTwoCardImgText(i)),
			this.getAppropriateTwoCardText(i),
			"default"
			);
			
		}
		
		private function getAppropriateTwoCardText(i:int):String
		{
			
			if(i==0)
			{
				return this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.options.option1.text;
			}
			else
			{
				return this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.options.option2.text;
			}
			
			return this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.options.option1.text;
		}
		
		private function getAppropriateTwoCardImgText(i:int):String
		{
			if(i==0)
			{
				return this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.options.option1.cardImg;
			}
			else
			{
				return this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.options.option2.cardImg;
			}
			
			return this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.options.option1.cardImg;
		}
		
		
		
		private function buildthreeCardScreen():Sprite
		{
			
			this.assignContentsData();
			
			var threeCardScreen:Sprite = new Sprite();
			
			//add temporary back here
			threeCardScreen.addChild(getCardBackDrop());
			
			var poppupTitleTextSpr:Sprite = new Sprite();
			var poppupTitleText:TextField = aF.popUpTitleText();
			poppupTitleTextSpr.addChild(poppupTitleText);
			threeCardScreen.addChild(poppupTitleTextSpr);
			
			poppupTitleText.text = this.popupTitle;
			
			
			
			poppupTitleTextSpr.x = 193;
			poppupTitleTextSpr.y = 151;

			
			//Place carousel here now.
			var carousel:Sprite = this.aF.getPopupCarousel(this.carouselSprites(), "default")
			threeCardScreen.addChild(carousel);
			carousel.x = 153;
			carousel.y = 243;
			
			
			return threeCardScreen;
			
			
		}
		
		private function carouselSprites():Array
		{
			
			if(this.displayType == "jobs")
			{
			
					if((this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.format.@job_status &&
					this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.format.@job_status == "lose")
						||PlayerResources.currentSquare==57)
					{
						this.doJobCheck = true;
					}
			}
			
			
			var carouselArr:Array = new Array();
			
			var numberOfCarouselItems:int = this.contentsData.get_numberOfItems();
			for(var carouIndex:int = 0; carouIndex<numberOfCarouselItems; carouIndex++)
			{
			
				if(this.doJobCheck)
				{
					if(this.gp.positionsXML.game_data.jobs
					.graduate_jobs.job[carouIndex].title == PlayerResources.job)
					{this.jobNumberRemoved = carouIndex; continue;}
				}
				
				var dddd:Object = this.contentsData.getCardContents(carouIndex);
				
					//place items into our single box
					var cardHolder1:Sprite = new Sprite();
					
					var cardHolderBack:Sprite = new Sprite();
					
					var cdtempl:Bitmap = new this.cardBack() as Bitmap;
					cardHolderBack.addChild(cdtempl);
					
					
					switch(this.displayType)
					{	
						case "jobs":{this.populateJobCard(cardHolderBack,dddd); break;}
						case "houses":{this.populateHouseCard(cardHolderBack,dddd); break;}
					}
					
						
					cardHolder1.addChild(cardHolderBack);
					
					
					
					cardHolder1.addEventListener
					("addedToStage" ,this.twoCardScr_AddedToStage);
					
				carouselArr.push(cardHolder1);
			
			}
			
			
			return carouselArr;
		}
		
		private function twoCardScr_AddedToStage(evt:Event):void
		{
			evt.target.addEventListener(MouseEvent.CLICK ,this.cardClicked);
		}
		
		private function cardClicked(evt:MouseEvent):void
		{
			this.makeResultSettings(evt.target.name);
			PlayerResources.playerRoutes.careerOrCollege = evt.target.name;
			this.WhenDecisionIsMade();
			
		}
		
		private function makeResultSettings(cardName:String):void
		{
			var cardNoClicked:int = 0;
			
			switch(this.cardFormat)
			{
				case "two-card":
				{
					this.gp.chosenPath = parseInt(cardName.substr(8));
						
					if(this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.cancelSpin.length() > 0)
					{
						if( this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.cancelSpin == 1 )
						{
							this.gp.cancelSpin = true;
						}
					}
					
						
					if(this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.extraActions.length() > 0)
					{
						
						if( this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.extraActions == "true" )
						{
							this.gp.performExtraActions = true;
						}
					}
					
					if(PlayerResources.currentSquare==50&&
					this.gp.chosenPath == 1)
					{
						this.gp.gf.updateUiMajorText("Return to School");
						this.gp.gf.updateUiMinorText
						("Pay "+GlobalFunctions.formatMoneyToString(50000));
						this.gp.manageMonies("MINUS", 50000 );
						this.gp.update_moneyText(PlayerResources.money);
						
						
					}
					
					break;
				}
				
				case "three-card":
				{
						
					if(this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.cancelSpin.length() > 0)
					{
						if( this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.cancelSpin == 1 )
						{
							this.gp.cancelSpin = true;
						}
					}
					
					cardNoClicked = parseInt(cardName.substr(5));
					this.setThreeCardResources(cardNoClicked);
					break;
				}
			}
			
		}
		
		private function setThreeCardResources(cardNoClicked:int):void
		{
			//we removed an indexed job so then, we will check for the job removed.
			if(this.doJobCheck)
			{
				if(cardNoClicked >= this.jobNumberRemoved){cardNoClicked++;}
			}
		
			//this.jobNumberRemoved
			
			switch(this.displayType)
			{	
				case "jobs":
				{ 
					if(this.contentsData.careerPath == "GRADUATE")
					{
						PlayerResources.salary = this.gp.positionsXML.game_data.jobs.graduate_jobs.job[cardNoClicked].salary.@monies;
						PlayerResources.maximum_salary = this.gp.positionsXML.game_data.jobs.graduate_jobs.job[cardNoClicked].maximum_salary.@monies;
						PlayerResources.salary_taxes = this.gp.positionsXML.game_data.jobs.graduate_jobs.job[cardNoClicked].taxes.@monies;

						PlayerResources.job = this.gp.positionsXML.game_data.jobs.graduate_jobs.job[cardNoClicked].title;
					}
					
					if(this.contentsData.careerPath == "NON_GRADUATE")
					{
						PlayerResources.salary = this.gp.positionsXML.game_data.jobs.non_graduate_jobs.job[cardNoClicked].salary.@monies;
						PlayerResources.maximum_salary = this.gp.positionsXML.game_data.jobs.non_graduate_jobs.job[cardNoClicked].maximum_salary.@monies
						PlayerResources.salary_taxes = this.gp.positionsXML.game_data.jobs.non_graduate_jobs.job[cardNoClicked].taxes.@monies;
						
						PlayerResources.job = this.gp.positionsXML.game_data.jobs.graduate_jobs.job[cardNoClicked].title;
					}
					
					break;
				}
				
				case "houses":
				{
					/*
					//Check to see if we need to add out
					var houseBuyingMoney:Number = 
					this.gp.positionsXML.game_data.houses.house[cardNoClicked].buy.@monies;
					
					
					var houseSellingMoney:Number = 
					this.gp.positionsXML.game_data.houses.house[cardNoClicked].sell.@monies;
					
					
					var houseInsuranceMoney:Number = 
					this.gp.positionsXML.game_data.houses.house[cardNoClicked].insurance.@monies;
					
					//If we have an old house, we need to sell it
					this.gp.manageMonies("PLUS", PlayerResources.house_selling_price);
					
					//Store the buying, selling and insurance price of this house here.
					PlayerResources.house_buying_price = houseBuyingMoney;
					PlayerResources.house_selling_price = houseSellingMoney;
					PlayerResources.house_insurance = houseInsuranceMoney;
					
					//minus the money taken to buy this house from cash in hand
					this.gp.manageMonies("MINUS", houseBuyingMoney);
					this.gp.update_moneyText(PlayerResources.money);
					
					*/
					
					break;
				}
			}
			
		}
		
		
		private function assignContentsData():void
		{
			
			switch(this.displayType)
			{
				case "jobs":
				{
					this.contentsData = new Jobs(this.gp.positionsXML);
					break;
				}
				
				case "houses":
				{
					//Get the type of house we need to load
					//this.gp.positionsXML.game_data.houses.house[cardNoClicked].buy.@monies;
					
					var houseType:String = "starter";
					
					if(this.gp.positionsXML.board_places
					.place[PlayerResources.currentSquare]
					.action.house_type.length()>0)
					{
						houseType = 
						this.gp.positionsXML.board_places
						.place[PlayerResources.currentSquare]
						.action.house_type;
					}
					
					this.contentsData = new Houses(this.gp.positionsXML, houseType);
					break;
				}
			}
		}
		
		private function populateHouseCard(card:Sprite, card_dataObj:Object):void
		{
					//create the textbox for the career name
					var spr:Sprite = new Sprite();
					//var careerName:TextField = new TextField();
					var careerName:TextField = aF.getCardText();
					card.addChild(spr);
					spr.addChild(careerName);
					careerName.text = card_dataObj.houseName;
					
					//careerName.background = true;
					//careerName.backgroundColor = 0xFF0000;
					careerName.width = 185;
					careerName.height = 24;
					//spr.x = 5;
					spr.y = 18;
					
					
					//create the textbox for the career name
					var salspr:Sprite = new Sprite();
					var salaryTxt:TextField = aF.getCardText();
					card.addChild(salspr);
					salspr.addChild(salaryTxt);
					var salaryTxtFormat:TextFormat = salaryTxt.getTextFormat();
					salaryTxtFormat.color = Config.APP_COLORS.REGULAR_GREEN;
					salaryTxtFormat.size = 20;
					salaryTxtFormat.bold = false;
					salaryTxt.defaultTextFormat = salaryTxtFormat;
					
					salaryTxt.text = card_dataObj.buyText;
					
					//salaryTxt.background = true;
					//salaryTxt.backgroundColor = 0xFF0000;
					salaryTxt.width = 185;
					salaryTxt.height = 24;
					//salspr.x = 5;
					salspr.y = 45;
					
					
					
					
					//Now, we will display this according to whether or not the user needs
					//a loan. If the user needs a loan, we make sure that the user knows
					//how much money he needs
					var buyMonies:Number = parseFloat(card_dataObj.buyMonies);
					
					if(PlayerResources.money<buyMonies)
					{
						
						var loanStr:String = "You need a &#163;"+
						(buyMonies-PlayerResources.money)+
						" loan";
						
					//create the textbox for the career name
					var loanTxtspr:Sprite = new Sprite();
					var loanTxt:TextField = aF.getCardText();
					card.addChild(loanTxtspr);
					loanTxtspr.addChild(loanTxt);
					
					var minSalaryTextFormat:TextFormat = loanTxt.getTextFormat();
					minSalaryTextFormat.color = Config.APP_COLORS.FONT_GREY;
					minSalaryTextFormat.size = 16;
					loanTxt.defaultTextFormat = minSalaryTextFormat;
					
					loanTxt.htmlText = loanStr;
					loanTxt.width = 185;
					loanTxt.height = 24;
					loanTxtspr.y = 70;
					
					}	
					
					
					
					
					//create the textbox for the career name
					var jobPicspr:Sprite = new Sprite();
					var carouselBitmap:Bitmap = card_dataObj.bit_map as Bitmap;
					card.addChild(jobPicspr);
					jobPicspr.addChild(carouselBitmap);
					jobPicspr.x = 31;
					jobPicspr.y = 99;
					


					
					
					
					
					//create the textbox for the career name
					var spr1:Sprite = new Sprite();
					//var careerName:TextField = new TextField();
					var careerName1:TextField = aF.getCardText();
					card.addChild(spr1);
					spr1.addChild(careerName1);
					careerName1.text = card_dataObj.sellText;
					
					//careerName.background = true;
					//careerName.backgroundColor = 0xFF0000;
					careerName1.width = 185;
					careerName1.height = 24;
					//spr.x = 5;
					spr1.y = 224;
					
					
					
					
					
					
					//create the textbox for the career name
					var minSalaryspr1:Sprite = new Sprite();
					var minSalaryTxt1:TextField = aF.getCardText();
					card.addChild(minSalaryspr1);
					minSalaryspr1.addChild(minSalaryTxt1);
					
					var minSalaryTextFormat1:TextFormat = minSalaryTxt1.getTextFormat();
					minSalaryTextFormat1.color = Config.APP_COLORS.FONT_GREY;
					minSalaryTextFormat1.size = 16;
					minSalaryTxt1.defaultTextFormat = minSalaryTextFormat1;
					
					minSalaryTxt1.text = card_dataObj.insuranceText;
					
					//minSalaryTxt1.background = true;
					//minSalaryTxt1.backgroundColor = 0xFF0000;
					minSalaryTxt1.width = 185;
					minSalaryTxt1.height = 24;
					//minSalaryspr1.x = 5;
					minSalaryspr1.y = 251;
					
					
		}
		
		private function populateJobCard(cardHolderBack:Sprite, dddd:Object):void
		{
			
			
					//create the textbox for the career name
					var spr:Sprite = new Sprite();
					//var careerName:TextField = new TextField();
					var careerName:TextField = aF.getCardText();
					cardHolderBack.addChild(spr);
					spr.addChild(careerName);
					careerName.text = dddd.jobName;
					
					//careerName.background = true;
					//careerName.backgroundColor = 0xFF0000;
					careerName.width = 185;
					careerName.height = 24;
					//spr.x = 5;
					spr.y = 23;
					
			
					
					
					
					
					
					//create the textbox for the career name
					var jobPicspr:Sprite = new Sprite();
					var carouselBitmap:Bitmap = dddd.bit_map as Bitmap;
					cardHolderBack.addChild(jobPicspr);
					jobPicspr.addChild(carouselBitmap);
					jobPicspr.x = 31;
					jobPicspr.y = 68;
					
					
					
					
					
					//create the textbox for the career name
					var salspr:Sprite = new Sprite();
					var salaryTxt:TextField = aF.getCardText();
					cardHolderBack.addChild(salspr);
					salspr.addChild(salaryTxt);
					var salaryTxtFormat:TextFormat = salaryTxt.getTextFormat();
					salaryTxtFormat.color = Config.APP_COLORS.REGULAR_GREEN;
					salaryTxtFormat.size = 18;
					salaryTxtFormat.bold = false;
					salaryTxt.defaultTextFormat = salaryTxtFormat;
					
					salaryTxt.text = dddd.salaryTxt;
					
					//salaryTxt.background = true;
					//salaryTxt.backgroundColor = 0xFF0000;
					salaryTxt.width = 185;
					salaryTxt.height = 24;
					//salspr.x = 5;
					salspr.y = 198;
					
					
					
					
					
					
					
					
					
					//create the textbox for the career name
					var minSalaryspr:Sprite = new Sprite();
					var minSalaryTxt:TextField = aF.getCardText();
					cardHolderBack.addChild(minSalaryspr);
					minSalaryspr.addChild(minSalaryTxt);
					
					var minSalaryTextFormat:TextFormat = minSalaryTxt.getTextFormat();
					minSalaryTextFormat.color = Config.APP_COLORS.FONT_GREY;
					minSalaryTextFormat.size = 13;
					minSalaryTxt.defaultTextFormat = minSalaryTextFormat;
					
					minSalaryTxt.text = dddd.maximum_salaryTxt;
					
					//minSalaryTxt.background = true;
					//minSalaryTxt.backgroundColor = 0xFF0000;
					minSalaryTxt.width = 185;
					minSalaryTxt.height = 24;
					//minSalaryspr.x = 5;
					minSalaryspr.y = 219;
					
					
					
					//new KeyboardControlTool(minSalaryspr);
					
					
					
					
					
					//create the textbox for the career name
					var payTaxspr:Sprite = new Sprite();
					var payTaxTxt:TextField = aF.getCardText();
					cardHolderBack.addChild(payTaxspr);
					payTaxspr.addChild(payTaxTxt);
					
					var payTaxTxtFormat:TextFormat = payTaxTxt.getTextFormat();
					//payTaxTxtFormat.color = Config.APP_COLORS.FONT_GREY;
					payTaxTxtFormat.size = 14;
					payTaxTxt.defaultTextFormat = payTaxTxtFormat;
					
					payTaxTxt.text = dddd.taxesTxt;
					
					//payTaxTxt.background = true;
					//payTaxTxt.backgroundColor = 0xFF0000;
					payTaxTxt.width = 185;
					payTaxTxt.height = 24;
					//payTaxspr.x = 5;
					payTaxspr.y = 247;
					
					
					
					//new KeyboardControlTool(payTaxspr);
					
			
		}
		
	
	}
	
}