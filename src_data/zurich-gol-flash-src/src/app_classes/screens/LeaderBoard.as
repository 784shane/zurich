package src.app_classes.screens
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import src.app_classes.forms.AppForms;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;
	
	import Config;
	import src.app_classes.GameFlow;
	import GlobalFunctions;
	import GeneralAnimations;
	import PlayerResources;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class LeaderBoard extends AppForms
	{
		
		include "../extensions/LeaderBoardScreenHelper.as";
		
		[Embed (source="../../../res/trash/13.Zurich_GOL_LEADERBOARDS_COMPANY.jpg" )]
		protected var blockScreenImg:Class;
		
		[Embed (source="../../../res/feed/thumbnails/Avatar-feed.1.png" )]
		protected var feedImg1:Class;
		
		[Embed (source="../../../res/feed/thumbnails/Avatar-feed.2.png" )]
		protected var feedImg2:Class;
		
		[Embed (source="../../../res/feed/thumbnails/Avatar-feed.3.png" )]
		protected var feedImg3:Class;
		
		[Embed (source="../../../res/feed/thumbnails/Avatar-feed.4.png" )]
		protected var feedImg4:Class;
		
		[Embed (source="../../../res/feed/thumbnails/Avatar-feed.5.png" )]
		protected var feedImg5:Class;
		
		[Embed (source="../../../res/feed/thumbnails/Avatar-feed.6.jpg" )]
		protected var feedImg6:Class;
		
		private var animatedParts:Array = new Array();
		
		private var gf:GameFlow;
		
		private var lb_data:Object;
		
		private var lb_rows:Sprite
		
		//private var leaderboardType:String = "default";
		
		private var leaderboardType:String = "default";
		
		public function LeaderBoard(gf:GameFlow)
		{
			this.gf = gf;
			this.gf.getLeaderBoardData(this.leaderboardDataReady);
		}
		
		private function leaderboardDataReady(obj:Object):void
		{
			//save the database data here. Now we can use this data
			//later when we are building the leaderboard
			this.lb_data = obj;
			this.gf.LeaderBoardInitiated();
		}
		
		public function buildLeaderboard():Sprite
		{
			return this.getLeaderBoardRow();
		}
		
		public function getLeaderboardButtons():Sprite
		{
			var leaderboardButtons:Sprite = new Sprite();
			/*
			var button1:Sprite = new Sprite();
			button1.graphics.beginFill(0xFFFFFF);
			button1.graphics.drawRoundRect(0, 0, 216, 62, 5, 5);
			//button1.graphics.drawRect(0, 0, 654, 62);
			button1.graphics.endFill();
			leaderboardButtons.addChild(button1);
			button1.name = "currentPositionButton";
			this.setButtonListeners(button1);
			*/
			//leaderBoardTab
			
			var button1:Sprite = leaderBoardTab("Current position",180,70,2);
			leaderboardButtons.addChild(button1);
			button1.name = "tab1";
			button1.y = 12;
			this.setButtonListeners(button1);
			//big x - 0
			
			
			var button2:Sprite = leaderBoardTab("Player top 5",180,70,2);
			leaderboardButtons.addChild(button2);
			button2.name = "tab2";
			//button2.x = button1.x+button1.width+1;
			button2.x = 234;
			//button2.y = 12;
			this.setButtonListeners(button2);
			//big x - 234
			
			
			var button3:Sprite = leaderBoardTab("Company top 5",180,70,2);
			leaderboardButtons.addChild(button3);
			button3.name = "tab3";
			button3.x = button2.x+button2.width+1;
			button3.y = 12;
			this.setButtonListeners(button3);
			
			
			
			
			button1.x = button2.x-(button1.width+1);
			
			//big x - 474
			
			
			
			
			return leaderboardButtons;
		}
		
		public function getLeaderBoardRow():Sprite
		{
			
			this.gf.setAppropriateTab("", "off");
			this.gf.setAppropriateTab("leaderboard", "on");
			
			
			var leaderBoardContainer:Sprite = new Sprite();
			
			
			//leaderBoardContainer.addChild(new blockScreenImg());
			
			
			leaderBoardContainer.addChild(GlobalFunctions.getBackDrop());
			
			var exitButtonSpr:Sprite =  new Sprite();
			exitButtonSpr.addChild(new popupClose());
			leaderBoardContainer.addChild(exitButtonSpr);
			exitButtonSpr.x = 941;
			exitButtonSpr.y = 64;
			exitButtonSpr.buttonMode = true;
			exitButtonSpr.addEventListener(MouseEvent.CLICK, this.exitClicked);
			
			
			
			
			var theButtons:Sprite = getLeaderboardButtons();
			leaderBoardContainer.addChild(theButtons);
			theButtons.x=162;
			theButtons.y=187;
			
			
		
			
			
			
			this.lb_rows = new Sprite();
			this.buildDefaultRow();
			leaderBoardContainer.addChild(this.lb_rows);
			this.lb_rows.x=163;
			this.lb_rows.y=245;
			
			
			var buttonTitle:String = "Resume Game";
			if(PlayerResources.currentSquare==128)
			{buttonTitle = "Play Again!";}
			
			var playButton:Sprite = this.getFormButton(buttonTitle);
			leaderBoardContainer.addChild(playButton);
			playButton.x = 401;
			playButton.y = 644;
			//playButton.mouseChildren = false;
			//new KeyboardControlTool(playButton);
				
			playButton.addEventListener(MouseEvent.CLICK, this.exitClicked);
			
			
			return leaderBoardContainer;
		}
		
		private function buildCompanyRow():void
		{
			
			this.animatedParts.length = 0;
			//topFiveCompanies":[{"company_name":"Robinsons","company_score":"64000"}
			//this.lb_data.leaderboard_data.topFivePlayers.length
			for(var i:int = 0; 
			i<this.lb_data.leaderboard_data.topFiveCompanies.length; i++)
			{
			
			//Making sure that I get different color for the leaderboard
			//rows. Here I am using a modulus
			var alternateColor:uint = 
			((i%2)==0)?Config.APP_COLORS.TABLE_ROW1:Config.APP_COLORS.TABLE_ROW2;
			
			var row:Sprite = new Sprite();
			
			
			//pic
			var picSurround:Sprite = new Sprite();
			picSurround.graphics.beginFill(alternateColor);
			picSurround.graphics.drawRoundRect(0, 0, 654, 62, 5, 5);
			//picSurround.graphics.drawRect(0, 0, 654, 62);
			picSurround.graphics.endFill();
			row.addChild(picSurround);
			//picSurround.x=164;
			//picSurround.y=245;
			
			
			var numText:TextField = getLeaderBoardText("numText");
			row.addChild(numText);
			numText.name = "numText";
			this.defineSpecificTextBoxes(numText);
			this.defineSpecificTextBoxStyle(numText);
			numText.text = ""+(i+1);
			
			
			var leaderboardCompanyNameTxt:TextField = getLeaderBoardText("CompNameText");
			row.addChild(leaderboardCompanyNameTxt);
			leaderboardCompanyNameTxt.name = "leaderboardCompanyNameTxt";
			this.defineSpecificTextBoxes(leaderboardCompanyNameTxt);
			this.defineSpecificTextBoxStyle(leaderboardCompanyNameTxt);
			leaderboardCompanyNameTxt.text = 
			this.lb_data.leaderboard_data.topFiveCompanies[i].company_name;
			
			
			
			var leaderboardTotalMoneyTxt:TextField = getLeaderBoardText("moneyText");
			row.addChild(leaderboardTotalMoneyTxt);
			leaderboardTotalMoneyTxt.name = "leaderboardTotalMoneyTxt";
			this.defineSpecificTextBoxes(leaderboardTotalMoneyTxt);
			this.defineSpecificTextBoxStyle(leaderboardTotalMoneyTxt);
			leaderboardTotalMoneyTxt.htmlText = 
			GlobalFunctions.formatMoneyToString(this.lb_data.leaderboard_data.topFiveCompanies[i].company_score);
			
			
			
			this.lb_rows.addChild(row);
			row.y = 0;//(i*62)+(i*2);
			this.animatedParts.push({item:row, Ypos:((i*62)+(i*2))});
			
			
			
			}		
		}
		
		private function buildDefaultRow(dataType:String = "topFive"):void
		{
			var useDefaultAvatar:Boolean = false;
			this.animatedParts.length = 0;
			
			var loopLength:int = 0;
			
			if(dataType == "MeAndOthers")
			{
				loopLength = 
				this.lb_data.leaderboard_data.meAndPlayers.length;
			}
			else
			{
				loopLength = 
				this.lb_data.leaderboard_data.topFivePlayers.length;
			}
			
			
			
			if(loopLength<2)
			{
				useDefaultAvatar = true;
			}
			
			
			
			
			for(var i:int = 0; i<loopLength; i++)
			{
			
			//Making sure that I get different color for the leaderboard
			//rows. Here I am using a modulus
			var alternateColor:uint = 
			((i%2)==0)?Config.APP_COLORS.TABLE_ROW1:Config.APP_COLORS.TABLE_ROW2;
			
			var row:Sprite = new Sprite();
			
			
			//pic
			var picSurround:Sprite = new Sprite();
			picSurround.graphics.beginFill(alternateColor);
			picSurround.graphics.drawRoundRect(0, 0, 654, 62, 5, 5);
			//picSurround.graphics.drawRect(0, 0, 654, 62);
			picSurround.graphics.endFill();
			row.addChild(picSurround);
			//picSurround.x=164;
			//picSurround.y=245;
			
			var numText:TextField = getLeaderBoardText("numText");
			row.addChild(numText);
			numText.name = "numText";
			this.defineSpecificTextBoxes(numText);
			this.defineSpecificTextBoxStyle(numText);
			numText.text = ""+(i+1);
			
			//leaderboard pic
			var userPicSurround:Sprite =  new Sprite();
			
			row.addChild(userPicSurround);
			userPicSurround.x = 62;
			userPicSurround.y = 13;
			
			
			
			var leaderboardNameTxt:TextField = getLeaderBoardText("nameText");
			row.addChild(leaderboardNameTxt);
			leaderboardNameTxt.name = "leaderboardNameTxt";
			this.defineSpecificTextBoxes(leaderboardNameTxt);
			this.defineSpecificTextBoxStyle(leaderboardNameTxt);
			
			
			
			var leaderboardCompanyNameTxt:TextField = getLeaderBoardText("CompNameText");
			row.addChild(leaderboardCompanyNameTxt);
			leaderboardCompanyNameTxt.name = "leaderboardCompanyNameTxt";
			this.defineSpecificTextBoxes(leaderboardCompanyNameTxt);
			this.defineSpecificTextBoxStyle(leaderboardCompanyNameTxt);
			
			
			
			var leaderboardTotalMoneyTxt:TextField = getLeaderBoardText("moneyText");
			row.addChild(leaderboardTotalMoneyTxt);
			leaderboardTotalMoneyTxt.name = "leaderboardTotalMoneyTxt";
			this.defineSpecificTextBoxes(leaderboardTotalMoneyTxt);
			this.defineSpecificTextBoxStyle(leaderboardTotalMoneyTxt);
			
			
			if(dataType == "MeAndOthers")
			{
			
				numText.text = this.lb_data.leaderboard_data.meAndPlayers[i].pos;
				
				
				leaderboardNameTxt.text = 
				this.lb_data.leaderboard_data.meAndPlayers[i].first_name+" "+
				this.lb_data.leaderboard_data.meAndPlayers[i].last_name;
				
				leaderboardTotalMoneyTxt.htmlText = 
				GlobalFunctions.formatMoneyToString(this.lb_data.leaderboard_data.meAndPlayers[i].monies);
				
				leaderboardCompanyNameTxt.text = 
				this.lb_data.leaderboard_data.meAndPlayers[i].company_name;
				
				if(useDefaultAvatar)
				{
				userPicSurround
				.addChild(this.getAppropriateIcon(
				GlobalFunctions.changeAvatarNameToInt(PlayerResources.player_avatar)
				));
				}
				else
				{
				userPicSurround
				.addChild(this.getAppropriateIcon(
				parseInt(this.lb_data.leaderboard_data.meAndPlayers[i].pic)
				));
				}
				
				//useDefaultAvatar = true;
				//GlobalFunctions.changeAvatarNameToInt(PlayerResources.player_avatar);
			}
			else
			{
				leaderboardNameTxt.text = 
				this.lb_data.leaderboard_data.topFivePlayers[i].first_name+" "+
				this.lb_data.leaderboard_data.topFivePlayers[i].last_name;
				
				leaderboardTotalMoneyTxt.htmlText = 
				GlobalFunctions.formatMoneyToString(this.lb_data.leaderboard_data.topFivePlayers[i].monies);
				
				leaderboardCompanyNameTxt.text = 
				this.lb_data.leaderboard_data.topFivePlayers[i].company_name;
				
				userPicSurround
				.addChild(this.getAppropriateIcon(
				parseInt(this.lb_data.leaderboard_data.topFivePlayers[i].pic)
				));
			
			}
			
			
			//row.alpha = 0;
			this.lb_rows.addChild(row);
			
			//row.rotationX = -90;
			row.y = 0;//(i*62)+(i*2);
			
			this.animatedParts.push({item:row, Ypos:(i*62)+(i*2)});
			
			}	
			
			
		}
		
		/*
	
	
	{"leaderboard_data":{"topFivePlayers":[{"first_name":"James","last_name":"Johnson","company_name":"Financial Times","pic":"1","monies":"2834000"},{"first_name":"John","last_name":"Smith","company_name":"FT","pic":"4","monies":"2444000"},{"first_name":"James","last_name":"Sloan","company_name":"Arc Finance Group limited","pic":"6","monies":"2386000"},{"first_name":"Elliott","last_name":"Dennahy","company_name":"Financial Times","pic":"6","monies":"1477000"},{"first_name":"Becca","last_name":"Sawyer","company_name":"Mindshare","pic":"4","monies":"1392000"}],"topFiveCompanies":[{"company_name":"Financial Times","company_score":"2834000"},{"company_name":"FT","company_score":"2444000"},{"company_name":"Arc Finance Group limited","company_score":"2386000"},{"company_name":"Mindshare","company_score":"1392000"},{"company_name":"Maido","company_score":"65000"}],"meAndPlayers":[{"first_name":"Elliott","last_name":"Dennahy","company_name":"Financial Times","pic":"6","monies":"1477000"},{"first_name":"Becca","last_name":"Sawyer","company_name":"Mindshare","pic":"4","monies":"1392000"},{"first_name":"James","last_name":"Sloan","company_name":"Arc Finance Group limited","pic":"6","monies":"1209000"},{"first_name":"Shane","last_name":"Daniel","company_name":"Maido","pic":"2","monies":"65000"}]}}
	
	
	
	*/
		
	
	}
	
}