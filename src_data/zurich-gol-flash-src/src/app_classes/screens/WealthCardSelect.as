package src.app_classes.screens
{
	import flash.filters.GlowFilter;
	import flash.text.TextField;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.events.MouseEvent;
	import flash.events.Event;
	
	//temp
	import src.app_classes.GamePlay;
	
	import src.app_classes.actions.FlipCard;
	import src.app_classes.forms.AppForms;
	import src.app_classes.config.DropShadows;
	import src.app_classes.config.GlowFilters;
	import GeneralAnimations;
	import GlobalFunctions;
	
	import src.app_classes.tools.KeyboardControlTool;
			
	
	public class WealthCardSelect extends AppForms
	{
		
		
		
		[Embed (source="../../../res/wealthCards/wealth_Z.png" )]
		protected var wealth_Z:Class;
		
		[Embed (source="../../../res/wealthCards/wealth_life.png" )]
		protected var wealth_life:Class;
		
		[Embed (source="../../../res/wealthCards/wealth_card_back.jpg" )]
		protected var wealthCardBack:Class;
		
		[Embed (source="../../../res/trash/05.Zurich_GOL_YOU_CHOSE.jpg" )]
		protected var ccchhho:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/0.png" )]
		protected var wealthCard1:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/1.png" )]
		protected var wealthCard2:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/2.png" )]
		protected var wealthCard3:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/3.png" )]
		protected var wealthCard4:Class;
		
		[Embed (source="../../../res/wealthCards/wealthCards/4.png" )]
		protected var wealthCard5:Class;
		
		//res\wealthCards\wealthCards
	
		private var myFilters:Array = new Array();
		
		private var randomCardArray:Array = new Array(0,1,2,3,4);
		
		private var wealthScrn:Sprite;
		
		private var titleSpr:Sprite;
		
		private var allCardsContainer:Sprite;
		
		
		private var cardYpos:Array = 
		[{pos:72},{pos:72},{pos:72},
		{pos:427},{pos:427}];
		
		
		private var noOfCardsAllowed:int = 3;
		
		private var noOfCardsToRemove:int;
		
		private var noOfCardsRemoved:int = 0;
		
		private var cardsShuffleCount:int = 0;
		
		private var cardsSelected:Array = new Array;
		
		private var functionUponCardsSelected:Function;
		
		private var gf:GamePlay;
		
		private var flipCd:FlipCard;
		
		private var showBackground:Boolean;
		
		public function WealthCardSelect(gf:GamePlay, whenSelected:Function, showBackground:Boolean = false)
		{
			
			this.gf = gf;
			this.showBackground = showBackground
			this.functionUponCardsSelected = whenSelected;
			
			//Randomise the array here so we can place one on each card.
			//Temporarily dont randomise array for testing purposes.
			this.setRandomArray();
			
            this.myFilters.push(new GlowFilters().getWealthCardGlow());
		}
		
		private function setRandomArray():void
		{
			//Randomise the array here so we can place one on each card.
			this.randomCardArray.sort(randomSort);
		}
		
		private function randomSort(a:*, b:*):Number
		{
		    if (Math.random() < 0.5) return -1;
		    else return 1;
		}
		
		public function getWealthCardSelectScreen(
		screenTitle:String="Select 3 Share The Wealth cards",
		noOfCardsToPick:int = 3 
		):Sprite
		{
			
			this.noOfCardsAllowed = noOfCardsToPick;
			
			//var wealthScrn:Sprite
			this.wealthScrn = new Sprite();
			
			this.allCardsContainer = new Sprite();
			
			if(this.showBackground)
			{
				this.wealthScrn.addChild(GlobalFunctions.getBackDrop());
			}
			
			//this.wealthScrn.addChild(new ccchhho());
			
			//We will place all the cards into this container
			this.wealthScrn.addChild(this.allCardsContainer);
			
			this.titleSpr = this.getWealthScreenTitleText(screenTitle);
			this.wealthScrn.addChild(this.titleSpr);
			this.titleSpr.x = 194;
			this.titleSpr.y = 367;
			
		
			
			
			
			//var spr1:Sprite = this.getWealthCardBackImg();
			//this.flipCd = new FlipCard();
			var spr1:Sprite = new Sprite();
			this.addExternal(spr1);
			var spr1Mc:MovieClip = new FlipCard().
			getFlipCard(this.getWealthCardBackImg(), this.getBackImg(this.randomCardArray[0]));
			spr1Mc.name = "flipCard";
			spr1.addChild(spr1Mc);
			this.allCardsContainer.addChild(spr1);
			spr1.mouseChildren = false;
			spr1.x = 283.5;//191;
			spr1.y = 72;
			spr1.name = "card0";
			
			this.setWealthCardEvents(spr1);
			spr1
			.addEventListener(MouseEvent.CLICK,
			this.wealthCardClicked);
                                  
			//var spr2:Sprite = this.getWealthCardBackImg();
			var spr2:Sprite = new Sprite();
			this.addExternal(spr2);
			var spr2Mc:MovieClip = new FlipCard().
			getFlipCard(this.getWealthCardBackImg(), this.getBackImg(this.randomCardArray[1]));
			spr2Mc.name = "flipCard";
			spr2.addChild(spr2Mc);
			this.allCardsContainer.addChild(spr2);
			spr2.mouseChildren = false;
			spr2.x = 487.5;//395;
			spr2.y = 72;
			spr2.name = "card1";
			
			this.setWealthCardEvents(spr2);
			spr2
			.addEventListener(MouseEvent.CLICK,
			this.wealthCardClicked);
			
			//var spr3:Sprite = this.getWealthCardBackImg();
			var spr3:Sprite = new Sprite();
			this.addExternal(spr3);
			var spr3Mc:MovieClip = new FlipCard().
			getFlipCard(this.getWealthCardBackImg(), this.getBackImg(this.randomCardArray[2]));
			spr3Mc.name = "flipCard";
			spr3.addChild(spr3Mc);
			this.allCardsContainer.addChild(spr3);
			spr3.mouseChildren = false;
			spr3.x = 691.5;//599
			spr3.y = 72;
			spr3.name = "card2";
			
			this.setWealthCardEvents(spr3);
			spr3
			.addEventListener(MouseEvent.CLICK,
			this.wealthCardClicked);
			
			var spr4:Sprite = new Sprite();
			this.addExternal(spr4);
			var spr4Mc:MovieClip = new FlipCard().
			getFlipCard(this.getWealthCardBackImg(), this.getBackImg(this.randomCardArray[3]));
			spr4Mc.name = "flipCard";
			spr4.addChild(spr4Mc);
			this.allCardsContainer.addChild(spr4);
			spr4.mouseChildren = false;
			spr4.x = 385.5;//293
			spr4.y = 427;
			spr4.name = "card3";
			
			this.setWealthCardEvents(spr4);
			spr4
			.addEventListener(MouseEvent.CLICK,
			this.wealthCardClicked);
			
			//var spr5:Sprite = this.getWealthCardBackImg();
			
			var spr5:Sprite = new Sprite();
			this.addExternal(spr5);
			var spr5Mc:MovieClip = new FlipCard().
			getFlipCard(this.getWealthCardBackImg(), this.getBackImg(this.randomCardArray[4]));
			spr5Mc.name = "flipCard";
			spr5.addChild(spr5Mc);
			this.allCardsContainer.addChild(spr5);
			spr5.mouseChildren = false;
			spr5.x = 589.5;//497;
			spr5.y = 427;
			spr5.name = "card4";
			
			this.setWealthCardEvents(spr5);
			spr5
			.addEventListener(MouseEvent.CLICK,
			this.wealthCardClicked);
			
			
			
			
			
			
			
			
			/*
			
			var rotationSprContainer:Sprite = new Sprite();
			var rotationSpr:MovieClip = new _cardRotationSpr() as MovieClip;
			//rotationSpr.addEventListener("addedToStage", cardSprAddedToScreen);
			//rotationSpr.name="rotationSpr"+i;
			//rotationSpr.buttonMode = true;
			//rotationSpr.mouseChildren = false;
			rotationSprContainer.addChild(rotationSpr);
			wealthScrn.addChild(rotationSprContainer);
				
			//rotationSprContainer.x = cardData[i].pos.x+96.5;
			//rotationSprContainer.y = cardData[i].pos.y;
			*/
			
			
			return this.wealthScrn;
		}
		
		private function setWealthCardEvents(card:Sprite):void
		{
			card.buttonMode = true;
			card.addEventListener(MouseEvent.MOUSE_OVER, wealthCardMoused);
			card.addEventListener(MouseEvent.MOUSE_OUT, wealthCardMoused);
		}
		
		private function wealthCardMoused(evt:MouseEvent):void
		{
			var cardRing:Sprite = 
			(evt.target.getChildByName("wealthCardExternal") as Sprite);
					
			switch(evt.type)
			{
				case "mouseOver":
				{
					GeneralAnimations.basic_displayItemToggle
					(cardRing,null);
					break;
				}
				
				case "mouseOut":
				{
					GeneralAnimations.basic_displayItemToggle
					(cardRing,null,0);
					break;
				}
			}
		}
		
		private function addExternal(spr:Sprite):void
		{
			var wealthCardExternal:Sprite = new Sprite();
			wealthCardExternal.graphics.beginFill(0xBB00BB, 1);
			wealthCardExternal.graphics.drawRoundRect(0, 0, 191, 296, 10, 10);
			wealthCardExternal.graphics.endFill();
			spr.addChild(wealthCardExternal);
			wealthCardExternal.name = "wealthCardExternal";
			wealthCardExternal.filters = this.myFilters;
			wealthCardExternal.alpha=0;
			
			wealthCardExternal.x = -92.5;
			wealthCardExternal.y = 3;
		}
		
		private function getWealthCardBackImg():Sprite
		{
			var wealthCardSprSurround:Sprite = new Sprite();
			wealthCardSprSurround.mouseChildren = false;
			wealthCardSprSurround.buttonMode = true;
			
			
			var wealthCardExternal:Sprite = new Sprite();
			wealthCardExternal.graphics.beginFill(0xBB00BB, 1);
			wealthCardExternal.graphics.drawRoundRect(0, 0, 191, 296, 10, 10);
			wealthCardExternal.graphics.endFill();
			wealthCardSprSurround.addChild(wealthCardExternal);
			wealthCardExternal.name = "wealthCardExternal";
			wealthCardExternal.filters = this.myFilters;
			wealthCardExternal.alpha=0;
			
			
			
			
			
			
			var wealthCardSpr:Sprite = new Sprite();
			
			
			
			
			
			var wealthCardBackSpr:Sprite = new Sprite();
			wealthCardBackSpr.graphics.beginFill(0xFFFFFF, 1);
			wealthCardBackSpr.graphics.drawRoundRect(0, 0, 185, 290, 10, 10);
			wealthCardBackSpr.graphics.endFill();
			wealthCardSpr.addChild(wealthCardBackSpr);
			
			
			//create the Interior sprite that will hold the
			//contents of the card
			var wealthCardInterior:Sprite = new Sprite();
			wealthCardSpr.addChild(wealthCardInterior);
			wealthCardInterior.x=6;
			wealthCardInterior.y=6;
			
			var wealthCardInteriorMask:Sprite = new Sprite();
			wealthCardInteriorMask.graphics.beginFill(0xBBBB00, 1);
			wealthCardInteriorMask.graphics.drawRect(0, 0, 173, 278);
			wealthCardInteriorMask.graphics.endFill();
			wealthCardSpr.addChild(wealthCardInteriorMask);
			wealthCardInteriorMask.x=6;
			wealthCardInteriorMask.y=6;
			
			wealthCardInterior.mask = wealthCardInteriorMask;
			
			
			//Now place the Zurich Z in the interior Sprite
			var zimgSpr:Sprite = new Sprite();
			var Z_Img:Bitmap = new wealth_Z() as Bitmap;
			zimgSpr.addChild(Z_Img);
			wealthCardInterior.addChild(zimgSpr);
			zimgSpr.x = -11;
			zimgSpr.y = -18;
			
			
			//Now place the Zurich Z in the interior Sprite
			var wealth_lifeSpr:Sprite = new Sprite();
			var wealth_lifeImg:Bitmap = new wealth_life() as Bitmap;
			wealth_lifeSpr.addChild(wealth_lifeImg);
			wealthCardInterior.addChild(wealth_lifeSpr);
			wealth_lifeSpr.x = 37;
			wealth_lifeSpr.y = 120;
			
			
			wealthCardSprSurround.addChild(wealthCardSpr);
			wealthCardSpr.x = 3;
			wealthCardSpr.y = 3;
			
			
			
			
			
			
			/*
			var ttr:TextField = new TextField();
			wealthCardSpr.addChild(ttr);
			ttr.text = GlobalFunctions.getRandomInt()+"";
			*/
			
			return wealthCardSprSurround;
		}
		
		private function getWealthScreenTitleText(screenTitle:String):Sprite
		{
			var poppupTitleTextSpr:Sprite = new Sprite();
			var poppupTitleText:TextField = this.popUpTitleText();
			poppupTitleText.name="screenTitle";
			poppupTitleTextSpr.addChild(poppupTitleText);
			poppupTitleText.text = screenTitle;
			
			
			return poppupTitleTextSpr
		}
		
		private function wealthCardClicked(evt:MouseEvent):void
		{
			var showSelect:Boolean = true;
			var newYPos:Number = 0;
			var card:Sprite = (evt.target as Sprite);
			var cardNoClicked:int = parseInt(card.name.substr(4));
			var cardXPos:int = card.x;
			
			
			if(cardYpos[cardNoClicked].pos==card.y)
			{showSelect=true; newYPos = card.y-8;}
			else
			{
			showSelect=false; 
			newYPos = cardYpos[cardNoClicked].pos;
			}
			
			var noInArray:Boolean = 
			this.manageCardsSelectedArray(cardNoClicked,showSelect, cardXPos);
			
			if(noInArray)
			{
				GeneralAnimations.applyBasicMove(evt.target, newYPos);
			}
				
			if(this.cardsSelected.length>(this.noOfCardsAllowed-1))
			{
				//We will start animating our cards here as we lookj to close the screen.
				//We will close the screen using the function following.
				this.startClosingProcedures();
				//this.functionUponCardsSelected(this.getAllCardsSelected());
			}
			
			
			
		}
		
		private function getAllCardsSelected():Array
		{
			var selCard:Array = new Array();
			for(var i:int=0; i<this.cardsSelected.length; i++)
			{
				selCard.push(this.randomCardArray[this.cardsSelected[i].card]);
			}
			return selCard;
		}
		
		private function manageCardsSelectedArray(cardPos:int, showSelect:Boolean, cardXPos:Number):Boolean
		{
			var doAnimation:Boolean = true;
			switch(showSelect)
			{
				case true:
				{
					if(this.cardsSelected.length<this.noOfCardsAllowed)
					{
					this.cardsSelected.push({card:cardPos, x:cardXPos});
					}
					else
					{
						doAnimation = false;
					}
					break;
				}
				
				case false:
				{
					for(var i:int=0; i<this.cardsSelected.length; i++)
					{
						if(this.cardsSelected[i].card==cardPos)
						{
							this.cardsSelected.splice(i,1);
							break;
						}
					}
					break;
				}
			}
			
			return doAnimation;
			
		}
		
		private function isCardSelected():void
		{
		
		}
		
		private function startClosingProcedures():void
		{
			
			//Here, we will be diming all the cards except for the ones selected.
			//Once this is done transistion the others to the center
			
			var cardsInContainer:int = 
			this.allCardsContainer.numChildren;
			
			this.noOfCardsToRemove = 
			cardsInContainer - this.noOfCardsAllowed;
			
			for(var i:int = 0; i<cardsInContainer; i++)
			{
				
					
				var selected:Boolean = false;
				for(var j:int = 0; j<this.noOfCardsAllowed; j++)
				{
					
					if(this.allCardsContainer.getChildAt(i).name.substr(4) == 
					this.cardsSelected[j].card)
					{
						selected = true;
						break;
					}
					
				}
				
				
				
				if(!selected)
				{
					//this.gf.tf.text += this.allCardsContainer.getChildAt(i).name;
					//Since this is not selected, we will make it invisible
					//now
					
					GeneralAnimations.basic_displayItemToggle
					(
					this.allCardsContainer.getChildAt(i),
					this.cardsRemoved, 0, 0.5, 0
					);
				}
				else
				{
					
					//Remove all MouseEvents
					
					(this.allCardsContainer.getChildAt(i) as Sprite)
					.buttonMode = false;
					
					(this.allCardsContainer.getChildAt(i) as Sprite)
					.removeEventListener
					(MouseEvent.MOUSE_OVER, wealthCardMoused);
					
					(this.allCardsContainer.getChildAt(i) as Sprite)
					.removeEventListener
					(MouseEvent.MOUSE_OUT, wealthCardMoused);
					
					(this.allCardsContainer.getChildAt(i) as Sprite)
					.removeEventListener
					(MouseEvent.CLICK, this.wealthCardClicked);
					
					
				}
				
					
			}
			
		}
		
		private function cardsRemoved():void
		{
			if((this.noOfCardsToRemove-1) == this.noOfCardsRemoved)
			{
				//this.shuffleCardsToShiftInPos();
				this.eraseScreenTitle();
			}
			//Every time we finish removing a card we will
			//increment this variable
			this.noOfCardsRemoved++;
		}
		
		private function shuffleCardsToShiftInPos():void
		{
			this.cardsSelected.sortOn(["x"], [Array.NUMERIC]);
			//We have now sorted the remaining cards on screen as to 
			//order from left to right, lets shuffle them now to the
			//center position of the screen.
			
			var workingSpace:Number = 
			((this.noOfCardsAllowed*20)-20)+(this.noOfCardsAllowed*185);
			
			var startingPoint:Number = ((980-workingSpace)/2)+92.5;
			
			
			for(var j:int = 0; j<this.noOfCardsAllowed; j++)
			{
				
				//Use the name of the card to get the Sprite
				var theCard:Sprite = 
				this.allCardsContainer.
				getChildByName("card"+this.cardsSelected[j].card) as Sprite;
				
				
				//Remove ring around card	
				(theCard
				.getChildByName("wealthCardExternal") as Sprite)
				.alpha = 0;
				
				GeneralAnimations.positionTo
				(
					theCard,
					this.uponCardsShuffle,
					startingPoint,
					231,
					0.5
				);
				
				//20 is the margin/space between the cards
				startingPoint+=theCard.width+20;	
				
			}
			
		}
		
		private function uponCardsShuffle():void
		{
			//cardsShuffleCount
			
			if((this.noOfCardsAllowed-1) == this.cardsShuffleCount)
			{
				this.displayOnScreenTitle();
			}
			//Every time we finish removing a card we will
			//increment this variable
			this.cardsShuffleCount++;
		}
		
		private function eraseScreenTitle():void
		{
			GeneralAnimations.basic_displayItemToggle
			(
				this.titleSpr,
				this.onScreenTitleErased, 0, 0.5, 0
			);
		}
		
		private function onScreenTitleErased():void
		{
			this.shuffleCardsToShiftInPos();
			/*
			this.titleSpr.y = 158;
			(this.titleSpr.getChildByName("screenTitle") as textField).text =
			"You have chosen...";
			//Reposition title, write new copy 
			//then make visible again
			*/
		}
		
		private var revealedSoFar:int = 0;
		private var mcs:Array = new Array();
		private var confirmButton:Sprite;
		
		private function displayOnScreenTitle():void
		{
			
			
			this.titleSpr.x = 197;
			this.titleSpr.y = 157;
			(this.titleSpr.getChildByName("screenTitle") as TextField).text =
			"You have chosen...";
			//Reposition title, write new copy 
			//then make visible again
			GeneralAnimations.basic_displayItemToggle
			(
				this.titleSpr,
				this.onScreenTitleRedisplayed, 1, 0.5, 0
			);
			
		}
		
		private function ready(evt:MouseEvent):void
		{
			this.functionUponCardsSelected(this.getAllCardsSelected());
		}
		
		
		private function onScreenTitleRedisplayed():void
		{
			this.startCardReveal();
		}
		
		private function startCardReveal():void
		{
			for(var j:int = 0; j<this.noOfCardsAllowed; j++)
			{
				
				//Use the name of the card to get the Sprite
				var theCard:Sprite = 
				this.allCardsContainer.
				getChildByName("card"+this.cardsSelected[j].card) as Sprite;
				
				var fc:MovieClip = theCard.getChildByName("flipCard") as MovieClip;
				this.mcs.push(fc);
				fc.addEventListener("enterFrame", playForward);
				//this.gf.tf.text += theCard.name+" - ";
				//(theCard.getChildByName("flipCard") as MovieClip).gotoAndPlay(3);
				/*
				evt.currentTarget.addEventListener("enterFrame", playForward);
				evt.currentTarget.gotoAndPlay(1);
				*/
				//
			}
			
			this.openClams();	
		}
		
		private function playForward(evt:Event):void
		{
			if(evt.currentTarget.currentFrame==20)
			{
				evt.currentTarget.gotoAndStop(20);
				evt.currentTarget.removeEventListener("enterFrame", playForward);
				this.openClams();
			}
		}
		
		private function openClams():void
		{
			if(this.revealedSoFar < this.noOfCardsAllowed)
			{
				this.mcs[this.revealedSoFar].gotoAndPlay(1);
			}
			else
			{
				
				//Position confirm button on page.
				this.confirmButton = getConfirmButton()
				this.wealthScrn.addChild(this.confirmButton);
				this.confirmButton.x = 401;
				this.confirmButton.y = 567;
				this.confirmButton.alpha = 0;
				
				this.confirmButton.addEventListener(MouseEvent.CLICK, this.ready);
			
			
				//Display the Button now so we can move on
				GeneralAnimations.basic_displayItemToggle
				(
					this.confirmButton,
					this.onConfirmButtonRedisplayed, 1, 0.5, 0
				);
			}
			this.revealedSoFar++;
		}
		
		private function onConfirmButtonRedisplayed():void
		{
				//Move on to next screen
		}
		
		private function getConfirmButton():Sprite
		{
			var continueButton:Sprite = getFormButton("Ok");
			//advertScrn.addChild(continueButton);
			//continueButton.x = 401;
			//continueButton.y = 590;
			
			//continueButton.addEventListener(MouseEvent.CLICK, this.continueButtonClicked);
			
			//this.cccB = continueButton;
			return continueButton;
		}
		
		private function getBackImg(num:int):Sprite
		{
			var continueButton:Sprite = new Sprite();
			switch(num)
			{
				case 0:
				{
					continueButton.addChild(new wealthCard1() as Bitmap);
					break;
				}
				
				case 1:
				{
					continueButton.addChild(new wealthCard2() as Bitmap);
					break;
				}
				
				case 2:
				{
					continueButton.addChild(new wealthCard3() as Bitmap);
					break;
				}
				
				case 3:
				{
					continueButton.addChild(new wealthCard4() as Bitmap);
					break;
				}
				
				default:
				{
					continueButton.addChild(new wealthCard5() as Bitmap);
				}
			}
			return continueButton;
		}
		
	
	}
	
}