package src.app_classes.screens.CardContents
{
	
	
	import flash.display.Bitmap;
	
	public class CardContents
	{
		
		protected var house_type:String = "starter";
		protected var display_type:String = "jobs";
		public var careerPath:String = "GRADUATE";
		
		
		////////////////////JOBS/////////////////////////////////////////////
		[Embed (source="../../../../res/cardImages/careers/accountant.png" )]
		protected var accountant:Class;
		
		[Embed (source="../../../../res/cardImages/careers/athlete.png" )]
		protected var athlete:Class;
		
		[Embed (source="../../../../res/cardImages/careers/financial-advisor.png" )]
		protected var financial_advisor:Class;
		
		[Embed (source="../../../../res/cardImages/careers/entertainer.png" )]
		protected var entertainer:Class;
		
		[Embed (source="../../../../res/cardImages/careers/graphicdesigner.png" )]
		protected var graphicdesigner:Class;
		
		[Embed (source="../../../../res/cardImages/careers/hairdresser.png" )]
		protected var hairdresser:Class;
		
		[Embed (source="../../../../res/cardImages/careers/lawyer.png" )]
		protected var lawyer:Class;
		
		[Embed (source="../../../../res/cardImages/careers/mechanic.png" )]
		protected var mechanic:Class;
		
		[Embed (source="../../../../res/cardImages/careers/salesperson.png" )]
		protected var salesperson:Class;
		
		[Embed (source="../../../../res/cardImages/careers/teacher.png" )]
		private var teacher:Class;
		
		[Embed (source="../../../../res/cardImages/careers/vet.png" )]
		protected var vet:Class;
		
		[Embed (source="../../../../res/cardImages/careers/policeman.png" )]
		protected var policeman:Class;
		
		
		
		////////////////////HOUSES///////////////////////////////////////////
		[Embed (source="../../../../res/cardImages/houses/executivesuite.png" )]
		protected var executivesuite:Class;
		
		[Embed (source="../../../../res/cardImages/houses/flat.png" )]
		protected var flat:Class;
		
		[Embed (source="../../../../res/cardImages/houses/maisonette.png" )]
		protected var maisonette:Class;
		
		[Embed (source="../../../../res/cardImages/houses/mansion.png" )]
		protected var mansion:Class;
		
		[Embed (source="../../../../res/cardImages/houses/modernvictorian.png" )]
		protected var modernvictorian:Class;
		
		[Embed (source="../../../../res/cardImages/houses/motorhome.png" )]
		protected var mobilehome:Class;
		
		[Embed (source="../../../../res/cardImages/houses/mountainchalet.png" )]
		protected var mountainchalet:Class;
		
		[Embed (source="../../../../res/cardImages/houses/penthouse.png" )]
		protected var penthousesuite:Class;
		
		[Embed (source="../../../../res/cardImages/houses/tudorstyle.png" )]
		protected var tudorstyle:Class;
		
		[Embed (source="../../../../res/cardImages/houses/mountainchalet.png" )]
		protected var luxurymountainchalet:Class;
		
		
		
		////////////////////TWO CARDS///////////////////////////////////////////
		[Embed (source="../../../../res/cardImages/twocardsImages/career.png" )]
		protected var career:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/college.png" )]
		protected var college:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/familypath.png" )]
		protected var familypath:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/pathoflife.png" )]
		protected var pathoflife:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/payrise.png" )]
		protected var payrise:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/riskypath.png" )]
		protected var riskypath:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/retiremansion.png" )]
		protected var retiremansion:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/countryestate.png" )]
		protected var countryestate:Class;
		
		
		
		
		
		protected var mainXML:XML;
		
		public function CardContents()
		{
			
		}
		
		
		public function get_numberOfItems():int
		{
			switch(this.display_type)
			{
				case "jobs":
				{
					return this.get_JobsAmt();
					break;
				}
				
				case "houses":
				{
					return this.get_HousesAmt();
					break;
				}
			}
			
			return 0;
			
		}
		
		
		private function get_JobsAmt():int
		{
			switch(this.careerPath)
			{
				case "GRADUATE":
				{
					return this.mainXML.game_data.jobs.graduate_jobs.job.length();
					break;
				}
				
				case "NON_GRADUATE":
				{
					return this.mainXML.game_data.jobs.non_graduate_jobs.job.length();
					break;
				}
			}
			
			return 0;
			
		}
		
		
		private function get_HousesAmt():int
		{
			if(this.house_type=="starter")
			{
				return this.mainXML.game_data
				.houses.starter.house.length();
			}
			return this.mainXML.game_data
			.houses.upgrades.house.length();
		}
		
		
		
		public function getCardContents(indx:int):Object
		{
			
			var obj:Object = new Object();
			switch(this.display_type)
			{
				case "jobs":
				{
			
			
			
					switch(this.careerPath)
					{
						case "GRADUATE":
						{
							obj.jobName = this.mainXML.game_data.jobs.graduate_jobs.job[indx].title;
							obj.salaryTxt = this.mainXML.game_data.jobs.graduate_jobs.job[indx].salary;
							obj.maximum_salaryTxt = this.mainXML.game_data.jobs.graduate_jobs.job[indx].maximum_salary;
							obj.taxesTxt = this.mainXML.game_data.jobs.graduate_jobs.job[indx].taxes;
							obj.bit_map = this.getCardBitmap(this.mainXML.game_data.jobs.graduate_jobs.job[indx].img.@code_name);
							break;
						}
						
						case "NON_GRADUATE":
						{
							obj.jobName = this.mainXML.game_data.jobs.non_graduate_jobs.job[indx].title;
							obj.salaryTxt = this.mainXML.game_data.jobs.non_graduate_jobs.job[indx].salary;
							obj.maximum_salaryTxt = this.mainXML.game_data.jobs.non_graduate_jobs.job[indx].maximum_salary;
							obj.taxesTxt = this.mainXML.game_data.jobs.non_graduate_jobs.job[indx].taxes;
							obj.bit_map = this.getCardBitmap(this.mainXML.game_data.jobs.non_graduate_jobs.job[indx].img.@code_name);
							break;
						}
					}
			
			
			
					break;
				}
				
				case "houses":
				{
					
					
					if(this.house_type=="starter")
					{
						
						//this.mainXML.game_data.houses.house.
						obj.houseName = this.mainXML.game_data.houses.starter.house[indx].title;
						obj.buyText = this.mainXML.game_data.houses.starter.house[indx].buy;
						obj.buyMonies = this.mainXML.game_data.houses.starter.house[indx].buy.@monies;
						obj.loanText = this.mainXML.game_data.houses.starter.house[indx].loan;
						obj.sellText = this.mainXML.game_data.houses.starter.house[indx].sell;
						obj.insuranceText = this.mainXML.game_data.houses.starter.house[indx].insurance;
						obj.bit_map = this.getCardBitmap(this.mainXML.game_data.houses.starter.house[indx].img.@code_name);
						
					}
					else
					{
						
						//this.mainXML.game_data.houses.house.
						obj.houseName = this.mainXML.game_data.houses.upgrades.house[indx].title;
						obj.buyText = this.mainXML.game_data.houses.upgrades.house[indx].buy;
						obj.buyMonies = this.mainXML.game_data.houses.upgrades.house[indx].buy.@monies;
						obj.loanText = this.mainXML.game_data.houses.upgrades.house[indx].loan;
						obj.sellText = this.mainXML.game_data.houses.upgrades.house[indx].sell;
						obj.insuranceText = this.mainXML.game_data.houses.upgrades.house[indx].insurance;
						obj.bit_map = this.getCardBitmap(this.mainXML.game_data.houses.upgrades.house[indx].img.@code_name);
						
					}
					
					
					break;
				}
			}
			
			
			
			return obj;
		
		}
		
		protected function getCardBitmap(codename:String):Bitmap
		{
			var bmp:Bitmap
			
			switch(codename)
			{
				case "college":
				{
					return new college() as Bitmap;
					break;
				}
				
				case "athlete":
				{
					return new athlete() as Bitmap;
					break;
				}
				
				case "entertainer":
				{
					return new entertainer() as Bitmap;
					break;
				}
				
				case "salesperson":
				{
					return new salesperson() as Bitmap;
					break;
				}
				
				case "mechanic":
				{
					return new mechanic() as Bitmap;
					break;
				}
				
				case "hairdresser":
				{
					return new hairdresser() as Bitmap;
					break;
				}
				
				case "accountant":
				{
					return new accountant() as Bitmap;
					break;
				}
				
				case "graphicdesigner":
				{
					return new graphicdesigner() as Bitmap;
					break;
				}
				
				case "financial_advisor":
				{
					return new financial_advisor() as Bitmap;
					break;
				}
				
				case "lawyer":
				{
					return new lawyer() as Bitmap;
					break;
				}
				
				case "teacher":
				{
					return new teacher() as Bitmap;
					break;
				}
				
				case "vet":
				{
					return new vet() as Bitmap;
					break;
				}
				
				case "policeman":
				{
					return new policeman() as Bitmap;
					break;
				}
				
		/////////////////////////////////////////////////////////////
				
				case "tudorstyle":
				{
					return new tudorstyle() as Bitmap;
					break;
				}
				
				case "mobilehome":
				{
					return new mobilehome() as Bitmap;
					break;
				}
				
				case "flat":
				{
					return new flat() as Bitmap;
					break;
				}
				
				case "maisonette":
				{
					return new maisonette() as Bitmap;
					break;
				}
				
				case "executivesuite":
				{
					return new executivesuite() as Bitmap;
					break;
				}
				
				case "modernvictorian":
				{
					return new modernvictorian() as Bitmap;
					break;
				}
				
				case "mansion":
				{
					return new mansion() as Bitmap;
					break;
				}
				
				case "penthousesuite":
				{
					return new penthousesuite() as Bitmap;
					break;
				}
				
				case "luxurymountainchalet":
				{
					return new luxurymountainchalet() as Bitmap;
					break;
				}
				
			}
			
			
			return new Bitmap();
			
		}
	
	}
	
	
	
}