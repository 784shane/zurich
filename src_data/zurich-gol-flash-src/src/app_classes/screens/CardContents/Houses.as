package src.app_classes.screens.CardContents
{
	
	import src.app_classes.screens.CardContents.CardContents;
	
	public class Houses extends CardContents
	{
		
		public function Houses(theXml:XML, houseType:String)
		{
			this.display_type = "houses";
			this.house_type = houseType;
			this.mainXML = theXml;
		}
	
	}
	
}