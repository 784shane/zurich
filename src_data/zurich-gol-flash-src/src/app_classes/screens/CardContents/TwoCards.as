package src.app_classes.screens.CardContents
{
	import flash.display.Bitmap;
	
	import src.app_classes.screens.CardContents.CardContents;
	
	public class TwoCards extends CardContents
	{
		
		public function TwoCards()
		{
			
		}
		
		public function getTwoCardImage(imgAlias:String):Bitmap
		{
			switch(imgAlias)
			{
				case "career":
				{
					return new career() as Bitmap;
					break;
				}
				
				case "college":
				{
					return new college() as Bitmap;
					break;
				}
				
				case "familypath":
				{
					return new familypath() as Bitmap;
					break;
				}
				
				case "pathoflife":
				{
					return new pathoflife() as Bitmap;
					break;
				}
				
				case "payrise":
				{
					return new payrise() as Bitmap;
					break;
				}
				
				case "riskypath":
				{
					return new riskypath() as Bitmap;
					break;
				}
				
				case "countryestate":
				{
					return new countryestate() as Bitmap;
					break;
				}
				
				case "retiremansion":
				{
					return new retiremansion() as Bitmap;
					break;
				}
			}
			
			return new riskypath() as Bitmap;
			
			
		}
		
		/*career:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/familypath.png" )]
		protected var familypath:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/pathoflife.png" )]
		protected var pathoflife:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/payrise.png" )]
		protected var payrise:Class;
		
		[Embed (source="../../../../res/cardImages/twocardsImages/riskypath.png" )]
		protected var riskypath*/
	
	}
	
}