package src.app_classes.screens.CardContents
{
	
	import src.app_classes.screens.CardContents.CardContents;
	import PlayerResources;
	
	public class Jobs extends CardContents
	{
		
		
		public function Jobs(theXml:XML)
		{
			//this.careerPath = "NON_GRADUATE";
			//this.careerPath = "GRADUATE";
			this.careerPath = PlayerResources.lifePath;
			this.display_type = "jobs";
			this.mainXML = theXml;
		}
	
	}
	
}