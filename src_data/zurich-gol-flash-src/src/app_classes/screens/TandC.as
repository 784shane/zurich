package src.app_classes.screens
{
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;
	import flash.text.TextFieldAutoSize;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	
	import src.app_classes.tools.MainStylesheet;
	import src.app_classes.config.TACText;
	import GlobalFunctions;
	import PlayerResources;
	
	
	
	import src.app_classes.forms.AppForms;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class TandC extends AppForms
	{
		
		private var yMousepos:Number = 0;
		
		private var scrollBarKnob:Sprite;
		
		private var tandcsBoxInterior:Sprite;
		
		private var tandcsBoxInteriorMask:Sprite;
		
		private var funcWhenExitButtonClicked:Function;
		/*
		[Embed (source="../../../res/trash/16.Zurich_GOL_T_AND_C.jpg" )]
		protected var tandcScreen:Class;
		*/
		
		public function TandC(whenExitClicked:Function)
		{
			this.funcWhenExitButtonClicked = whenExitClicked;
		}
		
		public function getTacs():Sprite
		{
			var tandcs:Sprite = new Sprite();
			
			
			tandcs.addChild(GlobalFunctions.getBackDrop());
			
			//tandcs.addChild(new tandcScreen());
			
			
			
			var tandcsBox:Sprite = new Sprite();
			tandcs.addChild(tandcsBox);
			tandcsBox.x = 260;
			tandcsBox.y = 117;
			
			var advertBack:Sprite = new Sprite();
			advertBack.graphics.lineStyle(2,0xFFFFFF);
			advertBack.graphics.beginFill(0xFFFFFF,0.3);
			advertBack.graphics.drawRoundRect(0, 0, 460, 563, 6, 6);
			advertBack.graphics.endFill();
			tandcsBox.addChild(advertBack);
			
			
			var closeButtonSpr:Sprite = new Sprite();
			var closeButton:Bitmap = new popupClose() as Bitmap;
			closeButtonSpr.addChild(closeButton);
			tandcsBox.addChild(closeButtonSpr);
			closeButtonSpr.buttonMode = true;
			closeButtonSpr.x = 411;
			closeButtonSpr.y = 27;
			
			closeButtonSpr.addEventListener(MouseEvent.CLICK, this.closeButtonClicked);
			
			
			this.tandcsBoxInterior = new Sprite();
			/*this.tandcsBoxInterior.graphics.beginFill(0xBBCC00,0.5);
			this.tandcsBoxInterior.graphics.drawRect(0, 0, 360, 503);
			this.tandcsBoxInterior.graphics.endFill();*/
			tandcsBox.addChild(this.tandcsBoxInterior);
			this.tandcsBoxInterior.x = 53;
			this.tandcsBoxInterior.y = 62;
			
			this.tandcsBoxInteriorMask = new Sprite();
			this.tandcsBoxInteriorMask.graphics.beginFill(0xBBCC00,0.5);
			this.tandcsBoxInteriorMask.graphics.drawRect(0, 0, 360, 445);
			this.tandcsBoxInteriorMask.graphics.endFill();
			tandcsBox.addChild(this.tandcsBoxInteriorMask);
			this.tandcsBoxInteriorMask.x = 53;
			this.tandcsBoxInteriorMask.y = 62;
			
			
			
			this.tandcsBoxInterior.mask = this.tandcsBoxInteriorMask;
			
			
			
			/*
			//set Interior text field
			var tf:TextField = this.getMainTextField()
			this.tandcsBoxInterior.addChild(tf);
			tf.x = 20;
			
			
			var tf1:TextField = this.getBodyTextField();//paragraphTextBox()
			this.tandcsBoxInterior.addChild(tf1);
			tf1.x = 20;
			tf1.y = tf.y+tf.height+5;
			
			
			var tf2:TextField = this.getMainTextField("h3 heading");
			this.tandcsBoxInterior.addChild(tf2);
			tf2.x = 20;
			tf2.y = tf1.y+tf1.height+5;
			
			
			var tf3:TextField = this.getBodyTextField();//paragraphTextBox()
			this.tandcsBoxInterior.addChild(tf3);
			tf3.x = 20;
			tf3.y = tf2.y+tf2.height+5;
			*/
			
			
			var tacArray:Array = new TACText().getTACText();
			var rowY:Number = 0;
			for(var lp:int = 0; lp<tacArray.length; lp++)
			{
				
				var tacRow:Sprite = new Sprite();
				this.tandcsBoxInterior.addChild(tacRow);
				tacRow.y = rowY;
				
				var tf:TextField = this.getMainTextField(tacArray[lp][0]);
				tacRow.addChild(tf);
				tf.x = 20;
				
				var tacBodyRow:Sprite = new Sprite();
				tacRow.addChild(tacBodyRow);
				tacBodyRow.y = tf.y+tf.height+5;
				
				var body_tf:TextField = this.getMainTextField(""+(lp+1));
				tacBodyRow.addChild(body_tf);
				body_tf.x = 20;
				
				var tf1:TextField = this.getBodyTextField(tacArray[lp][2]);//paragraphTextBox()
				tacBodyRow.addChild(tf1);
				tf1.x = 50;
				//tf1.y = tf.y+tf.height+5;
				
				rowY+=tacRow.height;
			
			}	
			
			
			
			
			
			var scrollBar:Sprite = getScrollBar();
			tandcs.addChild(scrollBar);
			scrollBar.x = 675;
			scrollBar.y = 184;
			
			
			
			return tandcs;
		}
		
		private function getScrollBar():Sprite
		{
		
			var scrollBar:Sprite = new Sprite();
			
			var scrollBarLong:Sprite = new Sprite();
			scrollBarLong.addChild(new scrollLong());
			scrollBar.addChild(scrollBarLong);
			scrollBar.name = "scrollBar";
			
			this.scrollBarKnob = new Sprite();
			this.scrollBarKnob.addChild(new scrollKnob());
			scrollBar.addChild(this.scrollBarKnob);
			this.scrollBarKnob.buttonMode = true;
			
			this.scrollBarKnob.addEventListener
			(MouseEvent.MOUSE_DOWN, this.activateScrollBar);
			
			PlayerResources.mainStage.addEventListener
			(MouseEvent.MOUSE_UP, this.activateScrollBar);
			
			return scrollBar;
			
		}
		
		private function activateScrollBar(evt:MouseEvent):void
		{
			
			switch(evt.type)
			{
				case "mouseUp":
				{
					PlayerResources.mainStage
					.removeEventListener
					(MouseEvent.MOUSE_MOVE, this.moveScrollKnob);
					
					break;
				}
				
				case "mouseDown":
				{
					this.yMousepos = evt.stageY;
					
					PlayerResources.mainStage
					.addEventListener
					(MouseEvent.MOUSE_MOVE, this.moveScrollKnob);
					break;
				}
			}
			
			
		}
		
		private function moveScrollKnob(evt:MouseEvent):void
		{
			
			if(evt.stageY>this.yMousepos)
			{
				this.scrollBarKnob.y += 
				(evt.stageY - this.yMousepos);
				
				if(this.scrollBarKnob.y >= 402)
				{
					this.scrollBarKnob.y = 402;
				}
				
				//this.tandcsBoxInterior
			}
			else
			{
				
				this.scrollBarKnob.y -= 
				(this.yMousepos - evt.stageY);
				
				if(this.scrollBarKnob.y<=0)
				{
					this.scrollBarKnob.y = 0;
				}
				
			}
			
			var perc:Number = 
			(this.scrollBarKnob.y/402)*100;
			
			var multiplicand:Number = 
			(this.tandcsBoxInterior.height<this.tandcsBoxInteriorMask.height)?
			this.tandcsBoxInteriorMask.height-this.tandcsBoxInterior.height:
			this.tandcsBoxInterior.height-this.tandcsBoxInteriorMask.height;
			
			this.tandcsBoxInterior.y = 
			(((perc/100)*(multiplicand))*-1)+62;
			
			this.yMousepos = evt.stageY;
			
		}
		
		public function getMainTextField
		(txtStr:String = "The Game of Life FT Adviser online game terms and conditions"):TextField
		{
			
			var headTxt:TextField = bodyHeaderTextBox();
			var tff:TextFormat = headTxt.getTextFormat();
			tff.size = 18;
			tff.align = "left";
			headTxt.defaultTextFormat = tff;
			headTxt.y = 0;
			headTxt.width = 320;
			//headTxt.height = 503;
			//headTxt.background = true;
			headTxt.wordWrap = true;
			headTxt.autoSize = TextFieldAutoSize.LEFT;
			
			headTxt.htmlText = txtStr;
			
			return headTxt;
			
		}	
		
		public function getBodyTextField(t:String = ""):TextField
		{
			
			var advertBodyTxt:TextField = paragraphTextBox();
			var advertBodyTxtFormat:TextFormat = advertBodyTxt.getTextFormat();
			advertBodyTxtFormat.size = 15;
			advertBodyTxtFormat.color = 0x5a5a5a;
			advertBodyTxtFormat.align = "left";
			advertBodyTxt.defaultTextFormat = advertBodyTxtFormat;
			advertBodyTxt.width = 290;
			//advertBodyTxt.background = true;
			advertBodyTxt.wordWrap = true;
			advertBodyTxt.autoSize = TextFieldAutoSize.LEFT;
			//advertBodyTxt.htmlText = "You use the";
			
			advertBodyTxt.htmlText = t;
			
			/*
			advertBodyTxt.htmlText = "You use the tag to define an HTML control in MXML. "+
			"Specify an id value if you intend to refer to a component elsewhere in your MXML, "+
			"either in another tag or in an ActionScript block. You specify the location of the "+
			"HTML page to display by setting the location property.The following example "+
			"demonstrates the use of an HTML control in a simple application. The HTML control's "+
			"location property is set to 'http://labs.adobe.com/', so that URL is opened in the "+
			"control when it loads. In addition, when the back and forward are clicked they "+
			"call the controls historyBack() and historyForward() methods. A TextInput control "+
			"allows the user to enter a URL location. When a third 'go' button is clicked, the "+
			"HTML control's location property is set to the text property of the input text field.";
			*/
			
			return advertBodyTxt;
			
		}	
		
		public function closeButtonClicked(evt:MouseEvent):void
		{
			this.funcWhenExitButtonClicked();
		}	
		
		
	
	}
	
}