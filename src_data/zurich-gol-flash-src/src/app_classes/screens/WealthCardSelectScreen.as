package src.app_classes.screens
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.Bitmap;
	
	import flash.text.TextField;
	
	import src.app_classes.tools.PictureLoader;
	import GlobalFunctions;
	
	public class WealthCardSelectScreen
	{
		
		private var cardBackImg:Object;
		private var noOfCards:int = 6;
		private var cardHolder:Sprite;
		public var unidentified_cards_onscreen:Array = new Array();
		public var tf:TextField;
		
		private var carSelectedResponseFunction:Function
		
		private var cardPosIdentity:Array = new Array();
		
		public function WealthCardSelectScreen(c_Back_img:Object, carSelectedResponseFunction:Function, tf:TextField)
		{
			this.tf = tf;
			this.carSelectedResponseFunction = carSelectedResponseFunction;
			this.cardBackImg = c_Back_img;
			this.buildCards();
		}
		
		public function getWealthCardScreen():Sprite
		{
			return this.cardHolder;
		}
		
		private function buildCards():void
		{
			this.cardHolder = new Sprite();
			
			this.cardHolder.addChild(GlobalFunctions.getBackDrop());
			
			var wealthCardCounter:int = 0;
			
			for(var a:int = 0; a<2; a++)
			{
				var yPos:int = 0+(a*290)+(a*65);
				
				for(var b:int = 0; b<3; b++)
				{
					var spr:Sprite = new Sprite();
					spr.addChild(new Bitmap(Bitmap(this.cardBackImg).bitmapData));
					this.unidentified_cards_onscreen.push(spr);
					spr.name = "anonymous_wealthCard_"+wealthCardCounter;
					spr.buttonMode = true;
					this.cardHolder.addChild(spr);
					
					this.cardPosIdentity.push(Math.floor(Math.random()*3));
					
					spr.addEventListener(MouseEvent.CLICK,this.carSelectedResponseFunction);
					spr.x = 0+(b*185)+(b*18);
					spr.y = yPos;
					
					wealthCardCounter++;
				}
			}
			
		}
		
		public function getCardPositionIdentityArray():Array
		{
			return this.cardPosIdentity;
		}
	
	}
	
}