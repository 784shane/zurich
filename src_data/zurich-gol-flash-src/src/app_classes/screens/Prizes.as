package src.app_classes.screens
{
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFieldAutoSize;
	import flash.events.MouseEvent;
	
	import src.app_classes.forms.AppForms;
	import src.app_classes.GameFlow;
	import GlobalFunctions;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class Prizes extends AppForms
	{	
		
		[Embed (source="../../../res/prizes/prizes_main.png" )]
		protected var prizesImg:Class;
		
		[Embed (source="../../../res/trash/15.Zurich_GOL_PRIZES.jpg" )]
		protected var testScreen:Class;
		
		private var gf:GameFlow;
	
		public function getPrizes():Sprite
		{
			this.gf.setAppropriateTab("", "off");
			this.gf.setAppropriateTab("prizes", "on");
			
			var prizes:Sprite = new Sprite();
			
			//prizes.addChild(new testScreen());
			
			prizes.addChild(GlobalFunctions.getBackDrop());
			
			
			
			var exitButtonSpr:Sprite =  new Sprite();
			exitButtonSpr.addChild(new popupClose());
			prizes.addChild(exitButtonSpr);
			exitButtonSpr.x = 941;
			exitButtonSpr.y = 64;
			exitButtonSpr.buttonMode = true;
			exitButtonSpr.addEventListener(MouseEvent.CLICK, this.exitClicked);
			
			
			
			
			var prizesImgSpr:Sprite = new Sprite();
			prizesImgSpr.addChild(new prizesImg());
			prizesImgSpr.x = 277;
			prizesImgSpr.y = 145;
			
			prizes.addChild(prizesImgSpr);
			
			
			var prizesTextHolder:Sprite = new Sprite();
			
			
			var tf1:TextField = this.getBodyTextField("This game will be available to play for four weeks "+ 
			"from 5th November 2013 - 05 December 2013. At the "+
			"end of these four weeks, the player with the highest "+
			"score will win an iPad. The player with the second "+
			"highest score will win an iPad Mini.");//paragraphTextBox()
			prizesTextHolder.addChild(tf1);
			
			var tf2:TextField = this.getBodyTextField("The company leaderboard will display the sum of the "+
			"highest daily score from the players within that "+
			"particular company. Play with your team for a chance to "+
			"win Red Letter Day vouchers to the value of &#163;100 per person, "+
			"up to a maximum value of &#163;2,500.");//paragraphTextBox()
			prizesTextHolder.addChild(tf2);
			tf2.y = tf1.y+tf1.height+10;
			
			
			/*
			var tf1:TextField = this.getBodyTextField(tacArray[lp][2]);//paragraphTextBox()
				tacBodyRow.addChild(tf1);
				tf1.x = 50;
				//tf1.y = tf.y+tf.height+30;
			*/
			
			prizes.addChild(prizesTextHolder);
			prizesTextHolder.x = 321;
			prizesTextHolder.y = 452;
			
			
			//new KeyboardControlTool(prizesTextHolder);
			
			return prizes;
		}
	
		public function Prizes(gf:GameFlow)
		{
			this.gf = gf;
		}
		
		private function exitClicked(evt:MouseEvent):void
		{
			this.gf.setAppropriateTab("", "off");
			this.gf.closePrizes();
		}
		
		public function getBodyTextField(t:String = ""):TextField
		{
			
			var advertBodyTxt:TextField = paragraphTextBox();
			var advertBodyTxtFormat:TextFormat = advertBodyTxt.getTextFormat();
			advertBodyTxtFormat.size = 15;
			advertBodyTxtFormat.color = 0x5a5a5a;
			advertBodyTxtFormat.align = "left";
			advertBodyTxt.defaultTextFormat = advertBodyTxtFormat;
			advertBodyTxt.width = 334;
			//advertBodyTxt.background = true;
			advertBodyTxt.wordWrap = true;
			advertBodyTxt.autoSize = TextFieldAutoSize.LEFT;
			//advertBodyTxt.htmlText = "You use the";
			
			advertBodyTxt.htmlText = t;
			
			/*
			advertBodyTxt.htmlText = "You use the tag to define an HTML control in MXML. "+
			"Specify an id value if you intend to refer to a component elsewhere in your MXML, "+
			"either in another tag or in an ActionScript block. You specify the location of the "+
			"HTML page to display by setting the location property.The following example "+
			"demonstrates the use of an HTML control in a simple application. The HTML control's "+
			"location property is set to 'http://labs.adobe.com/', so that URL is opened in the "+
			"control when it loads. In addition, when the back and forward are clicked they "+
			"call the controls historyBack() and historyForward() methods. A TextInput control "+
			"allows the user to enter a URL location. When a third 'go' button is clicked, the "+
			"HTML control's location property is set to the text property of the input text field.";
			*/
			
			return advertBodyTxt;
			
		}
		
	}
	
	
		
		
	
}