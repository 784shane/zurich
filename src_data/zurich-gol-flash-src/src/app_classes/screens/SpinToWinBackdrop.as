package src.app_classes.screens
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	
	import src.app_classes.GamePlay;
	import src.app_classes.forms.AppForms;
	import GlobalFunctions;
	import src.app_classes.tools.KeyboardControlTool;
	
	public class SpinToWinBackdrop extends AppForms
	{
		
		[Embed (source="../../../res/trash/spinToWin/spintowin_labelGraphic.png" )]
		protected var spintowinGraphicx:Class;
		
		[Embed (source="../../../res/trash/06.Zurich_GOL_SPIN_EXAMPLE.jpg" )]
		protected var backd:Class;
		
		private var gp:GamePlay;
		private var spinningType:String = "spin_to_win";//retirement_spin
		
		public function SpinToWinBackdrop(gp:GamePlay)
		{
			this.gp = gp;
		}
		
		public function buildSpinToWinBackdrop(spinning_type:String):Sprite
		{
			
			this.spinningType = spinning_type;
			this.gp.typeOfSpin = spinning_type;
			
			
			//Create a backgroundConmtainer
			var backdrop:Sprite = new Sprite();
			
			//Display a background
			//backdrop.addChild(GlobalFunctions.getBackDrop());
			
			//backdrop.addChild(new backd());
			
			
			
			
			
			var poppupTitleTextSpr:Sprite = new Sprite();
			var poppupTitleText:TextField = popUpTitleText();
			poppupTitleTextSpr.addChild(poppupTitleText);
			backdrop.addChild(poppupTitleTextSpr);
			
			poppupTitleText.text = this.getSpinningPageTitleText();
			poppupTitleTextSpr.x = 192;
			poppupTitleTextSpr.y = 90;
			
			
			
			if(this.spinningType == "spin_to_win")
			{
				//This is the background graphic that is built for the info container.
				var stw_banner:Sprite = new Sprite();//spinToWinBanner();
				stw_banner.addChild(new spintowinGraphicx());
				backdrop.addChild(stw_banner);
				stw_banner.x = 227;
				stw_banner.y = 145;
			}	
			
			return backdrop;
			
		}
		
		private function getSpinningPageTitleText():String
		{
			
			switch(this.spinningType)
			{
				case "regular":
				{
					return "Spin to move!";
					break;
				}
				
				case "enhance_pension":
				{
					return "Spin to enhance your pension!";
					break;
				}
				
				default:
				{
					//Only if spin to win will we be removing panels
					this.gp.removePanels_ForSpinToWin();
					return "Spin to Win!";
				}
			}
			
			return "Spin to Win!";
		}
	
	}
	
}