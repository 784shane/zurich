package src.app_classes.screens
{
	
	import src.app_classes.forms.AppForms;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import com.greensock.TweenLite;
	import com.greensock.easing.*;
	
	import src.app_classes.tools.Ribbon;
	import src.app_classes.tools.RollText;
	import GeneralAnimations;
	import PlayerResources;
	import src.app_classes.GameFlow;
	import GlobalFunctions;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class GrandTotal extends AppForms
	{
		
		[Embed (source="../../../res/totalScreen/total_circle.png" )]
		protected var total_circle:Class;
		
		[Embed (source="../../../res/totalScreen/star.png" )]
		protected var star:Class;
		
		[Embed (source="../../../res/totalScreen/smallLCards.png" )]
		protected var smallLCards:Class;
		
		[Embed (source="../../../res/totalScreen/totalZ.png" )]
		protected var totalZ:Class;
		
		
		/*[Embed (source="../../../res/trash/congrats.jpg" )]
		protected var congrats:Class;*/
		
		[Embed (source="../../../res/totalScreen/total_circle_separated.png" )]
		protected var total_circle_separated:Class;
		
		[Embed (source="../../../res/trash/totals/12.Zurich_GOL_GAME_TOTAL_4.jpg" )]
		protected var congrats:Class;
		
		
		private var activityText:TextField;
		private var adjustmentText:TextField;
		private var debugText:TextField;
		private var ribbonBarLeft:Sprite;
		private var ribbonBarRight:Sprite;
		private var ribbonBowLeft:Sprite;
		private var ribbonBowRight:Sprite;
		private var ribbonOpenCount:int = 0;
		private var noOfScreenRounds:int = 0;
		private var screenRoundsCount:int = 0;
		private var theRibbon:Ribbon;
		private var rollTextHolder:Sprite;
		
		private var activityCopy:String;
		private var adjustmentCopy:String;
		private var starBox:Sprite;
		private var smallLCardsSprite:Sprite;
		
		private var totalScreen:Sprite;
		private var buttonContainer:Sprite;
		private var activityTextSpr:Sprite;
		private var adjustmentTextSpr:Sprite;
		
		private var separatedCirleSprite:Sprite;
		private var expBow:MovieClip;
		
		private var gf:GameFlow;
		
		
		private var ccc:int = 0;
		
		private var GrandTotalContent:Array = 
		[
		{activity:"calc_loans_and_int", activityText:"REPAY ALL\nLOANS AND INTEREST"},
		{activity:"sell_property", activityText:"SELL YOUR\nPROPERTY"},
		{activity:"calc_money_for_children", activityText:
		GlobalFunctions.formatMoneyToString(10000)
		+" FOR EACH\nCHILD FROM THE BANK"},
		{activity:"calc_money_for_life_tiles", activityText:"TOTAL LIFE TILES"},
		{activity:"congratulations", activityText:"CONGRATULATIONS!"}
		];
		
		//GlobalFunctions.formatMoneyToString
		
		public function GrandTotal(the_gf:GameFlow)
		{
			this.gf = the_gf;
			this.noOfScreenRounds = 
			this.GrandTotalContent.length;
		}
		
		public function getScreen():Sprite
		{
			this.totalScreen = new Sprite();
			
			this.totalScreen.addChild(GlobalFunctions.getBackDrop());
			
			//temp congrats screen
			//this.totalScreen.addChild(new congrats());
			
			
			var totalZzz:Sprite = new Sprite();
			totalZzz.addChild(new totalZ());
			this.totalScreen.addChild(totalZzz);
			totalZzz.x = 468;
			totalZzz.y = 115;
			
			
			//new KeyboardControlTool(totalZzz);
			
			
			
			
			var spr:MovieClip = new animatedCircle() as MovieClip;
			spr.addEventListener("addedToStage", animatedCircleAdded);
			this.totalScreen.addChild(spr);
			spr.x = 288;
			spr.y = 130;
			
			
			this.starBox = new Sprite();
			var star1:Bitmap = this.getStar();
			this.starBox.addChild(star1);
			var star2:Bitmap = this.getStar();
			this.starBox.addChild(star2);
			star2.x = 40;
			var star3:Bitmap = this.getStar();
			this.starBox.addChild(star3);
			star3.x = 80;
			this.totalScreen.addChild(this.starBox);
			this.starBox.x = 443;
			this.starBox.y = 310;
			this.starBox.alpha = 0;
			
			
			this.smallLCardsSprite = new Sprite();
			this.smallLCardsSprite.addChild(new smallLCards());
			this.totalScreen.addChild(this.smallLCardsSprite);
			this.smallLCardsSprite.x = 459;
			this.smallLCardsSprite.y = 257;
			this.smallLCardsSprite.alpha = 0;
			
			
			//smallLCards
			
			
			
			this.activityTextSpr = this.theActivityText();
			this.totalScreen.addChild(this.activityTextSpr);
			this.activityTextSpr.x = 361;
			this.activityTextSpr.y = 219;
			//spr1.y = 202;
			
			
			
			this.adjustmentTextSpr = this.theAdjustmentText();
			this.totalScreen.addChild(this.adjustmentTextSpr);
			this.adjustmentTextSpr.x = 361;
			this.adjustmentTextSpr.y = 336;
			
			//new KeyboardControlTool(this.adjustmentTextSpr);
			
			
			
			
			
			
			//add ribbon with total score
			this.theRibbon = new Ribbon();
			this.totalScreen.addChild(this.theRibbon);
			this.theRibbon.x = 236;
			this.theRibbon.y = 373;
			
			this.separatedCirleSprite = new Sprite();
			var separatedCirle:Bitmap = new total_circle_separated() as Bitmap;
			this.separatedCirleSprite.addChild(separatedCirle);
			this.totalScreen.addChild(this.separatedCirleSprite);
			//this.separatedCirleSprite.addChild(this.expBow);
			//total_circle_separated
			this.separatedCirleSprite.x = 288;
			this.separatedCirleSprite.y = 130;
			this.separatedCirleSprite.alpha = 0;
			
			
			
			this.debugText = new TextField();
			this.separatedCirleSprite.addChild(this.debugText);
			this.debugText.text = "";
			
			
			
			
			this.expBow = new expandedBow() as MovieClip;
			this.totalScreen.addChild(this.expBow);
			this.expBow.x = 224;
			this.expBow.y = 115;
			this.expBow.alpha = 0;
			
			
			this.expBow.addEventListener("addedToStage", function(evt:Event):void
			{
				(evt.target as MovieClip).gotoAndStop(1);
			});
			
			
			
			
			//add ribbon with total score
			//This this.rollTextHolder will hold the 
			//text for the number roll
			this.rollTextHolder = new Sprite();
			this.totalScreen.addChild(this.rollTextHolder);
			this.rollTextHolder.x = 289;
			this.rollTextHolder.y = 387;
			
			
			
			
			
			
			this.buttonContainer =  new Sprite();
			
			
			
			
			
			var buttonHeadSpr:Sprite = new Sprite();
			var headTxt:TextField = bodyHeaderTextBox();
			var tff:TextFormat = headTxt.getTextFormat();
			tff.size = 18;
			//tff.align = "left";
			headTxt.defaultTextFormat = tff;
			headTxt.width = 980;
			//headTxt.background = true;
			//headTxt.wordWrap = true;
			//headTxt.autoSize = TextFieldAutoSize.CENTER;
			headTxt.htmlText = 
			"You can either stick with this or try "+
			"your luck and play again?";
			
			
			var headTxt1:TextField = bodyHeaderTextBox();
			var tff1:TextFormat = headTxt1.getTextFormat();
			tff1.size = 15;
			//tff1.align = "left";
			headTxt1.defaultTextFormat = tff1;
			headTxt1.width = 980;
			//headTxt1.background = true;
			//headTxt1.wordWrap = true;
			//headTxt1.autoSize = TextFieldAutoSize.CENTER;
			headTxt1.htmlText = 
			"(If this is not your highest score, "+
			"submitting it will not overwrite your top score.)";
			
			//this.totalScreen.addChild(buttonHeadSpr);
			this.buttonContainer.addChild(buttonHeadSpr);
			buttonHeadSpr.addChild(headTxt);
			buttonHeadSpr.addChild(headTxt1);
			headTxt1.y = 36;
			
			
			
			
			
			
			
			var continueButton:Sprite = getFormButton("Stick with this...");
			this.buttonContainer.addChild(continueButton);
			continueButton.x = 301;
			continueButton.y = 78;
			continueButton.name = "stick_button";
			
			
			
			var continueButton1:Sprite = getFormButton("Play again");
			this.buttonContainer.addChild(continueButton1);
			continueButton1.x = 501;
			continueButton1.y = 78;
			continueButton1.name = "play_button";
			
			continueButton.addEventListener(MouseEvent.CLICK, this.continueButtonClicked);
			continueButton1.addEventListener(MouseEvent.CLICK, this.continueButtonClicked);
			
			
			
			
			return this.totalScreen;
			
		}
		
		private function continueButtonClicked(evt:MouseEvent):void
		{
			switch(evt.target.name)
			{
				case "stick_button":
				{
					evt.target.removeEventListener(MouseEvent.CLICK, this.continueButtonClicked);
				
					
				
					this.gf.leaderboardUponGameEnd();
					break;
				}
				
				case "play_button":
				{
					this.gf.restartGame();
					break;
				}
			}
		}
		
		private function addButtons():void
		{
			this.buttonContainer.alpha = 0;
			this.totalScreen.addChild(this.buttonContainer);
			this.buttonContainer.x = 0;//301;
			this.buttonContainer.y = 552;//630;
			//this.buttonContainer.alpha = 1;
		}
		
		private function setNextDisplay():void
		{
		
			var newAdjstCopy:String;
			
			this.screenRoundsCount++;
			if(this.screenRoundsCount<this.noOfScreenRounds)
			{
			
			var theAct:String = 
			this.GrandTotalContent[this.screenRoundsCount].activity;
			
			
			switch(theAct)
			{
			
				case "calc_loans_and_int":
				{
					
					
					
					
					this.adjustmentCopy = "";
					break;
				}
			
				case "sell_property":
				{
					var houseMoney:Number = PlayerResources.house_selling_price;
					newAdjstCopy = "+"+
					GlobalFunctions.formatMoneyToString(houseMoney);
					this.adjustmentCopy = newAdjstCopy;
					
					PlayerResources.money = PlayerResources.money+houseMoney;
					
					break;
				}
			
				case "calc_money_for_children":
				{
				
					var moneyPerChild:Number = PlayerResources.numOfChildren*10000;
					newAdjstCopy = "+"+
					GlobalFunctions.formatMoneyToString(moneyPerChild);
					this.adjustmentCopy = newAdjstCopy;
					
					PlayerResources.money = PlayerResources.money+moneyPerChild;
					
					break;
				}
			
				case "calc_money_for_life_tiles":
				{
					var moneyPerLifeCards:Number = PlayerResources.lifeCards*2000;
					newAdjstCopy = "+"+
					GlobalFunctions.formatMoneyToString(moneyPerLifeCards);
					this.adjustmentCopy = newAdjstCopy;
					
					PlayerResources.money = PlayerResources.money+moneyPerLifeCards;
					
					break;
				}
			
				case "congratulations":
				{
					this.adjustmentCopy = "YOUR FINAL SCORE IS";
					break;
				}
				
			}
			
			
				this.activityCopy =
				this.GrandTotalContent[this.screenRoundsCount].activityText;
				//this.adjustmentCopy = "-90000";
				this.startDisplayProcess();
			}
			else
			{
				
				this.gf.sendGoolgleAnaylticsInfo("Completed_Game");
				
				
				this.addButtons();
				//fade in the buttons now
				GeneralAnimations.basic_displayItemToggle
				(this.buttonContainer, null, 1, 1, 0);
						
			}			
		}
		
		private function activitySprSetting(stage:String):void
		{
			
			var actTxtBox:TextField = (this.activityTextSpr
					.getChildByName("activityText") as TextField);
					
			var actTF:TextFormat = actTxtBox.getTextFormat();
					
			switch(stage)
			{
			
				case "calc_loans_and_int":
				{
					break;
				}
			
				case "sell_property":
				{
					
					break;
				}
			
				case "calc_money_for_children":
				{
					//actTF.size = 55;
					//actTxtBox.defaultTextFormat = actTF;
					actTxtBox.width = 370;
					//actTxtBox.background = true;
					this.activityTextSpr.x = 306;
					this.activityTextSpr.y = 220;
					
					break;
				}
			
				case "calc_money_for_life_tiles":
				{
					this.activityTextSpr.y = 190;
					break;
				}
			
				case "congratulations":
				{
					
					actTF.size = 55;
					actTxtBox.defaultTextFormat = actTF;
					actTxtBox.width = 294;
					this.activityTextSpr.x = 343;
					this.activityTextSpr.y = 238;
					
					
					var adjTF:TextFormat = this.adjustmentText.getTextFormat();
					adjTF.size = 25;
					this.adjustmentText.defaultTextFormat = adjTF;
					this.adjustmentTextSpr.y = 332;
					/*actTxtBox.width = 294;
					this.activityTextSpr.x = 343;
					this.activityTextSpr.y = 332;*/
					
					break;
				}
				
			}
		}
		
		private function startDisplayProcess():void
		{
			this.SwitchOffSprite("activityText");
		}
		
		private function startDisplayProcess2():void
		{
			this.SwitchOffSprite("adjustmentText");
		}
		
		private function setDisplayTexts():void
		{
			this.activitySprSetting
			(this.GrandTotalContent[this.screenRoundsCount].activity);
			
			
			this.activityText.htmlText = this.activityCopy;
			this.adjustmentText.htmlText = this.adjustmentCopy;
			
			/*
			if(this.screenRoundsCount==(this.noOfScreenRounds-1))
			{
			this.adjustmentText.htmlText = this.adjustmentCopy;
			}*/
			
			
			
			
			if(this.screenRoundsCount==(this.noOfScreenRounds-2))
			{
				this.adjustmentTextSpr.x = 427;
				this.adjustmentTextSpr.y = 291;
			}
			else if(this.screenRoundsCount==(this.noOfScreenRounds-1))
			{
				this.adjustmentTextSpr.x = 364;
				this.adjustmentTextSpr.y = 332;
			}
			else
			{
				this.adjustmentTextSpr.x = 361;
				this.adjustmentTextSpr.y = 336;
			}
			
			
			this.displayActivityText();
		}
		
		private function SwitchOffSprite(spriteName:String = ""):void
		{
			switch(spriteName)
			{
				case "activityText":
				{
					//this.activityText
					GeneralAnimations.basic_displayItemToggle
					(this.activityText, this.startDisplayProcess2, 0, 0.5, 2);
					break;
				}
				
				case "adjustmentText":
				{
					GeneralAnimations.basic_displayItemToggle
					(this.adjustmentText, this.setDisplayTexts, 0, 0.5);
					

					GeneralAnimations.basic_displayItemToggle
						(this.smallLCardsSprite, null, 0, 0.5);
			
			
					break;
				}
				
				case "adjustmentTextx":
				{
					break;
				}
				
				default:
				{
					
				}
			}
		}
		
		private function animatedCircleAdded(evt:Event):void
		{
			evt.target.gotoAndStop(1);
			evt.target.addEventListener("enterFrame", this.animatedCircleRunning);
			evt.target.gotoAndPlay(1);
		}
		
		private function animatedCircleRunning(evt:Event):void
		{
			if(evt.target.currentFrame==15)
			{
				//this.onRibbonOpen();
				evt.target.stop();
				evt.target.removeEventListener("enterFrame", this.animatedCircleRunning);
				this.separatedCirleSprite.alpha = 1;
				evt.target.alpha = 0;

				
				//Now that circle is drawn we, need to open ribbon
				this.expBow.alpha = 1;
				
				this.expBow.addEventListener
				("enterFrame", this.bowOpening);
				this.expBow.gotoAndPlay(1);
				
				
			}
			
			
			
		}
		
		private function bowOpening(evt:Event):void
		{
			if(evt.target.currentFrame==15)
			{
				evt.target.stop();
				evt.target
				.removeEventListener("enterFrame", this.bowOpening);
				this.onRibbonOpen();
			}
		}
		
		public function onRibbonOpen():void
		{
			this.displayActivityText();
		}
		
		public function displayActivityText():void
		{
			GeneralAnimations.basic_displayItemToggle
			(this.activityText, this.onActivityTextDisplayed, 1, 0.5);
			
			if(this.screenRoundsCount==(this.noOfScreenRounds-1))
			{
				GeneralAnimations.basic_displayItemToggle
				(this.starBox, null, 1, 0.5);
			}
			
		}
		
		public function onActivityTextDisplayed():void
		{
			
			GeneralAnimations.basic_displayItemToggle
			(this.adjustmentText, this.onAdjustmentTextDisplayed, 1, 0.5);
			
			if(this.screenRoundsCount==(this.noOfScreenRounds-2))
			{
				GeneralAnimations.basic_displayItemToggle
				(this.smallLCardsSprite, null, 1, 0.5);
			}
		}
		
		public function onAdjustmentTextDisplayed():void
		{
			if(this.screenRoundsCount==0)
			{
			this.placeRollText();
			}
			else
			{
				GeneralAnimations.basic_displayItemToggle
				(this.rollTextHolder, this.afterRollTextReDisplayed, 0, 0.5);
			}	
		}
		
		public function afterRollTextReDisplayed():void
		{
				//Empty this.rollTextHolder container at this time
				//so we can now replace it with a new roll text
				GlobalFunctions.emptyContainer(this.rollTextHolder);
				//Make sure this.rollTextHolder container is ready again
				//to receive the new roll text. This time we will make
				//it visible again.
				this.rollTextHolder.alpha = 1;
				//Now place roll text
				this.placeRollText();
		}
		
		private function placeRollText():void
		{
			
			//Temporarily place this here. REMOVE WHEN BUG FIXED
			//Temporarily place this here. REMOVE WHEN BUG FIXED
			//Temporarily place this here. REMOVE WHEN BUG FIXED
			//Temporarily place this here. REMOVE WHEN BUG FIXED
			//Temporarily place this here. REMOVE WHEN BUG FIXED
			if(PlayerResources.money<0){PlayerResources.money = PlayerResources.money*-1}
			
			
			
			
			
			
			var moneyStr:String = (PlayerResources.money).toString();
			
			var retireRibbonTxt:Sprite = new RollText().getRollText(moneyStr, this.setNextDisplay);
			this.rollTextHolder.addChild(retireRibbonTxt);
			retireRibbonTxt.x = (404-retireRibbonTxt.width)/2;//289;//693
		}
		
		private function theActivityText():Sprite
		{
			var activityTxtSpr:Sprite = new Sprite();
			this.activityText = this.getActivityText();
			activityText.name = "activityText";
			//this.activityText.background = true;
			//this.activityText.height = 128;
			activityTxtSpr.addChild(this.activityText);
			this.activityText.htmlText = 
			this.GrandTotalContent[this.screenRoundsCount].activityText;
			//Config.test;//"REPAY ALL\nLOANS AND INTEREST";
			//Config.GrandTotalContent[0].activityText;//"REPAY ALL\nLOANS AND INTEREST";
			this.activityText.alpha = 0;
			
			
			return activityTxtSpr;
		}
		
		private function theAdjustmentText():Sprite
		{
			var adjustmentTextSpr:Sprite = new Sprite();
			this.adjustmentText = this.getActivityText();
			
			var ttFormat:TextFormat = this.adjustmentText.getTextFormat();
			ttFormat.size = 20;
			this.adjustmentText.defaultTextFormat = ttFormat;
				
			adjustmentTextSpr.addChild(this.adjustmentText);
			this.adjustmentText.htmlText = "- &#163;13,450";
			this.adjustmentText.alpha = 0;
			
			return adjustmentTextSpr;
		}
		
		private function getStar():Bitmap
		{
			return new star() as Bitmap;
		}
		
		
		
	
	}
	
}