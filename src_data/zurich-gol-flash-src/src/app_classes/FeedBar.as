package src.app_classes
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	import src.app_classes.forms.AppForms;
	import src.app_classes.tools.gameStyles;
	import src.app_classes.actions.FeedActions;
	import GlobalFunctions;
	import GeneralAnimations;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	import flash.display.Bitmap;
	
	
	public class FeedBar extends Sprite
	{
		
		include "extensions/FeedExtensions.as";
		
		[Embed (source="../../res/feed/thumbnails/Avatar-feed.1.png" )]
		protected var feedImg1:Class;
		
		[Embed (source="../../res/feed/thumbnails/Avatar-feed.2.png" )]
		protected var feedImg2:Class;
		
		[Embed (source="../../res/feed/thumbnails/Avatar-feed.3.png" )]
		protected var feedImg3:Class;
		
		[Embed (source="../../res/feed/thumbnails/Avatar-feed.4.png" )]
		protected var feedImg4:Class;
		
		[Embed (source="../../res/feed/thumbnails/Avatar-feed.5.png" )]
		protected var feedImg5:Class;
		
		[Embed (source="../../res/feed/thumbnails/Avatar-feed.6.jpg" )]
		protected var feedImg6:Class;
		
		[Embed (source="../../res/feed/feedBlock.png" )]
		protected var feedBlock:Class;
		
		[Embed (source="../../res/feed/feedSeparatorTop.png" )]
		protected var feedSeparatorTop:Class;
		
		[Embed (source="../../res/feed/feedSeparator.png" )]
		protected var feedSeparator:Class;
		
		[Embed (source="../../res/feed/feedArrowDown.png" )]
		protected var feedArrowDown:Class;
		
		[Embed (source="../../res/feed/feedArrowUp.png" )]
		protected var feedArrowUp:Class;
		
		private var aF:AppForms;
		
		public var feedData:Object;
		
		public var numberOfFeedItemsUsed:int = 0;
		
		private var feedSingleContainer:Sprite;
		
		private var feedMainBox:Sprite;
		
		private var feedViewPort:Sprite;
		private var feedViewPort2:Sprite;
		private var bottomStrip:Sprite;
		
		private var firstFeedBottomMark:Number = 0;
		private var lastFeedBottomMark:Number = 0;
		
		private var short:Boolean = false;
		
		
		public var rf:TextField;
		private var tf:TextField;
		private var feedActs:FeedActions;
		
		public function FeedBar(feedActs:FeedActions)
		{
			this.feedActs = feedActs;
			this.aF = new AppForms();
		}
		
		public function getFeedBar(data:Object):Sprite
		{
			this.createFeedTopStrip();
			this.createFeedBarTop();
			this.createFeedBar(data);
			this.createFeedBottomStrip();
			
			this.setInitialSizes();
			
			return this;
		}
		
		private function setInitialSizes():void
		{
			//lastFeedBottomMark
			//this.feedViewPort.height = this.firstFeedBottomMark;
			this.feedViewPort.height = this.lastFeedBottomMark;
			this.feedViewPort2.height = this.feedViewPort.height;
			this.bottomStrip.y = (this.feedViewPort2.y+this.feedViewPort2.height)-14;
			
			//Now set the feed as short. Let the software know that its short
			//by setting the boolean to true.
			this.short = false;
			//this.short = true;
			
		}
		
		private function createFeedTopStrip():void
		{
			//This is the rounded corners of the feed.
			//Here, we will be creating a slim strip
			//of the top of the feed bar
			var feedTop:Sprite = new Sprite();
			this.addChild(feedTop);
			
			//Now fill this slim strip with the background of the feed
			GlobalFunctions.addSprite_appTransparentBackground
			(feedTop,206,20,true);
			
			//Create a mask now so that we can cut off the ends of the
			//rounded box that we dont want
			var feedTopMask:Sprite = new Sprite();
			feedTopMask.graphics.beginFill(0xBBCC00);
    		feedTopMask.graphics.drawRect(0,0,206,8);
			feedTopMask.graphics.endFill();
			this.addChild(feedTopMask);
			
			//Mask out the part that we dont want to show.
			//Only reveal the part we want to display
			feedTop.mask = feedTopMask;
			
		}
		
		private function createFeedBottomStrip():void
		{
			this.bottomStrip = new Sprite();
			
			//This is the rounded corners of the feed.
			//Here, we will be creating a slim strip
			//of the top of the feed bar
			var feedTop:Sprite = new Sprite();
			this.bottomStrip.addChild(feedTop);
			
			var feedArrowSpr:Sprite = new Sprite();
			feedArrowSpr.buttonMode = true;
			var feedArrowUp:Bitmap = new feedArrowUp() as Bitmap;
			feedArrowSpr.addChild(feedArrowUp);
			feedArrowUp.x = -7;
			feedArrowUp.y = -4.5;
			this.bottomStrip.addChild(feedArrowSpr);
			feedArrowSpr.y = 18.5;
			feedArrowSpr.x = 104;
			
			feedArrowSpr.addEventListener(MouseEvent.CLICK, this.makeShort);
			
			
			
			
			//Now fill this slim strip with the background of the feed
			GlobalFunctions.addSprite_appTransparentBackground
			(feedTop,206,30,true);
			
			
			//Create a mask now so that we can cut off the ends of the
			//rounded box that we dont want
			var feedTopMask:Sprite = new Sprite();
			feedTopMask.graphics.beginFill(0xBBCC00);
    		feedTopMask.graphics.drawRect(0,0,206,17);
			feedTopMask.graphics.endFill();
			this.bottomStrip.addChild(feedTopMask);
			
			//place this mask at the bottom of the strip box so we
			//can see the rounded corners at the bottom.
			feedTopMask.y = feedTop.height - feedTopMask.height;
			
			//Mask out the part that we dont want to show.
			//Only reveal the part we want to display
			feedTop.mask = feedTopMask;
			
			
			
			
			this.addChild(this.bottomStrip);
			
			this.bottomStrip.y = 
			((this.feedMainBox.y+this.feedMainBox.height) -
			feedTopMask.y) - 40;
			
		}
		
		private function createFeedBarTop():void
		{
			var feedTop:Sprite = new Sprite();
			this.addChild(feedTop);
			feedTop.y = 8;
			
			
			
			feedTop.addEventListener(MouseEvent.CLICK, 
			this.addSingle);
			
			
			//Add the background color here
			GlobalFunctions.addSprite_appTransparentBackground
			(feedTop,206,62);
			
			
			
			var feedTitleSpr:Sprite = new Sprite();
			this.rf = this.aF.getCardText();
			feedTitleSpr.addChild(rf);
			feedTop.addChild(feedTitleSpr);
			
			var rffm:TextFormat = this.rf.getTextFormat();
			rffm.size = 18;
			rffm.align = "left";
			this.rf.defaultTextFormat = rffm;
			
			rf.text = "Player Feed";
			feedTitleSpr.x = 20;
			feedTitleSpr.y = 4;
			
			
			var s:Sprite = new Sprite();
			s.addChild(new feedSeparatorTop());
			feedTop.addChild(s);
			
			s.y=40;
			s.x=1;
		}
		
		private function createFeedBar(feed:Object):void
		{
			
			this.feedData = feed;
			
			var noOfFeed:int = feed.feed_data.data.length;
			var feedLoop:int = noOfFeed>2?3:noOfFeed;
			
			
			this.feedMainBox = new Sprite();
			
			this.feedSingleContainer = new Sprite();
			
			var sngTop:Number = 0;
			
			
			for(var i:int=0; i<feedLoop; i++)
			{
				
				var avatarInt:int = 
				parseInt(feed.feed_data.data[i].feed_avatar);
				
				var thumbnail:Bitmap = this.getFeedThumbnail(avatarInt);
				
				var feedMsg:String = 
				feed.feed_data.data[i].feed_msg;
				
				var feedName:String = 
				feed.feed_data.data[i].first_name +" "+
				feed.feed_data.data[i].last_name;
				
				var companyName:String = 
				feed.feed_data.data[i].company;
				
				
				var single:Sprite = 
				this.getFeedSingle(thumbnail,feedMsg,feedName,companyName);
				single.name = "feed_single_"+i;
				this.feedSingleContainer.addChild(single);
				
				
				//Get the end of the feed at the top.
				//This is important so we can collapse(mask) the
				//feed at the correct point.
				if(i==0)
				{
					this.firstFeedBottomMark = 
					single.getChildByName("feedSeparatorSpr").y+(4);
				}
				
				
				
				single.y = sngTop;
				sngTop += single.height+20;
				
				//this will keep a record of the number of feed Single
				//items thats used so far. Actually, the number of items
				//used so far is what is used at setup time.
				this.numberOfFeedItemsUsed++;
			
			}
			
			
			this.feedViewPort = new Sprite();
			this.feedViewPort.graphics.beginFill(0xBBCC00);
    		this.feedViewPort.graphics.drawRect(0,0,206,(sngTop-20));
			this.feedViewPort.graphics.endFill();
			this.feedMainBox.addChild(this.feedViewPort);
			this.feedSingleContainer.mask = this.feedViewPort;
			
			
			
			
			this.lastFeedBottomMark = 
			this.feedViewPort.height;
			
			
			
			
			
			this.feedMainBox.addChild(this.feedSingleContainer);
			this.addChild(this.feedMainBox);
			
			
			//this.gg(this.feedMainBox);
			GlobalFunctions.addSprite_appTransparentBackground
			(
			this.feedMainBox,
			(this.feedMainBox.width),//+36),
			(this.feedMainBox.height+40)
			);
			
			this.feedMainBox.y = 70;
			
			
			
			
			
			
			
			
			this.feedViewPort2 = new Sprite();
			this.feedViewPort2.graphics.beginFill(0xBBCC00);
    		this.feedViewPort2.graphics.drawRect(0,0,206,this.feedMainBox.height-40);
			this.feedViewPort2.graphics.endFill();
			this.addChild(this.feedViewPort2);
			this.feedViewPort2.y = this.feedMainBox.y
			this.feedMainBox.mask = this.feedViewPort2;
			
			
			
			
			
			
			
		}
		
		private function makeShort(evt:MouseEvent):void
		{
			
			this.feedActs.requestGamePause();	
			
			var d:Number = this.lastFeedBottomMark;
			if(!short)
			{d = this.firstFeedBottomMark;}
			else
			{d = this.lastFeedBottomMark;}
			
			//this.tf.text = d+" - short";
			
			GeneralAnimations.toggleFeedExpansion
			(
			this.feedViewPort, d,
			this.feedViewPort2, this.bottomStrip,
			evt.target as Sprite,
			this.feedHeightAdjusted
			);
			
			if(short){short = false;}else{short = true;}
			
		}
		
		private function feedHeightAdjusted():void
		{
			this.feedActs.requestGameResumption();
		}
		
		private function getFeedThumbnail(num:int):Bitmap
		{
			
		    switch(num)
		    {
		    	case 1:
		    	{
		    		return new feedImg1() as Bitmap;
		    		break;
		    	}
		    	
		    	case 2:
		    	{
		    		return new feedImg2() as Bitmap;
		    		break;
		    	}
		    	
		    	case 3:
		    	{
		    		return new feedImg3() as Bitmap;
		    		break;
		    	}
		    	
		    	case 4:
		    	{
		    		return new feedImg4() as Bitmap;
		    		break;
		    	}
		    	
		    	case 5:
		    	{
		    		return new feedImg5() as Bitmap;
		    		break;
		    	}
		    	
		    	case 6:
		    	{
		    		return new feedImg6() as Bitmap;
		    		break;
		    	}
		    	
		    	default:
		    	{
		    		return new feedImg6() as Bitmap;
		    	}
		    }
		    
		    
		    return new feedImg6() as Bitmap;
		    
		}
		
	}
}