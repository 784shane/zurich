		
		//Writing more functions of the GamePlay.as class here
		//This is done to organize our code better for GamePlay
		//can become very long if we do not do 
		//something like this
		
		private function spinToWinClose():void
		{
			//Now that the SpinToWin layer is no longer visible,
			//we are going to empty it so we can access stuff behind it.
			GlobalFunctions.emptyContainer(
			this.lyrs.getLayer("spinToWin"));
			
			//Remember to make this visible again after emptying it.
			this.lyrs.getLayer("spinToWin").alpha = 1;
			
			//Use the results of the spin to update the
			//and the bank.
			//Increase the bank resource now.
			var moneyFromSpin:Number = 
			getMoneyFromSpinAmt(this.spinToWin_result);
			
			PlayerResources.money+= moneyFromSpin;
			
			var newMoneyText:String = GlobalFunctions.formatMoneyToString(PlayerResources.money);
			
			this.Res.uiMajorText.text = "You spun a "+this.spinToWin_result;
			this.Res.uiMinorText.htmlText = "Receive "+
			GlobalFunctions.formatMoneyToString(moneyFromSpin);
			
			this.Res.uiMoniesText.htmlText = newMoneyText;
			
			//this.manageRetireSpinMoney(num);
			
			this.displaySpinner();
		}
		
		private function retire_spinToWinClose():void
		{
			GlobalFunctions.emptyContainer(
			this.lyrs.getLayer("spinToWin"));
			
			this.manageRetireSpinMoney();
			
		}
		
		private function enhance_spinToWinClose():void
		{
					
			GlobalFunctions.emptyContainer(
			this.lyrs.getLayer("spinToWin"));		
			
			this.manageEnhanceSpinMoney();
			
		}
		
		private function getMoneyFromSpinAmt(spinAmt:int):Number
		{
			switch(spinAmt)
			{
				case 1:
				case 2:
				case 3:
				{
					return 25000;
					break;
				}
				
				case 4:
				case 5:
				case 6:
				case 7:
				{
					return 50000;
					break;
				}
				
				case 8:
				case 9:
				case 10:
				{
					return 100000;
					break;
				}
			}
			
			return 0;
		}
		
		private function manageEnhanceSpinMoney():void
		{
			var myMonies:Number = this.spinToWin_result*10000;
			var majorStr:String = "You spun a "+this.spinToWin_result;
			var minorStr:String = "Recieve "+
			GlobalFunctions.formatMoneyToString(myMonies);
			
			//Update our monies
			PlayerResources.money+=myMonies;
			
			this.gf.updateUiText(majorStr, minorStr);
			this.gf.updateUiMoneyText(PlayerResources.money);
			
			//Show the Spinner now
			this.displaySpinner();
	
	
		}
		
		private function manageRetireSpinMoney():void
		{
		
			var myMonies:Number = PlayerResources.money;
			var majorStr:String = "";
			var minorStr:String = "";
			
			switch(this.spinToWin_result)
			{
				case 1:
				case 2:
				case 3:
				case 4:
				{
					majorStr = "You spun a "+this.spinToWin_result;
					minorStr = "You lost half your total";
					PlayerResources.money = (myMonies/2);
					break;
				}
				
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				{
					//GlobalFunctions.formatMoneyToString(newMoney);
					majorStr = "You spun a "+this.spinToWin_result;
					minorStr = "Double your total. Receive "+
					GlobalFunctions.formatMoneyToString(myMonies);
					PlayerResources.money = (myMonies*2);
					break;
				}
				
				default :
				{
					majorStr = "You spun a "+this.spinToWin_result;
					minorStr = "Keep what you have.";
				}
			}
			
			this.gf.updateUiText(majorStr, minorStr);
			this.gf.updateUiMoneyText(PlayerResources.money);
			
	//After placing the update in the UI, we need to give a second or two then
	//call screen
	this.gf.showGrandTotal();
			
		
		}
		
