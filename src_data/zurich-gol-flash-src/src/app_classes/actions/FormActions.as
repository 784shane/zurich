	
		//Writing more functions of the GameFlow.as class here
		//This is done to organize our code better for GameFlow
		//can become very long if we do not do 
		//something like this
		
		
		public function displayLoginForm(showBackground:Boolean = false):void
		{
			//We are going to slide in these layers
			this.emptyLayer("interactionLayer");
			this.lf = new LoginForm(this);
			var s:Sprite = this.lf.getLoginForm(showBackground);
			s.addEventListener("addedToStage",this.loginFormAdded);

			
			
			if(showBackground)
			{
			
				this.layers.getLayer("pagesBackground")
				.addChild(GlobalFunctions.getBackDrop());
				
				this.layers.getLayer("pagesLayer").x = -980;
				
				(this.layers.getLayer("pagesLayer").
				getChildAt(0) as Sprite).x = 980;
			
				(this.layers.getLayer("pagesLayer").
				getChildAt(1) as Sprite).x = 980;
			
				(this.layers.getLayer("pagesLayer").
				getChildAt(0) as Sprite).addChild(s);
				
				this.mainGamePlay.game_pitstops = new Array();
				
				PlayerResources.currentSquare = 0;
			
			}
			else
			{
				this.displayPageScreen(s);
			}
			
		
		}
		
		public function getCompaniesList():void
		{
			this.bi.getCompaniesList(this.afterReceivingCompaniesList);
		}
		
		public function afterReceivingCompaniesList(evt:Event):void
		{
			
			
			var companiesData:Object = JSON.parse(evt.target.data);
				
			this.lf.companiesArray = new Array();
				
			if(companiesData.companies_data.msg
			&&companiesData.companies_data.msg=="AVAILABLE_COMPANIES")
			{
				
				for(var i:int = 0;
				i<companiesData.companies_data.data.length; 
				i++)
				{
					this.lf.companiesArray.push(
					companiesData.companies_data.data[i].name.toString()
					);
					//"evt.target.data"+
					//companiesData.companies_data.msg;
				}
				
			}
			
			
			//this.tf.text += "-after-"+this.lf.companiesArray[0];
		}
		
		
		public function formAddedToScreen(evt:Event):void
		{
			
			this.lf.animateDrop();
			
			//Once we can determine that the login form has been added to the
			//stage, we will now set Event Listeners
			switch(evt.target.name)
			{
				case "loginFormFields":
				{
					this.lf.reg_button.addEventListener(MouseEvent.CLICK, this.loginToApp);
					break;
				}
				
				case "registerFormFields":
				{
					this.lf.reg_button.addEventListener(MouseEvent.CLICK, this.collectDataToSave);
					break;
				}
			}
			
		}
		
		
		private function loginFormAdded(evt:Event):void
		{
			this.lf.animateDrop();
			//Once we can determine that the login form has been added to the
			//stage, we will now set Event Listeners
			
			
			this.lf.loginSwitchButton.addEventListener(MouseEvent.CLICK, this.loginSwitchClicked);
			this.lf.registerSwitchButton.addEventListener(MouseEvent.CLICK, this.loginSwitchClicked);
		}
		
		private function loginSwitchClicked(evt:MouseEvent):void
		{
			//this.tf.text = evt.target.name+" - "+this.lf.formFields.numChildren;
			switch(evt.target.name)
			{
				
				case "loginButton":
				{
					this.lf.setForm("login", this.tf);
					break;
				}
				
				case "registerButton":
				{
					this.lf.setForm("register", this.tf);
					break;
				}
				
			}
		}
		
		private function loginToApp(evt:MouseEvent):void
		{
			this.lf.reg_button.removeEventListener(MouseEvent.CLICK, this.loginToApp);
			
			
			//On the login form, we will clear all validation marks because
			//we are going to check the database, then place new validation marks on screen.
			this.lf.clearAllValidatedMarks();
					
			var loginData:Object = new Object();
			loginData.emailAdd = (this.lf.lf_emailAddress_tf.getChildByName("field_txtBox") as TextField).text
			loginData.password = (this.lf.lf_password_tf.getChildByName("field_txtBox") as TextField).text;
			
			this.bi.requestLogin(loginData);
					
		}
		
		private function dealWithLoginAction(loginAttemptData:Object):void
		{
			
			switch(loginAttemptData.msg)
			{
				
				case "USER_EMAIL_ADDRESS_FOUND":
				{
					
					this.lf.reg_button.addEventListener(MouseEvent.CLICK, this.loginToApp);
					
					//Mark user email as Correct
					(this.lf.lf_emailAddress_tf.parent
					.getChildByName("validNo") as Sprite).alpha = 0;
					
					this.lf.validatonControl(
					(this.lf.lf_emailAddress_tf.parent.getChildByName("validYes") as Sprite)
					);
					
					//means password wrong
					(this.lf.lf_password_tf.parent
					.getChildByName("validYes") as Sprite).alpha = 0;
					
					this.lf.validatonControl(
					(this.lf.lf_password_tf.parent.getChildByName("validNo") as Sprite)
					);
					break;
				}
				
				case "USER_SUCCESSFULLY_LOGGED_IN":
				{
					//logged in successfully
					
					this.setLoginButton();
					
					this.savedGameData = loginAttemptData;
					
					this.sendGoolgleAnaylticsInfo("User_logged_in");
					
					this.getScreen("HOW_TO_PLAY");
					/*
					if(
					loginAttemptData.savedGame
					&&loginAttemptData.savedGame.msg=="SUCCESS"
					)
					{
						
						CardAnimation.closeCardSelectionScreen(this.layers, this.showContinueCurrentGameQuestion);
						//this.tf.text = "races";
					//this.getScreen("SHOW_SAVEDGAME_QUESTION");
					}
					else
					{
						this.setBasicGameData();
						this.getScreen("PLAYER_SELECT");
					}	
					*/
					break;
				}
				
				case "USER_NOT_FOUND":
				{
					
					this.lf.reg_button.addEventListener(MouseEvent.CLICK, this.loginToApp);
					
					//means email is wrong for sure
					
					//Since user cannot be found, we will make sure we put a validation
					//mark here so that the user can know whats happening.
					
					//Mark user email as wrong
					(this.lf.lf_emailAddress_tf.parent
					.getChildByName("validYes") as Sprite).alpha = 0;
					
					this.lf.validatonControl(
					(this.lf.lf_emailAddress_tf.parent.getChildByName("validNo") as Sprite)
					);
					
					//means password wrong
					(this.lf.lf_password_tf.parent
					.getChildByName("validYes") as Sprite).alpha = 0;
					
					this.lf.validatonControl(
					(this.lf.lf_password_tf.parent.getChildByName("validNo") as Sprite)
					);
					
					break;
				}
				
				default:
				{
					
					this.lf.reg_button.addEventListener(MouseEvent.CLICK, this.loginToApp);
					//this.tf.text = loginAttemptData.msg;
				
				}
			}
		}
		
		private function inGame_onHtpScreenExitButtonClicked():void
		{
			//this.emptyLayer("forms");
			
			//this.savedGameData = loginAttemptData;
			
			if(
			this.savedGameData.savedGame
			&&this.savedGameData.savedGame.msg=="SUCCESS"
			)
			{
						
				this.showContinueCurrentGameQuestion(0);
			}
			else
			{
				this.setBasicGameData();
				this.getScreen("PLAYER_SELECT");
			}	
				
		}
		
		private function setLoginButton():void
		{
			(this.res.lgnTextBoxSprite
			.getChildByName('txtBox') as TextField).text = "LOGOUT";
			
			if(this.res.lgnTextBoxSprite.hasEventListener(MouseEvent.CLICK))
			{
				this.res.lgnTextBoxSprite.removeEventListener(MouseEvent.CLICK,
				this.res.listenerDictionary.getCLICKListener("login"));
				
				this.res.lgnTextBoxSprite.addEventListener(MouseEvent.CLICK,
				this.res.listenerDictionary.getCLICKListener("logout"));
				
			}
		}
		
		public function showContinueCurrentGameQuestion(i:int):void
		{
			var buttonNames:Object = {button1:"Yes please", button2:"Start a new game"};
			this.displayPageScreen(new ContinueCurrentGameQuestion().getLogoutScreen
			(this.afterAnsweringContinueQuestion, buttonNames));
		}
		
		public function afterAnsweringContinueQuestion(logoutAnswer:String):void
		{
			switch(logoutAnswer)
			{
				case "no":
				{
					this.emptyLayer("blockLayer");
					this.emptyLayer("forms");
					this.emptyLayer("interactionLayer");
					
					this.emptyLayer("pagesBackground");
					
					GlobalFunctions.deleteBothPages
					(this.layers.getLayer("pagesLayer") as Sprite);
					
					this.restoreSavedGameVars(this.savedGameData);
					break;
				}
				
				case "yes":
				{
					//this.emptyLayer("forms");
					this.restoreDataFromDatabase(this.savedGameData);
					this.getScreen("PLAYER_SELECT");
					//this.tf.text = this.savedGameData.savedGame.msg;
					//this.ldnScreen = new LoadingScreen();
					//this.ldnScreen.loadScreenImages(this.afterLoadingScreenResourcesLoaded);
					break;
				}
			}
			
		}
		
		private function restoreDataFromDatabase(savedVars:Object):void
		{
		
			this.gameRestored = false;
			
			
			this.setBasicGameData();
			/*
			//set car on track according to the last saved game
			this.mainGamePlay.restoreSavedPositions(
			savedVars.savedGame.data.car_square_postion,
			savedVars.savedGame.data.car_color,
			savedVars.savedGame.data.wheel_last_spin,
			savedVars.savedGame.data.wheel_last_rotation,
			savedVars.savedGame.data.needle_last_rotation
			);
			
			//Set Ui text here. We update the user's name, monies
			//and number of life
			this.res.uiLifeText.text = "x "+savedVars.savedGame.data.life_cards;
			this.res.uiMoniesText.htmlText = "&#163;"+savedVars.savedGame.data.cash_in_hand;
			
			//Change the avatar here
			//the format to send to this function
			//must be 'Avatar-select.1' where 1 will
			//be the dynamic variable
			this.changePlayerAvatar("Avatar-select."+savedVars.savedGame.data.avatar);
			
			//Now animate in the panels and set the game at ready state.
			this.setReadyToPlayState();
			*/
		}
		
		private function restoreSavedGameVars(savedVars:Object):void
		{
		
			this.gameRestored = true;
			
			
			this.setBasicGameData();
			
			//Set Ui text here. We update the user's name, monies
			//and number of life
			this.res.uiLifeText.text = "x "+savedVars.savedGame.data.life_cards;
			this.res.uiMoniesText.htmlText = "&#163;"+savedVars.savedGame.data.cash_in_hand;
			
			//Remember to set up the resources as well.
			PlayerResources.money = parseFloat(savedVars.savedGame.data.cash_in_hand);
			PlayerResources.lifeCards = parseInt(savedVars.savedGame.data.life_cards);
			
			
			
			PlayerResources.sex = savedVars.savedGame.data.sex;
			
			
			PlayerResources.salary = savedVars.savedGame.data.salary;
			PlayerResources.maximum_salary = savedVars.savedGame.data.maximum_salary;
			PlayerResources.salary_taxes = savedVars.savedGame.data.salary_taxes;
			PlayerResources.house_buying_price = savedVars.savedGame.data.house_buying_price;
			PlayerResources.house_selling_price = savedVars.savedGame.data.house_selling_price;
			PlayerResources.house_insurance = savedVars.savedGame.data.house_insurance;
			PlayerResources.lifePath = savedVars.savedGame.data.lifePath;
			PlayerResources.numOfChildren = savedVars.savedGame.data.numOfChildren;
			PlayerResources.job = savedVars.savedGame.data.job;
			
			PlayerResources.player_wealthCards = savedVars.savedGame
			.data.wealth_cards.split(",");
			
			
			
			//set car on track according to the last saved game
			this.mainGamePlay.restoreSavedPositions(
			savedVars.savedGame.data.car_square_postion,
			savedVars.savedGame.data.car_color,
			savedVars.savedGame.data.wheel_last_spin,
			savedVars.savedGame.data.wheel_last_rotation,
			savedVars.savedGame.data.needle_last_rotation
			);
			
			
			//this.mainGamePlay.setPegs();
			this.mainGamePlay.restorePegs(savedVars.savedGame.data.pegs);
			
			
			//Change the avatar here
			//the format to send to this function
			//must be 'Avatar-select.1' where 1 will
			//be the dynamic variable
			this.changePlayerAvatar("Avatar-select."+savedVars.savedGame.data.avatar);
			
			//Now animate in the panels and set the game at ready state.
			this.setReadyToPlayState();
			
		}
		
		private function setBasicGameData():void
		{
			//Save the User Id in the PlayerResources
			this.res.uiNameText.text = this.savedGameData.data.first_name;
			PlayerResources.player_uid = this.savedGameData.data.user;
			PlayerResources.player_name = this.savedGameData.data.first_name; 
			PlayerResources.player_last_name = this.savedGameData.data.last_name; 
			
		}
		
		private function collectDataToSave(evt:MouseEvent):void
		{
		
			this.lf.reg_button.removeEventListener(MouseEvent.CLICK, this.collectDataToSave);
			
			if(this.lf.proceedToRegistration())
			{
	
				//On the login form, we will clear all validation marks because
				//we are going to check the database, then place new validation marks on screen.
				this.lf.clearAllValidatedMarks();
				
				var f_name:String = (this.lf.firstName_tf.getChildByName("field_txtBox") as TextField).text;
				var l_name:String = (this.lf.lastName_tf.getChildByName("field_txtBox") as TextField).text;
				var company_name:String = (this.lf.companyName_tf.getChildByName("field_txtBox") as TextField).text;
				var email:String = (this.lf.emailAddress_tf.getChildByName("field_txtBox") as TextField).text;
				var postCode:String = (this.lf.postCode_tf.getChildByName("field_txtBox") as TextField).text;
				var password1:String = (this.lf.password1_tf.getChildByName("field_txtBox") as TextField).text;
				var password2:String = (this.lf.password2_tf.getChildByName("field_txtBox") as TextField).text;
				
				var regdata:Object = new Object();
				regdata.f_name = f_name;
				regdata.l_name = l_name;
				regdata.company_name = company_name;
				regdata.email = email;
				regdata.postCode = postCode;
				regdata.password = password1;
				 
				this.bi.sendToRegister(regdata, this.registerCompleted);
				
			}	
			
			
		}
		
		public function registerCompleted(evt:Event):void
		{
			
			
			var registerData:Object = JSON.parse(evt.target.data);
			
			
			
			//Now that the player has registered we need to change the login
			//tab in the title bar to a logout tab
			
			var holdingObj:Object = {};
			holdingObj.data=
			{
			user:registerData.user,
			first_name:registerData.first_name,
			last_name:registerData.last_name,
			company_name:registerData.company_name
			};
			
			PlayerResources.player_company = registerData.company_name;
			
			this.savedGameData = holdingObj;
			
			this.setBasicGameData();
			
			this.sendGoolgleAnaylticsInfo("Registered_User");
			
			//this will insert logout at the top (Title bar)
			this.setLoginButton();
					
			this.getScreen("PLAYER_SELECT");
			
			
		}
		
		public function howtoplayClicked():void
		{
			//Pause the game.
			this.pauseGame();
			
			this.htp = new HowToPlay(this, this.onHtpScreenExitButtonClicked);
			this.htp.loadScreenImages(this.howToPlaySceenLoaded);
		}
		
		
		private function show_gf_how_to_play():void
		{
			/*this.emptyLayer("forms");
			layers.getLayer("forms").alpha=1;
			this.layers.getLayer("forms")
			.addChild(new PostLoadingScreen(this.postLoadingScreenLoaded).getPostLoadingScreen());
			*/
			
			
			this.htp = new HowToPlay(this, this.inGame_onHtpScreenExitButtonClicked);
			this.htp.loadScreenImages(this.inGame_howToPlaySceenLoaded);
		}
		
		private function inGame_howToPlaySceenLoaded():void
		{
			
			var howToPlayScreen:Sprite = this.htp.getHowToPlayScreen(false);
			howToPlayScreen.addEventListener("addedToStage", this.htp.whenAddedToStage);
			
			this.displayPageScreen(howToPlayScreen);
			
			
			
		}
		
		private function howToPlaySceenLoaded():void
		{
			
			//this.layers.getLayer("blockLayer").addChild(new BlockScreen());
			this.emptyLayer("blockLayer");
			this.layers.getLayer("blockLayer").alpha=1;
			
			var howToPlayScreen:Sprite = this.htp.getHowToPlayScreen(true);
			howToPlayScreen.addEventListener("addedToStage", this.htp.whenAddedToStage);
			
			this.layers.getLayer("blockLayer").addChild
			(howToPlayScreen);
			
		}
		
		private function onHtpScreenExitButtonClicked():void
		{
			this.emptyLayer("blockLayer");
			
			//If game was paused, we will resume it now.
			this.resumeGame();
		}
		
		private function showPrizesScreen():void
		{
			var tc:Prizes = new Prizes(this);
			this.layers.getLayer("blockLayer").addChild(tc.getPrizes());
		}
		
		public function closePrizes():void
		{
			this.layers.getLayer("blockLayer").alpha=0;
			GlobalFunctions.emptyContainer(this.layers.getLayer("blockLayer"));
			this.layers.getLayer("blockLayer").alpha=1;
			
			this.resumeGame();
		}
		
		private function displayPageScreen(newPage:Sprite):void
		{
			var activePage:Sprite = 
			GlobalFunctions.getActivePage
			(this.layers.getLayer("pagesLayer"));
			
			var inactivePage:Sprite = 
			GlobalFunctions.getInactivePage
			(this.layers.getLayer("pagesLayer"));
			
			inactivePage.addChild
			(newPage);
			
			//Adjust layers for insertion of new page
			GlobalFunctions.repositionPages
			(activePage, inactivePage);
			
			GeneralAnimations.movePageInPosition
			((inactivePage.parent as Sprite), activePage);
		}
		
