		
		//Writing more functions of the GameFlow.as class here
		//This is done to organize our code better for GameFlow
		//can become very long if we do not do 
		//something like this
		
		public function getLeaderBoardData(theDataReturnFunction:Function):void
		{
				this.bi.getLeaderboardData(theDataReturnFunction);
		}
		
		
		public function LeaderBoardInitiated():void
		{
			this.layers.getLayer("blockLayer").addChild(this.lboard.buildLeaderboard());
			this.lboard.revealLeaderboardContents();
		}
		
		
		public function clearLeaderboard():void
		{
			this.layers.getLayer("blockLayer").alpha=0;
			GlobalFunctions.emptyContainer(this.layers.getLayer("blockLayer"));
			this.layers.getLayer("blockLayer").alpha=1;
			
			//If game was paused, we will resume it now.
			this.resumeGame();
			
			if(PlayerResources.currentSquare == 128)
			{
				this.restartGame();
			}
		}
		