		
		//Writing more functions of the GamePlay.as class here
		//This is done to organize our code better for GamePlay
		//can become very long if we do not do 
		//something like this
		
		private function doIHaveWealthCard(wealthCardsArray:Object):Boolean
		{
			
			var haveCard:Boolean = false;
			
			
			for(var i:int = 0; i<wealthCardsArray.cardsNeeded.length; i++)
			{
				
				for(var j:int = 0; j<PlayerResources.player_wealthCards.length; j++)
				{
					if(GlobalFunctions.getWealthCardNameFromNumber(PlayerResources.player_wealthCards[j])==
					wealthCardsArray.cardsNeeded[i])
					{
						haveCard = true;
						break;
					}
				}
				
				if(haveCard){break;}
					
			}
			
			return haveCard;
		}
		
		private function getCardsNeeded(pos:int):Object
		{
			
			var wealthDecisionStyle:String = "automatic";
			
			var returnedArray:Object;// = new Object();
			var cards:Array = new Array();
			var noOfCards:int = 
			this.positionsXML.board_places.place[pos].action.wealthCards.cardOptions.card.length();
			
			for(var i:int = 0; i<noOfCards; i++)
			{
				
				var wealthCardName:String = this.positionsXML.board_places.place[pos]
				.action.wealthCards.cardOptions.card[i];
				
				wealthDecisionStyle = this.wealthDecisionStyleFunction(wealthCardName);
				
				cards.push(wealthCardName);
			}
			
			returnedArray = {decisionStyle:wealthDecisionStyle, cardsNeeded:cards};
			
			return returnedArray;
		}
		
		/*
		private function getWealthCardNameFromNumber(cardNo:int):String
		{
			switch(cardNo)
			{
				case 0:
				{
					return "pay";
					break;	
				}
				
				case 1:
				{
					return "exemption";
					break;	
				}
				
				case 2:
				{
					return "car";
					break;	
				}
				
				case 3:
				{
					return "home";
					break;	
				}
				
				case 4:
				{
					return "health";
					break;	
				}
			}
			
			return "exemption";
			
		}
		*/
		
		private function wealthDecisionStyleFunction(cardName:String):String
		{
			switch(cardName)
			{
				
				case "pay":
				case "exemption":
				{
					return "manual";
					break;	
				}
				
				case "home":
				case "health":
				case "car":
				{
					return "automatic";
					break;	
				}
				
			}
			
			return "automatic";
		}
		
