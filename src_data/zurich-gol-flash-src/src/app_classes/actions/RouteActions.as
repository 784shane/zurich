		
		//Writing more functions of the GamePlay.as class here
		//This is done to organize our code better for GamePlay
		//can become very long if we do not do 
		//something like this
		
		private function getAlternativeRouteJumpSpots():Number
		{
			//Where is the car now? We can get the spot from using
			var spot:int = this.nextPlace;
			
			switch(spot)
			{
				case Config.alternative_routes.
				route1.regular.start.board_num:
				{
					return Config.alternative_routes.
							route1.start.frame;
					break;
				}
				
				case Config.alternative_routes.
				route2.regular.start.board_num:
				{
					return Config.alternative_routes.
							route2.start.frame;
					break;
				}
				
				case Config.alternative_routes.
				route3.regular.start.board_num:
				{
					return Config.alternative_routes.
							route3.start.frame;
					break;
				}
				
				case Config.alternative_routes.
				route4.regular.start.board_num:
				{
					return parseFloat(Config.alternative_routes.
							route4.start.frame);
					break;
				}
				
			}
			
			//If the car is still on alternative route 
			//we will not be jumping it at all.
			return this.positionsXML.board_places.
			place[spot].@frame;
			
		}
		
		private function getXMLSpotsToCheck():void
		{
			this.alternativeRoutesToCheck = new Array();
			var getMoreSquares:Boolean = false;
			var startOfGame:Boolean = false;
			this.mergeRouteEnd = false;
			
			
			//We are returning
			var presentSpot:int = 
			this.getAdjustedInCaseOfLastRegularSpot();
			
			if(this.isCarPoisedToEnterAlterRoute())
			{
			//this.tf.text = "a";
				presentSpot = 
			this.getAdjustedInCaseOfLastRegularSpot();
			}
			else
			{
			//this.tf.text = "b";
				presentSpot=this.nextPlace+1;
			}
			
			//this.tf.text += this.nextPlace+" == "+presentSpot;
			
			//if(this.nextPlace==0){startOfGame = true; presentSpot = 128;}
			
			
			var roll:int = this.numToMoveto;
			for(var i:int = 0; i<roll; i++)
			{
				
				
				
				
				
				if(!getMoreSquares && presentSpot > this.getEndOfTheAlternativeRoute())
				{
					presentSpot = 
					this.getMergeExitChangeoverBoardNumber(presentSpot-1);
					this.mergeRouteFrame = this.getRouteChangeoverSpot(presentSpot);
					
					this.mergeNormalFrame = this.convertNumToFrame(presentSpot);
					
					this.mergeRouteEnd = true;
					getMoreSquares = true;
				}
				
				this.alternativeRoutesToCheck
				.push(presentSpot);
				
				
				if(i==(roll-1))
				{
					this.frameToStopAt = 
					this.convertNumToFrame(presentSpot);
					this.lastPlace = presentSpot;
					
					//this.tf.text += " --- "+this.alternativeRoutesToCheck.length+" -- ";
				}
				
				presentSpot++;
				
			}
			
			//this.tf.text += " --- "+this.alternativeRoutesToCheck.length+" -- ";
			
			
			
			
		}
		
		private function getEndOfTheAlternativeRoute():Number
		{
		
			//Where is the car now? We can get the spot from using
			var spot:int = 
			this.getAdjustedInCaseOfLastRegularSpot();
			//this.nextPlace;
			
			
			if(spot >= Config.alternative_routes.
				route1.start.board_num
			&& spot <= Config.alternative_routes.
				route1.end.board_num)
			{
				return Config.alternative_routes.
				route1.end.board_num;
			} 
			
			
			if(spot >= Config.alternative_routes.
				route2.start.board_num
			&& spot <= Config.alternative_routes.
				route2.end.board_num)
			{
				return Config.alternative_routes.
				route2.end.board_num;
			} 
			
			
			if(spot >= Config.alternative_routes.
				route3.start.board_num
			&& spot <= Config.alternative_routes.
				route3.end.board_num)
			{
				return Config.alternative_routes.
				route3.end.board_num;
			} 
			
			
			if(spot >= Config.alternative_routes.
				route4.start.board_num
			&& spot <= Config.alternative_routes.
				route4.end.board_num)
			{
				return Config.alternative_routes.
				route4.end.board_num;
			}
			
			return 0;
			
		}
		
		private function getAlternativeExitBoardNo():Number
		{
		
			//Where is the car now? We can get the spot from using
			var spot:int = //this.nextPlace;
			this.getAdjustedInCaseOfLastRegularSpot()
			
			
			if(spot >= Config.alternative_routes.
				route1.start.board_num
			&& spot <= Config.alternative_routes.
				route1.end.board_num)
			{
				return Config.alternative_routes.
				route1.regular.end.board_num;
			} 
			
			
			if(spot >= Config.alternative_routes.
				route2.start.board_num
			&& spot <= Config.alternative_routes.
				route2.end.board_num)
			{
				return Config.alternative_routes.
				route2.regular.end.board_num;
			} 
			
			
			if(spot >= Config.alternative_routes.
				route3.start.board_num
			&& spot <= Config.alternative_routes.
				route3.end.board_num)
			{
				return Config.alternative_routes.
				route3.regular.end.board_num;
			} 
			
			
			if(spot >= Config.alternative_routes.
				route4.start.board_num
			&& spot <= Config.alternative_routes.
				route4.end.board_num)
			{
				return Config.alternative_routes.
				route4.regular.end.board_num;
			}
			
			return 0;
			
		}
		
		private function regularOrAlternative():void
		{
		
			//Where is the car now? We can get the spot from using
			var spot:int = this.nextPlace;
		
			this.routeType = "regular"; 
			
			
			if(spot >= Config.alternative_routes.
				route1.start.board_num
			&& spot <= Config.alternative_routes.
				route1.end.board_num)
			{
				this.routeType = "alternative";
			} 
			
			
			if(spot >= Config.alternative_routes.
				route2.start.board_num
			&& spot <= Config.alternative_routes.
				route2.end.board_num)
			{
				this.routeType = "alternative";
			} 
			
			
			if(spot >= Config.alternative_routes.
				route3.start.board_num
			&& spot <= Config.alternative_routes.
				route3.end.board_num)
			{
				this.routeType = "alternative";
			} 
			
			
			if(spot >= Config.alternative_routes.
				route4.start.board_num
			&& spot <= Config.alternative_routes.
				route4.end.board_num)
			{
				this.routeType = "alternative";
			}
			
		}
		
		private function alternativeRoutePittstops():Boolean
		{
			
			
			var lastPlaceSaved:Boolean = false;
			var placeRollInPittStop:Boolean = false;
			
			//this.tf.text = "";
			
			//game_pitstops
			for(var i:int = 0; 
			i<this.alternativeRoutesToCheck.length; i++)
			{
				//this.alternativeRoutesToCheck
				if
				(
				   this.positionsXML.board_places.place[this.alternativeRoutesToCheck[i]]
				   .action.briefstop != undefined
				   &&
				   this.positionsXML.board_places.place[this.alternativeRoutesToCheck[i]]
				   .action.briefstop == true
				)
				{
					this.game_pitstops.push(this.alternativeRoutesToCheck[i]);
					//this.tf.text += "bstp ---"+this.alternativeRoutesToCheck[i];
					placeRollInPittStop = true;
					
					//Make sure we dont place the last of this loop
					//twice into the pitstops array
					if(i==(this.alternativeRoutesToCheck.length-1))
					{
						lastPlaceSaved = true;
					}
				}
				
				if
				(
				   this.positionsXML.board_places.place[this.alternativeRoutesToCheck[i]].
				   action.passover_action != undefined
				   &&
				   this.positionsXML.board_places.place[this.alternativeRoutesToCheck[i]]
				   .action.passover_action == true
				)
				{
					var poa:Object = new Object();
					poa.xmlIndex = this.alternativeRoutesToCheck[i];
					poa.frame =  this.positionsXML.board_places
								.place[this.alternativeRoutesToCheck[i]].@frame;
					this.passoverActionList.push(poa);
					//this.tf.text += "poa ---"+this.alternativeRoutesToCheck[i];
					
					//Make sure we dont place the last of this loop
					//twice into the pitstops array
					//if(i==(this.alternativeRoutesToCheck.length-1))
					//{
					//	lastPlaceSaved = true;
					//}
				}
				
			}
			
			//Since we found places to stop at and we are using
			//the game_pitstops array throughout the roll, we want to 
			//make sure that the car stops at its destination roll
			//as well. Thats why we place the final roll's place in the
			//array as well.
			if(placeRollInPittStop && !lastPlaceSaved)
			{
				if(i==(this.alternativeRoutesToCheck.length-1))
				{
					this.game_pitstops
					.push(this.alternativeRoutesToCheck[i]);
				}
			}
			
			return placeRollInPittStop;
			
		}
		
		private function getAdjustedInCaseOfLastRegularSpot():int
		{
			
			var spot:int = this.nextPlace;
			if(spot == 0){spot = 1;}

			switch(spot)
			{
				
				case Config.alternative_routes.
				route1.regular.start.board_num:
				{
					return Config.alternative_routes.
							route1.start.board_num;
					break;
				}
				
				case Config.alternative_routes.
				route2.regular.start.board_num:
				{
					return Config.alternative_routes.
							route2.start.board_num;
					break;
				}
				
				case Config.alternative_routes.
				route3.regular.start.board_num:
				{
					return Config.alternative_routes.
							route3.start.board_num;
					break;
				}
				
				case Config.alternative_routes.
				route4.regular.start.board_num:
				{
					return Config.alternative_routes.
							route4.start.board_num;
					break;
				}
				
			}
			
			//If the car is still on alternative route 
			//we will not be jumping it at all.
			return spot;
			
		}
		
		private function isCarPoisedToEnterAlterRoute():Boolean
		{
			
			var spot:int = this.nextPlace;
			if(spot == 0){spot = 1;}
			
			switch(spot)
			{
				
				case Config.alternative_routes.
				route1.regular.start.board_num:
				{
					return true;
					break;
				}
				
				case Config.alternative_routes.
				route2.regular.start.board_num:
				{
					return true;
					break;
				}
				
				case Config.alternative_routes.
				route3.regular.start.board_num:
				{
					return true;
					break;
				}
				
				case Config.alternative_routes.
				route4.regular.start.board_num:
				{
					return true;
					break;
				}
				
			}
			
			//If the car is still on alternative route 
			//we will not be jumping it at all.
			return false;
			
		}
		
		private function getRouteChangeoverSpot(presentSpot:int):int
		{
			switch(presentSpot)
			{
				
				case parseInt(Config.alternative_routes.
				route1.regular.end.board_num):
				{
					return parseInt(Config.alternative_routes.
				route1.end.exitFrame);
					break;
				}
				
				case parseInt(Config.alternative_routes.
				route2.regular.end.board_num):
				{
					return parseInt(Config.alternative_routes.
				route2.end.exitFrame);
					break;
				}
				
				case parseInt(Config.alternative_routes.
				route3.regular.end.board_num):
				{
					return parseInt(Config.alternative_routes.
				route3.end.exitFrame);
					break;
				}
				
				case parseInt(Config.alternative_routes.
				route4.regular.end.board_num):
				{
					return parseInt(Config.alternative_routes.
				route4.end.exitFrame);
					break;
				}
				
			}
			return 0;
		}
		
		private function getMergeExitChangeoverBoardNumber(presentSpot:int):int
		{
			//this.tf.text = presentSpot+" --- ";
			switch(presentSpot)
			{
				
				case Config.alternative_routes.
				route1.end.board_num:
				{
					return Config.alternative_routes.
				route1.regular.end.board_num;
					break;
				}
				
				case Config.alternative_routes.
				route2.end.board_num:
				{
					return Config.alternative_routes.
				route2.regular.end.board_num;
					break;
				}
				
				case Config.alternative_routes.
				route3.end.board_num:
				{
					return Config.alternative_routes.
				route3.regular.end.board_num;
					break;
				}
				
				case Config.alternative_routes.
				route4.end.board_num:
				{
					return Config.alternative_routes.
				route4.regular.end.board_num;
					break;
				}
				
			}
			return presentSpot;
		}
		
			