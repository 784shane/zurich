package src.app_classes.actions
{
	
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class FlipCard
	{
		
		private var flipCd:MovieClip;
		private var front_card_img:Sprite;
		private var back_card_img:Sprite;
	
		public function getFlipCard(front_card:Sprite, back_card:Sprite):MovieClip
		{
			this.front_card_img = front_card;
			this.back_card_img = back_card;
			this.flipCd = new _cardRotationSpr() as MovieClip;
			this.flipCd.addEventListener("addedToStage", this.cardAddedToStage);
			return this.flipCd;
		}
		
		private function cardAddedToStage(evt:Event):void
		{
			
			this.flipCd._crRotationCards.rotationCard1.addChild(this.front_card_img);
			this.front_card_img.x = -93;
			this.front_card_img.y = 3;
			
			this.flipCd._crRotationCards.rotationCard2.addChild(this.back_card_img);
			this.back_card_img.x = -97;
			this.back_card_img.y = 0;
			
			evt.currentTarget.addEventListener("enterFrame", this.cardRunning);
		}
		
		private function cardRunning(evt:Event):void
		{
			evt.currentTarget.gotoAndStop(1);
			evt.currentTarget.removeEventListener("enterFrame", this.cardRunning);
		}
	}	
}