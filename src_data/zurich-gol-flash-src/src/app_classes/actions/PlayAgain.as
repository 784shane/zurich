package src.app_classes.actions
{
	import flash.display.MovieClip;
	
	import PlayerResources;
	import src.app_classes.GameFlow;
	import flash.text.TextField;
    import flash.utils.Timer;
	import src.app_classes.tools.Resources;
	import GlobalFunctions;
	
	public class PlayAgain
	{
	
		private var theRes:Resources;
		private var gf:GameFlow;
		private var tf:TextField;
		private var spinnerTimerPaused:Boolean = false;
		private var mainTimerPaused:Boolean = false;
		private var spinnerTimer:Timer;
		private var mainTimer:Timer;
		//private var spinnerTimerPaused:Boolean = false;
		private var pausedItems:Array = new Array();
		
		public function PlayAgain(gf:GameFlow, res:Resources, tf:TextField)
		{
			this.theRes = res;
			this.gf = gf;
			this.tf = tf;
			//public static var maximum_salary:Number = 10000;
		}
		
		public function setNewGameEnvironment():void
		{
			//Initiate the Game resources
			PlayerResources.player_wealthCards = new Array();//1,0,0);
			PlayerResources.salary = 0;
			PlayerResources.money = 10000;
			PlayerResources.maximum_salary = 0;
			PlayerResources.lifeCards = 0;
			PlayerResources.lifePath = "GRADUATE";
			PlayerResources.currentSquare = 0;
			PlayerResources.playerPegs = "";
			PlayerResources.numOfChildren = 0;
			PlayerResources.gamesavedAtEnd = false;
			/*
			public static var playerRoutes:Object = 
			{
				careerOrCollege : "CAREER"
			};
			*/
			
		
		}
		
		public function clearUi():void
		{
			this.theRes.uiMajorText.text = "";
			this.theRes.uiMinorText.text = "";
			this.theRes.uiMoniesText.htmlText = 
			GlobalFunctions.formatMoneyToString(PlayerResources.money);
			this.theRes.uiLifeText.text = "0";
		
		}
		
		public function pauseGame():void
		{
			
			this.spinnerTimer = 
			this.gf.getSpinnerTimer();
			
			this.mainTimer = this.gf.getMainTimer();
			
			
			if(this.spinnerTimer.running)
			{
				this.spinnerTimer.reset();
				this.spinnerTimerPaused = true;
			}
			
			
			if(this.mainTimer.running)
			{
				this.mainTimer.reset();
				this.mainTimerPaused = true;
			}
			
			if(this.gf.getSpinner().isPlaying)
			{
				this.gf.getSpinner().stop();
				this.pausedItems.push(this.gf.getSpinner());
			}
			
			if(this.gf.getCarTrack().isPlaying)
			{
				this.gf.getCarTrack().stop();
				this.pausedItems.push(this.gf.getCarTrack());
			}
			//this.gf.getSpinner().isPlaying
			//this.gf.getCarTrack().isPlaying
			//pausedItems
			
		}
		
		public function resumeGame():void
		{
			if(this.pausedItems.length>0)
			{
				for(var i:int = 0; i<this.pausedItems.length; i++)
				{
					this.pausedItems[i].play();
				}
				
				//remove all items of the array.
				this.pausedItems.splice
				(0, this.pausedItems.length);
				
			}
			
			//restart spinner timer again
			if(this.spinnerTimerPaused)
			{
				this.spinnerTimerPaused = false;
				this.spinnerTimer.start();
			}
			
			//restart main timer again
			if(this.mainTimerPaused)
			{
				this.mainTimerPaused = false;
				this.mainTimer.start();
			}
		}
		
	}
}