package src.app_classes.actions
{
	
	import flash.display.Sprite;
	
	import src.app_classes.appLayers;
	import src.app_classes.FeedBar;
	import src.app_classes.GamePlay;
	
	public class FeedActions
	{
		
		private var lastFeedUpdate:Number = 0;
		private var lastFeedDrop:Number = 0;
		private var layers:appLayers;
		private var fBar:FeedBar;
		private var gp:GamePlay;
		
		
		private var numberToSecondsBeforeDrop:Number = /*100;*/30000;
		private var numberToSecondsBeforeUpdate:Number = 300000;
		//five minutes
		
		public function FeedActions()
		{
		
		}
		
		public function updateToGamePlayInstance(gp:GamePlay):void
		{
			this.gp = gp;
		}
		
		public function requestGamePause():void
		{
			this.gp.requestGamePause();
		}
		
		public function requestGameResumption():void
		{
			this.gp.requestGameResumption();
		}
		
		public function readyToUpdate():Boolean
		{
			//If this time was not set, then we wont post our feed.
			if(this.lastFeedUpdate == 0)
			{this.setPresentUpdateTime(); return false;}
			//Check to see if the update time is surpass, if this is the
			//case, then we will; update our feed here and keep a record 
			//of the time we updated it
			
			var nowTime:Number = new Date().time;
			
			if((nowTime - this.lastFeedUpdate) >= numberToSecondsBeforeUpdate)
			{
				this.setPresentUpdateTime();
				return true;
			}
			
			return false;
		}
		
		public function setPresentUpdateTime():void
		{
			this.lastFeedUpdate = new Date().time;
		}
		
		public function setLastFeedDropTime():void
		{
			this.lastFeedDrop = new Date().time;
		}
		
		public function populateFeed(feedData:Object, lyrs:appLayers):void
		{
			this.layers = lyrs;
			var feedLayer:Sprite = this.layers.getLayer("feedLayer");
			this.fBar = new FeedBar(this);
			feedLayer.addChild(this.fBar.getFeedBar(feedData));
			feedLayer.x = 733;
			//feedLayer.y = 90;
			//feedLayer.y = -143;
			
			this.layers.getLayer("feedLayer").y = -1*feedLayer.height;
		}
		
		public function dropFeedNow():Boolean
		{
			if(this.lastFeedDrop == 0)
			{this.setLastFeedDropTime(); return false;}
			
			var nowTime:Number = new Date().time;
			
			if((nowTime - this.lastFeedDrop) >= numberToSecondsBeforeDrop)
			{
				this.setLastFeedDropTime();
				return true;
			}
			
			return false;
		}
		
		public function performFeedInsertion():void
		{
			
			if(this.fBar.feedData.feed_data.data.length>0)
			{
			
			var numUsed:Number = this.fBar.numberOfFeedItemsUsed;
			var numToUse:Number = 0;
			
			
			
				if(this.fBar.feedData.feed_data.data.length>numUsed)
				{
					numToUse = numUsed;
					this.fBar.numberOfFeedItemsUsed++;
				}
				else
				{
					numToUse = 0;
					this.fBar.numberOfFeedItemsUsed = 1;
				}
			
			var thumbnailId:int = this.fBar.feedData.feed_data.data[numToUse].feed_avatar;
			var feedMsg:String = this.fBar.feedData.feed_data.data[numToUse].feed_msg;
			var feedName:String = 
			this.fBar.feedData.feed_data.data[numToUse].first_name+" "+
			this.fBar.feedData.feed_data.data[numToUse].last_name;
			var feedCompanyName:String = this.fBar.feedData.feed_data.data[numToUse].company;
				
			this.fBar.dropSingleFeed(thumbnailId, feedMsg, feedName, feedCompanyName);
			
			}	
			
		}
	}
}