package src.app_classes.animations
{
	import flash.display.Sprite;
	import Config;
	import src.app_classes.appLayers;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	public class CardAnimation
	{
	
		public function CardAnimation()
		{
		
		}
		
		public static function cardSelectedAnimation(theObject:Object,state:String):void
		{
			var movementAmt:int;
			switch(state)
			{
				case "SELECTED":
				{
					movementAmt = Config.CARD_MOVE_TO_Y_VALUE;
					break;
				}
				
				case "UNSELECTED":
				{
					movementAmt = -1*Config.CARD_MOVE_TO_Y_VALUE;
					break;
				}
			}
			
			TweenLite.to(theObject, 0.2, {delay:0, y:(theObject.y + movementAmt), ease:Back.easeOut});
			
			//TweenLite.to(this.spinner_actor, 5, {delay:1, alpha:0, ease:Back.easeOut, onComplete:onFinishTween, onCompleteParams:[5,this.spinner_actor]});
			
			//after a delay of 3 seconds, tween mc for 5 seconds, sliding it across the screen by changing its "x" property to 300, using the Back.easeOut ease to make it shoot past it and come back, and then call the onFinishTween() function, passing two parameters: 5 and mc
/*TweenLite.to(mc, 5, {delay:3, x:300, ease:Back.easeOut, onComplete:onFinishTween, onCompleteParams:[5, mc]});
function onFinishTween(param1:Number, param2:MovieClip):void {
    trace("The tween has finished! param1 = " + param1 + ", and param2 = " + param2);
}
 */
 
 
		}
		
		public static function closeCardSelectionScreen(layers:appLayers, callB:Function):void
		{
			
			//layers.getLayer("forms").addChild(s);
			
			TweenLite.to(layers.getLayer("forms"), 1, {delay:0, alpha:0, onComplete:function():void{callB(2);}});
			//TweenLite.to(this.spinner_actor, 5, {delay:1, alpha:0, ease:Back.easeOut, onComplete:onFinishTween, onCompleteParams:[5,this.spinner_actor]});

		}
		
		public static function openCardSelectionScreen(layers:appLayers):void
		{
			
			//layers.getLayer("forms").addChild(s);
			
			TweenLite.to(layers.getLayer("forms"), 1, {delay:0, alpha:1});
			//TweenLite.to(this.spinner_actor, 5, {delay:1, alpha:0, ease:Back.easeOut, onComplete:onFinishTween, onCompleteParams:[5,this.spinner_actor]});

		}
		
		public static function moveCarousel(theCarouselSlider:Sprite, pos:int):void
		{
			TweenLite.to(theCarouselSlider, 1, {delay:0, ease:Back.easeOut, x:pos});
		}
	
	}
}