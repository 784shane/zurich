﻿package src.app_classes.animations
{
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class MoneyAnimationCtrl
	{
		
		private var money_grow:MovieClip;
		private var money_lose:MovieClip;
		private var ml_animationCount:int = 0;
		private var mg_animationCount:int = 0;
		private var mg_restartAnimation:Boolean = false;
		private var ml_restartAnimation:Boolean = false;
	
		public function MoneyAnimationCtrl(mg:MovieClip, ml:MovieClip)
		{
			this.money_grow = mg;
			this.money_lose = ml;
			this.setInitialMoneyActorsState();
		}
	
		private function setInitialMoneyActorsState():void
		{
			//set Money Grow and Lose Actors
			this.money_grow.alpha = 0;
			this.money_lose.alpha = 0;
			this.money_grow.gotoAndStop(1);
			this.money_lose.gotoAndStop(1);
		}
	
		public function animateMoneyGain():void
		{
			
			if(this.money_grow.currentFrame!=1)
			{
			
				this.money_grow.alpha = 1;
				this.mg_animationCount = 0;
				this.money_grow.stop();
				this.money_grow.removeEventListener("enterFrame", this.mg_rollingAnimation);
				this.money_grow.addEventListener("enterFrame", this.mg_rollingAnimation);
				this.money_grow.gotoAndPlay(1);
				//this.mg_restartAnimation = true;
			}
			else
			{
				this.money_grow.alpha = 1;
				this.money_grow.addEventListener("enterFrame", this.mg_rollingAnimation);
				this.money_grow.gotoAndPlay(1);
			}
			
		}
	
		public function animateMoneyLose():void
		{
			this.money_lose.alpha = 1;
			this.money_lose.addEventListener("enterFrame", this.ml_rollingAnimation);
			this.money_lose.gotoAndPlay(1);
		}
	
		public function ml_rollingAnimation(evt:Event):void
		{
			if(evt.target.currentFrame==1)
			{
				this.ml_animationCount++;
				if(this.ml_animationCount>0)
				{
					evt.target.removeEventListener("enterFrame", this.ml_rollingAnimation);
					evt.target.alpha = 0;
					evt.target.gotoAndStop(1);
					this.ml_animationCount = 0;
				}
				
			}
		}
	
		public function mg_rollingAnimation(evt:Event):void
		{
			if(evt.target.currentFrame==1)
			{
				this.mg_animationCount++;
				if(this.mg_animationCount>0)
				{
					evt.target.removeEventListener("enterFrame", this.mg_rollingAnimation);
					evt.target.alpha = 0;
					evt.target.gotoAndStop(1);
					this.mg_animationCount = 0;
				}
				
			}
		}
	
	}
}