package src.app_classes
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.display.Bitmap;
	import src.app_classes.GamePlay;
	
	import GlobalFunctions;
	import src.app_classes.tools.PictureLoader;
	import src.app_classes.tools.Resources;
	import src.app_classes.forms.UiForm;
	import Config;
	import PlayerResources;
	import src.app_classes.appLayers;
	import src.app_classes.screens.mapViewScreen;
	import src.app_classes.tools.RollText;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	
	
	public class PlayerUi extends Sprite
	{
		
		[Embed (source="../../res/ui/compass/needle.png" )]
		protected var compassNeedle:Class;	

		[Embed (source="../../res/ui/compass/bg-on.png" )]
		protected var compassBgOn:Class;	

		[Embed (source="../../res/ui/compass/bg-off.png" )]
		protected var compassBgOff:Class;
		
		
		private var resClass:Resources;
		
		private var numOfResourcesLoaded:int = 0;
		
		public var avatarSprite:Sprite = null;
		
		public var UiResources:Array = new Array();
		
		private var ddd:Sprite = new Sprite();
		
		private var UiFormClass:UiForm;
		
		private var theLayers:appLayers;
		
		private var gp:GamePlay;
		
		public function PlayerUi
		(ResourcesClass:Resources, theAppLayers:appLayers, gp:GamePlay)
		{
			
			this.theLayers = theAppLayers;
			this.gp = gp;
			
			//naming the PlayerUi here
			this.name = "PlayerUi";
			
			this.resClass = ResourcesClass;
			this.UiFormClass = new UiForm();
		 	this.loadImages();
		}
		
		private function loadImages():void
		{
			for(var i:int = 0; i<Config.PlayerUiResources.assets.length; i++)
			{
				
				var pl:PictureLoader = new PictureLoader(this.resourcesLoaded);
				var img:String = Config.PlayerUiResources.assets[i].src;
				pl.load_image(Config.PlayerUiResources.assets[i].name,img,0);
				
			}
		}
		
		private function resourcesLoaded(plReturned:PictureLoader, f:Object):void
		{
			var pp:Object = new Object();
			pp.imgName = f.id;
			pp.imgObj = plReturned;
			this.UiResources.push(pp);
			
			if(this.numOfResourcesLoaded == (Config.PlayerUiResources.assets.length-1))
			{
				this.setUiElements();
			}
			
			this.numOfResourcesLoaded++;
		}
		
		private function setUiElements():void
		{
			
			var uiCard:Sprite = new Sprite();
			uiCard.name = "uiCard";
			this.addChild(uiCard);
			var avatarAdded:Boolean = false;
			//uiCard.alpha = 0.4;
			
			
			
			
			
			
			for(var i:int = 0; i<Config.PlayerUiResources.assets.length; i++)
			{
				var continueLoading:Boolean = true;
				var isAvatar:Boolean = false;
				
				if((Config.PlayerUiResources.assets[i].name).indexOf("Avatar")>-1)
				{
					isAvatar = true;
					
				}	
				
				
				if(!isAvatar || !avatarAdded)
				{
					for(var j:int = 0; j<this.UiResources.length; j++)
					{
						var breakHere:Boolean = false;
						
						if(isAvatar){avatarAdded = true;}
						
						if(this.UiResources[j].imgName == Config.PlayerUiResources.assets[i].name)
						{
							var obj:Bitmap = this.UiResources[j].imgObj.content;
							var kkk:Sprite = new Sprite();
							kkk.addChild(obj);
							uiCard.addChild(kkk);
							if(isAvatar){this.avatarSprite = kkk;}
							kkk.x = Config.PlayerUiResources.assets[i].positions.x;
							kkk.y = Config.PlayerUiResources.assets[i].positions.y; 
							break;
						}
						
					}
				}	
				
			}	
				
			
			
			
			uiCard.addChild(this.getUiCompass());
			
			
			
			
			var nameSpr:Sprite = new Sprite();
			this.resClass.uiNameText = UiFormClass.getNameField();//new TextField();
			nameSpr.addChild(this.resClass.uiNameText);
			uiCard.addChild(nameSpr);
			//this.resClass.uiNameText.text = "Frederick";
			nameSpr.x = 104;
			nameSpr.y = 32;
			
			
			
			var nameSpr1:Sprite = new Sprite();
			this.resClass.uiMoniesText = UiFormClass.getMoneyReportField();//new TextField();
			nameSpr1.addChild(this.resClass.uiMoniesText);
			uiCard.addChild(nameSpr1);
			this.resClass.uiMoniesText.htmlText = this.formatMoney();
			//tf.text = this.formatMoney();//"$"+Config.PLAYER_START_MONEY;
			nameSpr1.x = 104;
			nameSpr1.y = 56;
			
			new KeyboardControlTool(nameSpr1);
			
			
			
			
			var nameSpr2:Sprite = new Sprite();
			this.resClass.uiLifeText = UiFormClass.getLifeReportField();//new TextField();
			nameSpr2.addChild(this.resClass.uiLifeText);
			uiCard.addChild(nameSpr2);
			this.resClass.uiLifeText.text = "x3";
			nameSpr2.x = 141;
			nameSpr2.y = 80;
			
			//new KeyboardControlTool(nameSpr2);
			
			
		
			//Major Ui Text
			var newSpr:Sprite = new Sprite();
			this.resClass.uiMajorText = UiFormClass.getMajorUiTextField();
			//this.resClass.uiMajorText.background = true;
			//this.resClass.uiMajorText.backgroundColor = 0xBBCC00;
			newSpr.addChild(this.resClass.uiMajorText);
			uiCard.addChild(newSpr);
			newSpr.x = 219;
			newSpr.y = 39;
			//this.resClass.uiMajorText.text ="Congratulations, it's a girl!";
			
			
			
			
			//Minor Ui Text
			var newSprx:Sprite = new Sprite();
			this.resClass.uiMinorText = UiFormClass.getMinorUiTextField();//new TextField();
			//this.resClass.uiMinorText.background = true;
			//this.resClass.uiMinorText.backgroundColor = 0xBBBB00;
			newSprx.addChild(this.resClass.uiMinorText);
			uiCard.addChild(newSprx);
			newSprx.x = 219;
			newSprx.y = 70;
			//this.resClass.uiMinorText.text ="Collect $1,000";
			
			
			/*
			var retireRibbonTxt:Sprite = new RollText("majorui").getRollText("888888", this.setNextDisplay);
			uiCard.addChild(retireRibbonTxt);
			*/
			
				
		}
		
		private function setNextDisplay():void
		{
			//this.resClass.uiMinorText.text = "done";
		}
		
		private function getReportBox():Sprite
		{
			var report:Sprite = new Sprite();
			/*
			var majorReportContainer:Sprite = new Sprite();
			
			var majorTextSpr:Sprite = new Sprite();
			majorReportContainer.addChild(majorTextSpr);
			
			this.resClass.uiMajorText = UiFormClass.getMajorUiTextField();//new TextField();
			this.resClass.uiMajorText.background = true;
			this.resClass.uiMajorText.backgroundColor = 0xBBCC00;
			majorTextSpr.addChild(this.resClass.uiMajorText);
			
			var majorTextMask:Sprite = new Sprite();
			majorTextMask.graphics.beginFill(0xBB00BB);
			majorTextMask.graphics.drawRect(0, 0, 326, 32);
			majorTextMask.graphics.endFill();
			majorReportContainer.addChild(majorTextMask);
			
			majorTextSpr.mask = majorTextMask;
			
			report.addChild(majorReportContainer);
			*/
			return report;
		}
		
		private function formatMoney():String
		{
			var startMoney:Number = Config.PLAYER_START_MONEY;
			PlayerResources.money = startMoney;
			
			return GlobalFunctions.formatMoneyToString(startMoney);
			
		}
		
		private function getUiCompass():Sprite
		{
			
			var compassUi:Sprite = new Sprite();
			compassUi.buttonMode = true;
			compassUi.addEventListener(MouseEvent.CLICK,this.compassClicked);
			
			
			var bgOff:Bitmap = new compassBgOff() as Bitmap;
			compassUi.addChild(bgOff);
			bgOff.x=569; 
			bgOff.y=44;
			
			
			var compassNeedle:Bitmap = new compassNeedle() as Bitmap;
			var d:Sprite = new Sprite();
			d.addChild(compassNeedle);
			compassUi.addChild(d);
			d.x=580; 
			d.y=57;
				
			return compassUi;
		}
		
		private function compassClicked(evt:MouseEvent):void
		{
			//pause game
			this.gp.requestGamePause();
			
			this.theLayers.getLayer("zoomedGameLayer")
			.addChild(new mapViewScreen(this.gp, this.resClass).getMapViewScreen());
		}
		
		private function mv_frameRunning(evt:Event):void
		{
			evt.target.stop();
			evt.target.removeEventListener(Event.ENTER_FRAME,this.mv_frameRunning);
			evt.target.gotoAndStop(PlayerResources.currentSquare);
			
		}
		
	}
}