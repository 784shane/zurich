package src.app_classes
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.display.Graphics;
	import flash.text.TextField;
	import flash.text.StyleSheet;
	
	import src.app_classes.tools.appStyles;
	
	
	import flash.filters.BitmapFilter;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BlurFilter;
	
	public class appLayers extends Sprite
	{
		private var mainSprite:Sprite;
		public var noOfLayersToCreate:int = 14;
		public var noOfLayersAdded:int = 0;
		
		//Layers ***********************
		public var backgroundLayer:Sprite = new Sprite();
		public var mainTrackLayer:Sprite = new Sprite();
		public var blurryLayer:Sprite = new Sprite();
		public var wheelLayer:Sprite = new Sprite();
		public var spinToWinBackgroundLayer:Sprite = new Sprite();
		public var uiLayer:Sprite = new Sprite();
		public var blockLayer:Sprite = new Sprite();
		public var formsLayer:Sprite = new Sprite();
		public var pagesBackground:Sprite = new Sprite();
		public var pagesLayer:Sprite = new Sprite();
		public var interactionLayer:Sprite = new Sprite();
		public var adsLayer:Sprite = new Sprite();
		public var titleLayer:Sprite = new Sprite();
		public var feedLayer:Sprite = new Sprite();
		public var zoomedGameLayer:Sprite = new Sprite();
		//******************************
		
		private var layer_array:Array;
		
		private var callBackFunc:Function;
		
		
		/*temp*/
		//private var tf:TextField;
		
		
		public function appLayers(mainSprite:Sprite, callBackFunc:Function)
		{
			this.callBackFunc = callBackFunc;
			this.mainSprite = mainSprite;
		}
		
		public function createLayers():void
		{
			//If another layer is to be created, 
			//remember to update - noOfLayersToCreate
			
			this.backgroundLayer.addEventListener("addedToStage",onAddedTo);
			this.mainTrackLayer.addEventListener("addedToStage",onAddedTo);
			this.blurryLayer.addEventListener("addedToStage",blurryLayerAdded);
			this.spinToWinBackgroundLayer.addEventListener("addedToStage",onAddedTo);
			this.wheelLayer.addEventListener("addedToStage",onAddedTo);
			this.uiLayer.addEventListener("addedToStage",onAddedTo);
			this.blockLayer.addEventListener("addedToStage",onAddedTo);
			this.interactionLayer.addEventListener("addedToStage",onAddedTo);
			this.formsLayer.addEventListener("addedToStage",onAddedTo);
			this.pagesBackground.addEventListener("addedToStage",onAddedTo);
			this.pagesLayer.addEventListener("addedToStage",onAddedTo);
			this.titleLayer.addEventListener("addedToStage",onAddedTo);
			this.feedLayer.addEventListener("addedToStage",onAddedTo);
			this.zoomedGameLayer.addEventListener("addedToStage",onAddedTo);
			this.adsLayer.addEventListener("addedToStage",onAddedTo);
			
			
			//add one layer here. This blurry layer will contain
			//all the items that will be blurred
			//when using popups.
			this.mainSprite.addChild(this.blurryLayer);
		}
		
		private function blurryLayerAdded(evt:Event):void
		{
			
			this.noOfLayersAdded++;
			
			
			this.blurryLayer.addChild(this.backgroundLayer);
			this.blurryLayer.addChild(this.mainTrackLayer);
			this.blurryLayer.addChild(this.feedLayer);
			this.mainSprite.addChild(this.spinToWinBackgroundLayer);
			this.mainSprite.addChild(this.wheelLayer);
			this.mainSprite.addChild(this.uiLayer);
			this.mainSprite.addChild(this.interactionLayer);
			this.mainSprite.addChild(this.adsLayer);
			this.mainSprite.addChild(this.pagesBackground);
			this.mainSprite.addChild(this.formsLayer);
			this.mainSprite.addChild(this.pagesLayer);
			this.mainSprite.addChild(this.zoomedGameLayer);
			this.mainSprite.addChild(this.blockLayer);
			this.mainSprite.addChild(this.titleLayer);
			
		}
		
		private function onAddedTo(evt:Event):void
		{
			
			this.noOfLayersAdded++;
			if(this.noOfLayersAdded == this.noOfLayersToCreate)
			{
				this.layer_settings();
				this.callBackFunc();
			}
			
		}
		
		private function layer_settings():void
		{
			
			//Using stylesheets example
			/*
			this.tf = new TextField();
			this.blurryLayer.addChild(tf);
			tf.y = 300;
			var css:StyleSheet = new appStyles();
			//this is the stylesheet created in the appStyles class
			tf.styleSheet = css;
			tf.htmlText='<div class="bragster">Googlehahah</div>';
			*/
			
			
			
			//After all layers are created, we will add these settings
			//to individual layers
			
			//wheel layer
			this.wheelLayer.x = 483;
			this.wheelLayer.y = 255;
			
			//blur layer
			/*
			this.blurryLayer.x = 0;
			this.blurryLayer.y = 0;
			this.blurryLayer.width = 980;
			this.blurryLayer.height = 750;
			this.blurryLayer.graphics.beginFill(0xFFFFFF);
			this.blurryLayer.graphics.drawRect(0, 0, 100,100);
			this.blurryLayer.graphics.endFill();
			*/
			
			/*
			var filter:BitmapFilter = new BlurFilter(20, 20, BitmapFilterQuality.MEDIUM);
			var myFilters:Array = new Array();
			myFilters.push(filter);

			this.blurryLayer.filters = myFilters;
			*/
			//this.blurryLayer.alpha = 0.2;
			
			
			
			/*
			var d:Sprite = new Sprite();
			d.graphics.beginFill(0xFF0000);
			d.graphics.drawRect(0, 0, 100,100);
			d.graphics.endFill();
			this.blurryLayer.addChild(d);
			*/
		}
		
		public function createPages():void
		{
			var page1:Sprite = new Sprite();
			page1.name = "page1";
			this.pagesLayer.addChild(page1);
			
			var page2:Sprite = new Sprite();
			page2.name = "page2";
			this.pagesLayer.addChild(page2);
		}
		
		public function getLayer(theLayerName:String):Sprite
		{
			switch(theLayerName)
			{
				case "forms":
				{
					return this.formsLayer;
					break;
				}
				
				case "background":
				{
					return this.backgroundLayer;
					break;
				}
				
				case "wheel":
				{
					return this.wheelLayer;
					break;
				}
				
				case "mainTrack":
				{
					return this.mainTrackLayer;
					break;
				}
				
				case "blurLayer":
				{
					return this.blurryLayer;
					break;
				}
				
				case "uiLayer":
				{
					return this.uiLayer;
					break;
				}
				
				case "titleLayer":
				{
					return this.titleLayer;
					break;
				}
				
				case "feedLayer":
				{
					return this.feedLayer;
					break;
				}
				
				case "zoomedGameLayer":
				{
					return this.zoomedGameLayer;
					break;
				}
				
				case "interactionLayer":
				{
					return this.interactionLayer;
					break;
				}
				
				case "blockLayer":
				{
					return this.blockLayer;
					break;
				}
				
				case "adsLayer":
				{
					return this.adsLayer;
					break;
				}
				
				case "spinToWin":
				{
					return this.spinToWinBackgroundLayer;
					break;
				}
				
				case "pagesLayer":
				{
					return this.pagesLayer;
					break;
				}
				
				case "pagesBackground":
				{
					return this.pagesBackground;
					break;
				}
				
			}
			
			return new Sprite();
		}
	}
}