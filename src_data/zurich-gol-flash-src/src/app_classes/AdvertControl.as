package src.app_classes
{
	import flash.display.Sprite;
	import src.app_classes.appLayers;
	import GlobalFunctions;
	import src.app_classes.GamePlay;
	
	
	import src.app_classes.screens.Advertisement;
	
	public class AdvertControl
	{
		
		private var functionAfterAdvert:Function;
		private var layers:appLayers;
		private var advertScreen:Advertisement;
		private var copyXML:XML;
		private var gp:GamePlay;
		
		public function AdvertControl(gp:GamePlay, lyrs:appLayers, mainXML:XML)
		{
			this.gp = gp;
			this.copyXML = mainXML;
			this.layers = lyrs;
		}
		
		public function displayAdvert(xmlPos:int, functionAfterAd:Function = null):void
		{
			
			var titleCopy:String = 
			this.copyXML.board_places.place[xmlPos].action.advert.advert_title_copy;
			
			var bodyCopy:String = 
			this.copyXML.board_places.place[xmlPos].action.advert.advert_body_copy;
			//this.positionsXML.board_places.place[(num)].@frame
			//advert_title_copy
			
			this.functionAfterAdvert = functionAfterAd;
			
			this.advertScreen = new Advertisement(this.gp, this.onAdvertDismissed);
			this.layers.getLayer("adsLayer").alpha=1;
			this.layers.getLayer("adsLayer").addChild(
			this.advertScreen.getAdvertScreen(titleCopy,bodyCopy)
			);
			
		}
		
		private function onAdvertDismissed():void
		{
			this.layers.getLayer("adsLayer").alpha=0;
			GlobalFunctions.emptyContainer(this.layers.getLayer("adsLayer"));
			this.layers.getLayer("adsLayer").alpha=1;
			
			this.functionAfterAdvert();
		}
		
	}
}