package src.app_classes.tools
{
	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	
	import GeneralAnimations;
	
	
	import Config;
	
	public class Ribbon extends Sprite
	{
		
		[Embed (source="../../../res/totalScreen/ribbonLeft.png" )]
		private var ribbonLeft:Class;
		
		[Embed (source="../../../res/totalScreen/ribbonRight.png" )]
		private var ribbonRight:Class;
		
		private var ribbonBarLeft:Sprite;
		private var ribbonBarRight:Sprite;
		private var ribbonBowLeft:Sprite;
		private var ribbonBowRight:Sprite;
		
		private var ribbonOpenCount:int = 0;
		private var whenOpenComplete:Function;
		
		public function Ribbon()
		{
			this.alpha = 0;
			this.buildRibbon();
			//this.addChild(new ribbonRight() as Bitmap);
		}
		
		private function buildRibbon():void//Sprite
		{
			var spr:Sprite = new Sprite();
			
			var ribbonBarContainerLeft:Sprite = 
			this.getRibbonHorizontalBar();
			this.addChild(ribbonBarContainerLeft);
			ribbonBarContainerLeft.x = 46;
			//ribbonBarContainerLeft.y = -50;
			
			
			var ribbonBarContainerRight:Sprite = 
			this.getRibbonHorizontalBar();
			this.addChild(ribbonBarContainerRight);
			ribbonBarContainerRight.x = 255;
			//ribbonBarContainerRight.y = -50;
			
			
			this.ribbonBarLeft = 
			ribbonBarContainerLeft.getChildByName("strips") as Sprite;
			this.ribbonBarLeft.x = 199;
			this.ribbonBarRight = 
			ribbonBarContainerRight.getChildByName("strips") as Sprite;
			this.ribbonBarRight.x = -199;
			
			
			this.ribbonBowRight = getRibbonBow("right");
			this.addChild(this.ribbonBowRight);
			//this.ribbonBowRight.x = 457;
			this.ribbonBowRight.x = 265;
			
			this.ribbonBowLeft = getRibbonBow("left");
			this.addChild(this.ribbonBowLeft);
			//this.ribbonBowLeft.x = 0;
			this.ribbonBowLeft.x = 192;
			
			/*
			spr.x = 236;
			spr.y = 373;
			
			
			var lrgTxtSpr:Sprite = new Sprite();
			var lrgTxt:TextField = this.getFinalScreenLargeText();			
			lrgTxtSpr.addChild(lrgTxt);
			spr.addChild(lrgTxtSpr);
			lrgTxt.htmlText = "&#163;19,8765";
			lrgTxtSpr.x=53;
			*/
			
			
			
			//return spr;
		}
		
		private function getRibbonHorizontalBar():Sprite
		{
			var horizontalBar:Sprite = new Sprite();
			
			var ribbonMask:Sprite = new Sprite();
			ribbonMask.graphics.beginFill(0xBBCC00,1);
			ribbonMask.graphics.drawRect(0, 0, 209, 107);
			ribbonMask.graphics.endFill();
			horizontalBar.addChild(ribbonMask);
			
			var strips:Sprite = getRibbonBar();
			horizontalBar.addChild(strips);
			strips.name = "strips";
			
			var _r_ribbonTop:Sprite = getRibbonBar();
			strips.addChild(_r_ribbonTop);
			
			var _r_ribbonBottom:Sprite = getRibbonBar();
			strips.addChild(_r_ribbonBottom);
			_r_ribbonBottom.y = 100;
			
			strips.mask = ribbonMask;
			
			
			
			return horizontalBar;
			
		}
		
		public function getRibbonBow(dir:String = "right"):Sprite
		{
			var ribbonBow:Sprite = new Sprite();
			
			var ribbonBar:Sprite = new Sprite();
			ribbonBar.graphics.beginFill
			(Config.APP_COLORS.DARK_BLUE,1);
			ribbonBar.graphics.drawRect(0, 0, 7, 107);
			ribbonBar.graphics.endFill();
			
			ribbonBow.addChild(ribbonBar);
			
			
			var ribbonBowRight:Sprite = new Sprite();
			if(dir=="right")
			{
			ribbonBowRight.addChild(new ribbonRight() as Bitmap);
			}
			else
			{
			ribbonBowRight.addChild(new ribbonLeft() as Bitmap);
			}
			ribbonBow.addChild(ribbonBowRight);
			
			
			
			if(dir=="left")
			{
			ribbonBar.x=46;
			}
			
			if(dir=="right")
			{
			ribbonBowRight.x=10;
			}
			
			
			ribbonBowRight.y=7;
			
			
			return ribbonBow;
		}
		
		public function getRibbonBar():Sprite
		{
			var ribbonBar:Sprite = new Sprite();
			ribbonBar.graphics.beginFill(Config.APP_COLORS.DARK_BLUE,1);
			ribbonBar.graphics.drawRect(0, 0, 209, 7);
			ribbonBar.graphics.endFill();
			
			return ribbonBar;
		}
		
		public function onRibbonDisplayed():void
		{
			GeneralAnimations.moveRibbonHorizontally(this.ribbonBarLeft,0, this.onRibbonOpen);
			GeneralAnimations.moveRibbonHorizontally(this.ribbonBarRight,0, this.onRibbonOpen);
			GeneralAnimations.moveRibbonHorizontally(this.ribbonBowLeft,0, this.onRibbonOpen);
			GeneralAnimations.moveRibbonHorizontally(this.ribbonBowRight,457, this.onRibbonOpen);
		}
		
		public function openRibbon(uponComplete:Function):void
		{
			this.whenOpenComplete = uponComplete;
			GeneralAnimations.basic_displayItemToggle(this, this.onRibbonDisplayed, 1);
		}
		
		public function onRibbonOpen():void
		{
			this.ribbonOpenCount++;
			if(this.ribbonOpenCount == 4)
			{
				this.whenOpenComplete();
			}
		}
		
		
	}
}