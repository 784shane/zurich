package src.app_classes.tools
{
	
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldAutoSize;
	
	import src.app_classes.config.DropShadows;
	import Config;
	
	
	
	
	public class gameStyles extends customStyleSheet
	{
		
		public static function form_TextboxStyle(tf:TextField, tfContainer:Sprite):void
		{
			tfContainer.graphics.beginFill(0xFFFFFF);
			tfContainer.graphics.lineStyle(1,0xCBCBCB);
			//tfContainer.graphics.drawRect(0, 0, 298, 44);
			tfContainer.graphics.drawRoundRect(0, 0, 298, 44, 5, 5);
			tfContainer.graphics.endFill();
			
			
			//Add two Sprites to this container. One is the mouse
			//on sprite and the other is the select on
			var mOver:Sprite = new Sprite();
			mOver.graphics.beginFill(0xF2F2F2);
			//mOver.graphics.lineStyle(1,0xCBCBCB);
			//mOver.graphics.drawRect(0, 0, 298, 44);
			mOver.graphics.drawRoundRect(0, 0, 298, 44, 5, 5);
			mOver.graphics.endFill();
			tfContainer.addChild(mOver);
			mOver.alpha = 0;
			mOver.name = "mOver";
			
			
			var selectable:Sprite = new Sprite();
			selectable.graphics.beginFill(0xF2F2F2);
			//selectable.graphics.lineStyle(1,0xCBCBCB);
			//selectable.graphics.drawRect(0, 0, 298, 44);
			selectable.graphics.drawRoundRect(0, 0, 298, 44, 5, 5);
			selectable.graphics.endFill();
			tfContainer.addChild(selectable);
			selectable.alpha = 0;
			selectable.name = "selectable";
			
			
			tf.defaultTextFormat = customStyleSheet.formatTextBox(tf);
			
			
			tf.selectable = false;
			tf.type = TextFieldType.INPUT;
			//tf.wordWrap = true;
			tf.height = 30;
			tf.width = 276;
			tf.x = 12;
			tf.y = 6;
			
		}
		
		public static function form_ButtonTextStyle(tf:TextField):void
		{
			tf.embedFonts = true;
			tf.defaultTextFormat = customStyleSheet.formatform_ButtonTextBox(tf);
			
			
			tf.type = TextFieldType.INPUT;
			tf.selectable = false;
			tf.width = 180;
			tf.height = 30;
			tf.x = 0;
			tf.y = 15;
			
		}
		
		public static function gt_activityText(tf:TextField):void
		{
			tf.embedFonts = true;
			tf.defaultTextFormat = customStyleSheet.gtActivityTextBoxStyle(tf);
			//tf.type = TextFieldType.INPUT;
			tf.selectable = false;
			
		}
		
		public static function form_TextLabelStyle(tf:TextField):void
		{
			tf.embedFonts = true;
			tf.defaultTextFormat = customStyleSheet.formatTextLabel(tf);
			tf.type = TextFieldType.DYNAMIC;
			tf.selectable = false;
			tf.width = 180;
			tf.height = 30;
			tf.x = 0;
			tf.y = 15;
			
		}
		
		public static function majorUiTfStyle(tf:TextField, txtInstance:String = "major"):void
		{
			if(txtInstance == "major")
			{
				tf.defaultTextFormat = customStyleSheet.formatMajorUiTextStyle(tf);
			}
			
			if(txtInstance == "minor")
			{
				tf.defaultTextFormat = customStyleSheet.formatMinorUiTextStyle(tf);
			}
			
			if(txtInstance == "money")
			{
				tf.defaultTextFormat = customStyleSheet.formatMoneyUiTextStyle(tf);
			}
			
			tf.type = TextFieldType.DYNAMIC;
			tf.selectable=false;
			
			//tf.background = true;
			//tf.backgroundColor = 0xBB0000;
			tf.wordWrap = true;
			//tf.alpha = 0.4;
			//tf.border = true;
		}
		
		public static function FeedNameText_TfStyle(tf:TextField):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.textColor = Config.APP_COLORS.FONT_GREY;
			//tf.background = true;
			//tf.backgroundColor = 0xBB0000;
			tf.wordWrap = true;
			tf.selectable=false;
			tf.autoSize = TextFieldAutoSize.LEFT;
			tf.defaultTextFormat =
			customStyleSheet.formatFeedNameTextStyle(tf);
			
			/*
			var txtFormat:TextFormat = bodyHeadTxt.getTextFormat();
			txtFormat.size=12;
			txtFormat.align="left";
			if(bold){txtFormat.bold=true;}
			bodyHeadTxt.defaultTextFormat = txtFormat;
			*/
		}
		
		public static function feedText_TfStyle(tf:TextField, boldIt:Boolean = false):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.textColor = Config.APP_COLORS.FONT_GREY;
			//tf.background = true;
			//tf.backgroundColor = 0xBB0000;
			tf.wordWrap = true;
			tf.selectable=false;
			tf.autoSize = TextFieldAutoSize.LEFT;
			tf.defaultTextFormat =
			customStyleSheet.formatFeedTextStyle(tf, boldIt);
			
			//result.autoSize=tfs;
			
			//tf.alpha = 0.4;
			//tf.border = true;
		}
		
		public static function htpPara_TfStyle(tf:TextField):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.textColor = Config.APP_COLORS.FONT_GREY;
			tf.wordWrap = true;
			tf.selectable=false;
			tf.autoSize = TextFieldAutoSize.LEFT;
			tf.defaultTextFormat =
			customStyleSheet.htpParaTextStyle(tf);
		}
		
		public static function titleBarTextStyle(tf:TextField):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.selectable=false;
			tf.embedFonts = true;
			tf.defaultTextFormat = 
			customStyleSheet.newTitlebarStyle(tf);
			//tf.background = true;
			//tf.backgroundColor = 0xBBCC00;
		}
		
		public static function titleTextStyle(tf:TextField):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.textColor=0xFFFFFF;
			tf.selectable=false;
			customStyleSheet.formatTitleBarTextStyle(tf);
			//tf.background = true;
			//tf.backgroundColor = 0xBBCC00;
		}
		
		public static function cardTextStyle(tf:TextField, getBold:Boolean=true):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.selectable=false;
			tf.defaultTextFormat =
			customStyleSheet.formatCardTextStyle(tf, getBold);
			
			//tf.background = true;
			//tf.backgroundColor = 0xBBCC00;
		}
		
		public static function largeNumbersTextStyle(tf:TextField):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.selectable=false;
			tf.width = 404;
			tf.height = 150;
			tf.defaultTextFormat =
			customStyleSheet.largeNumbersTextStyle(tf);
			
			//tf.background = true;
			//tf.backgroundColor = 0xBBCC00;
			
			var myFilters:Array = new Array();
            myFilters.push(new DropShadows().getScreenTitleShadow());//gameStyles.getBitmapFilter());
            tf.filters = myFilters;
            
		}
		
		public static function popupTitleTextStyle(tf:TextField):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.selectable=false;
			tf.defaultTextFormat =
			customStyleSheet.formatpopupTitleTextStyle(tf);
			
			var myFilters:Array = new Array();
            myFilters.push(new DropShadows().getScreenTitleShadow());//gameStyles.getBitmapFilter());
            tf.filters = myFilters;
			
		}
		
		public static function leaderboardTextStyle
		(tf:TextField, txtTypeName:String):void
		{
			tf.type = TextFieldType.DYNAMIC;
			tf.selectable=false;
			tf.defaultTextFormat = 
			customStyleSheet.formatLeaderboardTextStyle(tf, txtTypeName);
			
			//tf.background = true;
			//tf.backgroundColor = 0xBBCC00;
		}
		
	}
}