package src.app_classes.tools
{
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequestMethod;
	import flash.events.Event;
	
	import flash.text.TextField;
	
	import PlayerResources;

	
	public class BackendInterface
	{
		private var tf:TextField = tf;
		private var functionWhenFinished:Function;
		private var saveDataBackCallBack:Function;
		private var leaderboardDataReady:Function;
		private var registerCompleteFunction:Function;
		private var presentActivity:String = "NONE";
		
		private var gameAtEnd:int = 0;
		
		public function BackendInterface(tf:TextField,callBack:Function)
		{
			this.tf = tf;
			this.functionWhenFinished = callBack;
		}
		
		public function sendData():void
		{
			
		}

		public function requestLogin(loginData:Object):void
		{
			this.presentActivity = "login";
			var loader : URLLoader = new URLLoader;
		    var urlreq:URLRequest = new URLRequest("/login");
		    var urlvars: URLVariables = new URLVariables;
		    
		    
		    urlreq.method = URLRequestMethod.POST;
		    
		    
		    urlvars.em = loginData.emailAdd;
			urlvars.pw = loginData.password;
			
		    urlreq.data = urlvars;
		    
		    
		    loader.addEventListener(Event.COMPLETE, loginRequestCompleted);
		    loader.load(urlreq);
		    
		}

		public function sendToRegister(regdata:Object, registerCompleteFunc:Function):void
		{
			
			this.registerCompleteFunction = registerCompleteFunc;
			this.presentActivity = "register";
		    var loader : URLLoader = new URLLoader;
		    var urlreq:URLRequest = new URLRequest("/savedata");
		    var urlvars: URLVariables = new URLVariables;
		    
		    //loader.dataFormat = URLLoaderDataFormat.VARIABLES;
		    
		    urlreq.method = URLRequestMethod.POST;
		    
			urlvars.f_name = regdata.f_name;
			urlvars.l_name = regdata.l_name;
			urlvars.company_name = regdata.company_name;
			urlvars.email = regdata.email;
			urlvars.postCode = regdata.postCode;
			urlvars.password = regdata.password;
			
		    urlreq.data = urlvars;
		    
		    
		    loader.addEventListener(Event.COMPLETE, registerCompleteFunc);
		    loader.load(urlreq);
		    //loader.load(null);
		    
		    
		    
		}
		
		public function onRegisterComplete(evt:Event):void
		{
			this.registerCompleteFunction();
		}
		
		public function loginRequestCompleted(evt:Event):void
		{
			//test login returned data here.
			var loginAttemptData:Object = JSON.parse(evt.target.data);
			this.functionWhenFinished(this.presentActivity,loginAttemptData);
		}
		
		public function getSavedGame(sdBFunc:Function):void
		{
			this.saveDataBackCallBack = sdBFunc;
			//set user temporarily
			var uid:String = "1";
			
			
			this.presentActivity = "register";
		    var loader : URLLoader = new URLLoader;
		    var urlreq:URLRequest = new URLRequest("/retrieve_saved_game");
		    var urlvars: URLVariables = new URLVariables;
		    
		    //loader.dataFormat = URLLoaderDataFormat.VARIABLES;
		    //data to send to the url
		    urlreq.method = URLRequestMethod.POST;
			urlvars.uid = uid;
			
		    urlreq.data = urlvars;
		    
		    
		    loader.addEventListener(Event.COMPLETE, this.onRetrievedData);
		    loader.load(urlreq);
		    
		    
		    
			
			//this.functionWhenFinished(this.presentActivity,evt.target.data);
		}
		
		private function onRetrievedData(evt:Event):void
		{
			var savedData:Object = JSON.parse(evt.target.data);
			this.saveDataBackCallBack(savedData);
			//var arr:Array = JSON.parse(evt.target.data) as Array;
			//this.tf.text = evt.target.data;//arr.one;
			//var arr:Array = (JSON.decode(evt.target.data) as Array);
			//this.saveDataBackCallBack(savedData);
		}
		
		public function saveGameState(obj:Object, endOfGame:int = 0):void
		{
			
			this.gameAtEnd = endOfGame;
			
			
		    var loader : URLLoader = new URLLoader;
		    var urlreq:URLRequest = new URLRequest("/save_game");
		    //assign obj here as URLvariables
		    var urlvars: URLVariables = new URLVariables;
		    
		    //loader.dataFormat = URLLoaderDataFormat.VARIABLES;
		    
		    
			//data to send to the url
			urlvars.uid = obj.uid;
			urlvars.car_square_postion = obj.car_square_postion;
			urlvars.cash_in_hand = obj.cash_in_hand;
			urlvars.life_cards = obj.life_cards;
			urlvars.car_color = obj.car_color;
			urlvars.avatar = obj.avatar;
			urlvars.pegs = obj.pegs;
			urlvars.wheel_last_spin = obj.wheel_last_spin;
			urlvars.wheel_last_rotation = obj.wheel_last_rotation;
			urlvars.needle_last_rotation = obj.needle_last_rotation;
			urlvars.game_completed = obj.game_completed;
			urlvars.sex = obj.sex;
			urlvars.salary = obj.salary;
			urlvars.maximum_salary = obj.maximum_salary;
			urlvars.salary_taxes = obj.salary_taxes;
			urlvars.house_buying_price = obj.house_buying_price;
			urlvars.house_selling_price = obj.house_selling_price;
			urlvars.house_insurance = obj.house_insurance;
			urlvars.lifePath = obj.lifePath;
			urlvars.numOfChildren = obj.numOfChildren;
			urlvars.job = obj.job;
			urlvars.wealth_cards = obj.wealth_cards;
			
			
			/*
			//dataToSave.wealth_cards = PlayerResources.wealth_cards;
			*/
			
			
		    urlreq.data = urlvars;
		    urlreq.method = URLRequestMethod.POST;
		    
			//urlvars = obj as URLVariables;
		    
		    
		    loader.addEventListener(Event.COMPLETE, this.onGameStateSaved);
		    loader.load(urlreq);
		}
		
		private function onGameStateSaved(evt:Event):void
		{
			if(this.gameAtEnd==1)
			{
				this.presentActivity = "game_saved_at_end";
				
				this.functionWhenFinished
				(this.presentActivity,{});
			}
			
		}
		
		public function getLeaderboardData(lb_dataReady:Function):void
		{
			this.leaderboardDataReady = lb_dataReady;
			
			var loader : URLLoader = new URLLoader;
		    var urlreq:URLRequest = new URLRequest("/leaderboard_data");
		    //assign obj here as URLvariables
		    var urlvars: URLVariables = new URLVariables;
		  
		    urlvars.uid = PlayerResources.player_uid;
			
			//this.tf.text = "uid - "+PlayerResources.player_uid;
		    
			//data to send to the url
			/*urlvars.uid = obj.uid;
			urlvars.car_square_postion = obj.car_square_postion;
			urlvars.cash_in_hand = obj.cash_in_hand;
			urlvars.life_cards = obj.life_cards;
			urlvars.car_color = obj.car_color;
			urlvars.avatar = obj.avatar;
			urlvars.wheel_last_spin = obj.wheel_last_spin;
			urlvars.wheel_last_rotation = obj.wheel_last_rotation;
			urlvars.needle_last_rotation = obj.needle_last_rotation;
			*/
			
		    urlreq.data = urlvars;
		    urlreq.method = URLRequestMethod.POST;
		    
		    
		    loader.addEventListener(Event.COMPLETE, this.onLeaderboardDataRetrieved);
		    loader.load(urlreq);
		}
		
		private function onLeaderboardDataRetrieved(evt:Event):void
		{
			var leaderboardData:Object = JSON.parse(evt.target.data);
			this.leaderboardDataReady(leaderboardData);
		}
		
		public function postFeed(afterPostingFeed:Function, feedObj:Object):void
		{
			//afterPostingFeed(feedObj.txt+" - "+feedObj.uid);
			
			var loader : URLLoader = new URLLoader;
		    var urlreq:URLRequest = new URLRequest("/post_feed");
		    //assign obj here as URLvariables
		    var urlvars: URLVariables = new URLVariables;
		  
		    
		    
			//data to send to the url
			urlvars.uid = feedObj.uid;
			urlvars.feedTxt = feedObj.txt;
			urlvars.avatar = feedObj.avatar;
			
			
		    urlreq.data = urlvars;
		    urlreq.method = URLRequestMethod.POST;
		    
		    
		    loader.addEventListener(Event.COMPLETE, afterPostingFeed);
		    loader.load(urlreq);
		}
		
		public function getFeed(afterReceivingFeed:Function):void
		{
			//afterPostingFeed(feedObj.txt+" - "+feedObj.uid);
			
			var loader : URLLoader = new URLLoader;
		    var urlreq:URLRequest = new URLRequest("/retrieve_feed");
		    //assign obj here as URLvariables
		    var urlvars: URLVariables = new URLVariables;
		    
		    
			urlvars.uid = PlayerResources.player_uid;
			
		  
		    
		    /*
			//data to send to the url
			urlvars.uid = feedObj.uid;
			urlvars.feedTxt = feedObj.txt;
			urlvars.avatar = feedObj.avatar;
			*/
			
		    urlreq.data = urlvars;
		    urlreq.method = URLRequestMethod.POST;
		    
		    
		    loader.addEventListener(Event.COMPLETE, afterReceivingFeed);
		    loader.load(urlreq);
			//afterReceivingFeed("afterReceivingFeed");
		}
		
		public function getCompaniesList
		(afterReceivingFeed:Function):void
		{
			var loader : URLLoader = new URLLoader;
		    var urlreq:URLRequest = new URLRequest("/get_companies");
		    //assign obj here as URLvariables
		    //var urlvars: URLVariables = new URLVariables;
		   	//urlreq.data = urlvars;
		    urlreq.method = URLRequestMethod.POST;
		    
		    
		    loader.addEventListener(Event.COMPLETE, afterReceivingFeed);
		    loader.load(urlreq);
		}
		
	}
	
}