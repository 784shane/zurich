package src.app_classes.tools
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	
	import src.app_classes.forms.UiForm;
	import src.app_classes.tools.KeyboardControlTool;
	import src.app_classes.forms.AppForms;
	import GlobalFunctions;
	import GeneralAnimations;
	
	public class RollText extends AppForms
	{
		
		private var UiFormClass:UiForm;
		
		
		private var dbText:TextField;
		private var columnRollCount:int = 0;
		private var numOfColumns:int = 0;
		private var onCompleteRollFunc:Function;
		private var textType:String;
		private var txtYOffset:Number = -15;
		
		public function RollText(txt_type:String = "default")
		{
			
			this.UiFormClass = new UiForm();
			this.textType = txt_type;
			this.txtYOffset = this.setTxtYOffset();
		}
		
		public function getRollText
		(numberString:String = "2019025",
		onComplete:Function = null):Sprite
		{
			this.onCompleteRollFunc = onComplete;
			var rollTextContainer:Sprite = new Sprite();
			
			var rollTextInterior:Sprite = new Sprite();
			rollTextContainer.addChild(rollTextInterior);
			
			
			this.numOfColumns = numberString.length;
			
			var xPos:int = 0;
			for(var a:int = 0; a<this.numOfColumns; a++)
			{
				
				if(a==0)
				{
				var poundSign:Sprite = this.getPoundSign();
				//digitSpirit.name = a+"-"+this.numOfColumns;
				rollTextInterior.addChild(poundSign);
				poundSign.x = xPos;
				xPos+=this.poundSignWidthAdjustment(poundSign.width);
				}
				
				
				var digitSpirit:Sprite = this.getDigit(a, numberString);
				digitSpirit.name = a+"-"+this.numOfColumns;
				rollTextInterior.addChild(digitSpirit);
				digitSpirit.x = xPos;
				xPos+=this.digitWidthAdjustment(digitSpirit.width);
				//digitSpirit.addEventListener("addedToStage", this.move);
				
				
				
				
				if(((this.numOfColumns-a)==4)||
				((this.numOfColumns-a)==7)||
				((this.numOfColumns-a)==10)||
				((this.numOfColumns-a)==13))
				{
				var comma:Sprite = this.getComma();
				//digitSpirit.name = a+"-"+this.numOfColumns;
				rollTextInterior.addChild(comma);
				xPos = this.commaPositionAdjustment(xPos);
				comma.x = xPos;
				xPos+=this.commaWidthAdjustment(comma.width);
				
				}
				
				
				
				
				digitSpirit.addEventListener("addedToStage", this.move);
			}
			
			
			var txtBack:Sprite = new Sprite();
			//txtBack.x = poundTxt.width;
			txtBack.graphics.beginFill(0x000000, 0.1);
			txtBack.graphics.drawRect(0, 0, rollTextInterior.width, this.getMaskHeight());
			txtBack.graphics.endFill();
			rollTextInterior.addChild(txtBack);
			
			
			rollTextInterior.mask = txtBack;
			
			/*
			var tt:TextField = new TextField();
			txtBack.addChild(tt);
			tt.text = "tell me"
			
			
			new KeyboardControlTool(txtBack);
			*/
				
			return rollTextContainer;
		}
		
		private function getDigit(a:int, numberString:String):Sprite
		{
			var digitSpirit:Sprite = new Sprite();
			
			//digitSpirit.x = xPos;
			
			
			var tPos:int = 0;
			var h:int = 0;
			var fff:String = numberString.substr(a,1);
			var lengthOfNumberStrip:int = 40+(parseInt(fff)+1);
			
			
			
			for(var i:int = 0; i<lengthOfNumberStrip; i++)
			{
				
			
				
			var rollTxts:TextField = this.getCorrectTextField();
			digitSpirit.addChild(rollTxts);
			rollTxts.y = tPos+this.txtYOffset;
			rollTxts.htmlText = h.toString();
			rollTxts.width = this.getDigitWidth();//rollTxts.textWidth+5;
			rollTxts.x = 0;//poundTxt.width;
			//rollTxts.background = true;
			
			/*
			var tt:TextField = new TextField();
			tt.text = xPos+"";
			tt.width = 40;
			digitSpirit.addChild(tt);
			tt.y = (tPos-15);
			*/
			
			tPos += rollTxts.height;
			h++;
			if(h>9){h=0;}
			}
			
			
				
			//xPos+=digitSpirit.width-5;
			
			return digitSpirit;
		}
		
		private function getComma():Sprite
		{
			var digitSpirit:Sprite = new Sprite();
			var rollTxts:TextField = this.getCorrectTextField();
			//rollTxts.backgroundColor = 0xBBCC00;
			//rollTxts.background = true;
			digitSpirit.addChild(rollTxts);
			rollTxts.y = this.txtYOffset;
			rollTxts.htmlText = ",";
			rollTxts.width = this.getCommaWidth();
			
			return digitSpirit;
		}
		
		private function getPoundSign():Sprite
		{
			var digitSpirit:Sprite = new Sprite();
			var rollTxts:TextField = this.getCorrectTextField();
			//rollTxts.backgroundColor = 0xBB00BB;
			//rollTxts.background = true;
			digitSpirit.addChild(rollTxts);
			rollTxts.y = this.txtYOffset;
			rollTxts.htmlText = "&#163;";
			rollTxts.width = this.getPoundSignWidth();
			
			//lrgTxt.htmlText = "&#163;2,000,025";
			
			return digitSpirit;
		}
		
		private function getCorrectTextField():TextField
		{
			var tf:TextField;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = UiFormClass.getMoneyReportField();//getMajorUiTextField();
					tf.height = 19;
					/*var ffx:TextFormat = tf.getTextFormat();
					ffx.size = 18;
					tf.defaultTextFormat = ffx;*/
					//tf = this.getFinalScreenLargeText();
					//tf.height = 95;
					break;
				}
				
				default:
				{
					tf = this.getFinalScreenLargeText();
					tf.height = 95;
				}
			}
			
			return tf;
			
		}
		
		private function getCommaWidth():Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = 10;
					break;
				}
				
				default:
				{
					tf = 25;
				}
			}
			
			return tf;
			
		}
		
		private function getPoundSignWidth():Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = 10;
					break;
				}
				
				default:
				{
					tf = 38;
				}
			}
			
			return tf;
			
		}
		
		private function getDigitWidth():Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = 10;
					break;
				}
				
				default:
				{
					tf = 40;
				}
			}
			
			return tf;
			
		}
		
		private function poundSignWidthAdjustment(w:Number):Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = w-3;
					break;
				}
				
				default:
				{
					tf = w-8;
				}
			}
			
			return tf;
			
		}
		
		private function commaPositionAdjustment(w:Number):Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = w;
					break;
				}
				
				default:
				{
					tf = w-8;
				}
			}
			
			return tf;
			
		}
		
		private function commaWidthAdjustment(w:Number):Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = w-8;
					break;
				}
				
				default:
				{
					tf = w-8;
				}
			}
			
			return tf;
			
		}
		
		private function digitWidthAdjustment(w:Number):Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = w-3;
					break;
				}
				
				default:
				{
					tf = w-5
				}
			}
			
			return tf;
			
		}
		
		private function setTxtYOffset():Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = -3;
					break;
				}
				
				default:
				{
					tf = -15
				}
			}
			
			return tf;
			
		}
		
		private function getMaskHeight():Number
		{
			var tf:Number = 0;
			switch(this.textType)
			{
				case "majorui":
				{
					tf = 19;
					break;
				}
				
				default:
				{
					tf = 80;
				}
			}
			
			return tf;
			
		}
		
		private function move(evt:Event):void
		{
			GeneralAnimations.rollInText(evt.currentTarget as Sprite, this.rollComplete);
		}
		
		private function rollComplete():void
		{
			this.columnRollCount++;
			if(this.columnRollCount == this.numOfColumns)
			{
				this.onCompleteRollFunc();
			}
		}
	}
	
	
}