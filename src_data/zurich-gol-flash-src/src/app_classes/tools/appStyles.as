package src.app_classes.tools
{
	
	import flash.text.StyleSheet;
	import flash.events.Event;
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLLoaderDataFormat;
	
	public class appStyles extends StyleSheet
	{
		
		private var css_loader:URLLoader = new URLLoader();
		
		public function appStyles()
		{
			this.css_loader.addEventListener(Event.COMPLETE, this.createLayersx);
			this.css_loader.dataFormat = URLLoaderDataFormat.TEXT;
			this.css_loader.load(new URLRequest("my_style.css"));
			
			
			//this.setAppStyles();
		}
		
		private function createLayersx(evt:Event):void
		{
			this.parseCSS(evt.target.data);
		}
		
		private function setAppStyles():void
		{
			this.setStyle_Heading();
			this.setStyle_Div();
		}
		
		private function setStyle_Heading():void
		{
			var heading:Object = new Object();
            heading.fontWeight = "bold";
            heading.color = "#FF0000";
			
			this.setStyle(".heading", heading);
		}
		
		private function setStyle_Div():void
		{
			var div:Object = new Object();
            div.fontWeight = "bold";
            div.color = "#bb0000";
			
			
			this.setStyle("div", div);
		}
		
	}
}