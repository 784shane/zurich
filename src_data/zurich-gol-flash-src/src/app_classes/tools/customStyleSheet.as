package src.app_classes.tools
{
	
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import Config;
	
	public class customStyleSheet extends TextFormat
	{
		
		[Embed(source="../../../res/fonts/MouseMemoirs-Regular.ttf",
	    fontName = "MouseMemoirs-Regular",
	    mimeType = "application/x-font",
	    fontWeight="normal",
	    fontStyle="normal",
	    //unicodeRange="englishRange",
	    advancedAntiAliasing="true",
	    embedAsCFF="false")]
		private var myEmbeddedFont:Class;
		
		[Embed(source="../../../res/fonts/RobotoCondensed-Bold.ttf",
	    fontName = "RobotoCondensed-Bold",
	    mimeType = "application/x-font",
	    fontWeight="normal",
	    fontStyle="normal",
	    //unicodeRange="englishRange",
	    advancedAntiAliasing="true",
	    embedAsCFF="false")]
		private var myEmbeddedFontx:Class;
		
		[Embed(source="../../../res/fonts/RobotoCondensed-Regular.ttf",
	    fontName = "RobotoCondensed-Regular",
	    mimeType = "application/x-font",
	    fontWeight="normal",
	    fontStyle="normal",
	    //unicodeRange="englishRange",
	    advancedAntiAliasing="true",
	    embedAsCFF="false")]
		private var myEmbeddedFontxx:Class;
		
		protected static function formatTextBox(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "RobotoCondensed-Regular";
			tformat.color = 0x929292;
			tformat.size = 21;
			tformat.bold = true;
			return tformat;
		}
		
		protected static function formatTextLabel(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "RobotoCondensed-Regular";
			tformat.color = Config.APP_COLORS.DARK_BLUE;
			tformat.size = 21;
			tformat.align = "right";
			//TextFormatAlign.LEFT
			tformat.bold = true;
			return tformat;
		}
		
		protected static function gtActivityTextBoxStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "MouseMemoirs-Regular";
			tformat.color = Config.APP_COLORS.DARK_BLUE;
			tformat.size = 42;
			tformat.align = "center";
			//TextFormatAlign.LEFT
			tformat.bold = true;
			return tformat;
		}
		
		protected static function formatform_ButtonTextBox(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "RobotoCondensed-Bold";
			tformat.color = Config.APP_COLORS.DARK_BLUE;
			tformat.size = 19;
			tformat.align = "center";
			//TextFormatAlign.LEFT
			tformat.bold = true;
			return tformat;
		}
		
		protected static function formatLeaderboardTextStyle(tf:TextField, txtTypeName:String):TextFormat
		{
			tf.embedFonts = true;
			var tformat:TextFormat = tf.getTextFormat();
			tformat.color = Config.APP_COLORS.DARK_BLUE;//0x2e3777;
			tformat.size = 26;
			tformat.bold = true;
			switch(txtTypeName)
			{
				case "numText":
				{
					tformat.font = "MouseMemoirs-Regular";
					break;
				}
				
				case "CompNameText":
				case "moneyText":
				{
					tformat.font = "RobotoCondensed-Bold";
					break;
				}
				
				case "nameText":
				{
					tformat.font = "RobotoCondensed-Regular";
					break;
				}
				
				default:
				{
					tformat.font = "RobotoCondensed-Regular";
				}
			}
			
			return tformat;
		}
		
		protected static function largeNumbersTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "MouseMemoirs-Regular";
			tformat.color = Config.APP_COLORS.CARD_BLUE;
			//Config.APP_COLORS.REGULAR_GREEN;
			tformat.size = 90;
			tformat.bold = true;
			tformat.align = "center";
			return tformat;
		}
		
		protected static function newTitlebarStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "RobotoCondensed-Regular";
			tformat.color = 0xFFFFFF;//Config.APP_COLORS.REGULAR_GREEN;
			tformat.size = 14;
			tformat.bold = true;
			tformat.align = "center";
			return tformat;
		}
		
		protected static function formatTitleBarTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			//tformat.font = "RobotoCondensed-Regular";
			tformat.color = Config.APP_COLORS.CARD_BLUE;
			//Config.APP_COLORS.REGULAR_GREEN;
			tformat.size = 20;
			tformat.bold = true;
			tformat.align = "center";
			return tformat;
		}
		
		protected static function formatCardTextStyle
		(tf:TextField, getBold:Boolean=true):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			if(getBold)
			{
				tformat.font = "RobotoCondensed-Bold";
			}
			else
			{
				tformat.font = "RobotoCondensed-Regular";
			}
			tformat.color = Config.APP_COLORS.CARD_BLUE;
			//Config.APP_COLORS.REGULAR_GREEN;
			tformat.size = 20;
			tformat.bold = true;
			tformat.align = "center";
			return tformat;
		}
		
		protected static function formatFeedTextStyle(tf:TextField, boldIt:Boolean = false):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			if(boldIt)
			{
				tformat.font = "RobotoCondensed-Bold";
			}
			else
			{
				tformat.font = "RobotoCondensed-Regular";
			}
			tformat.color = Config.APP_COLORS.FONT_GREY;
			tformat.size = 14;
			return tformat;
		}
		
		protected static function htpParaTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "RobotoCondensed-Regular";
			tformat.color = Config.APP_COLORS.FONT_GREY;
			tformat.size = 14;
			return tformat;
		}
		
		protected static function formatFeedNameTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "RobotoCondensed-Bold";
			tformat.color = Config.APP_COLORS.FONT_GREY;
			tformat.size = 14;
			return tformat;
		}
		
		protected static function formatpopupTitleTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.font = "MouseMemoirs-Regular";
			tformat.color = Config.APP_COLORS.CARD_BLUE;
			//Config.APP_COLORS.REGULAR_GREEN;
			tformat.size = 46;
			//tformat.bold = true;
			tformat.align = "center";
			return tformat;
		}
		
		protected static function formatUiTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = tf.getTextFormat();
			tformat.color = 0xFFB629;
			tformat.size = 24;
			tformat.bold = true;
			tformat.align = TextFormatAlign.CENTER;
			return tformat;
		}
		
		protected static function formatMajorUiTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = formatUiTextStyle(tf);
			tformat.font = "MouseMemoirs-Regular";
			return tformat;
		}
		
		protected static function formatMinorUiTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = formatUiTextStyle(tf);
			tformat.font = "RobotoCondensed-Bold";//"RobotoCondensed-Regular";
			return tformat;
		}
		
		protected static function formatMoneyUiTextStyle(tf:TextField):TextFormat
		{
			var tformat:TextFormat = formatUiTextStyle(tf);
			tformat.font = "MouseMemoirs-Regular";
			return tformat;
		}
		
		protected function setStyle_Heading():void
		{
			var heading:Object = new Object();
            heading.fontWeight = "bold";
            heading.color = "#FF0000";
			
			//this.setStyle(".heading", heading);
		}
		
	}

}