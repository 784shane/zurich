package src.app_classes.tools
{
	
	///LoadingScreen used this class very effectively
	//If we are making future classes we could use this
	
	import src.app_classes.tools.PictureLoader;
	
	public class ClassImageLoader
	{
		
		private var externalResourcesToLoad:*;
		private var localResourcesSaved:Array = new Array();
		private var callBackUponSavedImages:Function;
		private var imagesLoadTally:int = 0;
		
		public function ClassImageLoader()
		{
			this.externalResourcesToLoad = new Array();
		}
		
		public function saveImages
		(
		callbackOnSavedFunc:Function, images:Array
		):void
		{
			
			this.callBackUponSavedImages = callbackOnSavedFunc;
			this.externalResourcesToLoad = images;
			
			for(var i:int = 0; i<this.externalResourcesToLoad.length; i++)
			{
				
				var pl:PictureLoader = new PictureLoader(this.resourcesLoaded);
				var img:String = this.externalResourcesToLoad[i].src;
				pl.load_image(this.externalResourcesToLoad[i].name,img,0);
				
			}
			
		}
		
		private function resourcesLoaded(imgsReturned:PictureLoader, imagesInfo:Object):void
		{
			
			var pp:Object = new Object();
			pp.imgName = imagesInfo.id;
			pp.imgObj = imgsReturned;
			this.localResourcesSaved.push(pp);
			
			if(this.imagesLoadTally == (this.externalResourcesToLoad.length-1))
			{
				this.callBackUponSavedImages(this.localResourcesSaved);
			}
			
			this.imagesLoadTally++;
		}
		
	}
}