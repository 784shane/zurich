﻿package src.app_classes.tools
{
	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import flash.text.TextField;
	import flash.display.Stage;
	
	import PlayerResources
	
	public class KeyboardControlTool
	{
		
		private var controlSprite:*;
		private var gg:Sprite;
		private var tf:TextField;
		private var mainStage:Stage;
		
		public function KeyboardControlTool(theSprite:*)
		{
			this.controlSprite = theSprite;
			this.mainStage = PlayerResources.mainStage;
			this.setUpControls();
		}
		
		private function setUpControls():void
		{
			
			this.controlSprite.addEventListener(KeyboardEvent.KEY_DOWN, kdown);
			this.controlSprite.buttonMode = true;
			
			this.tf = new TextField();
			this.mainStage.addChild(this.tf);
			
		}

		public function kdown(evt:KeyboardEvent):void
		{
			
		    if (evt.keyCode == Keyboard.UP)
		    {
				this.controlSprite.y+=-1;
				this.tf.text = "y - "+this.controlSprite.y;
		    }
		    if (evt.keyCode == Keyboard.DOWN)
		    {
				this.controlSprite.y+=1;
				this.tf.text = "y - "+this.controlSprite.y;
		    }
		    if (evt.keyCode == Keyboard.LEFT)
		    {
				this.controlSprite.x+=-1;
				this.tf.text = "x - "+this.controlSprite.x;
		    }
		    if (evt.keyCode == Keyboard.RIGHT)
		    {
				this.controlSprite.x+=1;
				this.tf.text = "x - "+this.controlSprite.x;
		    }
			
		}
		
		private function kup(evt:KeyboardEvent):void
		{
		    if (evt.keyCode == Keyboard.SPACE)
		    {
		
		        //stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		
		        //stage.removeEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		
		    }
		}
		
	}
}