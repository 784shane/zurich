package src.app_classes.tools
{
	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.text.TextField;
	
	import Config;
	import src.app_classes.forms.AppForms;
	import src.app_classes.tools.RollText;
	
	import src.app_classes.tools.KeyboardControlTool;
	
	public class RibbonText extends AppForms
	{
		
		[Embed (source="../../../res/totalScreen/ribbonLeft.png" )]
		private var ribbonLeft:Class;
		
		[Embed (source="../../../res/totalScreen/ribbonRight.png" )]
		private var ribbonRight:Class;
		
		public function RibbonText()
		{
			
		}
		
		public function getRibbonText(moneyStr:String):Sprite
		{
			var spr:Sprite = new Sprite();
			
			var ribbonTop:Sprite = getRibbonBar();
			spr.addChild(ribbonTop);
			ribbonTop.x = 46;
			
			var ribbonBottom:Sprite = getRibbonBar();
			spr.addChild(ribbonBottom);
			ribbonBottom.x = 46;
			
			ribbonBottom.y = 100;
			
			var ribbonBowRight:Sprite = getRibbonBow("right");
			spr.addChild(ribbonBowRight);
			ribbonBowRight.x = 457;
			
			var ribbonBowLeft:Sprite = getRibbonBow("left");
			spr.addChild(ribbonBowLeft);
			//ribbonBowLeft.x = 411;
			
			
			
			/*
			var lrgTxtSpr:Sprite = new Sprite();
			var lrgTxt:TextField = this.getFinalScreenLargeText();			
			lrgTxtSpr.addChild(lrgTxt);
			spr.addChild(lrgTxtSpr);
			lrgTxt.htmlText = "&#163;2,000,025";
			lrgTxtSpr.x=53;
			*/
			
			var rt:Sprite = new RollText().getRollText(moneyStr);
			spr.addChild(rt);
			
			//space between the ribbon is 404.
			rt.x=53+((404-rt.width)/2);
			rt.y=15;
			
			
			return spr;
		}
		
		public function getRibbonBow(dir:String = "right"):Sprite
		{
			var ribbonBow:Sprite = new Sprite();
			
			var ribbonBar:Sprite = new Sprite();
			ribbonBar.graphics.beginFill(Config.APP_COLORS.DARK_BLUE);
			ribbonBar.graphics.drawRect(0, 0, 7, 107);
			ribbonBar.graphics.endFill();
			
			ribbonBow.addChild(ribbonBar);
			
			
			var ribbonBowRight:Sprite = new Sprite();
			if(dir=="right")
			{
			ribbonBowRight.addChild(new ribbonRight() as Bitmap);
			}
			else
			{
			ribbonBowRight.addChild(new ribbonLeft() as Bitmap);
			}
			ribbonBow.addChild(ribbonBowRight);
			
			
			
			
			if(dir=="left")
			{
			ribbonBar.x=46;
			}
			
			if(dir=="right")
			{
			ribbonBowRight.x=10;
			}
			
			
			ribbonBowRight.y=7;
			
			
			
			return ribbonBow;
		}
		
		public function getRibbonBar():Sprite
		{
			var ribbonBar:Sprite = new Sprite();
			ribbonBar.graphics.beginFill(Config.APP_COLORS.DARK_BLUE);
			ribbonBar.graphics.drawRect(0, 0, 418, 7);
			ribbonBar.graphics.endFill();
			
			return ribbonBar;
		}
		
	}
}