package src.app_classes.tools
{
	
	public class Validation
	{
		
		public static function is_text_blank(txt:String):Boolean
		{
			var theReturn:Boolean = false;
			var blankRegExp:RegExp = /(^\s*$)/;
			if(blankRegExp.test(txt)){return true;}
			return theReturn;
		}
		
		public static function isValidEmail(txt:String):Boolean
		{	
			var theReturn:Boolean = true;
        
	        if (is_text_blank(txt))
	        {
	            theReturn = false;
	        }
	        
	        if(theReturn)
			{
				var emailExp:RegExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/
				if(!emailExp.test(txt)){return false;}
			}	
	        
			return theReturn;
		}
		
		
	}
	
}