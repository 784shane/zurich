package src.app_classes.tools
{
	
	public class ListenerDictionary
	{
		
		private var loginClickListener:Function;
		private var logoutClickListener:Function;
		
		public function setCLICKListener(type:String,clickListener:Function):void
		{
			switch(type)
			{
				
				case "login":
				{
					this.loginClickListener = clickListener;
					break;
				}
				
				case "logout":
				{
					this.logoutClickListener = clickListener;
					break;
				}
			
			}	
		}
		
		public function getCLICKListener(type:String):Function
		{
			switch(type)
			{
				
				case "login":
				{
					return this.loginClickListener;
				}
				
				case "logout":
				{
					return this.logoutClickListener;
				}
			
			}
			
			return new Function();	
		}
		
	}	
	
}