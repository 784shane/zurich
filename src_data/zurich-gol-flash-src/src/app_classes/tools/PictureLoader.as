package src.app_classes.tools
{
	
	import flash.display.Loader;
	import flash.display.Sprite;	
	import flash.system.LoaderContext;	
	import flash.net.URLRequest;
	import flash.events.Event;
	//import flash.text.TextField;

	public class PictureLoader extends Loader
	{
		private var theS:Sprite;
		//private var dbtxt:TextField; 
		private var img_str:String;
		//function to call after image is loaded
		private var loadReport:Function;
		public var id:String;
		public var zIndex:int;
		
		public function PictureLoader(lr:Function)
		{
			//this.dbtxt = dbtxt;
			this.loadReport = lr;
			//assign listener to find out when items are loaded.
			this.contentLoaderInfo.addEventListener(Event.COMPLETE, load2);
		}
		
		public function load_image(id:String,imageStr:String,zIndex:int=0):void
		{
			this.id = id;
			this.img_str = imageStr;
			var fileRequest:URLRequest = new URLRequest(imageStr);
			this.load(fileRequest);
		}
		
		public function load2(e:Event):void
		{
			var reportObj:Object = {};
			reportObj.id = this.id;
			reportObj.zIndex = this.zIndex;
			
			this.loadReport(this,reportObj);
			
		}
		
		/*
		override public function load(request:URLRequest, context:LoaderContext = null):void
		{
			super.load(request, context);
		}
		*/
	}

}