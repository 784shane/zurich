package src.app_classes.tools
{
	import PlayerResources;
	import flash.display.MovieClip;
	
	public class Pegs
	{
		
		private var my_sex:String;
		public var numberOfPegsAdded:int = 1;
		private var marriedPegsAdded:Boolean = false;
		private var carContainer:MovieClip;
		private var pegs_position:Array = 
		[
		{x:8, y:-5}, {x:-3, y:-5}, {x:-3, y:5},
		{x:8, y:5}, {x:-3, y:15}, {x:8, y:15}				
		];
		
		public function setCar(gamecar:MovieClip):void
		{
			this.carContainer = gamecar;
		}
		
		public function placeActorPeg(sex:String):void
		{
			this.numberOfPegsAdded= 1;
			this.my_sex = sex;
			var actorPeg:MovieClip = this.getPeg(sex);
			this.carContainer.addChild(actorPeg);
			actorPeg.name = "peg"+this.numberOfPegsAdded;
			actorPeg.x = pegs_position[0].x;
			actorPeg.y = pegs_position[0].y;
			
			
			this.addToPegResource
			(sex,pegs_position[0].x,pegs_position[0].y);
				
		}
		
		public function resetSavedPegs(pegs:String):void
		{
			
		}
		
		public function restoreSinglePeg(theSex:String, xPos:Number, yPos:Number):void
		{
			var thePeg:MovieClip = 
			this.getPeg(theSex);
			this.carContainer.addChild(thePeg);
			thePeg.name = "peg"+this.numberOfPegsAdded;
			thePeg.x = xPos;
			thePeg.y = yPos;
			
			thePeg.x = this.pegs_position[this.numberOfPegsAdded].x;
			thePeg.y = this.pegs_position[this.numberOfPegsAdded].y;
			
					
			//this.addToPegResource
			//(theSex,xPos,yPos);
					
			this.numberOfPegsAdded++;
		}
		
		public function restoreAllPegs(theSex:String, pos:int):void
		{
			var thePeg:MovieClip = 
			this.getPeg(theSex);
			this.carContainer.addChild(thePeg);
			thePeg.name = "peg"+this.numberOfPegsAdded;
			thePeg.x = this.pegs_position[pos].x;
			thePeg.y = this.pegs_position[pos].y;
					
			this.numberOfPegsAdded++;
		}
		
		public function placePeg(variety:String):void
		{
			switch(variety)
			{
				
				case "married":
				{
					
					var theSex:String = 
					this.getPartnerSex(this.my_sex);
					
					var partnerPeg:MovieClip = 
					this.getPeg(theSex);
					this.carContainer.addChild(partnerPeg);
					partnerPeg.name = "peg"+this.numberOfPegsAdded;
					partnerPeg.x = pegs_position[1].x;
					partnerPeg.y = pegs_position[1].y;
					
					this.addToPegResource
					(theSex,
					pegs_position[1].x,pegs_position[1].y);
					
					this.numberOfPegsAdded++;
					
					break;
				}
				
				case "baby_boy":
				{
					var baby_boyPeg:MovieClip = 
					this.getPeg("m");
					this.carContainer.addChild(baby_boyPeg);
					baby_boyPeg.name = "peg"+this.numberOfPegsAdded;
					baby_boyPeg.x = pegs_position[(this.numberOfPegsAdded)].x;
					baby_boyPeg.y = pegs_position[(this.numberOfPegsAdded)].y;
					
					this.addToPegResource
					("m",
					baby_boyPeg.x,baby_boyPeg.y);
					
					this.numberOfPegsAdded++;
					PlayerResources.numOfChildren = 
					PlayerResources.numOfChildren+1;
					
					break;
				}
				
				case "baby_girl":
				{
					var baby_girlPeg:MovieClip = 
					this.getPeg("f");
					this.carContainer.addChild(baby_girlPeg);
					baby_girlPeg.name = "peg"+this.numberOfPegsAdded;
					baby_girlPeg.x = pegs_position[(this.numberOfPegsAdded)].x;
					baby_girlPeg.y = pegs_position[(this.numberOfPegsAdded)].y;
					
					this.addToPegResource
					("f",
					baby_girlPeg.x,baby_girlPeg.y);
					
					this.numberOfPegsAdded++;
					PlayerResources.numOfChildren = 
					PlayerResources.numOfChildren+1;
					
					break;
				}
				
				case "twins":
				{
					
					var child1Peg:MovieClip = 
					this.getPeg("f");
					this.carContainer.addChild(child1Peg);
					child1Peg.name = "peg"+this.numberOfPegsAdded;
					child1Peg.x = pegs_position[(this.numberOfPegsAdded)].x;
					child1Peg.y = pegs_position[(this.numberOfPegsAdded)].y;
					
					this.addToPegResource
					("f",
					child1Peg.x,child1Peg.y);
					
					this.numberOfPegsAdded++;
					
					
					if(this.numberOfPegsAdded<6)
					{
					var child2Peg:MovieClip = 
					this.getPeg("m");
					this.carContainer.addChild(child2Peg);
					child2Peg.name = "peg"+this.numberOfPegsAdded;
					child2Peg.x = pegs_position[(this.numberOfPegsAdded)].x;
					child2Peg.y = pegs_position[(this.numberOfPegsAdded)].y;
					
					this.addToPegResource
					("m",
					child2Peg.x,child2Peg.y);
					
					this.numberOfPegsAdded++;
					}
					
					
					PlayerResources.numOfChildren = 
					PlayerResources.numOfChildren+2;
					
					break;
				}
				
				default:{}//this.tf.text = "peg";}
			}
			
			
			
			
			
		}
		
		private function addToPegResource
		(sex:String, xPos:int, yPos:int):void
		{
			
			if(numberOfPegsAdded<6)
			{
				PlayerResources.playerPegs+=
				"|"+sex+","+xPos+","+yPos
			}
		}
		
		private function getPeg(sex:String):MovieClip
		{
			if(sex=="m"){ return new blue_peg() as MovieClip; }
			if(sex=="f"){ return new pink_peg() as MovieClip; }
			
			return new blue_peg() as MovieClip;
			
		}
		
		private function getPartnerSex(sex:String):String
		{
			if(sex=="m"){ return "f"; }
			if(sex=="f"){ return "m"; }
			
			return "m";
			
		}
		
	}
}