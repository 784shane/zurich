package src.app_classes.tools
{
	
	import mx.collections.ArrayList;
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.text.TextField;
	
	import src.app_classes.tools.ListenerDictionary;
	
	public class Resources
	{
		//Location of Application
		public var positionLocal:Boolean = false;
		
		//TitlePanel Items
		public var lgnTextBoxSprite:Sprite;
		
		//Ui Items
		public var uiMajorText:TextField = null;
		public var uiMinorText:TextField = null;
		public var uiNameText:TextField = null;
		public var uiMoniesText:TextField = null;
		public var uiLifeText:TextField = null;
		
		public var prizesOnState:Sprite = null;
		public var htpOnState:Sprite = null;
		public var leadOnState:Sprite = null;
		public var loginOnState:Sprite = null;
		
		//The two screen major Text Field
		public var TwoCardScreenMajorText:TextField = null;
		
		public var twoCardScreenBox:Sprite = null;
		
		public var threeCardScreenBox:Sprite = null;
		
		public var mapViewMC:MovieClip = null;
		
		//Listener Dictionary
		public var listenerDictionary:ListenerDictionary = new ListenerDictionary();;
		
		//ListenerDictionary
		
		public static function main_loading_resources():ArrayList
		{
			var loadingResourceList:ArrayList = new ArrayList();
			
			loadingResourceList.addItem(
			{res:"resources/images/Zurich_GOL_001.jpg",type:"image"}
			);
			
			return loadingResourceList;
			
		}
		
	}
}