package src.app_classes.tools
{
	
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.events.MouseEvent;
	
	
	import src.app_classes.forms.AppForms;
	import src.app_classes.forms.LoginForm;
	
	
	
	public class CompanySortCtrl extends AppForms
	{
		
		private var companyDropDown:Sprite;
		public var highlightedFound:Boolean = false;
		public var selectionMade:Boolean = false;
		public var highlitedPosition:int = 0;
		private var lf:LoginForm;
		
		public function CompanySortCtrl(lf:LoginForm)
		{
			this.lf = lf;
		}
		
		public function populateCompanyDropDown(wordList:Array):void
		{
			this.selectionMade = false;
			this.highlightedFound = false;
			
			if(this.companyDropDown.numChildren>0)
			{
				this.companyDropDown
				.removeChild(this.companyDropDown.getChildAt(0));
			}
			
			var cddInsert:Sprite = new Sprite();
			this.companyDropDown.addChild(cddInsert);
			
			var compTextPos:Number = 0;
			var compTab:int = 10;
			for(var cmpTxIndex:int = 0; cmpTxIndex<wordList.length; cmpTxIndex++)
			{
				var compTxt:MovieClip = _getTextField(compTab,false);
				cddInsert.addChild(compTxt);
				compTxt.y = compTextPos;
				compTxt.buttonMode = true;
				compTxt.mouseChildren = false;
				
				compTxt.addEventListener(MouseEvent.MOUSE_OVER, this.mousedOverCompDD);
				compTxt.addEventListener(MouseEvent.MOUSE_OUT, this.mousedOverCompDD);
				compTxt.addEventListener(MouseEvent.CLICK, this.mousedOverCompDD);
				
				(compTxt
				.getChildByName("field_txtBox") as TextField).text = 
				wordList[cmpTxIndex];
				
				compTextPos+=compTxt.height;
				compTab++;
			}
			
		}
		
		public function mousedOverCompDD(evt:MouseEvent):void
		{
			
			switch(evt.type)
			{
				case "mouseOver":
				{	
					(evt.currentTarget.getChildByName("selectable") as Sprite)
					.alpha = 1;	
					break;
				}
				
				case "mouseOut":
				{
					(evt.currentTarget.getChildByName("selectable") as Sprite)
					.alpha = 0;
					break;
				}
				
				case "click":
				{
					
					this.lf.assignCompanyAndCloseDD
					((evt.currentTarget.getChildByName("field_txtBox") as TextField)
					.text);
					
					break;
				}
				
			}
		}
		
		public function getCompanyDropDown():Sprite
		{
			this.companyDropDown = new Sprite();
			
			
			return this.companyDropDown;
			
		}
		
		public function clearCompanyDropDown():void
		{
			this.companyDropDown.removeChildAt(0);
		}
		
		public function positionMouseOverHighlight(dir:String):void
		{
			
			this.selectionMade = false;
			this.highlightedFound = false;
			this.highlitedPosition = 0;
			var numOfRows:int = 
			(this.companyDropDown.getChildAt(0) as Sprite).numChildren;
			
			//Find position of the mousedOver Color
			for(var i:int = 0; i<numOfRows; i++)
			{
				/*
				((this.companyDropDown.getChildAt(0) as Sprite)
				.getChildAt(i) as Sprite)
				.getChildByName("mOver").alpha = 1;
				*/
				
				if(((this.companyDropDown.getChildAt(0) as Sprite)
				.getChildAt(i) as Sprite)
				.getChildByName("selectable").alpha == 1)
				{
					this.highlightedFound = true;
					this.highlitedPosition = i;
				}
				
				/*
				//mOver
				((this.companyDropDown.getChildAt(0) as Sprite)
				.getChildAt(i) as Sprite)
				.getChildByName("selectable").alpha = 1;
				*/
			}
			
			if(this.companyDropDown.numChildren>0)
			{
				
							this.selectionMade = true;
							
				switch(dir)
				{
					case "up":
					{
						if(!this.highlightedFound)
						{
							((this.companyDropDown.getChildAt(0) as Sprite)
							.getChildAt(numOfRows-1) as Sprite)
							.getChildByName("selectable").alpha = 1;
						}
						else if(this.highlitedPosition>-1)
						{
							
							((this.companyDropDown.getChildAt(0) as Sprite)
							.getChildAt(this.highlitedPosition) as Sprite)
							.getChildByName("selectable").alpha = 0;
							
							((this.companyDropDown.getChildAt(0) as Sprite)
							.getChildAt(this.highlitedPosition-1) as Sprite)
							.getChildByName("selectable").alpha = 1;
						}
						//this.companyDropDown.getChildAt(0);
						break;
					}
					
					case "down":
					{
						if(!this.highlightedFound)
						{
							((this.companyDropDown.getChildAt(0) as Sprite)
							.getChildAt(0) as Sprite)
							.getChildByName("selectable").alpha = 1;
						}
						else if(this.highlitedPosition<numOfRows)
						{
							((this.companyDropDown.getChildAt(0) as Sprite)
							.getChildAt(this.highlitedPosition) as Sprite)
							.getChildByName("selectable").alpha = 0;
							
							((this.companyDropDown.getChildAt(0) as Sprite)
							.getChildAt(this.highlitedPosition+1) as Sprite)
							.getChildByName("selectable").alpha = 1;
						}
						//this.companyDropDown.getChildAt(0);
						break;
					}
				}
			}	
			
		}
		
		public function getHighlightedCompanyName():String
		{
			
			var selectableName:String = "";
			var numOfRows:int = 
			(this.companyDropDown.getChildAt(0) as Sprite).numChildren;
			
			for(var i:int = 0; i<numOfRows; i++)
			{
				if(((this.companyDropDown.getChildAt(0) as Sprite)
				.getChildAt(i) as Sprite)
				.getChildByName("selectable").alpha == 1)
				{
					
					selectableName = (((this.companyDropDown.getChildAt(0) as Sprite)
				.getChildAt(i) as Sprite)
				.getChildByName("field_txtBox") as TextField).text;
				
				break;
					//this.highlightedFound = true;
					//this.highlitedPosition = i;
					
					/*
					var compTxt:MovieClip = _getTextField(compTab,false);
				cddInsert.addChild(compTxt);
				compTxt.y = compTextPos;
				
				(compTxt
				.getChildByName("field_txtBox") as TextField).text = 
				wordList[cmpTxIndex];
					*/
				}
				
			}
			
			
			//this.highlitedPosition
			return selectableName;
		}
		
		
	}
	
}