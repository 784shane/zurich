<?php
namespace Album\Model\GameState\SaveGame;

use Zend\Db\Sql\Sql;

class SaveGameHelper
{

	protected $adapter;

	public function __construct($adapt)
	{
		$this->adapter = $adapt;
	}

	public function adjustCompanysScore($uid, $todaysDate, $score, $oldScore)
	{
		
		$date_updated_score = $todaysDate;
		$cScore = 0;
		$companyId = $this->getCompanyFromUserId($uid);
		
		//Get the Company id
		$selectSql = new Sql($this->adapter);
		$select = $selectSql->select();
		$select->from('company');
		$select->columns(array(
				'cScore' => 'company_score',
				'date_updated_score' => 'date_updated_score'
		));
		$predicate = $select->where;
		$predicate->equalTo('company.id',$companyId);
		$statement = $selectSql->prepareStatementForSqlObject($select);
		$selectResult = $statement->execute();
		
		
		if(count($selectResult)>0)
		{
			foreach($selectResult as $res)
			{
				$cScore = $res['cScore'];
				//We need to see when last this score was updated.
				//If the date is today then we need to remove the last
				//updated score and add the new score instead.
				$date_updated_score = $res['date_updated_score'];
				break;
			}
		
		}
		
		//Only if the date when the last score update was made are we going
		//to change subtract our last score added today before we add this
		//user's score.
		if($date_updated_score == $todaysDate)
		{
			//As we have gotten the old score that was saved, we need to subtract this new
			//score from the saved score. The next step is to add the score for the day
			//that is greater than the previous saved score
			$diff = (float)$cScore - (float)$oldScore;
			
			//We now add our new score in to get the company's new score
			$newScore = $diff+$score;
		}
		else
		//Else, we only will add this current score here. 
		//Now this is only done if the score wasn't updated today
		{
			$newScore = (float)$cScore + (float)$score;
		}
		
		$updateSql = new Sql($this->adapter);
		$update = $updateSql->update('company');
		$update->set(array(
				'company_score' => $newScore,
				'date_updated_score' => $todaysDate
				));
		
		//set the where predicate here so that the query knows which column to update
		$predicate = $update->where;
		$predicate->equalTo('company.id',$companyId);
		
		//Prepare the statement now
		$statement = $updateSql->prepareStatementForSqlObject($update);
		//Execute the statement
		$results = $statement->execute();
		
		
	}

	public function updateDayBook($uid, $score, $todaysDate, $oneDayAhead)
	{
		
		$companyId = $this->getCompanyFromUserId($uid);
		
		
		$sql = new Sql($this->adapter);
		$update = $sql->update();
		$update->table("company_day_book");
		$update->set(array('todays_highest_score' => $score));
		
		//set the where predicate here so that the query knows which column to update
		$predicate = $update->where;
		$predicate->equalTo('company_day_book.company_id',$companyId);
		$predicate->greaterThanOrEqualTo('company_day_book.day_book_entry_date',$todaysDate);
		$predicate->lessThanOrEqualTo('company_day_book.day_book_entry_date',$oneDayAhead);
		
		//Prepare the statement now
		$statement = $sql->prepareStatementForSqlObject($update);
		//Execute the statement
		$results = $statement->execute();
		
	}

	public function enterDayBook($uid, $score, $todaysDate, $oneDayAhead)
	{
		
		//As long as there is not an entry made today, we will enter
		//one here for this particular company

		$companyId = $this->getCompanyFromUserId($uid);
		
		$sql = new Sql($this->adapter);
		$insert = $sql->insert();
		$insert->into('company_day_book');
		$insert->values(array(
				'todays_highest_score' => $score,
				'company_id' => $companyId
		));
		
		$statement = $sql->prepareStatementForSqlObject($insert);
		$results = $statement->execute();
		
	}
	
	private function getCompanyFromUserId($uid)
	{
		
		$companyId = null;
		
		//Get the Company id
		$selectSql = new Sql($this->adapter);
		$select = $selectSql->select();
		$select->from('user');
		$select->columns(array(
				'comp' => 'company'
		));
		$predicate = $select->where;
		$predicate->equalTo('user.id',$uid);
		$statement = $selectSql->prepareStatementForSqlObject($select);
		$selectResult = $statement->execute();
		
		
		if(count($selectResult)>0)
		{
			foreach($selectResult as $res)
			{
				$companyId = $res['comp'];
				break;
			}
		
		}
		
		return $companyId;
		
	}
    
}