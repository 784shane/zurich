<?php
namespace Album\Model\GameState;

use Zend\Db\Sql\Sql;
use Album\Model\GameState\SaveGame\SaveGameHelper;

class GameState
{

	protected $adapter;

	public function __construct($adpt)
	{
		$this->adapter = $adpt;
	}
    
    public function saveGame($theParams)
    {
    	
    	//Assigned as default. These are for testing and
    	//may never make it into the database.
    	$uid = 1;
    	$car_square_postion=12;
    	$cash_in_hand=300000;
    	$life_cards=4;
    	$car_color="PURPLE";
    	$avatar=1;
    	$pegs="";
    	$wheel_last_spin=NULL;
    	$wheel_last_rotation=NULL;
    	$needle_last_rotation=NULL;
    	$game_completed=0;
		
		$sex="m";
		$salary=0;
		$maximum_salary=0;
		$salary_taxes=0;
		$house_buying_price=0;
		$house_selling_price=0;
		$house_insurance=0;
		$lifePath="GRADUATE";
		$numOfChildren=0;
		$job="";
		$wealth_cards="";
    	
    	


    	if($theParams->fromPost('uid'))
    	{
    		$uid=$theParams->fromPost('uid');
    	}
    	 
    	if($theParams->fromPost('car_square_postion'))
    	{
    		$car_square_postion=$theParams->fromPost('car_square_postion');
    	}
    	 
    	if($theParams->fromPost('cash_in_hand'))
    	{
    		$cash_in_hand=$theParams->fromPost('cash_in_hand');
    	}
    	 
    	if($theParams->fromPost('life_cards'))
    	{
    		$life_cards=$theParams->fromPost('life_cards');
    	}
    	 
    	if($theParams->fromPost('car_color'))
    	{
    		$car_color=$theParams->fromPost('car_color');
    	}
    	 
    	if($theParams->fromPost('avatar'))
    	{
    		$avatar=$theParams->fromPost('avatar');
    	}
    	 
    	if($theParams->fromPost('wheel_last_spin'))
    	{
    		$wheel_last_spin=$theParams->fromPost('wheel_last_spin');
    	}
    	 
    	if($theParams->fromPost('wheel_last_rotation'))
    	{
    		$wheel_last_rotation=$theParams->fromPost('wheel_last_rotation');
    	}
    	 
    	if($theParams->fromPost('needle_last_rotation'))
    	{
    		$needle_last_rotation=$theParams->fromPost('needle_last_rotation');
    	}
    	 
    	if($theParams->fromPost('pegs'))
    	{
    		$pegs=$theParams->fromPost('pegs');
    	}
    	 
    	if($theParams->fromPost('game_completed'))
    	{
    		$game_completed=$theParams->fromPost('game_completed');
    	}
    	 
    	if($theParams->fromPost('sex'))
    	{
    		$sex=$theParams->fromPost('sex');
    	}
    	 
    	if($theParams->fromPost('salary'))
    	{
    		$salary=$theParams->fromPost('salary');
    	}
    	 
    	if($theParams->fromPost('maximum_salary'))
    	{
    		$maximum_salary=$theParams->fromPost('maximum_salary');
    	}
    	 
    	if($theParams->fromPost('salary_taxes'))
    	{
    		$salary_taxes=$theParams->fromPost('salary_taxes');
    	}
    	 
    	if($theParams->fromPost('house_buying_price'))
    	{
    		$house_buying_price=$theParams->fromPost('house_buying_price');
    	}
    	 
    	if($theParams->fromPost('house_selling_price'))
    	{
    		$house_selling_price=$theParams->fromPost('house_selling_price');
    	}
    	 
    	if($theParams->fromPost('house_insurance'))
    	{
    		$house_insurance=$theParams->fromPost('house_insurance');
    	}
    	 
    	if($theParams->fromPost('lifePath'))
    	{
    		$lifePath=$theParams->fromPost('lifePath');
    	}
    	 
    	if($theParams->fromPost('numOfChildren'))
    	{
    		$numOfChildren=$theParams->fromPost('numOfChildren');
    	}
    	 
    	if($theParams->fromPost('job'))
    	{
    		$job=$theParams->fromPost('job');
    	}
    	 
    	if($theParams->fromPost('wealth_cards'))
    	{
    		$wealth_cards=$theParams->fromPost('wealth_cards');
    	}
    	
    	
    	$this->saveData
			    (
			    		$uid,
			    		$car_square_postion,
			    		$cash_in_hand,
			    		$life_cards,
			    		$car_color,
			    		$avatar,
			    		$pegs,
			    		$wheel_last_spin,
			    		$wheel_last_rotation,
			    		$needle_last_rotation,
			    		$game_completed,
						$sex,
						$salary,
						$maximum_salary,
						$salary_taxes,
						$house_buying_price,
						$house_selling_price,
						$house_insurance,
						$lifePath,
						$numOfChildren,
			    		$job,
			    		$wealth_cards
			    );
    	
    	//$gameStateModel = new SaveGameHelper($this->adapter);
    }
    
    private function saveData
    (
    		$uid,
			$car_square_postion,
			$cash_in_hand,
			$life_cards,
			$car_color,
			$avatar,
			$pegs,
			$wheel_last_spin,
			$wheel_last_rotation,
			$needle_last_rotation,
			$game_completed,
			$sex,
			$salary,
			$maximum_salary,
			$salary_taxes,
			$house_buying_price,
			$house_selling_price,
			$house_insurance,
			$lifePath,
			$numOfChildren,
			$job,
    		$wealth_cards
    )
    {
    	
    	$gameStateModel = new SaveGameHelper($this->adapter);
    	
    	//Before saving this ga,e we need to know which company this user is
    	//with. So we will get the company from the database here
    	
    		$earlierAmtFound = false;
    		$earlierSavedAmout = 0;
    		$todaysDate = date("Y-m-d");
    		$oneDayAhead = date('Y-m-d', strtotime($todaysDate . ' + 1 day'));
    		
    			
    		$selectSql = new Sql($this->adapter);
    		$select = $selectSql->select();
    		$select->from('user');
    		$select->columns(array(
    				'comp' => 'company'
    		));
    		
    		$select->join('company_day_book', 'user.company = company_day_book.company_id',
    				array(
    						'todays_highest_score',
    						'day_book_entry_date')
    		);
    		
    		$select->join('company', 'user.company = company.id',
    				array(
    						'company_score',
    						'date_updated_score')
    		);
    		
    		$predicate = $select->where;
    		$predicate->equalTo('user.id',$uid);
    		$predicate->greaterThanOrEqualTo('company_day_book.day_book_entry_date',$todaysDate);
    		$predicate->lessThanOrEqualTo('company_day_book.day_book_entry_date',$oneDayAhead);
    		$statement = $selectSql->prepareStatementForSqlObject($select);
    		$selectResult = $statement->execute();
    		
			
    		
    		if(count($selectResult)>0)
    		{
	    		foreach($selectResult as $res)
	    		{
	    			$earlierAmtFound = true;
	    			$earlierSavedAmout = $res['todays_highest_score'];
	    			break;
	    		}
    		
    		}
    		
    		
    		
    		//If there is a result, this means that earlier today, a score was entered
    		//for that company. If that's the case then we are to see if the entry
    		//is greater or less than the entry this period.
    		if($earlierSavedAmout<$cash_in_hand&& $game_completed == 1)
    		{
    			//note: If we found no result when we looked earlier, remember that $earlierSavedAmout
    			//was set at 0. So that means that we should make an entry since cash entering now
    			//will be greater than what was saved.
    			//echo "earlier saved amount is less";
    			
    			if($earlierAmtFound)
    			{
    				//Earlier amount was found so we need to update this in the company_day_book
    				//table
    				$gameStateModel->updateDayBook($uid, $cash_in_hand,$todaysDate,$oneDayAhead);
    			}
    			else
    			{
    				//Earlier amount was NOT found so we need to inset this entry 
    				//into the company_day_book table.
    				//We are inserting this since the 
    				$gameStateModel->enterDayBook($uid, $cash_in_hand,$todaysDate,$oneDayAhead);
    			}
    			
    			//This is called once. We will save update the company's score here
    			$gameStateModel->adjustCompanysScore($uid, $todaysDate, $cash_in_hand, $earlierSavedAmout);
    		}
    		

    		
    	try {
    		
    		
    		//We need to make sure that all other games are not
    		//allowed to be played again
    			 
    			$updateSql = new Sql($this->adapter);
    			$update = $updateSql->update();
    			//$update->from('game_saves');
    			$update->table("game_saves");
    			$update->set(array('game_completed' => 3));
    			$update_predicate = $update->where;
    		
    			$update_predicate->equalTo('user',$uid);
    			$update_predicate->equalTo('game_completed',0);
    			$update_statement = $updateSql->prepareStatementForSqlObject($update);
    			$updateResult = $update_statement->execute();
    			 
    		
    		
    		
    		
	    	$insertSql = new Sql($this->adapter);
	    	 
	    	$insert = $insertSql->insert();
	    	$insert->into('game_saves');
	    	$insert->values(array(
	    			"user"=>$uid,
	    			"car_square_postion"=>$car_square_postion,
	    			"cash_in_hand"=>$cash_in_hand,
	    			"life_cards"=>$life_cards,
	    			"car_color"=>$car_color,
	    			"avatar"=>$avatar,
	    			"pegs"=>$pegs,
	    			"wheel_last_spin"=>$wheel_last_spin,
	    			"wheel_last_rotation"=>$wheel_last_rotation,
	    			"needle_last_rotation"=>$needle_last_rotation,
	    			"game_completed"=>$game_completed,
	    			"sex"=>$sex,
	    			"salary"=>$salary,
	    			"maximum_salary"=>$maximum_salary,
	    			"salary_taxes"=>$salary_taxes,
	    			"house_buying_price"=>$house_buying_price,
	    			"house_selling_price"=>$house_selling_price,
	    			"house_insurance"=>$house_insurance,
	    			"lifePath"=>$lifePath,
	    			"numOfChildren"=>$numOfChildren,
	    			"job"=>$job,
	    			"wealth_cards"=>$wealth_cards
	    	));
			
			//echo $insert->__toString();
			
			//echo $insert->getSqlString($this->adapter->getPlatform());
			//exit;
			
			
			
			
	    	$statement = $insertSql->prepareStatementForSqlObject($insert);
	    	$results = $statement->execute();
	    	
	    	
	    	
			
			//print_r($results);
			
		}catch(Exception $e)
		{
			//echo $e->message;
		}	
			
	    
	    	
    }
    
    public function retrieveSavedGame($userId = 1)
    {
    	
    	$sql = new Sql($this->adapter);
    	$select = $sql->select();
    	$select->from('game_saves');
    	$select->join('user', 'user.id = game_saves.user', 
    			array(
    			'first_name',
    			'last_name')
    	);
    	$select->order('game_saves.id DESC');
    	$select->limit(1);
    	$predicate = $select->where;
    	$predicate->equalTo('game_saves.user',$userId);
    	$predicate->equalTo('game_saves.game_completed',0);
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
    	
    	
    	
    	$savedGameMessage = array("savedGame"=>array("msg"=>"NO_SAVED_GAMES", "data"=>array()));
    	
    	
    	if(count($results)>0)
    	{
    	
    		$savedGameData = array();
    		foreach($results as $res)
    		{
    			
	    		$savedGameData = array(
	    		//"user" => $res['user'],
	    		"car_square_postion" => $res['car_square_postion'],
	    		"cash_in_hand" => $res['cash_in_hand'],
	    		"life_cards" => $res['life_cards'],
	    		"car_color" => $res['car_color'],
	    		"avatar" => $res['avatar'],
	    		"wheel_last_spin" => $res['wheel_last_spin'],
	    		"wheel_last_rotation" => $res['wheel_last_rotation'],
	    		"needle_last_rotation" => $res['needle_last_rotation'],
	    		"sex" => $res['sex'],
	    		"salary" => $res['salary'],
	    		"maximum_salary" => $res['maximum_salary'],
	    		"salary_taxes" => $res['salary_taxes'],
	    		"house_buying_price" => $res['house_buying_price'],
	    		"house_selling_price" => $res['house_selling_price'],
	    		"house_insurance" => $res['house_insurance'],
	    		"lifePath" => $res['lifePath'],
	    		"numOfChildren" => $res['numOfChildren'],
	    		"job" => $res['job'],
	    		"pegs" => $res['pegs'],
	    		"wealth_cards" => $res['wealth_cards']
	    		//"first_name" => $res['first_name'],
	    		//"last_name" => $res['last_name']
	    		);
    			
	    		
	    		break;
	    		
	    		
    		}
    		
    		$savedGameMessage = array(
    				"savedGame"=>array("msg"=>"SUCCESS", "data"=>$savedGameData)
    				);
    		 
    	}
    	
    	
    	return $savedGameMessage;
	    	
    }
    
    public function saveToFeed($theParams)
    {
    	
    	$uid = 1;
    	$feedMsg = "xxx";
    	$avatar = 1;
    	
    	if($theParams->fromPost('uid'))
    	{
    		$uid=$theParams->fromPost('uid');
    	}
    	
    	if($theParams->fromPost('avatar'))
    	{
    		$avatar=$theParams->fromPost('avatar');
    	}
    	
    	if($theParams->fromPost('feedTxt'))
    	{
    		$feedMsg=$theParams->fromPost('feedTxt');
    	}
    	
    	
    	$firstName = "";
    	
    	$sql = new Sql($this->adapter);
    	$select = $sql->select();
    	$select->from('user');
    	$select->columns(array(
    			'first_name' => 'first_name'
    	));
    	$predicate = $select->where;
    	$predicate->equalTo('user.id', $uid);
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
    	
    	if(count($results)>0)
    	{
    		
    		foreach($results as $res)
    		{
    			$firstName = $res['first_name'];
    			break; 
    		}
    		 
    	}
    	
    	
    	$feedMsg = $firstName." ".$feedMsg;
    	
    	
    	
    	$sql = new Sql($this->adapter);
    	$insert = $sql->insert();
    	$insert->into('feed');
    	$insert->values(array(
    			'uid' => $uid,
    			'feed_msg' => $feedMsg,
    			'feed_avatar' => $avatar
    	));
    	
    	$statement = $sql->prepareStatementForSqlObject($insert);
    	$results = $statement->execute();
    	
    }
    
    public function getFeed($theParams)
    {
    	
    		$uid=1;
    		
		    if($theParams->fromPost('uid'))
		    {
		    	$uid=$theParams->fromPost('uid');
		    }
		    
    		$selectSql = new Sql($this->adapter);
    		$select = $selectSql->select();
    		$select->from('feed');
    		$select->columns(array(
    				'feed_avatar' => 'feed_avatar',
    				'feed_msg' => 'feed_msg',
    				'uid' => 'uid'
    		));
    		
	    	$select->join('user', 'user.id = feed.uid', 
	    			array(
	    			'first_name',
	    			'last_name',
	    			'company')
	    	);
	    	
	    	$select->join('company', 'user.company = company.id',
	    			array('company_name')
	    	);
	    	
	    	$predicate = $select->where;
	    	$predicate->notEqualTo('feed.uid', $uid);

    		$select->order('feed.id DESC');
    		//$select->limit(3);
    		
    		$statement = $selectSql->prepareStatementForSqlObject($select);
    		$selectResult = $statement->execute();
    		
    		
    		$feedData = 
    		array("feed_data"=>array("msg"=>"NO_FEED_AVAILABLE", "data"=>array()));
    		
    		
    		
    		
    		
    		
    		if(count($selectResult)>0)
    		{
    			
	    		foreach($selectResult as $res)
	    		{
	    			
	    			$feedRows[] = array(
	    					"uid" => $res['uid'],
	    					"feed_msg" => $res['feed_msg'],
	    					"feed_avatar" => $res['feed_avatar'],
	    					"first_name" => $res['first_name'],
	    					"last_name" => $res['last_name'],
	    					"company" => $res['company_name']
	    			);
	    			
	    		}
	    		

	    		$feedData = array(
	    				"feed_data"=>array("msg"=>"SUCCESS", "data"=>$feedRows)
	    		);
    		
    		};
    		
    		
    		echo json_encode($feedData);
    		
    		
    		
    		
    		
    }
    
    public function getCompanies()
    {
    		$selectSql = new Sql($this->adapter);
    		$select = $selectSql->select();
    		$select->from('company');
    		$select->columns(array(
    				'company_name' => 'company_name',
    				'id' => 'id'
    		));
    		

    		$select->order('company.company_name ASC');
    		//$select->limit(3);
    		
    		$statement = $selectSql->prepareStatementForSqlObject($select);
    		$selectResult = $statement->execute();
    		
    		
    		$companiesData = 
    		array("companies_data"=>array("msg"=>"NO_COMPANIES_AVAILABLE", "data"=>array()));
    		
    		
    		
    		
    		
    		
    		if(count($selectResult)>0)
    		{
	    		foreach($selectResult as $res)
	    		{
	    			$companiesData["companies_data"]["data"][] = 
	    			array("id"=>$res["id"],
	    					"name"=>$res["company_name"]);
	    		}
	    		
	    		$companiesData["companies_data"]["msg"] = 
	    		"AVAILABLE_COMPANIES";
    		}
    		
    		echo json_encode($companiesData);
    		
    }
    
    public function update_index()
    {
		//Get the time saved in the text file. If its more than 30 mins
		//since our last scape, we will scrape the wrapper now.
		$timeFile = "public/last_updated.txt";
		$timeNow = strtotime(date("Y-m-d H:i:s"));
		$last_updated_time = file_get_contents($timeFile);
		
		echo (($timeNow - $last_updated_time)/60);
		
		if((($timeNow - $last_updated_time)/60) > 20):
		
		$fp = fopen($timeFile, 'w');
		fwrite($fp, $timeNow);
		fclose($fp);


		echo "time to update"; exit;
		endif;
		echo "no its not time as yet.";
		exit;
		
		
		//echo (1383898871 - 1383898542);
		//1383898871
		//echo mktime("February 14th 2014");
		//1383898542
		//echo strtotime(date("Y-m-d H:i:s"));
		//print(file_get_contents("public/last_updated.txt"));
		//echo realpath("");
		//include "public/test.html";
		echo "we are testing here!";
	}
    
}