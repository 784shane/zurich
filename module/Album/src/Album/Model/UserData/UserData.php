<?php
namespace Album\Model\UserData;

use Zend\Db\Sql\Sql;
use Album\Model\UserData\LoginActions;
use Album\Model\GameState\GameState;

class UserData
{

	protected $adapter;

	public function __construct($adpt)
	{
		$this->adapter = 
		$adpt;
	}
    
    public function yy()
    {
    	
    	$sql = new Sql($this->adapter);
    	$select = $sql->select();
    	$select->from('company');
    	$select->where(array('id' => 1));
    	 
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
    	 
    	foreach($results as $res)
    	{
    		
    	}
    	
        return "yep";
    }
    
    public function attemptLogin($theParams)
    {
    	
    	$email = "";
    	$password = "";
    	
    	if($theParams->fromPost('em'))
    	{
    		$email=$theParams->fromPost('em');
    	}
    	
    	if($theParams->fromPost('pw'))
    	{
    		$password=$theParams->fromPost('pw');
    	}
    	
    	
    	
    	
    	
    	//Do nothing if there are blank email addresses and passwords
    	if($email==""&&$password=="")
    	{
    	$loginMsg = array("msg"=>"USER_NOT_FOUND");
    	$returnedData = array("msg"=>$loginMsg['msg']);
    	}
    	//As long as we have an email address or a password then we will
    	//check the database.
    	else
    	{
    		$loginActions = new LoginActions($this->adapter);
    		$loginMsg = $loginActions->userExists($email,$password);
    		 
    		$returnedData = array("msg"=>$loginMsg['msg']);
    	}
    	
    	
    	
    	
    	//Only if user was logged in successfully will we check the database for
    	//saved games.
    	if($loginMsg['msg'] == "USER_SUCCESSFULLY_LOGGED_IN")
    	{
    		//load GameStateModel
    		$gameStateModel = new GameState($this->adapter);
    		//$gameStateModel->retrieveSavedGame();
    		
    		$returnedData['data'] = array("user"=>$loginMsg['user'],
	    				  "first_name"=>$loginMsg['first_name'],
	    				  "last_name"=>$loginMsg['last_name']);
    		
    		//Merging saved Game with login result data if
    		//there's a saved game
    		$returnedData = 
    		array_merge($returnedData,$gameStateModel->retrieveSavedGame($loginMsg['user']));
    		
    	}
    	
    	echo json_encode($returnedData);
    	
    }
    
    public function postData($f_name="", 
    		$l_name="", $company_name="", 
    		$email="", $postCode="", $password="")
    {
    	/*
    	$f_name="lala";
    	$l_name="lla";
    	$company_name=1;
    	$email="aaa";
    	$postCode="aa";
    	$password="aa";
    	*/
    	
    	
    	
    	
    	$lastId = 0;
    	$company_id = 0;
    	
    	//Before insering, lets check to see if the User exists.
    	$loginActions = new LoginActions($this->adapter);
    	$loginMsg = $loginActions->userExists($email,$password);
    	
    	//This tells us that the user already exists.
    	if($loginMsg['msg'] == "USER_SUCCESSFULLY_LOGGED_IN")
    	{
    		
    		$lastId = $loginMsg['user'];
    		$company_id = $loginMsg['company'];
    		
    	}
    	else
    	{
    		

    		
    		
    		//Since the user doesnt exist, 
    		//we will place them in the database.
    		
    	    	//Get company Id here. Look in Company table.
		    	//If you find the company, lets get the id. If not,
		    	//insert a new company in there.
		    	$company_id = $this->seekOrInsertCompany($company_name);
		    	
		    	
		    	
		    	$sql = new Sql($this->adapter);
		    	
		    	$insert = $sql->insert();
		    	$insert->into('user');
		    	$insert->values(array(
		    			"first_name"=>$f_name,
		    			"last_name"=>$l_name,
		    			"email"=>$email,
		    			"company"=>$company_id,
		    			"post_code"=>$postCode,
		    			"password"=>$password
		    	));
		    	$statement = $sql->prepareStatementForSqlObject($insert);
		    	$results = $statement->execute();
		    	
		    	//GET the last inserted Id following this registration
		    	
		    	$sql = "SELECT max(id) FROM user";
		    	$statement = $this->adapter->query($sql);
		    	$results = $statement->execute();
		        
		    	
		        foreach($results as $res)
		        {
		        	$lastId = $res['max(id)'];
		        	break;
		        }
		        
		        
		        $sql = "SELECT company_name FROM company where id = '".$company_id."'";
		        $statement = $this->adapter->query($sql);
		        $comp_results = $statement->execute();
		        foreach($comp_results as $comp_res)
		        {
		        	$company_name = $comp_res['company_name'];
		        }
		        
    	}
    	
        return array("userId"=>$lastId,
        		"companyId"=>$company_id,
        		"company_name"=>$company_name);
        
    }
    
    private function seekOrInsertCompany($companyName)
    {
    	$companyId = 0;
    	$sql = new Sql($this->adapter);
    	$select = $sql->select();
    	//First of all, select certain columns from company table.
    	//We do not need to get everthing
    	
    	$select->from('company');
    	$select->columns(array(
    			'company_name',
    			'id'
    	));
    	
    	$select->where(array('company_name' => $companyName));
    	
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
    	
    	//Now if that company was not found, it will return
    	//a count of results less than 1.
    	if(count($results)>0)
    	{
    		foreach($results as $res)
    		{
    			$companyId = $res['id'];
    			break;
    		}	
    	}
    	else
    	{
    		$companyInsertSql = new Sql($this->adapter);
    		 
    		$companyInsert = $companyInsertSql->insert();
    		$companyInsert->into('company');
    		$companyInsert->values(array(
    				"company_name"=>$companyName
    		));
    		$companyStatement = $companyInsertSql->prepareStatementForSqlObject($companyInsert);
    		$companyResults = $companyStatement->execute();
    		
    		
    		//GET the last inserted Id following this registration
    		$lastCompanySql = new Sql($this->adapter);
    		$lastCompanySql = "SELECT max(id) FROM company";
    		$lastCompanyStatement = $this->adapter->query($lastCompanySql);
    		$lastCompanyResults = $lastCompanyStatement->execute();
    		
    		foreach($lastCompanyResults as $res)
    		{
    			$companyId = $res['max(id)'];
    			break;
    		}
    	}
    	
    	return $companyId;
    	
    }
    
    public function retrieve_leaderboard_data($uid)
    {
    	$leaderboard_data = array();
    	
    	$topFivePlayers = $this->getTopFivePlayers();
    	
    	$topFiveCompanies = $this->getTopFiveCompanies();
    	
    	$meAndPlayers = $this->getYourScore($uid);
    	
    	$leaderboard_data = array
    	(
    	"topFivePlayers"=>$topFivePlayers, 
    	"topFiveCompanies"=>$topFiveCompanies, 
    	"meAndPlayers"=>$meAndPlayers
    	);
    	
    	echo json_encode(array("leaderboard_data"=>$leaderboard_data));

    }
    
    private function getTopFiveCompanies()
    {
    	$topFiveCompanies = array();
    	 
    	$sql = new Sql($this->adapter);
    	$select = $sql->select();
    	//First of all, select certain columns from game_saves table.
    	//We do not need to get everthing
    	
    	$select->from('company');
    	$select->columns(array(
    			'company_name',
    			'company_score'
    	));
    	
    	$predicate = $select->where;
    	$predicate->notEqualTo('company_score', "0");
    	 
    	$select->order('company_score DESC');
    	$select->limit(5);
    	
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
    	
    	foreach($results as $res)
    	{
    		$topFiveCompanies[] = array(
    				"company_name"=>$res['company_name'],
    				"company_score"=>$res['company_score']
    		);
    	}
    	
    	return $topFiveCompanies;
    }
    
    private function getTopFivePlayers()
    {
    	$topFivePlayers = array();
    	 
    	$sql = new Sql($this->adapter);
    	$select = $sql->select();
    	//First of all, select certain columns from game_saves table.
    	//We do not need to get everthing
    	$select->from('game_saves');
    	$select->columns(array(
    			'monies' => 'cash_in_hand',
    			'pic' => 'avatar'
    	));
    	//Only select those games that were completed as we only want persons
    	//to appear on the database if they have finished the game
    	$select->where(array('game_completed' => 1));
    	 
    	//We need the person's first and last name as well since we'll be displaying
    	//this on the leaderboard
    	$select->join('user', 'user.id = game_saves.user',
    			array(
    					'first_name',
    					'last_name')
    	);
    	//we now join the query here to include as well the company name
    	//we will be displaying this on the leaderboard
    	$select->join('company', 'user.company = company.id',
    			array('company_name')
    	);
    	 
    	$select->order('monies DESC');
    	$select->limit(5);
    	
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
    	
    	foreach($results as $res)
    	{
    		$topFivePlayers[] = array(
    				"first_name"=>$res['first_name'],
    				"last_name"=>$res['last_name'],
    				"company_name"=>$res['company_name'],
    				"pic"=>$res['pic'],
    				"monies"=>$res['monies']
    		);
    	}
    	
    	return $topFivePlayers;
    }
    
    private function getYourScore($me)
    {
    	$meAndPlayers = array();
    	
    	//First of all, lets see if you are on the database
    	//with a game completed score.
    	//If not, no need to go any further.
    	
    	$sql = new Sql($this->adapter);
    	$select = $sql->select();
    	//First of all, select certain columns from game_saves table.
    	//We do not need to get everthing
    	$select->from('game_saves');
    	$select->columns(array(
    			'monies' => 'cash_in_hand',
    			'pic' => 'avatar'
    	));
    	
    	$select->where(array('game_completed' => 1));
    	$select->where(array('user' => $me));
    	
    	$select->limit(1);
    	
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
    	
    	
    	//If zero results, we need to use the top five players here
    	//instead.
    	//Or we can bring you back with zero.
    	if(count($results)>0)
    	{
    		$sql = new Sql($this->adapter);
    		$select = $sql->select();
    		//First of all, select certain columns from game_saves table.
    		//We do not need to get everthing
    		$select->from('game_saves');
    		$select->columns(array(
    				'monies' => 'cash_in_hand',
    				'pic' => 'avatar',
    				'user' => 'user'
    		));
    		 
    		$select->where(array('game_completed' => 1));
    		//$select->where(array('user' => 3));
    		 
    		//$select->limit(5);
    		$select->order('cash_in_hand DESC');
    		 
    		$statement = $sql->prepareStatementForSqlObject($select);
    		$results = $statement->execute();
    		
    		$myPositionInReults = 0;
			$myPositionCount = 1;
    		$overallNumOfResults = count($results);
			
			
			foreach($results as $res)
    		{
				
    			if($res['user']==$me)
				{
					$myPositionInReults = $myPositionCount; 
					break;
				}
    			
				$myPositionCount++;
    		
			}
			
			//First of all, get the maximum we could possibly get.
			$halfWay = floor($myPositionCount/2);
    		//$overallNumOfResults
			$r = ($overallNumOfResults-$myPositionInReults);
			
			if(($overallNumOfResults-$myPositionInReults)>=2)
			{
				$maxPossPos = $myPositionInReults+2;
			}
			else
			{
				$maxPossPos = $myPositionInReults + $r;
			}
			
			//Get starting point
			if(($maxPossPos-5)<0)
			{
				$startPoint = 0;
			}
			else
			{
				$startPoint = ($maxPossPos-5);
			}
			
			
			
			
			$posLoop = 0;
			
			
			
			
			$sql = new Sql($this->adapter);
    		$select = $sql->select();
    		//First of all, select certain columns from game_saves table.
    		//We do not need to get everthing
    		$select->from('game_saves');
    		$select->columns(array(
    				'monies' => 'cash_in_hand',
    				'pic' => 'avatar',
    				'user' => 'user'
    		));
    		 
    		$select->where(array('game_completed' => 1));
    		//$select->where(array('user' => 3));
    	 
    	//We need the person's first and last name as well since we'll be displaying
    	//this on the leaderboard
    	$select->join('user', 'user.id = game_saves.user',
    			array(
    					'first_name',
    					'last_name')
    	);
    	//we now join the query here to include as well the company name
    	//we will be displaying this on the leaderboard
    	$select->join('company', 'user.company = company.id',
    			array('company_name')
    	);
    		 
    		$select->limit($maxPossPos - $startPoint);
			$select->offset($startPoint);
    		$select->order('cash_in_hand DESC');
    		 
    		$statement = $sql->prepareStatementForSqlObject($select);
    		$results = $statement->execute();
			
			$mePos = $startPoint;
			foreach($results as $res)
			{
				$meAndPlayers[] = array(
    				"first_name"=>$res['first_name'],
    				"last_name"=>$res['last_name'],
    				"company_name"=>$res['company_name'],
    				"pic"=>$res['pic'],
    				"monies"=>$res['monies'],
    				"pos"=>($mePos+1)
				);
				
				$mePos++;
			}
    	
    	
    		
			
    	}
		else
		{
			$sql = new Sql($this->adapter);
    		$select = $sql->select();
    		//First of all, select certain columns from game_saves table.
    		//We do not need to get everthing
    		$select->from('user');
    		$select->columns(array(
    				'first_name' => 'first_name',
    				'last_name' => 'last_name'//,
    				//'pic' => 'avatar'
    		));
			
			$select->join('company', 'user.company = company.id',
    			array('company_name')
			);
			
			$select->where(array('user.id' => $me));
			
			
    		$statement = $sql->prepareStatementForSqlObject($select);
    		$results = $statement->execute();
			
			foreach($results as $res)
			{
				$meAndPlayers[] = array(
    				"first_name"=>$res['first_name'],
    				"last_name"=>$res['last_name'],
    				"company_name"=>$res['company_name'],
    				"pic"=>1,
    				"monies"=>0,
    				"pos"=>0
				);
			}
		}
    	
		return $meAndPlayers;
    	
    }
    
    public function get_stats()
    {
    	
    	
    	
	    	$sql = "SELECT game_saves.cash_in_hand,
	    	user.first_name, user.last_name, user.email
	    	FROM game_saves 
	    	LEFT JOIN user ON game_saves.user=user.id 
	    	where game_saves.game_completed=1 order by game_saves.cash_in_hand DESC";
	    			
	    	$statement = $this->adapter->query($sql);
	    	$results = $statement->execute();
	    	
	    	echo '<h3>Top Players</h3>';
	    	
	    	echo '<table border="1">';
	    	echo '<tr><td><strong>Name of Player</strong></td>
	    		<td align="center"><strong>Score</strong></td></tr>';
	    	
	    	foreach($results as $res)
	    	{
	    		//print_r($res);
	    		
	    		echo '<tr><td align="left">'.$res['first_name'].
	    		' '.$res['last_name'].'</td>
	    		<td align="left">'.$res['cash_in_hand'].'</td></tr>';
	    		
	    		//echo "<br /><br />";
	    		//echo $res['user']."<br />";
	    		
	    		
	    	}

	    	echo '</table>';
	    	
	    	
	    	
	    	
    	
    	
    	
	    	$sql = "SELECT company_name, company_score 
	    	FROM company order by company_score DESC";
	    			
	    	$statement = $this->adapter->query($sql);
	    	$results = $statement->execute();
	    	
	    	echo '<h3>Top Companies</h3>';
	    	
	    	echo '<table border="1">';
	    	echo '<tr><td><strong>Name of Company</strong></td>
	    		<td align="center"><strong>Score</strong></td></tr>';
	    	
	    	foreach($results as $res)
	    	{
	    		//print_r($res);
	    		
	    		echo '<tr><td align="left">'.$res['company_name'].'</td>
	    		<td align="left">'.$res['company_score'].'</td></tr>';
	    		
	    		//echo "<br /><br />";
	    		//echo $res['user']."<br />";
	    		
	    		
	    	}

	    	echo '</table>';
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	


	    	$sql = "SELECT COUNT(game_saves.user), game_saves.user,
	    	user.first_name, user.last_name, user.email, user.id,
	    	company.company_name, company.company_name, company.company_name,
	    	company.company_name
	    	FROM game_saves LEFT JOIN user ON game_saves.user=user.id
	    	LEFT JOIN company ON company.id=user.company
	    	GROUP by game_saves.user order by COUNT(game_saves.user) DESC";
	    	
	    	$statement = $this->adapter->query($sql);
	    	$results = $statement->execute();
	    	
	    	echo '<h3>Top No. of games PLAYED by single user</h3>';
	    	
	    	echo '<table border="1">';
	    	echo '<tr><td><strong>Name</strong></td>
	    		<td align="center"><strong>Games played</strong></td>
	    			<td><strong>Email</strong></td>
	    			<td><strong>Company</strong></td></tr>';
	    	
	    	foreach($results as $res)
	    	{
	    		//print_r($res);
	    	 
	    	echo '<tr><td align="left">'.$res['first_name'].
	    		' '.$res['last_name'].'</td>'.
	    		'<td align="center">'.$res['COUNT(game_saves.user)'].'</td>'.
	    				'<td align="left">'.$res['email'].'</td>
	    		<td align="left">'.$res['company_name'].'</td></tr>';
	    		    		 
	    		//echo "<br /><br />";
	    		//echo $res['user']."<br />";
	   
	    		    				 
	    	}
	    	
	    	echo '</table>';
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	
	    	


	    	$sql = "SELECT COUNT(game_saves.user), game_saves.user, 
	    	user.first_name, user.last_name, user.email, user.id, 
	    	company.company_name, company.company_name, company.company_name, 
	    	company.company_name 
	    	FROM game_saves LEFT JOIN user ON game_saves.user=user.id 
	    	LEFT JOIN company ON company.id=user.company 
	    	where game_saves.game_completed=1 
	    	GROUP by game_saves.user order by COUNT(game_saves.user) DESC";
	    	
	    	$statement = $this->adapter->query($sql);
	    	$results = $statement->execute();
	    	
	    	echo '<h3>Top No. of games COMPLETED(Final scores submitted) by single user</h3>';
	    	
	    	echo '<table border="1">';
	    	echo '<tr><td><strong>Name</strong></td>
	    		<td align="center"><strong>Games completed</strong></td>
	    			<td><strong>Email</strong></td>
	    			<td><strong>Company</strong></td></tr>';
	    	
	    	foreach($results as $res)
	    	{
	    		//print_r($res);
	    	 
	    	echo '<tr><td align="left">'.$res['first_name'].
	    		' '.$res['last_name'].'</td>'.
	    		'<td align="center">'.$res['COUNT(game_saves.user)'].'</td>'.
	    				'<td align="left">'.$res['email'].'</td>
	    		<td align="left">'.$res['company_name'].'</td></tr>';
	    		    		 
	    		//echo "<br /><br />";
	    		//echo $res['user']."<br />";
	   
	    		    				 
	    	}
	    	
	    	echo '</table>';
	    	
    	/*
    	
    	SELECT
    	hook_bait,
    	IF(hook_bait = 'boily', (SELECT brand FROM sample_table t2 WHERE t2.hook_bait = t1.hook_bait GROUP BY brand ORDER BY COUNT(brand) DESC LIMIT 1), '-') AS brand,
    	IF(hook_bait = 'boily', (SELECT flavour FROM sample_table t2 WHERE t2.hook_bait = t1.hook_bait GROUP BY flavour ORDER BY COUNT(flavour) DESC LIMIT 1), '-') AS flavour
    	FROM sample_table t1
    	GROUP BY hook_bait
    	ORDER BY COUNT(hook_bait) DESC
    	LIMIT 0, 1
    	*/
			
    }
    
}

