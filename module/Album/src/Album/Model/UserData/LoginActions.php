<?php

namespace Album\Model\UserData;


use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Sql\Predicate;
use Zend\Db\Sql\Sql\Where;

class LoginActions 
{
	private $adapter;
	
	public function __construct($adpt)
	{
		$this->adapter = $adpt;
	}
	
	public function userExists($email=""/*"1@2.com"*/,$password=""/*"password"*/)
	{
		
		
		$LoginMessage = array("msg" => "USER_NOT_FOUND");
		$predicate;
		
		
		$sql = new Sql($this->adapter);
    	$select = $sql->select();
    	$select->from('user');
    	
    	$predicate = $select->where;
    	$predicate->equalTo('email',$email);
    	 
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
     	
    	if(count($results)>0)
    	{
    		
    		$LoginMessage = 
    		array("msg" => "USER_EMAIL_ADDRESS_FOUND");
    		
    		
	    	foreach($results as $res)
	    	{
			
	    		if($res['password']==$password)
	    		{
	    			$LoginMessage = 
	    			array("msg" => "USER_SUCCESSFULLY_LOGGED_IN",
	    				  "user"=>$res['id'],
	    				  "first_name"=>$res['first_name'],
	    				  "last_name"=>$res['last_name'],
	    				  "company"=>$res['company']);
	    			break;
	    		}
	    	}
    	
    	}
    	
    	return $LoginMessage;
	}
	
}