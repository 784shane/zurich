<?php

namespace Zurich\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ZurichController extends AbstractActionController
{
	public function indexAction()
	{
		/*
		$view = new ViewModel(array(
				'message' => 'Hello world',
		));
		
		// Disable layouts; `MvcEvent` will use this View Model instead
		$view->setTerminal(true);
		
		return $view;
		*/
	}

	public function spinnerAction()
	{
		
		$id = 1;
		if($this->params('id'))
		{
			$id = $this->params('id');
		}
		
		$id = $this->params('id');
		
		$view = new ViewModel(array(
				'id' => $id,
		));
		$view->setTemplate('zurich/zurich/index');
		
		return $view;
		
		//echo $id;
		
		//exit;
	}

	/**
	 * Competition closed action
	 * @return Zend\View\Model\ViewModel
	 * 
	 */
	public function closedAction()
	{
		return new ViewModel();
	}

	/**
	 * Winners
	 * @return Zend\View\Model\ViewModel
	 * 
	 */
	public function winnersAction()
	{
		return new ViewModel();
	}

	/**
	 * Winners
	 * @return Zend\View\Model\ViewModel
	 * 
	 */
	public function termsconditionsAction()
	{
		return new ViewModel();
	}


	public function editAction()
	{
	}

	public function deleteAction()
	{
	}
	
	//@Override 
	public function viewAction()
	{
		echo "I am called";
		/*
		// get the article from the persistence layer, etc...
	
		$view = new ViewModel();
	
		$articleView = new ViewModel(array('article' => $article));
		$articleView->setTemplate('content/article');
	
		$primarySidebarView = new ViewModel();
		$primarySidebarView->setTemplate('content/main-sidebar');
	
		$secondarySidebarView = new ViewModel();
		$secondarySidebarView->setTemplate('content/secondary-sidebar');
	
		$sidebarBlockView = new ViewModel();
		$sidebarBlockView->setTemplate('content/block');
	
		$secondarySidebarView->addChild($sidebarBlockView, 'block');
	
		$view->addChild($articleView, 'article')
		->addChild($primarySidebarView, 'sidebar_primary')
		->addChild($secondarySidebarView, 'sidebar_secondary');
	
		return $view;
		*/
	}
}


/*
namespace Zurich\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ZurichController extends AbstractActionController
{
    public function indexAction()
    {
    	echo "wowxx";
    	exit;
    }

    public function addAction()
    {
    	echo "wow";
    	exit;
    }

    public function editAction()
    {
    }

    public function deleteAction()
    {
    }
}
*/