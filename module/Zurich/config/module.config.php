<?php

//echo __DIR__ . '/../view/layout/layout.phtml';
//exit;
return array(
    'controllers' => array(
        'invokables' => array(
            'Zurich\Controller\Zurich' => 'Zurich\Controller\ZurichController',
        ),
    ),
		
		// The following section is new and should be added to your file
		'router' => array(
				'routes' => array(
						'zurich' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/zurich[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Zurich\Controller\Zurich',
												'action'     => 'index',
										),
								),
						),
						'closed' => array(
							'type'    => 'literal',
							'options' => array(
								'route'    => '/competition-closed',
								'defaults' => array(
									'controller' => 'Zurich\Controller\Zurich',
									'action'     => 'closed',
								),
							),
						),
						'winners' => array(
							'type'    => 'literal',
							'options' => array(
								'route'    => '/winners',
								'defaults' => array(
									'controller' => 'Zurich\Controller\Zurich',
									'action'     => 'winners',
								),
							),
						),
						'termsconditions' => array(
							'type'    => 'literal',
							'options' => array(
								'route'    => '/termsconditions',
								'defaults' => array(
									'controller' => 'Zurich\Controller\Zurich',
									'action'     => 'termsconditions',
								),
							),
						),
				),
		),
		

		
    'view_manager' => array(
        'template_path_stack' => array(
            'zurich' => __DIR__ . '/../view',
        ),
    ),
    /*
		
		'view_manager' => array(
				'display_not_found_reason' => true,
				'display_exceptions'       => true,
				'doctype'                  => 'HTML5',
				'not_found_template'       => 'error/404',
				'exception_template'       => 'error/index',
				'template_map' => array(
						'layout/layout'           => __DIR__ . '/../view/layout/test.phtml',
						'zurich/index/index' => __DIR__ . '/../view/zurich/zurich/index/index.phtml',
						'error/404'               => __DIR__ . '/../view/error/404.phtml',
						'error/index'             => __DIR__ . '/../view/error/index.phtml',
				),
				'template_path_stack' => array(
						__DIR__ . '/../view',
				),
		)
		*/
		
);