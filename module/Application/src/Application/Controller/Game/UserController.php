<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller\Game;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class UserController extends AbstractActionController
{
	private $userDataModel = null;
	
    public function indexAction()
    {
    	
        return new ViewModel();
    }
    
    public function loginAction()
    {
    	
    	if($this->userDataModel == null)
    	{
    		$this->userDataModel = $this->getServiceLocator()->get('UserData');
    	}
    	
    	$this->userDataModel->attemptLogin($this->params());
    	exit;
        //return new ViewModel();
    }
    
    public function savedataAction()
    {
    	
    	if($this->userDataModel == null)
    	{
    		$this->userDataModel = $this->getServiceLocator()->get('UserData');
    	}
    	
    	$theParams = $this->params();
    	$f_name=""; $l_name=""; $company_name=1;
    	$email=""; $postCode=""; $password="";
    	
    	
    	if($theParams->fromPost('f_name'))
    	{
    		$f_name=$theParams->fromPost('f_name');
    	}

    	if($theParams->fromPost('l_name'))
    	{
    		$l_name=$theParams->fromPost('l_name');
    	}

    	if($theParams->fromPost('company_name'))
    	{
    		$company_name=$theParams->fromPost('company_name');
    	}

    	if($theParams->fromPost('email'))
    	{
    		$email=$theParams->fromPost('email');
    	}

    	if($theParams->fromPost('postCode'))
    	{
    		$postCode=$theParams->fromPost('postCode');
    	}

    	if($theParams->fromPost('password'))
    	{
    		$password=$theParams->fromPost('password');
    	}
    	
    	
    	$res = $this->userDataModel->postData
    	(
    			$f_name,
    			$l_name,
    			$company_name,
    			$email,
    			$postCode,
    			$password
    	);
    	
    	
    	if(count($res)>0){echo json_encode(array(
    			"user"=>$res["userId"], 
    			"companyId"=>$res["companyId"],
    			"first_name"=>$f_name, "last_name"=>$l_name,
    			"company_name"=>$res["company_name"]
    	));}
    	
    	exit;
    }

    public function getLeaderboardAction()
    {
    	/*
    	$sm = $this->getServiceLocator();
    	$albumTable = $sm->get('Album\Model\AlbumTable');
    	print_r($albumTable);
    	//print_r($sm);
    	//$this->albumTable = $sm->get('Album\Model\AlbumTable');
    	*/
		$uid = 3;
		$theParams = $this->params();
		if($theParams->fromPost('uid'))
    	{
    		$uid=$theParams->fromPost('uid');
    	}
		
		
    	$this->userDataModel = $this->getServiceLocator()->get('UserData');
    	$this->userDataModel->retrieve_leaderboard_data($uid);
    	//$this->userDataModel->retrieve_leaderboard_data();
    	
    	exit;
    }

    public function statsAction()
    {
    	$this->userDataModel = $this->getServiceLocator()->get('UserData');
    	$this->userDataModel->get_stats();
    	exit;
    }
}
