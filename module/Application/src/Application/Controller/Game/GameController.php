<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller\Game;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Sql\Sql;


class GameController extends AbstractActionController
{
	
	private $gameStateModel;
	
    public function indexAction()
    {
    	exit;
    }
    
    public function saveAction()
    {
    	$this->gameStateModel = $this->getServiceLocator()->get('GameState');
    	$this->gameStateModel->saveGame($this->params());
    	exit("Game Saved");
    }
    
    public function retrieveAction()
    {
    	

    	//We have built this again in the model GameState/
    	//We need to use this model instead to perform this retrieval action
    	///////////////////////////////////////////////////////////////////////
    	
    	
    	$adapt = $this->getServiceLocator()->get('DB_ADAPT');
    	
    	$sql = new Sql($adapt);
    	$select = $sql->select();
    	$select->from('game_saves');
    	$select->join('user', 'user.id = game_saves.user', 
    			array(
    			'first_name',
    			'last_name')
    	);
    	$predicate = $select->where;
    	$predicate->equalTo('game_saves.user',1);
    	$statement = $sql->prepareStatementForSqlObject($select);
    	$results = $statement->execute();
    	
    	
    	
    	$savedGameMessage = array("savedGame"=>array("msg"=>"NO_SAVED_GAMES", "data"=>array()));
    	
    	
    	if(count($results)>0)
    	{
    	
    	
    		$savedGameData = array();
    		//array_merge();
    		foreach($results as $res)
    		{
    			
	    		$savedGameData = array(
	    		"user" => $res['user'],
	    		"car_square_postion" => $res['car_square_postion'],
	    		"cash_in_hand" => $res['cash_in_hand'],
	    		"life_cards" => $res['life_cards'],
	    		"car_color" => $res['car_color'],
	    		"avatar" => $res['avatar'],
	    		"wheel_last_spin" => $res['wheel_last_spin'],
	    		"wheel_last_rotation" => $res['wheel_last_rotation'],
	    		"needle_last_rotation" => $res['needle_last_rotation'],
	    		"first_name" => $res['first_name'],
	    		"last_name" => $res['last_name']
	    		);
    			
	    		
	    		break;
	    		
	    		
    		}
    		
    		$savedGameMessage = array(
    				"savedGame"=>array("msg"=>"SUCCESS", "data"=>$savedGameData)
    				);
    		 
    	}
    	
    	
    	echo json_encode($savedGameMessage);
    	

    	//We have built this again in the model GameState/
    	//We need to use this model instead to perform this retrieval action
    	///////////////////////////////////////////////////////////////////////
    	
    	exit;
    }
    
    public function postFeedAction()
    {
    	$this->gameStateModel = $this->getServiceLocator()->get('GameState');
    	$this->gameStateModel->saveToFeed($this->params());
    	exit;
    }
    
    public function retrieveFeedAction()
    {
    	$this->gameStateModel = $this->getServiceLocator()->get('GameState');
    	$this->gameStateModel->getFeed($this->params());
    	exit;
    }
    
    public function getCompaniesAction()
    {
    	$this->gameStateModel = $this->getServiceLocator()->get('GameState');
    	$this->gameStateModel->getCompanies();
    	exit;
    }
    
    public function mobileAction()
    {
	
    }
    
    public function updateAction()
    {
		$this->gameStateModel = $this->getServiceLocator()->get('GameState');
    	$this->gameStateModel->update_index();
		exit;
    }
}
