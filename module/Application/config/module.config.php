<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */


return array(

    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Game\User' => 'Application\Controller\Game\UserController',
            'Application\Controller\Game\Game' => 'Application\Controller\Game\GameController'
        ),
    ),
    


    // The following section is new and should be added to your file
    'router' => array(
    		'routes' => array(
    		

    				'home' => array(
    						'type' => 'Zend\Mvc\Router\Http\Literal',
    						'options' => array(
    								'route'    => '/',
    								'defaults' => array(
    										'controller' => 'Application\Controller\Index',
											//'controller' => 'Zurich\Controller\Zurich',
    										'action'     => 'index',
    								),
    						),
    				),
    				
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
    				
    				
    		
    				'album' => array(
    						'type'    => 'segment',
    						'options' => array(
    								'route'    => '/album[/][:action][/:id]',
    								'constraints' => array(
    										'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
    										'id'     => '[0-9]+',
    								),
    								'defaults' => array(
    										'controller' => 'Album\Controller\Album',
    										'action'     => 'index',
    								),
    						),
    				),





//Just created a route for buju

            'buju' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/buju',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller',
            						'controller'    => 'Index',
            						'action'        => 'buju',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Just created a route for savedata

            'savedata' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/savedata',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'User',
            						'action'        => 'savedata',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Just created a route for savedata

            'login' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/login',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'User',
            						'action'        => 'login',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for GameSave

            'save_game' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/save_game',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'Game',
            						'action'        => 'save',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for retrieve_saved_game

            'retrieve_saved_game' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/retrieve_saved_game',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'Game',
            						'action'        => 'retrieve',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for retrieve_saved_game

            'stats' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/stats',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'User',
            						'action'        => 'stats',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for retrieve_saved_game

            'get_leaderboardData' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/leaderboard_data',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'User',
            						'action'        => 'getLeaderboard',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for retrieve_saved_game

            'post_feed' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/post_feed',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'Game',
            						'action'        => 'postFeed',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for retrieve_saved_game

            'retrieve_feed' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/retrieve_feed',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'Game',
            						'action'        => 'retrieveFeed',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for retrieve_saved_game

            'check_updated_index' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/update_index',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'Game',
            						'action'        => 'update',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for retrieve_saved_game

            'mobile' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/mobile',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'Game',
            						'action'        => 'mobile',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            ),
            
            
            //Route for retrieve_saved_game

            'get_companies' => array(
            		'type'    => 'Literal',
            		'options' => array(
            				'route'    => '/get_companies',
            				'defaults' => array(
            						'__NAMESPACE__' => 'Application\Controller\Game',
            						'controller'    => 'Game',
            						'action'        => 'getCompanies',
            				),
            		),
            		'may_terminate' => true,
            		'child_routes' => array(
            				'default' => array(
            						'type'    => 'Segment',
            						'options' => array(
            								'route'    => '/[:controller[/:action]]',
            								'constraints' => array(
            										'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
            										'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
            								),
            								'defaults' => array(
            								),
            						),
            				),
            		),
            )
            
            
            
    				
    				
    				
    				
    				
    		),
    ),
   
    
    
    



    
    
    
    /*
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    */

    'view_manager' => array(
    		'display_not_found_reason' => true,
    		'display_exceptions'       => true,
    		'doctype'                  => 'HTML5',
    		'not_found_template'       => 'error/404',
    		'exception_template'       => 'error/index',
    		'template_map' => array(
    				//'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
    				//'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
    				//'error/404'               => __DIR__ . '/../view/error/404.phtml',
    				//'error/index'             => __DIR__ . '/../view/error/index.phtml',
    
    				'layout/layout'           => __DIR__ . '/../../App/view/layout/test.phtml',
    				'application/index/index' => __DIR__ . '/../../App/view/app/index/index.phtml',
    				'error/404'               => __DIR__ . '/../../App/view/error/404.phtml',
    				'error/index'             => __DIR__ . '/../../App/view/error/index.phtml',
					'zoo_keep' => __DIR__ .  '/../../../view/application/game/game/mobile.phtml'
    		),
    		'template_path_stack' => array(
    				__DIR__ . '/../view',
    		),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
