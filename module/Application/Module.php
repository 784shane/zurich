<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Album\Model\Album;
use Album\Model\AlbumTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Album\Model\UserData\UserData;
use Album\Model\Test;

class Module
{
	
	private $dbAdapter = null;
	
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
    	/*
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
        */
    	return array(
    			'Zend\Loader\ClassMapAutoloader' => array(
    					__DIR__ . '/autoload_classmap.php',
    			),
    			'Zend\Loader\StandardAutoloader' => array(
    					'namespaces' => array(
    							__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
    					),
    			),
    	);
    }
    
    private function getDbAdapter($sm)
    {
    	if($this->dbAdapter==null)
    	{
    		$this->dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    	}
    	
    	return $this->dbAdapter;
    }
    
    public function getServiceConfig()
    {
    	return array(
    			'factories' => array
    			(
    					/*
    					'Album\Model\AlbumTable' =>  function($sm) {
    					$tableGateway = $sm->get('AlbumTableGateway');
    						$table = new AlbumTable($tableGateway);
    						return $table;
    					},
    					'AlbumTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new Album());
    						return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
    					},
    					*/
    					
    					'DB_ADAPT' => function($sm)
    					{
    						return $sm->get('Zend\Db\Adapter\Adapter');
    					},
    					
    					'UserData' => function($sm)
    					{
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						//$sm->get('\Album\Model\UserData\UserData');
    						//print_r($dbAdapter);
    						//exit;
    						return new \Album\Model\UserData\UserData($dbAdapter);
    						/*
    						$tableGateway = $sm->get('AlbumTableGateway');
    						$table = new AlbumTable($tableGateway);
    						return $table;
    						*/
    						/*
    						$model = new \Application\Model\UserData\UserData($this->getDbAdapter($sm));
    						return $model;
    						*/
    					},
    					
    					'GameState' => function($sm)
    					{
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$model = new \Album\Model\GameState\GameState($dbAdapter);
    						return $model;
    					},
    					
    					'AlbumTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new User());
    						return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
    					},
    					
    					'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
    					
    					)
    			);/*
    	return array(
    			'factories' => array(
    					'Album\Model\AlbumTable' =>  function($sm) {
    						$tableGateway = $sm->get('AlbumTableGateway');
    						$table = new AlbumTable($tableGateway);
    						return $table;
    					},
    					'AlbumTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new Album());
    						return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
    					},
    			),
    	);*/
    }
    

/* REAL TEMP
    // Add this method:
    public function getServiceConfig()
    {
    	return array(
    			'factories' => array(
    					'Album\Model\AlbumTable' =>  function($sm) {
    						$tableGateway = $sm->get('AlbumTableGateway');
    						$table = new AlbumTable($tableGateway);
    						return $table;
    					},
    					'AlbumTableGateway' => function ($sm) {
    						$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    						$resultSetPrototype = new ResultSet();
    						$resultSetPrototype->setArrayObjectPrototype(new Album());
    						return new TableGateway('album', $dbAdapter, null, $resultSetPrototype);
    					},
    			),
    	);
    }
*/
    
    
    
/*
PAIN
// Add this method:
    public function getServiceConfig()
    {
    	//$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
    	
        return array(
            'factories' => array(
                
                'DB_ADAPT' => function($sm)
                {	
                	return $this->getDbAdapter($sm);
                },
                
                'UserData' => function($sm)
                {	
                	$model = new \Application\Model\UserData\UserData($this->getDbAdapter($sm));
                	return $model;
                },
                
                'GameState' => function($sm)
                {	
                	$model = new \Application\Model\GameState\GameState($this->getDbAdapter($sm));
                	return $model;
                },
                
                'AlbumTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
                
            ),
        );
    } 
*/
    
    
}
